package kr.co.hi.medicare;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import kr.co.hi.medicare.utilhw.SharedPref;

import kr.co.hi.medicare.net.hwdata.Tr_login;


/**
 * 로그인후 유저정보.
 */
public class UserInfo {
    private final String TAG = getClass().getSimpleName();

    private SharedPreferences sp;
    public String savedId;
    public String savePwd;
    private boolean set_auto_login = false, isSavedId = false;
    private SharedPreferences.Editor editor;

    /**
     * 로그인 정보 가져오기
     * @return
     */
    public static Tr_login getLoginInfo() {
        String json = getLoginInfoJson();
        Gson gson = new Gson();
        Tr_login login = gson.fromJson(json, Tr_login.class);
        return login;
    }

    public static String getLoginInfoJson() {
        String json = SharedPref.getInstance().getPreferences(SharedPref.LOGIN_JSON);
        return json;
    }


    public boolean getIsAutoLogin() {
//        return set_auto_login;
        return SharedPref.getInstance().getPreferences(SharedPref.IS_AUTO_LOGIN, false);
    }

    public void setIsAutoLogin(boolean set_auto_login) {
        SharedPref.getInstance().savePreferences(SharedPref.IS_AUTO_LOGIN, set_auto_login);
//        editor.putBoolean("AUTO_LOGIN", set_auto_login);
//        this.set_auto_login = set_auto_login;
//        editor.commit();
    }

    public String getSaveId() {
//        return savedId;
        return SharedPref.getInstance().getPreferences(SharedPref.SAVED_LOGIN_ID);
    }

    public String getSavedPw() {
//        return savePwd;
        return SharedPref.getInstance().getPreferences(SharedPref.SAVED_LOGIN_PWD);
    }

    public void setSavedId(String savedId) {
        Log.i(TAG, "UserInfo.setSavedId="+savedId);
        SharedPref.getInstance().savePreferences(SharedPref.SAVED_LOGIN_ID, savedId);
//        editor.putString("ID", savedId);
//        this.savedId = savedId;
//        editor.commit();
    }

    public void setSavePwd(String savePwd) {
        Log.i(TAG, "UserInfo.setSavePwd="+savePwd);
        SharedPref.getInstance().savePreferences(SharedPref.SAVED_LOGIN_PWD, savePwd);
//        editor.putString("PW", savePwd);
//        this.savePwd = savePwd;
//        editor.commit();
    }


    private String seq = null, name = null, birth = null, phone = null, app_version = null, hiplanner_name = null, hiplanner_phone = null, nurse_name = null, nurse_phone = null, market_url = null;
    private String nick = null , nunm = null , nuhp = null , fpnm = null , fphp = null;
    private int sex = 0, forigner = 0;

    private boolean set_cancer_member = false, set_stroke_member = false;

//    public String add_reg_yn;
//    public String mission_walk_end_de;
//    public String mission_walk_start_de;
//    public String mission_alert_yn;
//    public String health_alert_yn;
//    public String sugar_alert_yn;
//    public String user_point_amt;
//    public String day_health_amt;
//    public String sugar_occur_de;
//    public String sugar_typ;
//    public String goal_water_ntkqy;
//    public String goal_mvm_stepcnt;
//    public String goal_mvm_calory;
//    public String mber_zone;
//    public String smkng_yn;
//    public String medicine_yn;
//    public String disease_nm;
//    public String mber_grad;
//    public String mber_actqy;
//    public String mber_bdwgh_goal;
//    public String mber_bdwgh_app;
//    public String mber_bdwgh;
//    public String mber_height;
//    public String mber_hp;
//    public String mber_nm;
//    public String mber_lifyea;
//    public String mber_sex;
//    public String mber_hp_newyn;
//    public String log_yn;
//    public String default_basic_goal;
//    public String day_basic_goal;
//    public String tot_basic_goal;
//    public String mber_sn;
//    public String insures_code;
//    public String api_code;
    //2019-03-19 park add
    public String age;
//    public String accml_sum_amt;
//    public String point_total_amt;
    public String disease_txt;
//    public String gclife_id;

    public UserInfo(Context context) {
        sp = context.getSharedPreferences("login", context.MODE_PRIVATE);
        editor = sp.edit();
//        savedId = sp.getString("ID", null);
//        savePwd = sp.getString("PW", null);
//        set_auto_login = sp.getBoolean("AUTO_LOGIN", false);


//        isSavedId = sp.getBoolean("SAVE_ID", false);
//        add_reg_yn = sp.getString("add_reg_yn", null);
//        mission_walk_end_de = sp.getString("mission_walk_end_de", null);
//        mission_walk_start_de = sp.getString("mission_walk_start_de", null);
//        mission_alert_yn = sp.getString("mission_alert_yn", null);
//        health_alert_yn = sp.getString("health_alert_yn", null);
//        sugar_alert_yn = sp.getString("sugar_alert_yn", null);
//        user_point_amt = sp.getString("user_point_amt", null);
//        day_health_amt = sp.getString("day_health_amt", null);
//        sugar_occur_de = sp.getString("sugar_occur_de", null);
//        sugar_typ = sp.getString("sugar_typ", null);
//        goal_water_ntkqy = sp.getString("goal_water_ntkqy", null);
//        goal_mvm_stepcnt = sp.getString("goal_mvm_stepcnt", null);
//        goal_mvm_calory = sp.getString("goal_mvm_calory", null);
//        mber_zone = sp.getString("mber_zone", null);
//        smkng_yn = sp.getString("smkng_yn", null);
//        medicine_yn = sp.getString("medicine_yn", null);
//        disease_nm = sp.getString("disease_nm", null);
//        mber_grad = sp.getString("mber_grad", null);
//        mber_actqy = sp.getString("mber_actqy", null);
//        mber_bdwgh_goal = sp.getString("mber_bdwgh_goal", null);
//        mber_bdwgh_app = sp.getString("mber_bdwgh_app", null);
//        mber_bdwgh = sp.getString("mber_bdwgh", null);
//        mber_height = sp.getString("mber_height", null);
//        mber_hp = sp.getString("mber_hp", null);
//        mber_nm = sp.getString("mber_nm", null);
//        mber_lifyea = sp.getString("mber_lifyea", null);
//        mber_sex = sp.getString("mber_sex", null);
//        mber_hp_newyn = sp.getString("mber_hp_newyn", null);
//        log_yn = sp.getString("log_yn", null);
//        default_basic_goal = sp.getString("default_basic_goal", null);
//        day_basic_goal = sp.getString("day_basic_goal", null);
//        tot_basic_goal = sp.getString("tot_basic_goal", null);
//        mber_sn = sp.getString("mber_sn", null);
//        insures_code = sp.getString("insures_code", null);
//        api_code = sp.getString("api_code", null);
//
//        //2019-03-19 park add
//        age = sp.getString("age", null);
//        accml_sum_amt = sp.getString("accml_sum_amt", null);
//        point_total_amt = sp.getString("point_total_amt", null);
//        disease_txt = sp.getString("disease_txt", null);
//        gclife_id = sp.getString("gclife_id", null);
/////////////////////////////////////////////////////////
//        hiplanner_name = sp.getString("HINAME", null);
//        hiplanner_phone = sp.getString("HIPHONE", null);
//        nurse_name = sp.getString("NNAME", null);
//        nurse_phone = sp.getString("NPHONE", null);
//        market_url = sp.getString("NEW_LINK", null);
//        seq = sp.getString("SEQ", null);
//        name = sp.getString("NAME", null);
//        phone = sp.getString("PHONE", null);
//        app_version = sp.getString("APPVER", null);
//        birth = sp.getString("BIRTH", null);
//        sex = sp.getInt("SEX", 0);
//        forigner = sp.getInt("FORIGNER", 0);
////        set_cancer_member = sp.getBoolean("PMYN", false);
////        set_stroke_member = sp.getBoolean("PMSTROKEYN", false);
//        nick = sp.getString("NICK", null);
//        nunm = sp.getString("NUNM", null);
//        nuhp = sp.getString("NUHP", null);
//        fpnm = sp.getString("FPNM", null);
//        fphp = sp.getString("FPHP", null);
    }

//    public void saveLoginInfo(Tr_login login) {
//        editor.putString("add_reg_yn", login. add_reg_yn);
//        editor.putString("mission_walk_end_de", login. mission_walk_end_de);
//
//        editor.putString("add_reg_yn", login. add_reg_yn);
//        editor.putString("mission_walk_end_de", login. mission_walk_end_de);
//        editor.putString("mission_walk_start_de", login. mission_walk_start_de);
//        editor.putString("mission_alert_yn", login. mission_alert_yn);
//        editor.putString("health_alert_yn", login. health_alert_yn);
//        editor.putString("sugar_alert_yn", login. sugar_alert_yn);
//        editor.putString("user_point_amt", login. user_point_amt);
//        editor.putString("day_health_amt", login. day_health_amt);
//        editor.putString("sugar_occur_de", login. sugar_occur_de);
//        editor.putString("sugar_typ", login. sugar_typ);
//        editor.putString("goal_water_ntkqy", login. goal_water_ntkqy);
//        editor.putString("goal_mvm_stepcnt", login. goal_mvm_stepcnt);
//        editor.putString("goal_mvm_calory", login. goal_mvm_calory);
//        editor.putString("mber_zone", login. mber_zone);
//        editor.putString("smkng_yn", login. smkng_yn);
//        editor.putString("medicine_yn", login. medicine_yn);
//        editor.putString("disease_nm", login. disease_nm);
//        editor.putString("mber_grad", login. mber_grad);
//        editor.putString("mber_actqy", login. mber_actqy);
//        editor.putString("mber_bdwgh_goal", login. mber_bdwgh_goal);
//        editor.putString("mber_bdwgh_app", login. mber_bdwgh_app);
//        editor.putString("mber_bdwgh", login. mber_bdwgh);
//        editor.putString("mber_height",  login. mber_height);
//        editor.putString("mber_hp", login. mber_hp);
//        editor.putString("mber_nm", login. mber_nm);
//        editor.putString("mber_lifyea", login. mber_lifyea);
//        editor.putString("mber_sex", login. mber_sex);
//        editor.putString("mber_hp_newyn", login. mber_hp_newyn);
//        editor.putString("log_yn", login. log_yn);
//        editor.putString("default_basic_goal", login. default_basic_goal);
//        editor.putString("day_basic_goal", login. day_basic_goal);
//        editor.putString("tot_basic_goal", login. tot_basic_goal);
//        editor.putString("mber_sn", login. mber_sn);
//        editor.putString("insures_code", login. insures_code);
//        editor.putString("api_code", login. api_code);
//        editor.putString("age", login. age);
////        setNick(login.nickname);
//        editor.putString("accml_sum_amt", login. accml_sum_amt);
//        editor.putString("point_total_amt", login. point_total_amt);
//        editor.putString("disease_txt", login. disease_txt);
//        editor.putString("gclife_id", login. gclife_id);
//    }



////////////////////////////////////////////////////
//
//
    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        editor.putString("SEQ", seq);
        this.seq = seq;
        editor.commit();
    }


    public String  getName() {
        return name;
    }

    public void setName(String name) {
        editor.putString("NAME", name);
        this.name = name;
        editor.commit();
    }

    public String  getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        editor.putString("BIRTH", birth);
        this.birth = birth;
        editor.commit();
    }

    public String  getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        editor.putString("PHONE", phone);
        this.phone = phone;
        editor.commit();
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        editor.putInt("SEX", sex);
        this.sex = sex;
        editor.commit();
    }

    public int getForigner() {
        return forigner;
    }

    public void setForigner(int forigner) {
        editor.putInt("FORIGNER", forigner);
        this.forigner = forigner;
        editor.commit();
    }

//    public boolean isSavedId() {
//        return isSavedId;
//    }
//    public void setIsSaveId(boolean isSaveId) {
//        editor.putBoolean("SAVE_ID", isSaveId);
//        this.isSavedId = isSaveId;
//        editor.commit();
//    }

    public String  getApp_version() {
        return app_version;
    }

//    public void setApp_version(String app_version) {
//        editor.putString("APPVER", app_version);
//        this.app_version = app_version;
//        editor.commit();
//    }

    public String  getHiplanner_name() {
        return hiplanner_name;
    }

    public void setHiplanner_name(String hiplanner_name) {
        editor.putString("HINAME", hiplanner_name);
        this.hiplanner_name = hiplanner_name;
        editor.commit();
    }

    public String  getHiplanner_phone() {
        return hiplanner_phone;
    }

    public void setHiplanner_phone(String hiplanner_phone) {
        editor.putString("HIPHONE", hiplanner_phone);
        this.hiplanner_phone = hiplanner_phone;
        editor.commit();
    }

    public String  getNurse_name() {
        return nurse_name;
    }

    public void setNurse_name(String nurse_name) {
        editor.putString("NNAME", nurse_name);
        this.nurse_name = nurse_name;
        editor.commit();
    }

    public String  getNurse_phone() {
        return nurse_phone;
    }

    public void setNurse_phone(String nurse_phone) {
        editor.putString("NPHONE", nurse_phone);
        this.nurse_phone = nurse_phone;
        editor.commit();
    }

    public String  getMarket_url() {
        return market_url;
    }

//    public void setMarket_url(String market_url) {
//        editor.putString("NEW_LINK", market_url);
//        this.market_url = market_url;
//        editor.commit();
//    }

    public void setQuestion(int no, int level, int sum)
    {
        editor.putInt("QUESTION" + no, level);
        editor.putInt("QUESTION" + no + "_SUM", sum);
        editor.commit();
    }

    public int getQuestionLevel(int no)
    {
        return sp.getInt("QUESTION" + no, 0);
    }

    public int getQuestionSum(int no)
    {
        return sp.getInt("QUESTION" + no + "_SUM", 0);
    }

//    public void clearQuestion()
//    {
//        for(int i=0; i < 10; i++)
//        {
//            editor.putInt("QUESTION" + i, 0);
//            editor.putInt("QUESTION" + i + "_SUM", 0);
//        }
//        editor.commit();
//    }

    public boolean getCancerMember() {
        return set_cancer_member;
    }

//    public void setCancerMember(boolean set_cancer_member) {
//        editor.putBoolean("PMYN", set_cancer_member);
//        this.set_cancer_member = set_cancer_member;
//        editor.commit();
//    }

    public boolean getStrokeMember() {
        return set_stroke_member;
    }

//    public void setStrokeMember(boolean set_stroke_member) {
//        editor.putBoolean("PMSTROKEYN", set_stroke_member);
//        this.set_stroke_member = set_stroke_member;
//        editor.commit();
//    }

    /**
     * 닉네임 가져오기
     * @return string
     */
    public String  getNick() {
        return nick;
    }

    /**
     * 닉네임 설정
     * @param nick 닉네임
     */
    public void setNick(String nick) {
        editor.putString("NICK", nick);
        this.nick = nick;
        editor.commit();
    }

    public String  getNunm() {
        return nunm;
    }

    public void setNunm(String nunm) {
        editor.putString("NUNM", nunm);
        this.nunm = nunm;
        editor.commit();
    }

    public String  getNuhp() {
        return nuhp;
    }

    public void setNuhp(String nuhp) {
        editor.putString("NUHP", nuhp);
        this.nuhp = nuhp;
        editor.commit();
    }

    public String  getFpnm() {
        return fpnm;
    }

    public void setFpnm(String fpnm) {
        editor.putString("FPNM", fpnm);
        this.fpnm = fpnm;
        editor.commit();
    }

    public String  getFphp() {
        return fphp;
    }

    public void setFphp(String fphp) {
        editor.putString("FPHP", fphp);
        this.fphp = fphp;
        editor.commit();
    }
}
