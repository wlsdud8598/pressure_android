package kr.co.hi.medicare.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.net.EServerAPI;
import kr.co.hi.medicare.net.Record;
import kr.co.hi.medicare.util.AlertDialogUtil;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.SharedPref;

/**
 * Created by jihoon on 2016-01-06.
 * 뒤로가기 웹뷰 클래스
 * @since 0, 1
 */
public class BackWebViewActivity extends BaseActivityMedicare implements View.OnClickListener{

//    public static final String API_URL = "http://www.walkie.co.kr/HL/cert/HS_HL_CERT_result.asp";
    public static final String API_URL = "https://wkd.walkie.co.kr/HS_HL/cert/HS_HL_CERT_result.asp";

    private WebView mWebView;
    private Intent mIntent;
    private String		mUrl;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backwebview);

        mIntent		=	getIntent();
        mUrl		=	mIntent.getStringExtra(EXTRA_URL);
        CLog.i("mUrl = " +mUrl);
        mWebView	=	(WebView) findViewById(R.id.web_view);

        mWebView.setWebViewClient(new TermsWebViewClinet());
        mWebView.setWebChromeClient(new TermsWebViewChromeClient());

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setSupportMultipleWindows(true);
        mWebView.getSettings().setSavePassword(false);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);

        mWebView.setBackgroundColor(Color.TRANSPARENT);

        if ( Build.VERSION.SDK_INT >= 11 )
            mWebView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

//        String postData = "member_id=" + CommonData.getInstance().getMemberId() +"&store_id="+ NetworkConst.getInstance().getMarketId() +
//                "&device_type=A" +
//                "&app_ver=" + CommonData.getInstance().getAppVer();
//        mWebView.postUrl(mUrl, EncodingUtils.getBytes(postData, "BASE64"));
        mWebView.loadUrl(mUrl);

    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        CLog.i("onStart() getClass().getSimpleName() = " + getClass().getSimpleName());
    }

    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {

            mWebView.goBack();

            return true;

        }

        return super.onKeyDown(keyCode, event);

    }

    @Override
    public void onClick(View v) {
        if ( mWebView.canGoBack()) {

            mWebView.goBack();

            return;

        }else{
            finish();
        }
    }

    private class TermsWebViewChromeClient extends WebChromeClient {

        @Override
        public boolean onJsAlert(WebView view, String url, String message,
                                 JsResult result) {
            // TODO Auto-generated method stub
            return super.onJsAlert(view, url, message, result);
        }
    }

    private class TermsWebViewClinet extends WebViewClient {

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//            handler.proceed(); // SSL 에러가 발생해도 계속 진행!
            CLog.i("onReceivedSslError()");
            AlertDialog.Builder alert = new AlertDialog.Builder(BackWebViewActivity.this);
            alert.setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed(); // SSL 에러가 발생해도 계속 진행!
                    dialog.dismiss();
                }
            }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();    // 취소
                    dialog.dismiss();
                }
            });
            alert.setMessage(getString(R.string.msg_dialog_serucity_content));
            alert.show();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            CLog.i("URL : " + url);

            if ( url.startsWith("http")){

                int mRecStartIndex = 0;
                int mRecLastIndex = 0;
                int mCertStartIndex = 0;
                int mCertLastIndex = 0;
                String mRecStr, mCertStr = "";

                mRecStartIndex = url.indexOf("=");
                mRecLastIndex = url.indexOf("&");
                mRecStr = url.substring(mRecStartIndex+1, mRecLastIndex);
                url = url.substring(mRecLastIndex+1);

                mCertStartIndex = url.indexOf("=");
                mCertStr = url.substring(mCertStartIndex+1);


                AsyncHttpClient client = new AsyncHttpClient();
                RequestParams params = new RequestParams();
                client.setTimeout(10000);
                client.addHeader("Accept-Charset", "UTF-8");
                client.addHeader("User-Agent", new WebView(getBaseContext()).getSettings().getUserAgentString());
                params.put("rec_cert", mRecStr);  // 회원 고유번호
                params.put("certNum", mCertStr);  // 회원 고유번호
                client.post(API_URL, params, mAsyncHttpHandler);
            }else{
                return super.shouldOverrideUrlLoading(view, url);
            }

            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
        }

    }

    @Override
    public void response(Record record) throws UnsupportedEncodingException {

    }

    @Override
    public void request(EServerAPI eServerAPI, Object obj) {

    }

    @Override
    public void networkException(Record record) {

    }

    private AsyncHttpResponseHandler mAsyncHttpHandler = new AsyncHttpResponseHandler() {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            CLog.i("onSuccess");

            try {

                int count = headers.length;

                for (int i = 0; i < count; i++) {
                    CLog.d(headers[i].getName() + ": " + headers[i].getValue());
                }

                String resultData = new String(responseBody);

                CLog.d("statusCode = " +statusCode);
                CLog.d("resultData = " +resultData.toString());

                JSONObject object = new JSONObject(resultData);

                String resultCode = object.getString("RESULT_CODE");
                switch(resultCode){
                    case "0000":
                        CLog.i("0000");
                        SharedPref.getInstance().savePreferences(SharedPref.CERTI_DATE, CDateUtil.getToday_yyyy_MM_dd());
                        setResult(RESULT_OK);
                        finish();
                        break;
                    case "6666":
                        UIThread(BackWebViewActivity.this, getString(R.string.confirm), getString(R.string.cancel), getString(R.string.msg_cert_error), true);
                        break;
                    default:
                        UIThread(BackWebViewActivity.this, getString(R.string.confirm), getString(R.string.cancel), getString(R.string.msg_cert_error), true);
                        break;
                }


            } catch (Exception e) {
                CLog.e(e.toString());
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            CLog.i("onFailure");
            String response = new String(responseBody);
            CLog.i("statusCode = " +statusCode);
            CLog.i("response = " +response);

        }
    };

    private void UIThread(final Activity activity, final String confirm, final String cancel, final String message, final boolean isFinish) {
        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                AlertDialogUtil.onAlertDialogTwoBtn(activity, confirm, cancel, message, isFinish);
            }
        }, 0);
    }

}
