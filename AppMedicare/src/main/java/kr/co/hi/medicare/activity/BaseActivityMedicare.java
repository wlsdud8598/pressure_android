package kr.co.hi.medicare.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.util.List;

import kr.co.hi.medicare.Defined;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.EServerAPI;
import kr.co.hi.medicare.net.HttpClientManager;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.NetworkListener;
import kr.co.hi.medicare.net.Record;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_hra_check_result_input;
import kr.co.hi.medicare.net.hwNet.ApiData;
import kr.co.hi.medicare.net.hwNet.BaseData;
import kr.co.hi.medicare.net.hwNet.CConnAsyncTask;
import kr.co.hi.medicare.net.hwdata.Tr_get_infomation;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.util.Util;
import kr.co.hi.medicare.util.permission.IPermissionResult;
import kr.co.hi.medicare.util.permission.PermissionUtil;
import kr.co.hi.medicare.utilhw.EditTextUtil;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.NetworkUtil;
import kr.co.hi.medicare.utilhw.SharedPref;


/**
 * BaseActivityMedicare
 * [통신시 request (요청) response (callback) networkException(네트워트 예외) 를 오버라이딩함.
 */
public abstract class BaseActivityMedicare extends AppCompatActivity {
    protected final String TAG = getClass().getSimpleName();

    private boolean mButtonClickEnabled = true;  // 버튼 클릭 유무
    private Handler mButtonClickEnabledHandler = new Handler();
    Handler mHandler = new Handler(Looper.getMainLooper());

    private LinearLayout mProgressLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPref.getInstance().initContext(BaseActivityMedicare.this);
        CLog.d("BaseActivityMedicare onCreate");
    }

    @Override
    public void setContentView(int resId) {
        View view = LayoutInflater.from(BaseActivityMedicare.this).inflate(resId, null);
        setContentView(view);
    }

    @Override
    public void setContentView(View view) {
        View baseView = LayoutInflater.from(BaseActivityMedicare.this).inflate(R.layout.base_activity_layout, null);
        LinearLayout baseContentsLayout = baseView.findViewById(R.id.base_content_layout);
        mProgressLayout = baseView.findViewById(R.id.base_progress_layout);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        baseContentsLayout.addView(view, params);
        super.setContentView(baseView);
    }

    private NetworkListener networkListener = new NetworkListener() {
        @Override
        public void onResponse(Record a_oRecord) {
            try {
                response(a_oRecord);
            } catch (UnsupportedEncodingException e) {
                CLog.e(Util.getExceptionLog(e));
                networkException(a_oRecord);
            }
        }

        @Override
        public void onNetworkException(Record a_oRecord) {
            networkException(a_oRecord);
        }
    };

    public void showProgress() {
        if (mProgressLayout != null)
            mProgressLayout.setVisibility(View.VISIBLE);
        else
            Log.e(TAG, "mProgressLayout is NULL");
    }

    public void hideProgress() {
        if (mProgressLayout != null)
            mProgressLayout.setVisibility(View.GONE);
        else
            Log.e(TAG, "mProgressLayout is NULL");
    }

    public void response(Record record) throws UnsupportedEncodingException {
    }

    ;

    public void request(EServerAPI eServerAPI, Object obj) {
    }

    ;

    public void networkException(Record record) {
    }

    ;


    public void sendApi(Record record) {
        record.setNetworkListener(networkListener);
        HttpClientManager.getInstances().request(record);
    }


    /**
     * 클릭 이벤트 중복 방지
     *
     * @param enabled true = 클릭 가능, false = 클릭 불가
     */
    public void setButtonClickEnabled(boolean enabled) {

        // 클릭을 활성화 할 때 0.5초의 delay가 있도록 한다. (연속클릭 방지 목적)
        if (enabled)
            mButtonClickEnabledHandler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    CLog.i("button click enable");
                    mButtonClickEnabled = true;
                }
            }, 500);
        else {
            CLog.i("button click disable");
            mButtonClickEnabled = false;
        }

    }

    /**
     * 클릭 이벤트 활성 여부 가져오기
     *
     * @return true = 클릭 가능, false = 클릭 불가
     */
    public boolean getButtonClickEnabled() {
        return mButtonClickEnabled;
    }


    // 전역변수
    public static final String EXTRA_EDIT = "EXTRA_EDIT";
    public static final String EXTRA_CM_SEQ = "EXTRA_CM_SEQ";
    public static final String EXTRA_TYPE = "EXTRA_TYPE";
    public static final String EXTRA_CM_TITLE = "EXTRA_CM_TITLE";
    public static final String EXTRA_CM_CONTENT = "EXTRA_CM_CONTENT";
    public static final String EXTRA_CM_GUBUN = "EXTRA_CM_GUBUN";
    public static final String EXTRA_PHOTO_ARR = "EXTRA_PHOTO_ARR";
    public static final String EXTRA_HEART_YN = "EXTRA_HEART_YN";
    public static final String EXTRA_GOOD_EA = "EXTRA_GOOD_EA";
    public static final String EXTRA_COMMENT_EA = "EXTRA_COMMENT_EA";
    public static final String EXTRA_POSITION = "EXTRA_POSITION";
    public static final String EXTRA_MEAL_WHEN = "EXTRA_MEAL_WHEN";
    public static final String EXTRA_DATE = "EXTRA_DATE";
    public static final String EXTRA_URL = "EXTRA_URL";
    public static final String EXTRA_MV_CATE_TITLE = "EXTRA_MV_CATE_TITLE";
    public static final String EXTRA_MV_CONTENT = "EXTRA_MV_CONTENT";
    public static final String EXTRA_MV_URL = "EXTRA_MV_URL";
    public static final String EXTRA_MV_NOTIFY = "EXTRA_MV_NOTIFY";
    public static final String EXTRA_MV_NOTIFYTITLE = "EXTRA_MV_NOTIFYTITLE";
    public static final String EXTRA_MOTION_CODE = "EXTRA_MOTION_CODE";
    public static final String EXTRA_ML_SEQ = "EXTRA_ML_SEQ";
    public static final String EXTRA_SERVICETYPE = "EXTRA_SERVICETYPE";
    public static final String EXTRA_NICK = "EXTRA_NICK";
    public static final String EXTRA_DELETEFLAG = "EXTRA_DELETEFLAG";
    public static final String EXTRA_PHOTO_LIST = "EXTRA_PHOTO_LIST";
    public static final String EXTRA_PHOTO_INDEX = "EXTRA_PHOTO_INDEX";

    public static final String EXTRA_AUTH = "EXTRA_AUTH";


    /**
     * 한화 통신 모듈
     */
    /**
     * 로드벨런싱
     *
     * @param context
     * @param cls
     * @param obj
     * @param step
     */
    private void getInformation(final Context context, final Class<?> cls, final Object obj, final ApiData.IStep step) {
        Tr_get_infomation.RequestData reqData = new Tr_get_infomation.RequestData();
        MediNewNetworkModule.doApi(BaseActivityMedicare.this, Tr_get_infomation.class, reqData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                Intent intent = new Intent(BaseActivityMedicare.this, MainActivityMedicare.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
//                finish();
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(BaseActivityMedicare.this, "네트워크 연결 상태를 확인해주세요.");
            }
        });
    }


    public void getData(final Context context, final Class<?> cls, final Object obj, final ApiData.IStep step) {
        getData(context, cls, obj, true, step, null);
    }

    public void getData(final Context context, final Class<?> cls, final Object obj, final boolean isShowProgress, final ApiData.IStep step, final ApiData.IFailStep failStep) {
        if (context == null) {
            // 앱을 빠르게 취소 하고 재 시작 시 오류 발생 될 때 있음
            Logger.e(TAG , "getData context is null");
            return;
        }
        if (NetworkUtil.getConnectivityStatus(context) == false) {
            CDialog.showDlg(context, "네트워크 연결 상태를 확인해주세요.");
            return;
        }
//        String url = "http://wkd.walkie.co.kr/SK/WebService/SK_Mobile_Call.asmx/SK_mobile_Call";
        String url = Defined.BASE_HTTPS_URL;
        Logger.i(TAG, "LoadBalance.cls=" + cls + ", url=" + url);
//        if (TextUtils.isEmpty(url) && (cls != Tr_get_infomation.class)) {
        if (TextUtils.isEmpty(url)) {
            getInformation(context, cls, obj, step);
            return;
        }

        if(!cls.getName().equals(Tr_hra_check_result_input.class.getName())) {
//            if (isShowProgress)
//                showProgress();
        }

        CConnAsyncTask.CConnectorListener queryListener = new CConnAsyncTask.CConnectorListener() {

            @Override
            public Object run() throws Exception {

                ApiData data = new ApiData();
                return data.getData(context, cls, obj);
            }

            @Override
            public void view(CConnAsyncTask.CQueryResult result) {
                hideProgress();

                if (result.result == CConnAsyncTask.CQueryResult.SUCCESS && result.data != null) {
                    if (step != null) {
                        step.next(result.data);
                    }

                } else {
//                    mBaseActivity.hideProgress();
                    if (failStep != null) {
                        failStep.fail();
                    } else {
                        Toast.makeText(BaseActivityMedicare.this, "데이터 수신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "CConnAsyncTask error::"+cls.getClass().getName());
                    }
                }
            }
        };

        CConnAsyncTask asyncTask = new CConnAsyncTask();
        asyncTask.execute(queryListener);
    }

    public void getData(final Context context, final BaseData baseData, final Object obj, final boolean isShowProgress, final ApiData.IStep step, final ApiData.IFailStep failStep) {
        if (context == null) {
            // 앱을 빠르게 취소 하고 재 시작 시 오류 발생 될 때 있음
            Logger.e(TAG , "getData context is null");
            return;
        }
        if (NetworkUtil.getConnectivityStatus(context) == false) {
            CDialog.showDlg(context, "네트워크 연결 상태를 확인해주세요.");
            return;
        }
//        String url = "http://wkd.walkie.co.kr/SK/WebService/SK_Mobile_Call.asmx/SK_mobile_Call";
        String url = Defined.BASE_HTTPS_URL;
        Logger.i(TAG, "LoadBalance.cls=" + baseData + ", url=" + url);
//        if (TextUtils.isEmpty(url) && (cls != Tr_get_infomation.class)) {
//        if (TextUtils.isEmpty(url)) {
//            getInformation(context, baseData, obj, step);
//            return;
//        }

        if(!baseData.getClass().getName().equals(Tr_hra_check_result_input.class.getName())) {
//            if (isShowProgress)
//                showProgress();
        }

        CConnAsyncTask.CConnectorListener queryListener = new CConnAsyncTask.CConnectorListener() {

            @Override
            public Object run() throws Exception {

                ApiData data = new ApiData();
                return data.getData(baseData, obj);
            }

            @Override
            public void view(CConnAsyncTask.CQueryResult result) {
                if (isShowProgress)
                    hideProgress();

                if (result.result == CConnAsyncTask.CQueryResult.SUCCESS && result.data != null) {
                    if (step != null) {
                        step.next(result.data);
                    }

                } else {
//                    mBaseActivity.hideProgress();
                    if (failStep != null) {
                        failStep.fail();
                    } else {
                        Toast.makeText(BaseActivityMedicare.this, "데이터 수신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "CConnAsyncTask error::"+baseData.getClass().getName());
                        hideProgress();
                    }
                }
            }
        };

        CConnAsyncTask asyncTask = new CConnAsyncTask();
        asyncTask.execute(queryListener);
    }

    /**
     * ################################################################################
     * ###########################   Permission 관련 처리 시작  ##########################
     */

    private IPermissionResult mIPermissionResult;
    public void permissionRequest(String[] permissions, int requestCode, IPermissionResult iResult) {
        mIPermissionResult = iResult;
        PermissionUtil.requestPermissions(BaseActivityMedicare.this, permissions, requestCode, mIPermissionResult);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionUtil.permissionResultCheck(BaseActivityMedicare.this, requestCode, permissions, new IPermissionResult() {
            @Override
            public void onPermissionResult(int requestCode, boolean isGranted, List<String> deniedPermissionList) {
                if (mIPermissionResult != null)
                    mIPermissionResult.onPermissionResult(requestCode, isGranted, deniedPermissionList);

                mIPermissionResult = null;
            }
        });
    }
    /**
     * ###########################   Permission 관련 처리 끝  ##########################
     * ################################################################################
     */


    /**
     * ###########################   Fragment 관련 처리 시작 ##########################
     * ################################################################################
     */

    public void replaceFragment(Fragment fragment) {
        replaceFragment(fragment, null);
    }

    public void replaceFragment(Fragment fragment, Bundle bundle) {
//        boolean isReplace = (fragment instanceof _LoginFragment) || (fragment instanceof MainFragment);
        replaceFragment(fragment, false, true, bundle);
    }

    public void replaceFragment(final Fragment fragment, final boolean isReplace, boolean isAnim, Bundle bundle) {
//        hideProgress();

        BaseFragmentMedi.newInstance(BaseActivityMedicare.this);
        if (isReplace)
            removeAllFragment();

        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (isAnim)
            transaction.setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit, R.anim.fragment_pop_enter, R.anim.fragment_pop_exit);

        if (bundle != null)
            fragment.setArguments(bundle);

        String tag = fragment != null ? fragment.getClass().getSimpleName() : "tag";

        transaction.replace(R.id.content_layout, fragment, tag);
        if (!isFinishing() && !isDestroyed()) {
            if (isReplace == false)
                transaction.addToBackStack(null);

            transaction.commit();
        } else {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isReplace == false)
                        transaction.addToBackStack(null);

                    transaction.commitAllowingStateLoss();
                }
            }, 100);
        }


        printFragmentLog();
    }



    private void printFragmentLog() {
        if (getSupportFragmentManager().getFragments() != null) {
            Logger.i(TAG, "replaceFragment.size=" + getSupportFragmentManager().getFragments().size());

            for (Fragment fg : getSupportFragmentManager().getFragments()) {
                if (fg != null)
                    Logger.i(TAG, "replaceFragment.name=" + fg.toString());
            }
        }
    }

    public void removeAllFragment() {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStackImmediate();
    }


    public Fragment getVisibleFragment() {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment != null && fragment.isVisible()) {
                return fragment;
            }
        }
        return null;
    }

    /**
     * ###########################   Fragment 관련 처리 종료 ##########################
     * ################################################################################
     */


    protected onKeyBackPressedListener onKeyBackPressedListener;
    protected onKeyBackCatchListener onKeyBackCatchListener;
    protected onFinishListener onFinishListener;
    public interface onKeyBackPressedListener {
         void onBack();
    }

    public interface onKeyBackCatchListener {
         void onCatch();
    }
    public interface onFinishListener{
         void catchFinish();
    }

    public void setOnKeyBackPressedListener(onKeyBackPressedListener listener) {
        onKeyBackPressedListener = listener;
    }

    public void setonKeyBackCatchListener(onKeyBackCatchListener listener) {
        onKeyBackCatchListener = listener;
    }

    public void setOnFinishListener(onFinishListener listener){
        onFinishListener = listener;
    }

    @Override
    public void finish() {
        super.finish();
        if(onFinishListener!=null){
            overridePendingTransition(R.anim.fragment_pop_stay, R.anim.fragment_pop_down);
            onFinishListener=null;
        }else{
            overridePendingTransition(R.anim.fragment_pop_enter, R.anim.fragment_pop_exit);
        }
    }

    @Override
    public void onBackPressed() {
        EditTextUtil.hideKeyboard(this);
        //back 전에 수행해야할 내용이 있으면 수행
        if(onKeyBackPressedListener!=null){
            onKeyBackPressedListener.onBack();
        }
        //물리 백버튼 클릭 시 종료하지 않고 다른 내용을 수행
        if(onKeyBackCatchListener!=null){
            onKeyBackCatchListener.onCatch();
        }else{
            super.onBackPressed();
        }

//        super.onBackPressed();
    }

    protected boolean mIsFinishing = false;
    public void finishStep() {
        if (mIsFinishing) {
            mIsFinishing = false;
//            finish();
            ActivityCompat.finishAffinity(this);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.message_finish_message), Toast.LENGTH_SHORT).show();
            mIsFinishing = true;
            mMasterHandler.sendEmptyMessageDelayed(HANDLE_FINISHING_EXPIRE, 3000L);
//            if (getClass().getName().equals(_MainActivity.class.getName())) {
//                Toast.makeText(getApplicationContext(), getString(R.string.message_finish_message), Toast.LENGTH_SHORT).show();
//                mIsFinishing = true;
//                mMasterHandler.sendEmptyMessageDelayed(HANDLE_FINISHING_EXPIRE, 3000L);
//            } else {
//                super.onBackPressed();
//            }
        }
    }



    public final int HANDLE_FINISHING_EXPIRE = -999;
    protected Handler mMasterHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HANDLE_FINISHING_EXPIRE:
                    mIsFinishing = false;
                    break;
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        SharedPref.getInstance().initContext(this);
    }
}
