package kr.co.hi.medicare.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.net.EServerAPI;
import kr.co.hi.medicare.net.Record;

/**
 * Created by suwun on 2016-06-20
 * 맞춤 동영상 주의(권장)사항 엑티비티
 * @since 0, 1
 */
public class CustomVideoNotifyActivity extends BaseActivityMedicare {

    private TextView mTitleTv, mMvNotifyTv;
    private Intent intent;
    private String mTitle;
    private int mMvNotify;
    private LinearLayout mFoodLay, mHealthLay;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_notifyvideo);
        init();
    }

    @Override
    public void response(Record record) throws UnsupportedEncodingException {

    }

    @Override
    public void request(EServerAPI eServerAPI, Object obj) {

    }

    @Override
    public void networkException(Record record) {

    }


    /**
     * 초기화
     */
    public void init(){

        mTitleTv = (TextView) findViewById(R.id.common_toolbar_title);
        mMvNotifyTv = (TextView) findViewById(R.id.notify_tv);

        mFoodLay    = (LinearLayout) findViewById(R.id.food_lay);
        mHealthLay  = (LinearLayout) findViewById(R.id.health_lay);

        intent = getIntent();
        if (intent != null){
            mTitle = intent.getStringExtra(EXTRA_MV_NOTIFYTITLE);
            mMvNotify = intent.getIntExtra(EXTRA_TYPE , 0);
        }


        mTitleTv.setText(mTitle);

        switch (mMvNotify){
            case 0:
                mHealthLay.setVisibility(View.VISIBLE);
                mFoodLay.setVisibility(View.GONE);
                break;
            case 1:
                mHealthLay.setVisibility(View.GONE);
                mFoodLay.setVisibility(View.VISIBLE);
                break;
        }



    }

    public void click_event(View v) {
        Intent intent;
        switch (v.getId()) {

        }
    }

}
