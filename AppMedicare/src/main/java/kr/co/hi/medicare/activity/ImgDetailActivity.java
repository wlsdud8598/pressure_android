package kr.co.hi.medicare.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.chrisbanes.photoview.PhotoView;

import kr.co.hi.medicare.R;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import kr.co.hi.medicare.net.EServerAPI;
import kr.co.hi.medicare.net.Record;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.util.HackyViewPager;

/**
 * Created by jihoon on 2016-01-28.
 * 사진 상세보기 클래스
 *
 * @since 0, 1
 */
public class ImgDetailActivity extends BaseActivityMedicare {

    private ViewPager pager;
    private ImagePagerAdapter mPagerAdapter;
    private TextView mPositionTv;

    private boolean isPageChange = false;

    // intent data
    private int mIntentPositon;
    private Intent intent = null;
    private ArrayList<String> mPhotoListItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.img_detail_activity);

        init();
    }

    @Override
    public void response(Record record) throws UnsupportedEncodingException {

    }

    @Override
    public void request(EServerAPI eServerAPI, Object obj) {

    }

    @Override
    public void networkException(Record record) {

    }

    /**
     * 초기화
     */
    public void init() {
        mPositionTv = (TextView) findViewById(R.id.detail_photo_position_text);
        pager = (HackyViewPager) findViewById(R.id.pager);

        mPhotoListItem = new ArrayList<String>();

        intent = getIntent();
        if (intent != null) {
            mIntentPositon = intent.getIntExtra(EXTRA_PHOTO_INDEX, 0);
            mPhotoListItem = intent.getStringArrayListExtra(EXTRA_PHOTO_LIST);

            CLog.i("mPhotoListItem.size()  --->  " + mPhotoListItem.size());

            mPagerAdapter = new ImagePagerAdapter();
            pager.setAdapter(mPagerAdapter);
            pager.setCurrentItem(mIntentPositon);
            setEvent();

        }
    }


    public void click_event(View v) {
        switch (v.getId()) {
            // 하단 팝업 (숨기기/신고)
            case R.id.back_btn:
                finish();
                break;
        }
    }

    /**
     * 이벤트 연결
     */
    public void setEvent() {
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int select) {
                // TODO Auto-generated method stub

                CLog.i("onPageSelected");
                isPageChange = true;
                mIntentPositon = select;

                mPositionTv.setText(String.format(getString(R.string.photo_detail_position), String.valueOf(select + 1), String.valueOf(mPhotoListItem.size())));   // 사진 타이틀

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub
            }
        });
    }


    /**
     * 사진 viewpager
     */
    private class ImagePagerAdapter extends PagerAdapter {

        private LayoutInflater inflater;

        ImagePagerAdapter() {
            inflater = getLayoutInflater();
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return mPhotoListItem.size();
        }

        @Override
        public Object instantiateItem(ViewGroup view, final int position) {

            View imageLayout = inflater.inflate(R.layout.detail_photo_item, view, false);
            assert imageLayout != null;
            final PhotoView imageView			=	(PhotoView)	imageLayout.findViewById(R.id.detail_photo_img);

            // start 사진 비율 작업
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
//            float mScale = getResources().getDisplayMetrics().density;

            Glide.with(ImgDetailActivity.this).load(mPhotoListItem.get(position))
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .placeholder(R.drawable.background_5_506bb0d7)).into(imageView);

//            CustomImageLoader.displayImage(ImgDetailActivity.this, mPhotoListItem.get(position), imageView);

            ((ViewPager) view).addView(imageLayout, 0);

            return imageLayout;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(ViewGroup container) {
            // TODO Auto-generated method stub
            if (!isPageChange) {
                CLog.i("startUopdate");

                mPositionTv.setText(String.format(getString(R.string.photo_detail_position), String.valueOf(mIntentPositon + 1), String.valueOf(mPhotoListItem.size())));
            }
            super.startUpdate(container);
        }
    }

}
