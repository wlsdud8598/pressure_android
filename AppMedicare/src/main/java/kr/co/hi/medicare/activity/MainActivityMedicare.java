package kr.co.hi.medicare.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.fragment.community.CommunityMainFragment;
import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.fragment.community.dialog.DialogCommon;
import kr.co.hi.medicare.fragment.health.HealthFragment;
import kr.co.hi.medicare.fragment.health.food.FoodManageFragment;
import kr.co.hi.medicare.fragment.health.message.HealthMessageFragment;
import kr.co.hi.medicare.fragment.health.pressure.PressureManageFragment;
import kr.co.hi.medicare.fragment.health.question.QuestionnaireFragment;
import kr.co.hi.medicare.fragment.health.step.StepManageFragment;
import kr.co.hi.medicare.fragment.home.HomeFragmentMedicare;
import kr.co.hi.medicare.fragment.medicareService.MedicareServiceFragment;
import kr.co.hi.medicare.fragment.mypage.MyPageFragmentMyInfo;
import kr.co.hi.medicare.fragment.mypage.MyPageMainFragment;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.push.NotiDummyActivity;
import kr.co.hi.medicare.util.AlramUtil;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.SharedPref;

/**
 *
 **/
public class MainActivityMedicare extends BaseActivityMedicare {

    public static final String INTENT_KEY_MOVE_MENU = "intent_key_move_menu"; // 이동할 메뉴
    public static final String INTENT_KEY_MOVE_MENU_PUSH = "intent_key_move_menu_push"; // 푸시 이동할 메뉴
    public static final int HOME_MENU_1 = 0;
    public static final int HOME_MENU_2 = 1;
    public static final int HOME_MENU_3 = 2;
    public static final int HOME_MENU_4 = 3;
    public static final int HOME_MENU_5 = 4;


    private RadioGroup mBottomMenuRadioGroup;

    private LinearLayout[] mMainLayouts;
    private RadioButton[] mBottomBtns;
    private View[] mBottomLines;
//    private Toolbar toolbar1,toolbar2;
    private View mBottomMenuLayout;

    private int TAB_PRE = R.id.main_bottom_menu1;
    private boolean isStopMoveTab=false;

    private LinearLayout[] mConentsLayouts;
    private final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 101;

    private CommonToolBar toolBar;
    private TextView new_message;
    private RelativeLayout message_lv;
    private View close_btn;
    private ImageView mHealthMessage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        registerAlarm();    // 8시 알람서비스 등록

        toolBar = (CommonToolBar)findViewById(R.id.medi_common_toolbar);

        mBottomMenuLayout = findViewById(R.id.bottom_menu_layout);

        mConentsLayouts = new LinearLayout[]{
                findViewById(R.id.main_fragment_1)
                , findViewById(R.id.main_fragment_2)
                , findViewById(R.id.main_fragment_3)
        };

        mMainLayouts = new LinearLayout[] {
                findViewById(R.id.main_fragment_1),
                findViewById(R.id.main_fragment_2),
                findViewById(R.id.main_fragment_3)
        };
        // 하단 메뉴 버튼
        mBottomBtns = new RadioButton[] {
                findViewById(R.id.main_bottom_menu1),
                findViewById(R.id.main_bottom_menu2),
                findViewById(R.id.main_bottom_menu3)
        };
        // 하단 메뉴 라인
        mBottomLines = new View[] {
                findViewById(R.id.main_bottom_menu_line1),
                findViewById(R.id.main_bottom_menu_line2),
                findViewById(R.id.main_bottom_menu_line3)
        };
        mFragments = new Fragment[mMainLayouts.length];
        mBottomMenuRadioGroup = findViewById(R.id.main_bottom_menu_radiogroup);
        mBottomMenuRadioGroup.setOnCheckedChangeListener(mCheckedChangeListener);
        ((RadioButton)findViewById(R.id.main_bottom_menu1)).setChecked(true);

        new_message = findViewById(R.id.new_message);
        message_lv = findViewById(R.id.message_lv);
        close_btn = findViewById(R.id.close_btn);
        new_message.setOnClickListener(mOnClickListener);
        close_btn.setOnClickListener(mOnClickListener);
        toolBar.setLeftIcon(R.drawable.health_m_btn_01, mOnClickListener);



    }


    RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            setFragment(checkedId);
        }
    };


    protected void setFragment(int checkedId) {
//        Tr_login user = UserInfo.getLoginInfo();

        if(isStopMoveTab) {
            isStopMoveTab=false;
            return;
        }

        switch (checkedId) {
            case R.id.main_bottom_menu1:
                selectFragment(HOME_MENU_1);
                toolBar.setTitle(getString(R.string.text_meal));
                toolBar.getBackBtn().setVisibility(View.VISIBLE);
//                toolBar.setLeftIcon(R.drawable.health_m_btn_01);

                break;
            case R.id.main_bottom_menu2:
                selectFragment(HOME_MENU_2);
                toolBar.setTitle(getString(R.string.text_pressure));
                toolBar.getBackBtn().setVisibility(View.VISIBLE);
//                toolBar.setLeftIcon(R.drawable.health_m_btn_01);
                break;
            case R.id.main_bottom_menu3:
//                if(TextUtils.isEmpty(user.nickname)){
//                    isStopMoveTab=true;
//                    ((RadioButton)findViewById(TAB_PRE)).setChecked(true);
//                    openDialogRegisterNick();
//                    return;
//                }
                selectFragment(HOME_MENU_3);
                toolBar.setTitle(getString(R.string.comm_profile));
                toolBar.getBackBtn().setVisibility(View.GONE);
                message_lv.setVisibility(View.GONE);
                break;
//            case R.id.main_bottom_menu4:
//                selectFragment(HOME_MENU_4);
//                break;
//            case R.id.main_bottom_menu5:
//                selectFragment(HOME_MENU_5);
//                break;
        }

        TAB_PRE=checkedId;
    }

    private void openDialogRegisterNick() {

        CommonFunction.openDialogRegisterNick(this, new DialogCommon.UpdateProfile() {
            @Override
            public void updateProfile(String NICK, String DISEASE_OPEN,String DISEASE_NM) {
                if(!NICK.equals("")&&!DISEASE_OPEN.equals("")) {
                    final DialogCommon dialogIntro = DialogCommon.showDialogIntro(MainActivityMedicare.this, NICK);
                    dialogIntro.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            ((RadioButton) findViewById(R.id.main_bottom_menu3)).setChecked(true);
                        }
                    });
                }else{
                    openDialogRegisterNick();
                }
            }
        });


//        Tr_login user = UserInfo.getLoginInfo();
//        DialogCommon.showDialog(
//                this,
//                "",
//                "Y",
//                CommonFunction.getDiseaseNM(user.disease_nm,user.disease_txt),
//                user.mber_sn,
//                new DialogCommon.UpdateProfile() {
//                    @Override
//                    public void updateProfile(String NICK, String DISEASE_OPEN,String DISEASE_NM) {
//                        if(!NICK.equals("")&&!DISEASE_OPEN.equals("")) {
//                            final DialogCommon dialogIntro = DialogCommon.showDialogIntro(MainActivityMedicare.this, NICK);
//                            dialogIntro.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                                @Override
//                                public void onDismiss(DialogInterface dialog) {
//                                        ((RadioButton) findViewById(R.id.main_bottom_menu3)).setChecked(true);
//                                }
//                            });
//                        }else{
//                            openDialogRegisterNick();
//                        }
//                    }
//                });
    }

    /**
     * 메뉴 보여주기
     * @param visibleIdx
     */
    private Fragment[] mFragments;
    private int mVisibleIdx = -1;
    private void selectFragment(final int visibleIdx) {
        mVisibleIdx = visibleIdx;
        mBottomBtns[visibleIdx].setChecked(true);

        Log.i(TAG, "selectFragment.visibleIdx="+visibleIdx);
        for (int i = 0; i < mMainLayouts.length; i++) {
            mMainLayouts[i].setVisibility(i == visibleIdx ? View.VISIBLE : View.GONE);
            mBottomLines[i].setVisibility(i == visibleIdx ? View.VISIBLE : View.INVISIBLE);
        }

        Fragment fragment = null;
        if (mMainLayouts[visibleIdx].getChildCount() == 0) {
            switch (visibleIdx) {
                case 0:
                    fragment = FoodManageFragment.newInstance();
                    break;
                case 1:
                    fragment = PressureManageFragment.newInstance();
                    break;
                case 2:
                    fragment = MyPageFragmentMyInfo.newInstance();
                    break;
//                case 0:
//                    fragment = HomeFragmentMedicare.newInstance();
//                    break;
//                case 1:
//                    fragment = HealthFragment.newInstance();
//                    break;
//                case 2:
//                    fragment = CommunityMainFragment.newInstance();
//                    break;
//                case 3:
//                    fragment = MedicareServiceFragment.newInstance();
//                    break;
//                case 4:
//                    fragment = MyPageMainFragment.newInstance();
//                    break;
            }

            if (fragment != null) {
                mFragments[visibleIdx] = fragment;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        replaceFragment(visibleIdx, mFragments[visibleIdx], true, false, null);
                    }
                }, 100);
            } else {
                Log.e(TAG, "Fragment is Null");
                mCurrentFragment = mFragments[visibleIdx];
                if (mCurrentFragment != null)
                    mCurrentFragment.onResume();
            }

        } else {
            mCurrentFragment = mFragments[visibleIdx];
            if (mCurrentFragment != null)
                mCurrentFragment.onResume();
        }
    }

    private Fragment mCurrentFragment;
    public void replaceFragment(int idx, final Fragment fragment, final boolean isReplace, boolean isAnim, Bundle bundle) {
        mCurrentFragment = fragment;
        for (int i = 0; i < mConentsLayouts.length; i++) {
            mConentsLayouts[i].setVisibility(i == idx ? View.VISIBLE : View.GONE);
        }

        if (!fragment.isAdded()) {
//            transaction.replace(R.id.main_content_layout, fragment, tag);
            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            if (isAnim)
                transaction.setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit, R.anim.fragment_pop_enter, R.anim.fragment_pop_exit);

            if (bundle != null)
                fragment.setArguments(bundle);

            String tag = fragment != null ? fragment.getClass().getSimpleName() : "tag";
            transaction.replace(mConentsLayouts[idx].getId(), fragment, tag);
            if (!isFinishing() && !isDestroyed()) {
//            if (isReplace == false)
                transaction.addToBackStack(null);

                try {
                    transaction.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                    transaction.commitAllowingStateLoss();
                }
            } else {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                    if (isReplace == false)
                        transaction.addToBackStack(null);
                        transaction.commitAllowingStateLoss();
                    }
                }, 100);
            }
            printFragmentLog();
        } else {

//            if (fragment != null)
//                fragment.onResume();
        }
    }

    private void printFragmentLog() {
        if (getSupportFragmentManager().getFragments() != null) {
            Logger.i(TAG, "replaceFragment.size=" + getSupportFragmentManager().getFragments().size());

            for (Fragment fg : getSupportFragmentManager().getFragments()) {
                if (fg != null)
                    Logger.i(TAG, "replaceFragment.name=" + fg.toString());
            }
        }
    }

    public void removeAllFragment() {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStackImmediate();
    }


    public Fragment getVisibleFragment() {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment != null && fragment.isVisible()) {
                return fragment;
            }
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        Log.i(TAG, "onBackPressed.mIsLandscape="+mIsLandscape);
        if (mIsLandscape) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            if (mCurrentFragment instanceof MedicareServiceFragment) {
                ((MedicareServiceFragment) mCurrentFragment).onBackPressed();
            } else {
                finishStep();
            }
        }
    }


    //클릭 이벤트 정의
    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int viewId = v.getId();
            Bundle bundle = new Bundle();
            switch (viewId){
                //탭버튼
                case R.id.common_toolbar_back_icon:
                    NewActivity.startActivity(MainActivityMedicare.this, HealthMessageFragment.class,null);
                    break;
                case R.id.close_btn:
                    SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MESSAGE, false);
                    toolBar.setLeftIcon(R.drawable.health_m_btn_01);
//                    mHealthMessage.setImageResource(R.drawable.health_m_btn_01);
                    message_lv.setVisibility(View.GONE);
                    break;
                case R.id.new_message:
                    NewActivity.startActivity(MainActivityMedicare.this, HealthMessageFragment.class,null);
                    break;
            }
        }
    };


    /**
     * 10시 알람 설정 하기
     */
    public void registerAlarm() {
        AlramUtil.setAlarmDailyMission(this);
//        Intent intent = new Intent(this, GCAlarmService.class);
//        PendingIntent sender = PendingIntent.getBroadcast(this, GCAlarmService.ALRAM_CODE_DAILY_MISSION, intent, 0);
//
//        if (sender != null) {
////            am.cancel(sender);
////            sender.cancel()
//            // TODO: 이미 설정된 알람이 없는 경우
//
//            Calendar calendar = (Calendar) Calendar.getInstance().clone();
////            if (calendar.get(Calendar.HOUR_OF_DAY) > 10) {
////                // 내일 아침 10시 00분에 처음 시작해서, 24시간 마다 실행되게, 10시 이전 세팅
////                calendar.add(Calendar.DATE, 1);
////            }
////            calendar.set(Calendar.HOUR_OF_DAY, 10);
////            calendar.set(Calendar.MINUTE, 0);
////            calendar.set(Calendar.SECOND, 0);
//
//            calendar.add(Calendar.MINUTE, 2);
//            calendar.set(Calendar.SECOND, 0);
////            Date tomorrow = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("2012-02-25 08:10:00");
//            AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
//            am.setInexactRepeating(AlarmManager.RTC, calendar.getTimeInMillis(), 24 * 60 * 60 * 1000, sender);
//
//            DateFormat dateFormat = getDateTimeInstance();
//            Log.i(TAG, "registerAlarm::"+dateFormat.format(new Date(calendar.getTimeInMillis())));
//        } else {
//            // TODO: 이미 설정된 알람이 있는 경우
//            Log.e(TAG, "registerAlarm 이미 설정 된 알람 있음");
//        }
    }

    public int getCurrentFragment() {
        return mVisibleIdx;
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPref.getInstance().initContext(this);



        pushmove();
    }

    /**
     * 가로모드 관련 처리
     */
    private boolean mIsLandscape = false;
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        mIsLandscape = Configuration.ORIENTATION_LANDSCAPE == newConfig.orientation;
        int visible = mIsLandscape ? View.GONE : View.VISIBLE;
        Log.i(TAG, "onConfigurationChanged.isPortVisible="+visible);
        mBottomMenuLayout.setVisibility(mIsLandscape ? View.GONE : View.VISIBLE);

        if (SharedPref.getInstance().getPreferences(SharedPref.HEALTH_MESSAGE, false)) {
            message_lv.setVisibility(visible);
        } else {
            message_lv.setVisibility(View.GONE);
        }
    }

    /**
     * 푸시메시지로 온 메뉴 이동 처리
     */
    public void pushmove(){
        String move_idx = SharedPref.getInstance().getPreferences(SharedPref.PUSH_MOVE_INDEX);
        String hist_sn  = SharedPref.getInstance().getPreferences(SharedPref.PUSH_MOVE_INDEX_HIST);
        Bundle bundle = new Bundle();
//        bundle.putString("BACKTITLE","홈");
        if(TextUtils.isEmpty(move_idx) == false){

            NotiDummyActivity.pushmove(this, move_idx,hist_sn);
        }
        SharedPref.getInstance().savePreferences(SharedPref.PUSH_MOVE_INDEX,"");
        SharedPref.getInstance().savePreferences(SharedPref.PUSH_MOVE_INDEX_HIST,"");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult.resultCode="+resultCode+", data="+data);
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                final int menuIdx = data.getIntExtra(INTENT_KEY_MOVE_MENU, -1);
                if (menuIdx != -1) {

                    switch (menuIdx){
                        case HOME_MENU_3:
                            if(TextUtils.isEmpty(UserInfo.getLoginInfo().nickname)){
                                openDialogRegisterNick();
                            }else{
                                selectFragment(menuIdx);
                            }
                            break;
                        default:
                            selectFragment(menuIdx);
                            break;
                    }


                } else {
                    super.onActivityResult(requestCode, resultCode, data);
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if(intent != null){
            final int menuIdx = intent.getIntExtra(INTENT_KEY_MOVE_MENU_PUSH,-1);
            if(menuIdx != -1){
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        selectFragment(menuIdx);
                    }
                }, 500);

            }
        }
    }
}

