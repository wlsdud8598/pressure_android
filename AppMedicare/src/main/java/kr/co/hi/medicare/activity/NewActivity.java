package kr.co.hi.medicare.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.util.Set;

import kr.co.hi.medicare.BuildConfig;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.CommunityCommentFragment;
import kr.co.hi.medicare.fragment.community.CommunityImageFragment;
import kr.co.hi.medicare.fragment.community.CommunityProfileFragment;
import kr.co.hi.medicare.fragment.community.CommunityTagFragment;
import kr.co.hi.medicare.fragment.community.CommunityWriteFragment;
import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.fragment.community.data.CommunityUserData;
import kr.co.hi.medicare.fragment.community.dialog.DialogCommon;
import kr.co.hi.medicare.net.EServerAPI;
import kr.co.hi.medicare.net.Record;
import kr.co.hi.medicare.utilhw.Logger;

/**
 * Created by mrsohn on 2017. 3. 6..
 */

public class NewActivity extends BaseActivityMedicare {
    private static final String TAG = NewActivity.class.getSimpleName();

    public static final String FRAGMENT_NAME = "fragment_name";
    public static final String FRAGMENT_BUNDLE = "fragment_bundle";

    public static int reqCode = 1111;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);
        initFragment(getFragment());
    }


    public Fragment getFragment() {
        Intent intent = getIntent();
        Fragment fragment = null;
        try {
            String className = intent.getStringExtra(FRAGMENT_NAME);
            Class<? extends Fragment> cl = (Class<? extends Fragment>) Class.forName(className);
            Constructor<?> co = cl.getConstructor();
            fragment = (Fragment) co.newInstance();
        } catch (Exception e) {
            Log.e(TAG, "getFragment", e);
        }

        return fragment;
    }

    public void initFragment(Fragment fragment) {
        replaceFragment(fragment, true, false, getBundle());
    }

    public static void startActivity(Activity activity, Fragment fragment, Bundle bundle) {
        Intent intent = getIntentData(activity, fragment.getClass(), bundle);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
    }

    public static void startActivity(Activity activity, Class<? extends Fragment> cls, Bundle bundle) {
        Intent intent = getIntentData(activity, cls, bundle);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
    }

    public static void startActivityForResult(Activity activity, int reqCode, Class<? extends Fragment> cls, Bundle bundle) {
        Intent intent = getIntentData(activity, cls, bundle);
        activity.startActivityForResult(intent, reqCode, null);
        activity.overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
    }

    public static void startActivity(Fragment fragment, Class<? extends Fragment> cls, Bundle bundle) {
        Intent intent = getIntentData(fragment.getContext(), cls, bundle);
        fragment.startActivity(intent);
        fragment.getActivity().overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
    }

    public static void startActivity2(Fragment fragment, Class<? extends Fragment> cls, Bundle bundle) {
        Intent intent = getIntentData(fragment.getContext(), cls, bundle);
        fragment.startActivity(intent);
        fragment.getActivity().overridePendingTransition(R.anim.fragment_pop_up, R.anim.fragment_pop_stay);
    }


    public static void startActivityForResult(Fragment fragment, int reqCode, Class<? extends Fragment> cls, Bundle bundle) {
        Intent intent = getIntentData(fragment.getContext(), cls, bundle);
        fragment.startActivityForResult(intent, reqCode, null);
        fragment.getActivity().overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
    }

    private static Intent getIntentData(Context context, Class<? extends Fragment> cls, Bundle bundle) {
        Intent intent = new Intent(context, NewActivity.class);
        intent.putExtra(FRAGMENT_NAME, cls.getName());
        if (bundle != null)
            intent.putExtra(FRAGMENT_BUNDLE, bundle);
        if (BuildConfig.DEBUG && bundle != null) {
            Logger.i(TAG, "getIntentData.intent="+intent);
            Set<String> keys = bundle.keySet();
            for (String key : keys)
                Logger.i(TAG, "getIntentData.bundle.key="+key+", "+bundle.get(key));
        }
        return intent;
    }

    private Bundle getBundle() {
        Bundle bundle = getIntent().getBundleExtra(FRAGMENT_BUNDLE);
        return bundle;
    }



    public void click_event(View v) {
        switch (v.getId()) {
            case R.id.activity_actrecord_ImageView_back:
                super.onBackPressed();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BaseFragmentMedi.newInstance(this);
    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, TAG+".onStop()");
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, TAG+".onDestroy()");
    }

    @Override
    public void response(Record record) throws UnsupportedEncodingException {

    }

    @Override
    public void request(EServerAPI eServerAPI, Object obj) {

    }

    @Override
    public void networkException(Record record) {

    }

    public static void moveToCommentPage(Fragment fragment,CommunityListViewData data, boolean isKeyPad,String PRE_PAGE, String CM_GUBUN){
        Bundle bundle = new Bundle();
        bundle.putBoolean(CommunityCommentFragment.KEYPAD_STATUS, isKeyPad);
        bundle.putSerializable(CommunityCommentFragment.POSTS_INFO,data);
        bundle.putString(CommunityCommentFragment.PRE_PAGE, PRE_PAGE);
        bundle.putString(CommunityCommentFragment.KEY_CMGUBUN, CM_GUBUN);
        startActivityForResult(fragment, CommunityCommentFragment.REQUEST_CODE_COMMENTINFO,CommunityCommentFragment.class,bundle);
    }

    public static void moveToProfilePage(String PRE_PAGE, CommunityUserData userData,Activity activity){

        if(userData.MBER_SN==null|| userData.MBER_SN.equals(""))
            return;

        Bundle bundle = new Bundle();
        bundle.putString(CommunityProfileFragment.KEY_PRE_PAGE, PRE_PAGE);
        bundle.putSerializable(CommunityProfileFragment.KEY_PROFILE_INFO,userData);
        NewActivity.startActivity(activity, CommunityProfileFragment.class, bundle); //닉네임 변경에 따라 추가 데이터 변경필요할지도..
    }

    public static void moveToTagPage(Activity activity, String title, String pre_page){
        Bundle bundle = new Bundle();
        bundle.putString(CommunityTagFragment.TAG_TITLE, title);
        bundle.putString(CommunityTagFragment.PRE_PAGE, pre_page);
        NewActivity.startActivity(activity, CommunityTagFragment.class, bundle);
    }

    public static void moveToImagePage(Fragment fragement,String CM_IMG1,String NICK){
        Bundle bundle = new Bundle();
        bundle.putString(CommunityImageFragment.KEY_IMAGE_PATH,CM_IMG1);
        bundle.putString(CommunityImageFragment.KEY_NICKNAME,NICK);
        NewActivity.startActivity2(fragement, CommunityImageFragment.class,bundle);
    }


    /**
     *
     * @param fragment
     *
     * 선택입력사항
     * @param data data.CM_COMMENT 글내용 전달 시 포함 / 선택
     * @param data data.CM_MEAL 글쓰기 시 칼로리 식단 등을 표현 할 경우 사용
     * @param ImagePath 이미지의 절대 경로 / 로컬이미지 사용 시 지정. 없을 경우 빈값
     * 필수입력사항
     * @param data data.CM_SEQ 값 에 따라 글쓰기/글수정이 결정됨 / 신규 글쓰기 시 0, 수정일 경우 1 이상의 값 입력
     * @param data data.ISSHARE 공유하기로 글 작성 시 반드시 true
     *
     */

    public static void moveToWritePage(final Fragment fragment,final CommunityListViewData data, final String ImagePath){
        if(TextUtils.isEmpty(UserInfo.getLoginInfo().nickname)){

            CommonFunction.openDialogRegisterNick(fragment.getContext(), new DialogCommon.UpdateProfile() {
                @Override
                public void updateProfile(String NICK, String DISEASE_OPEN,String DISEASE_NM) {
                    if(!NICK.equals("")&&!DISEASE_OPEN.equals("")) {
                        final DialogCommon dialogIntro = DialogCommon.showDialogIntro(fragment.getContext(), NICK);
                        dialogIntro.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                Bundle bundle = new Bundle();
                                bundle.putSerializable(CommunityWriteFragment.KEY_CONTENTS,data);
                                bundle.putString(CommunityWriteFragment.KEY_IMAGEPATH,ImagePath);
                                NewActivity.startActivityForResult(fragment, CommunityWriteFragment.REQUEST_CODE_WRITEINFO,CommunityWriteFragment.class,bundle);
                            }
                        });
                    }else{
                        moveToWritePage(fragment,data,ImagePath);
                    }
                }
            });
        }else{
            Bundle bundle = new Bundle();
            bundle.putSerializable(CommunityWriteFragment.KEY_CONTENTS,data);
            bundle.putString(CommunityWriteFragment.KEY_IMAGEPATH,ImagePath);
            NewActivity.startActivityForResult(fragment, CommunityWriteFragment.REQUEST_CODE_WRITEINFO,CommunityWriteFragment.class,bundle);
        }



    }



}
