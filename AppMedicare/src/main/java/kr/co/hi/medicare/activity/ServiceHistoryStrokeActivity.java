package kr.co.hi.medicare.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.io.UnsupportedEncodingException;

import kr.co.hi.medicare.BuildConfig;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.net.EServerAPI;
import kr.co.hi.medicare.net.Record;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;

/**
 * Created by jihoon on 2016-06-16.
 * 서비스 이력보기
 * @since 0, 1
 */
public class ServiceHistoryStrokeActivity extends BaseActivityMedicare {

    public static final int REQUEST_CODE_CERT   =   1;
//    public static final String CERT_API_URL = "http://www.walkie.co.kr/hL/cert/HS_HL_CERT_STEP01.asp";
    public static final String CERT_API_URL = "https://m.shealthcare.co.kr/HL_MED_COMMUNITY/CERT/HS_HL_CERT_STEP01.asp";


    private boolean mIsCert = false;    // 휴대폰 인증 여부
    private int mType = -1;
    private UserInfo user;

    private Intent intent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_history_stroke);
        user = new UserInfo(this);

        init();
    }

    private void init() {
        intent = getIntent();
        if (intent != null){
            mIsCert = intent.getBooleanExtra(EXTRA_AUTH , false);
        }

        String certidate = SharedPref.getInstance().getPreferences(SharedPref.CERTI_DATE);

        if(!certidate.equals("")){
            if(StringUtil.getIntVal(certidate) == StringUtil.getIntVal(CDateUtil.getToday_yyyy_MM_dd()))
                mIsCert = true;
            else
                mIsCert = false;
        }
    }

    public void click_event(View v) {
        boolean backflag = false;

        switch (v.getId()) {
            case R.id.history_1_layout:
                mType   =   0;
                break;
            case R.id.history_2_layout:
                mType   =   1;
                break;
            case R.id.history_3_layout:
                mType   =   2;
                break;
        }
        if (!backflag) {
            CLog.i("mType = " + mType);
            setIntent(mType);
        }
    }

    /**
     * 세부 이력보기 이동처리
     * @param type 타입 ( 0 ~ 4 )
     */
    public void setIntent(int type){
        Intent intent = null;
//        mIsCert = true;

        if(BuildConfig.DEBUG){
            mIsCert = true;
        }

        /**
         * 인증 무시 하기위해 mIsCert false일 떄로 변경 20190319
         */
        if(mIsCert){
            switch(type){
                case 0: // 방문 재활지원서비스
                    intent = new Intent(ServiceHistoryStrokeActivity.this, ServiceHistoryStrokeDetailVisitActivity.class);
                    break;
                case 1: // 스마트 재활지원서비스
                    intent = new Intent(ServiceHistoryStrokeActivity.this, ServiceHistoryStrokeDetailRentActivity.class);
                    break;
                case 2: // 이송차량 지원서비스
                    intent = new Intent(ServiceHistoryStrokeActivity.this, ServiceHistoryStrokeDetailMovercarActivity.class);
                    break;
            }
            intent.putExtra(EXTRA_SERVICETYPE , mType);
            startActivity(intent);
        }else{
            Tr_login login = UserInfo.getLoginInfo();
            intent = new Intent(ServiceHistoryStrokeActivity.this, BackWebViewActivity.class);
            if(BuildConfig.DEBUG)
                intent.putExtra("EXTRA_URL", CERT_API_URL + "?seq=" +login.seq);
            else
                intent.putExtra("EXTRA_URL", CERT_API_URL + "?seq=" +login.seq);
            startActivityForResult(intent, REQUEST_CODE_CERT);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        CLog.i("resultCode : " + resultCode);
        CLog.i("requestCode :" + requestCode);

        if (resultCode != Activity.RESULT_OK) {
            CLog.i("resultCode != RESULT_OK");
            return;
        }

        switch (requestCode) {
            case REQUEST_CODE_CERT:
                mIsCert =   true;
                setIntent(mType);
                break;
        }


        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void response(Record record) throws UnsupportedEncodingException {

    }

    @Override
    public void request(EServerAPI eServerAPI, Object obj) {

    }

    @Override
    public void networkException(Record record) {

    }
}
