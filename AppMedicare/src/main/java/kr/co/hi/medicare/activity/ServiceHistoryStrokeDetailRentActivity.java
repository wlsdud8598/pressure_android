package kr.co.hi.medicare.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import kr.co.hi.medicare.BuildConfig;
import kr.co.hi.medicare.Defined;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.adapter.HistoryStrokeRentListView_Adapter;
import kr.co.hi.medicare.bean.ServiceRentItem;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.net.EServerAPI;
import kr.co.hi.medicare.net.Record;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.AlertDialogUtil;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.util.Util;

/**
 * Created by suwun on 2017-02-24.
 * 서비스 이력보기 (렌트)
 * @since 0, 1
 */
public class ServiceHistoryStrokeDetailRentActivity extends BaseActivityMedicare {

    private UserInfo user;
    private TextView mTopTv1;// , mTopTv2;
    private ListView mListView;
    private int mType; // 0 렌트
    private Intent intent;
    private LinearLayout mTopLay ,  mTopLay1;

    private ArrayList<ServiceRentItem> mItem = new ArrayList<ServiceRentItem>();
    private HistoryStrokeRentListView_Adapter mAdapter;

    private boolean mDataFlag = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_history_first);
        user = new UserInfo(this);

        init();
    }

    private void init() {

        intent = getIntent();
        if (intent != null){
            mType = intent.getIntExtra(EXTRA_SERVICETYPE , 0);
        }

        CLog.i("mType --> " + mType);

        mTopLay1         = (LinearLayout) findViewById(R.id.top_lay1);
        mTopLay         = (LinearLayout) findViewById(R.id.top_lay);
        mTopLay.setVisibility(View.GONE);
        mTopTv1         = (TextView) findViewById(R.id.top_tv3);
//        mTopTv2         = (TextView) findViewById(R.id.top_tv2);
        mListView       = (ListView) findViewById(R.id.data_list);


        requestHistoryApi(mType);

    }


    /**
     * api 호출
     * @param mtype
     */

    public void requestHistoryApi(int mtype) {

        CLog.i("mtype --> " + mtype);

        try {
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            client.setTimeout(10000);
            client.addHeader("Accept-Charset", "UTF-8");
            client.addHeader("User-Agent", new WebView(getBaseContext()).getSettings().getUserAgentString());
            Tr_login login = UserInfo.getLoginInfo();

            JSONObject jObject = new JSONObject();
            if(BuildConfig.DEBUG){
                jObject.put("SEQ", "0001013000015");
            }else{
                jObject.put("SEQ", login.seq);
            }
//            jObject.put("SEQ", "0001013000015");
            jObject.put("DOCNO", "DZ009");

            params.put("strJson", jObject.toString());
            CLog.i("url = " + Defined.COMMUNITY_HTTPS_URL + "?" + params.toString());
            client.post(Defined.COMMUNITY_HTTPS_URL, params, mAsyncHttpHandler);
        } catch (Exception e) {
            CLog.e(e.toString());
        }

    }

    private AsyncHttpResponseHandler mAsyncHttpHandler = new AsyncHttpResponseHandler() {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            CLog.i("onSuccess");

            try {

                int count = headers.length;

                for (int i = 0; i < count; i++) {
                    CLog.v(headers[i].getName() + ": " + headers[i].getValue());
                }

                String response = new String(responseBody, "UTF-8");
                CLog.i("response = " +response);
                JSONObject resultData = null;

                if(response.startsWith("<")){   // api result 문자열 시작값이 "<" 로 시작하는 경우는 xml 폼
                    try {
                        response =  Util.parseXml(response);
                    }catch(Exception e){
                        CLog.e(e.toString());
                    }

                    resultData = new JSONObject(response);

                }else{
                    resultData = new JSONObject(response);
                }

                Intent intent;

                String resultCode = resultData.getString("DOCNO");
                switch(resultCode){

                    case "DZ009": //
                        try {
                            String resultLength = (String) resultData.get("DATA_LENGTH");
                            String resultGood = (String) resultData.get("GOODS_NAME");

                            if (!resultLength.equals("0")) {
                                JSONArray jsonArray = new JSONArray(resultData.getString("DATA"));
                                JSONObject object1 = jsonArray.getJSONObject(0);

                                switch (object1.getString("RESULT_CODE")) {
                                    case "0000":
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject object = jsonArray.getJSONObject(i);
                                            ServiceRentItem item = new ServiceRentItem(
                                                    object.getString("SS_SEQ"),
                                                    resultGood,
                                                    object.getString("REQDATE"),
                                                    object.getString("USEDATE"),
                                                    object.getString("ENDDATE"),
                                                    object.getString("CONFDATE"),
                                                    object.getString("RTNDATE"),
                                                    object.getString("SO_NAME"),
                                                    object.getString("USE_MEMO"),
                                                    object.getString("STATE_NAME"),
//                                                    object.getString("SCOUNT"),
                                                    object.getString("OSEQ"),
                                                    object.getString("RESULT_CODE"));
                                            mItem.add(item);
                                            mDataFlag = true;
                                        }
                                        break;
                                    case "4444":
                                        UIThread(ServiceHistoryStrokeDetailRentActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record), true);
                                        break;
                                    case "6666":
                                        UIThread(ServiceHistoryStrokeDetailRentActivity.this, getString(R.string.confirm), getString(R.string.msg_service_record) , true);
                                        break;
                                    case "9999":
                                        UIThread(ServiceHistoryStrokeDetailRentActivity.this, getString(R.string.confirm), getString(R.string.no_user) , true);
                                        break;

                                }
                                UiThread();
                                break;
                            } else {
                                UIThread(ServiceHistoryStrokeDetailRentActivity.this, getString(R.string.confirm), getString(R.string.meal_tok_notdata)  , true);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                }


            } catch (Exception e) {
                CLog.e(e.toString());
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            CLog.i("onFailure");
            UIThread(ServiceHistoryStrokeDetailRentActivity.this, getString(R.string.confirm), getString(R.string.networkexception) , true);

        }
    };


    public void click_event(View v) {

        switch (v.getId()) {
            // 뒤로
            case R.id.activity_actrecord_ImageView_back:
                finish();
                break;
        }
    }

    @Override
    public void request(EServerAPI eServerAPI, Object obj) {}

    @Override
    public void response(Record record) throws UnsupportedEncodingException {}

    @Override
    public void networkException(Record record) {}

    private void UIThread(final Activity activity, final String confirm, final String message , final boolean aa) {
        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                AlertDialogUtil.onAlertDialog(activity, confirm, message, aa);
            }
        }, 0);
    }

    private void UiThread() {
        final Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                CLog.i("UiThread ---> ");

                if (mDataFlag) {
//                    mTopLay.setVisibility(View.VISIBLE);
                    mTopLay1.setVisibility(View.VISIBLE);

                    mTopTv1.setText(getString(R.string.service_history_stroke_title_2));

//                    String imsi = String.valueOf(4 - mItem.size());
//                    mTopTv2.setText(imsi + "회");
                    mAdapter = new HistoryStrokeRentListView_Adapter(ServiceHistoryStrokeDetailRentActivity.this, mItem);
                    mListView.setAdapter(mAdapter);

                }
            }

        }, 0);


    }
}
