package kr.co.hi.medicare.activity;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResponse;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.component.PermissionDialog;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperFoodDetail;
import kr.co.hi.medicare.database.DBHelperFoodMain;
import kr.co.hi.medicare.database.DBHelperLog;
import kr.co.hi.medicare.database.DBHelperMessage;
import kr.co.hi.medicare.database.util.DBBackupManager;
import kr.co.hi.medicare.fragment.login.AgreeIntroFragment;
import kr.co.hi.medicare.fragment.login.LoginFirstInfoFragment1_2;
import kr.co.hi.medicare.googleFitness.GoogleFitInstance;
import kr.co.hi.medicare.googleFitness.SplashUploadGoogleFitData;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_get_app_info;
import kr.co.hi.medicare.net.data.Tr_get_hedctdata;
import kr.co.hi.medicare.net.data.Tr_menu_log_hist;
import kr.co.hi.medicare.net.hwNet.ApiData;
import kr.co.hi.medicare.net.hwNet.BaseData;
import kr.co.hi.medicare.net.hwdata.Tr_get_infomation;
import kr.co.hi.medicare.net.hwdata.Tr_get_meal_input_data;
import kr.co.hi.medicare.net.hwdata.Tr_get_meal_input_food_data;
import kr.co.hi.medicare.net.hwdata.Tr_infra_message;
import kr.co.hi.medicare.net.hwdata.Tr_infra_message_write;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.permission.IPermissionResult;
import kr.co.hi.medicare.util.permission.PermissionUtil;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.PackageUtil;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;
import kr.co.hi.medicare.value.Define;
import kr.co.hi.medicare.value.model.MessageModel;

import static java.text.DateFormat.getTimeInstance;

/**
 * 스플래쉬
 */
public class SplashActivityMedicare extends BaseActivityMedicare {
    private UserInfo user;
    private PackageInfo packageInfo;

    private final String[] PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medi_splash);

        // sqlite 파일이 없으면 복사
        new DBBackupManager().importDBAssets(SplashActivityMedicare.this);
//        checkAppVersion();//버전체크
          init();
    }

    /**
     * 클릭로그
     */

    private void sendLog(final Tr_login login){
        String sendFlag = SharedPref.getInstance().getPreferences(SharedPref.SEND_LOG_DATE);

        if(sendFlag == null || sendFlag.equals("")){
            sendFlag = CDateUtil.getToday_yyyy_MM_dd();
            SharedPref.getInstance().savePreferences(SharedPref.SEND_LOG_DATE,sendFlag);
        }

        if(StringUtil.getLong(sendFlag) < StringUtil.getLong(CDateUtil.getToday_yyyy_MM_dd())) {
            Tr_menu_log_hist.RequestData requestData = new Tr_menu_log_hist.RequestData();
            requestData.mber_sn = login.mber_sn;
            requestData.DATA = Tr_menu_log_hist.getArray(SplashActivityMedicare.this);
            requestData.DATA_LENGTH = String.valueOf(requestData.DATA.length());

            if(requestData.DATA.length() > 0) {

                MediNewNetworkModule.doApi(SplashActivityMedicare.this, Tr_menu_log_hist.class, requestData, new MediNewNetworkHandler() {
                    @Override
                    public void onSuccess(BaseData responseData) {
                        if (responseData instanceof Tr_menu_log_hist) {
                            Tr_menu_log_hist data = (Tr_menu_log_hist) responseData;

                            if (data.result_code.equals("0000")) {
                                DBHelper helper = new DBHelper(SplashActivityMedicare.this);
                                DBHelperLog db = helper.getLogDb();
                                db.delete_log();

                                SharedPref.getInstance().savePreferences(SharedPref.SEND_LOG_DATE, CDateUtil.getToday_yyyy_MM_dd());
                                //TODO 이동처리
                                doFirstData("Y".equals(login.add_reg_yn));

                            } else {
                                //TODO 이동처리
                                doFirstData("Y".equals(login.add_reg_yn));

                            }
                        } else {
                            //TODO 이동처리
                            doFirstData("Y".equals(login.add_reg_yn));
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, String response, Throwable error) {
                        CDialog.showDlg(SplashActivityMedicare.this, getString(R.string.networkexception))
                                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        finish();
                                    }
                                });
                    }
                });
            } else {
                //TODO 이동처리
                doFirstData("Y".equals(login.add_reg_yn));
            }
        } else {
            //TODO 이동처리
            doFirstData("Y".equals(login.add_reg_yn));
        }
    }


    /**
     * 앱 버전 체크
     */
    private void checkAppVersion() {
        final Tr_get_app_info.RequestData requestData = new Tr_get_app_info.RequestData();

        requestData.HP_SEQ = FirebaseInstanceId.getInstance().getToken();
        if (TextUtils.isEmpty(requestData.HP_SEQ))
            requestData.HP_SEQ = " ";
        requestData.APP_VER = PackageUtil.getVersionInfo(this);

        MediNewNetworkModule.doApi(SplashActivityMedicare.this, Tr_get_app_info.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_get_app_info) {
                    Tr_get_app_info data = (Tr_get_app_info)responseData;

                    Log.i(TAG, "recv.now_app_ver="+data.now_app_ver+", this APP_VER="+requestData.APP_VER+", "+(data.now_app_ver.compareTo(requestData.APP_VER)));

                    if (StringUtil.getFloat(data.now_app_ver) > StringUtil.getFloat(requestData.APP_VER)) {
                        CDialog.showDlg(SplashActivityMedicare.this, getString(R.string.update_version))
                                .setOkButton(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        final String updateUrl = "market://details?id="+getPackageName();
                                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
                                        List<ResolveInfo> resInfos = getPackageManager().queryIntentActivities(browserIntent, 0);
                                        Log.i("linkStore","list: "+resInfos);

                                        if(resInfos != null || resInfos.size() != 0) {

                                            for (ResolveInfo info : resInfos) {
                                                if (info.activityInfo.packageName.toLowerCase().contains("com.android.vending") ||
                                                        info.activityInfo.name.toLowerCase().contains("com.android.vending")) {

                                                    browserIntent.setPackage(info.activityInfo.packageName);
                                                    break;
                                                }
                                            }
                                        }

                                        startActivity(browserIntent);

                                    }
                                })
                                .setNoButtonDismiss("취소", new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
//                                        finish();
                                        reqPermissions();
                                    }
                                });
                    } else {
                        reqPermissions();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                Log.e(TAG, "onFailure.statusCode="+statusCode+", response="+response);
                CDialog.showDlg(SplashActivityMedicare.this, getString(R.string.networkexception))
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                finish();
                            }
                        });

            }
        });
    }


    /**
     * 권한 체크
     */
    private void reqPermissions() {
        PermissionUtil.permissionResultCheck(SplashActivityMedicare.this, 999, PERMISSIONS, new IPermissionResult() {
            @Override
            public void onPermissionResult(int requestCode, boolean isGranted, List<String> deniedPermissionList) {
                if (isGranted) {
//                    init();
                    googleFitnessAuthCheck();
                } else {
                    View view = LayoutInflater.from(SplashActivityMedicare.this).inflate(R.layout.splash_permission_dialog_view,null);
                    PermissionDialog dlg = PermissionDialog.showDlg(SplashActivityMedicare.this, view);
                    dlg.setTitle(getString(R.string.splash_permission_dlg_title));
                    dlg.setOkButton(getString(R.string.confirm), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            permissionRequest(PERMISSIONS, 0, new IPermissionResult() {
                                @Override
                                public void onPermissionResult(int requestCode, boolean isGranted, List<String> deniedPermissionList) {
                                    if (isGranted) {
                                        googleFitnessAuthCheck();
                                    } else {
                                        reqPermissions();
                                    }
                                }
                            });
                        }
                    });
                    dlg.setNoButton("종료", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                    dlg.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            finish();
                        }
                    });
                }
            }
        });
    }

    /**
     * 구글 피트니스 계정인증
     * 걸음수만을 조회하기 위해 TYPE_STEP_COUNT_CUMULATIVE, TYPE_STEP_COUNT_DELTA만을 요청한다.
     * TYPE_STEP_COUNT_CUMULATIVE는 누적걸음수를 한번에 조회하기 위해 필요한 권한이고,
     * TYPE_STEP_COUNT_DELTA는 단위시간별 걸음수를 확인하기 위해 필요한 권한이다.
     * 사용자의 필요에 따라 둘 중 하나만 요청해도 무방하다.
     */
    private void googleFitnessAuthCheck() {
        if(GoogleFitInstance.isFitnessAuth(SplashActivityMedicare.this)) {
            init();
        } else {
            // 구글 피트니스 계정 권한 요청
            GoogleFitInstance.requestGoogleFitnessAuth(SplashActivityMedicare.this);
        }
    }

    private void readData() {

        String uploadDate = SharedPref.getInstance().getPreferences(SharedPref.GOOGLE_FITNESS_UPLOAD_DATE2);
        Calendar yesterDayCal = (Calendar) Calendar.getInstance().clone();
        yesterDayCal.add(Calendar.DATE, -1);
        String yesterDay = CDateUtil.getFormattedStringyyyyMMdd(yesterDayCal.getTimeInMillis());
        if (TextUtils.isEmpty(uploadDate) || yesterDay.equals(uploadDate)) {
            SharedPref.getInstance().savePreferences(SharedPref.GOOGLE_FITNESS_UPLOAD_DATE2, yesterDay);
        } else {
            // 최종 업로드 날자
            Calendar uploadCal = CDateUtil.getCalendar_yyyyMMdd(uploadDate);
            uploadCal.set(uploadCal.get(Calendar.YEAR)
                    , uploadCal.get(Calendar.MONTH)
                    , uploadCal.get(Calendar.DAY_OF_MONTH)
                    , yesterDayCal.getMinimum(Calendar.HOUR_OF_DAY)
                    , yesterDayCal.getMinimum(Calendar.MINUTE)
                    , yesterDayCal.getMinimum(Calendar.SECOND));
            long startTime = uploadCal.getTimeInMillis();

            // 어제 날자
            yesterDayCal.set(yesterDayCal.get(Calendar.YEAR)
                    , yesterDayCal.get(Calendar.MONTH)
                    , yesterDayCal.get(Calendar.DAY_OF_MONTH)
                    , yesterDayCal.getMaximum(Calendar.HOUR_OF_DAY)
                    , yesterDayCal.getMaximum(Calendar.MINUTE)
                    , yesterDayCal.getMaximum(Calendar.SECOND));
            long endTime = yesterDayCal.getTimeInMillis();

            Fitness.getHistoryClient(this,
                    GoogleSignIn.getLastSignedInAccount(this))
                    .readData(new DataReadRequest.Builder()
                            .read(DataType.TYPE_STEP_COUNT_DELTA) // Raw 걸음 수
                            .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                            .build())
                    .addOnSuccessListener(new OnSuccessListener<DataReadResponse>() {
                        @Override
                        public void onSuccess(DataReadResponse response) {
                            DataSet dataSet = response.getDataSet(DataType.TYPE_STEP_COUNT_DELTA);
                            Log.i(TAG, "Data returned for Data type: " + dataSet.getDataType().getName());
                            DateFormat dateFormat = getTimeInstance();
                            for (DataPoint dp : dataSet.getDataPoints()) {
                                Log.i(TAG, "Data point:");
                                Log.i(TAG, "\tType: " + dp.getDataType().getName());

                                Log.i(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                                Log.i(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
                                for (Field field : dp.getDataType().getFields()) {
                                    Log.i(TAG, "\tField: " + field.getName() + " Value: " + dp.getValue(field));
                                }
                            }
                        }
                    });
        }

    }

    private void init() {
        user = new UserInfo(this);
        // 앱 버전.
        try {
            packageInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        final boolean guide = SharedPref.getInstance().getPreferences(GuideActivity.PREF_KEY_GUIDE, true);
        if (user.getIsAutoLogin()) {
            doMediLoginNew(user.getSaveId(), user.getSavedPw(), true);
        } else {
            Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    Intent intent;
//                    if (guide) {
//                        // 가이드가 true면.
//                        intent = new Intent(SplashActivityMedicare.this, GuideActivity.class);
//                    } else {
//                        intent = new Intent(SplashActivityMedicare.this, LoginActivityMedicare.class);
//                    }

                    intent = new Intent(SplashActivityMedicare.this, MainActivityMedicare.class);

                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
                }
            };
            handler.sendEmptyMessageDelayed(0, 2000);
        }
    }

//    private void UIThread(final Activity activity, final String confirm, final String message) {
//        Handler mHandler = new Handler(Looper.getMainLooper());
//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                AlertDialogUtil.onAlertDialog(activity, confirm, message);
//
//                Intent intent = new Intent(SplashActivityMedicare.this, LoginActivityMedicare.class);
//                startActivity(intent);
//                finish();
//            }
//        }, 0);
//    }

    /**
     * 신규 로그인 전문
     * @param id
     * @param pw
     * @param isAutoLogin
     */
    private void doMediLoginNew(final String id, final String pw, final boolean isAutoLogin) {
        final Tr_login.RequestData requestData = new Tr_login.RequestData();

        requestData.mber_id = id;
        requestData.mber_pwd = pw;

        MediNewNetworkModule.doApi(SplashActivityMedicare.this, Tr_login.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_login) {
                    Tr_login data = (Tr_login)responseData;
                    try {
                        isFirstLogin = data.add_reg_yn;
                        String resultCode = data.log_yn;
                        if ("Y".equals(resultCode)) {
                            if (requestData.mber_id.equals(user.getSaveId()) == false) {
                                // 기존 사용자 아이디와 다른경우 내부 sqlite 삭제
                                DBHelper helper = new DBHelper(SplashActivityMedicare.this);
                                helper.deleteAll();
                                SharedPref.getInstance().removeAllPreferences();    // sharedprefrence 초기화

                                // Sqlash 음식 초기데이터 true
                                SharedPref.getInstance().savePreferences(SharedPref.INTRO_FOOD_DB, true);

                                //건강메시지 On/Off
                                SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MESSAGE_CONFIRIM, false);
                            } else {
                                SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MESSAGE_CONFIRIM, true);
                            }
                            Define.getInstance().setLoginInfo(data);
//                            user.saveLoginInfo(data);
                            if (user.getIsAutoLogin()) {
                                user.setSavedId(id);
                                user.setSavePwd(pw);
                            }

//                            if (user.isSavedId()) {
//                                user.setSavedId(id);
//                            }
                            user.setIsAutoLogin(isAutoLogin);

                            SharedPref.getInstance().savePreferences(SharedPref.SPLASH_BUILD_GOOGLE_ACCOUNT, true);

                            SharedPref.getInstance().savePreferences(SharedPref.SAVED_LOGIN_PWD, requestData.mber_pwd);
                            SharedPref.getInstance().savePreferences(SharedPref.MBER_SN, data.mber_sn);

                            SharedPref.getInstance().savePreferences(SharedPref.SAVED_LOGIN_ID, requestData.mber_id);
                            SharedPref.getInstance().savePreferences(SharedPref.IS_LOGIN_SUCEESS, true);
//                        SharedPref.getInstance().savePreferences(SharedPref.IS_AUTO_LOGIN, mAutoLoginCheckBox.isChecked());

//                            data.mber = requestData.mber_id;

                            if(data.mber_bdwgh_app.equals("") || data.mber_bdwgh_app.isEmpty()){
                                data.mber_bdwgh_app = data.mber_bdwgh;
                            }

                            Define.getInstance().setLoginInfo(data);

                            //클릭로그 저장
                            sendLog(data);


                        } else {
                            // 존재하지 않는 사용자.
                            user.setIsAutoLogin(false);
                            Intent intent = new Intent(SplashActivityMedicare.this, LoginActivityMedicare.class);
//                            UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.login_wrong_id));
//                            Handler mHandler = new Handler(Looper.getMainLooper());
//                            mHandler.postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    AlertDialogUtil.onAlertDialog(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.login_wrong_id));
//                                    CDialog.showDlg(SplashActivity.this, getString(R.string.login_wrong_id));
//                                }
//                            }, 0);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                Log.e(TAG, "onFailure.statusCode="+statusCode+", response="+response);
                CDialog.showDlg(SplashActivityMedicare.this, getString(R.string.networkexception))
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                finish();
                            }
                        });

            }
        });
    }

    private String isFirstLogin;
    /**
     * 앱삭제시 저장된 3개월치 데이터 가져오기
     */
    private int m3MonthIdx = 2;
    private void doFirstData(final boolean isFirstInfo) {
//        showProgress();
        if (m3MonthIdx <= 7) {
            getFirstData(""+m3MonthIdx, new ApiData.IStep() {
                @Override
                public void next(Object obj) {
                    if (m3MonthIdx == 6) {
                        // 데이터가 6개 데이터 가져오기 되면 음식 데이터 가져오기(걸음,혈당,혈압,체중,물)

                        getFoodData(new ApiData.IStep() {
                            @Override
                            public void next(Object obj) {

                                // 건강 메시지 가져오기
                                getHealthMessageData(new ApiData.IStep() {
                                    @Override
                                    public void next(Object obj) {

                                        boolean isConfirmMsg = SharedPref.getInstance().getPreferences(SharedPref.HEALTH_MESSAGE_CONFIRIM, false);
                                        if(isConfirmMsg == false){
                                            DBHelper helper = new DBHelper(SplashActivityMedicare.this);
                                            DBHelperMessage db = helper.getMessageDb();
                                            List<MessageModel> messageList = db.getResultAll(helper, Tr_infra_message_write.INFRA_TY_ALL);
                                            if(messageList.size() > 0) {
                                                SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MESSAGE, true);
                                            }
                                        }
                                        if("N".equals(isFirstLogin)) {
//                                            stratGoogleClient(MainFragment.newInstance(), null);
                                            new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
//                                                    Intent intent = new Intent(SplashActivityMedicare.this, MainActivityMedicare.class);
//                                                    startActivity(intent);
//                                                    finish();
//                                                    getInformation(isFirstInfo);
                                                    googleFitnessDateUpload(isFirstInfo);
                                                }
                                            }, 1);
                                        }
                                        else{
//                                            Intent intent = new Intent(SplashActivityMedicare.this, MainActivityMedicare.class);
//                                            startActivity(intent);
//                                            finish();
//                                            getInformation(isFirstInfo);
                                            googleFitnessDateUpload(isFirstInfo);
                                        }
                                    }
                                });
                            }
                        });

                    } else {
                        // 3개월 데이터 가져오기 (걸음, 혈당 혈압 체중)
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                m3MonthIdx++;
                                doFirstData(isFirstInfo);
                            }
                        }, 1);
                    }
                }
            });
        }
    }

    /**
     * 구글 피트니스 걸음 데이터 업로드
     * @param isFirstInfo
     */
    private void googleFitnessDateUpload(final boolean isFirstInfo) {
        new SplashUploadGoogleFitData().uploadGoogleStepData(this, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                getInformation(isFirstInfo);
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                getInformation(isFirstInfo);
            }
        });
    }

    private void getInformation(final boolean isFirstInfo) {
        Tr_get_infomation.RequestData reqData = new Tr_get_infomation.RequestData();
        MediNewNetworkModule.doApi(SplashActivityMedicare.this, Tr_get_infomation.class, reqData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                Tr_login userInfo = UserInfo.getLoginInfo();

                if(userInfo.mber_grad.equals("10")){
                    if(userInfo.offerinfo_yn.equals("N")){
                        NewActivity.startActivity(SplashActivityMedicare.this, AgreeIntroFragment.class, null);
                    } else {
                        if (isFirstInfo) {
                            // 최초정보 입력 안했을 경우
                            NewActivity.startActivity(SplashActivityMedicare.this, LoginFirstInfoFragment1_2.class, null);
                        } else {
                            Intent intent = new Intent(SplashActivityMedicare.this, MainActivityMedicare.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);
                        }
                    }
                } else {
                    if (isFirstInfo) {
                        // 최초정보 입력 안했을 경우
                        NewActivity.startActivity(SplashActivityMedicare.this, LoginFirstInfoFragment1_2.class, null);
                    } else {
                        Intent intent = new Intent(SplashActivityMedicare.this, MainActivityMedicare.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                    }
                }

                finish();
                overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(SplashActivityMedicare.this, getString(R.string.networkexception))
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                finish();
                            }
                        });

            }
        });
    }

    /**
     * 3개월치 데이터 가져오기
     * @param code
     * @param iStep
     */
    private void getFirstData(final String code, final ApiData.IStep iStep) {
        // 한번 저장 완료가 되면 호출 하지 않기 위한 값
        boolean isSaved = SharedPref.getInstance().getPreferences(SharedPref.SAVED_LOGIN_ID+code , false);
        Log.i(TAG, "getFirstData.code="+code +", isSaved="+isSaved);
        if (isSaved) {
            iStep.next(null);
            return;
        }

        Calendar cal = Calendar.getInstance(Locale.KOREA);
        Date now = new Date();
        cal.setTime(now);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        Tr_get_hedctdata.RequestData requestData = new Tr_get_hedctdata.RequestData();
        requestData.mber_sn = UserInfo.getLoginInfo().mber_sn;

        requestData.get_data_typ = code;
        requestData.end_day = CDateUtil.getToday_yyyy_MM_dd();    // 오늘 날자
        cal.add(Calendar.MONTH, -3);                    // 시작날자 (3개월전
        requestData.begin_day = sdf.format(cal.getTimeInMillis());   // 20170301


        MediNewNetworkModule.doApi(SplashActivityMedicare.this, Tr_get_hedctdata.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_get_hedctdata) {
                    DBHelper helper = new DBHelper(SplashActivityMedicare.this);

                    // 1: 운동      2:혈당    3:혈압    4:체중    5:물data (제일 Low data), 6:심박수
                    Tr_get_hedctdata data = (Tr_get_hedctdata) responseData;
                    if ("1".equals(data.get_data_typ)) {
                        helper.getStepDb().insert(data, true);
                    } else if ("2".equals(data.get_data_typ)) {
                        helper.getSugarDb().insert(data, true);
                    } else if ("3".equals(data.get_data_typ)) {
                        helper.getPresureDb().insert(helper, data, true);
                    } else if ("4".equals(data.get_data_typ)) {
                        helper.getWeightDb().insert(data, true);
                    } else if ("5".equals(data.get_data_typ)) {
                        helper.getWaterDb().insert(data.data_list, true);
                    } else if ("6".equals(data.get_data_typ)) {
                        helper.getPPGDb().insert(data, true);
                    } else if ("7".equals(data.get_data_typ)) {
                        helper.getCalorieDb().insert(data, true);
                    }
                    SharedPref.getInstance().savePreferences(SharedPref.SAVED_LOGIN_ID + code, true);

                    iStep.next(responseData);
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {

            }
        });
    }

    private void getFoodData(final ApiData.IStep iStep) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        final String endDate = CDateUtil.getToday_yyyy_MM_dd();
        Calendar cal =  CDateUtil.getCalendar_yyyyMMdd(endDate);
        cal.add(Calendar.MONTH, -3);
        final String startDate = sdf.format(cal.getTimeInMillis());

        // 식사데이터 가져와서 sqlite 저장하기
        getFirstMealData(startDate, endDate, new ApiData.IStep() {
            @Override
            public void next(Object obj) {

                // 음식 데이터 가져와서 sqlite 저장하기
                getFirstFoodData(startDate, endDate, new ApiData.IStep() {
                    @Override
                    public void next(Object obj) {
                        iStep.next(obj);
                    }
                });
            }
        });
    }

    /**
     * 데이터 가져오기(식사)
     */
    private void getFirstMealData(final String beginDate, final String endDate, final ApiData.IStep iStep) {
        final boolean isSaved = SharedPref.getInstance().getPreferences(SharedPref.IS_SAVED_MEAL_DB, false);

        if (isSaved) {
            iStep.next(isSaved);
            return;
        }

        Tr_get_meal_input_data.RequestData requestData = new Tr_get_meal_input_data.RequestData();

        Tr_login login = UserInfo.getLoginInfo();
        requestData.mber_sn = login.mber_sn;
        requestData.begin_day = beginDate;
        requestData.end_day = endDate;

        getData(SplashActivityMedicare.this, Tr_get_meal_input_data.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_get_meal_input_data) {
                    Tr_get_meal_input_data data = (Tr_get_meal_input_data) obj;

                    DBHelper helper = new DBHelper(SplashActivityMedicare.this);
                    DBHelperFoodMain db = helper.getFoodMainDb();
                    db.insert(data.data_list);

                    SharedPref.getInstance().savePreferences(SharedPref.IS_SAVED_MEAL_DB, true);

                    iStep.next(true);
                } else {
                    iStep.next(false);
                }
            }
        });
    }


    /**
     * 데이터 가져오기 (음식)
     */
    private void getFirstFoodData(String startDate, String endDate, final ApiData.IStep iStep) {
        boolean isSaved = SharedPref.getInstance().getPreferences(SharedPref.IS_SAVED_FOOD_DB, false);
        Logger.i(TAG, "getFirstFoodData, isSaved="+isSaved);
        if (isSaved) {
            iStep.next(isSaved);
            return;
        }

        final Tr_get_meal_input_food_data.RequestData requestData = new Tr_get_meal_input_food_data.RequestData();
        Tr_login login = UserInfo.getLoginInfo();

        requestData.mber_sn = login.mber_sn;
        requestData.begin_day = startDate;
        requestData.end_day = endDate;

        getData(SplashActivityMedicare.this, Tr_get_meal_input_food_data.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_get_meal_input_food_data) {
                    Tr_get_meal_input_food_data data = (Tr_get_meal_input_food_data) obj;

                    DBHelper helper = new DBHelper(SplashActivityMedicare.this);
                    DBHelperFoodDetail db = helper.getFoodDetailDb();
                    db.insert(data.data_list);

                    SharedPref.getInstance().savePreferences(SharedPref.IS_SAVED_FOOD_DB, true);

                    iStep.next(true);
                } else {
                    iStep.next(false);
                }
            }
        });
    }

    /**
     * 건강 메시지 가져와 Sqlite 저장하기
     */
    private int mHealthMsgCnt = 1;
    private void getHealthMessageData(final ApiData.IStep iStep) {

        final boolean isSaved = SharedPref.getInstance().getPreferences(SharedPref.IS_SAVED_HEALTH_MESSAGE_DB, false);

        if (isSaved) {
            iStep.next(isSaved);
            return;
        }

        Tr_infra_message.RequestData reqData = new Tr_infra_message.RequestData();
        Tr_login login = UserInfo.getLoginInfo();

        reqData.mber_sn = login.mber_sn;
        reqData.pageNumber = ""+mHealthMsgCnt;

        getData(SplashActivityMedicare.this, Tr_infra_message.class, reqData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_infra_message) {
                    Tr_infra_message recvData = (Tr_infra_message) obj;
                    int pageNum = StringUtil.getIntVal(recvData.pageNumber);
                    int pageMaxNum = StringUtil.getIntVal(recvData.maxpageNumber);

                    DBHelper helper = new DBHelper(SplashActivityMedicare.this);
                    DBHelperMessage db = helper.getMessageDb();
                    db.insert(recvData.message_list, true);

                    if (pageNum > pageMaxNum) {
                        getHealthMessageData(iStep);
                    } else {
                        SharedPref.getInstance().savePreferences(SharedPref.IS_SAVED_HEALTH_MESSAGE_DB, true);
                        iStep.next(obj);
                    }

                    mHealthMsgCnt++;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GoogleFitInstance.GOOGLE_PERMISSION_REQ_CODE) {
            // 구글 피트니스 인증 처리
            if (resultCode == Activity.RESULT_OK) {
                init();
            } else {
                CDialog.showDlg(SplashActivityMedicare.this, getString(R.string.permission_setting_access), new CDialog.DismissListener() {
                    @Override
                    public void onDissmiss() {
                        googleFitnessAuthCheck();
                    }
                });
            }

        } else {

        }
    }
}
