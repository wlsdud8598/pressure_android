package kr.co.hi.medicare.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.bean.ServiceFirstItem;

/**
 * 건강정보 리스트뷰 어댑터.
 */
public class HistoryFirstListView_Adapter extends BaseAdapter {

    private Activity mActivity;
    private ArrayList<ServiceFirstItem> mListData = new ArrayList<>();
    private int mType;

    public HistoryFirstListView_Adapter(Activity activity , ArrayList<ServiceFirstItem> item , int type) {
        mActivity = activity;
        mType = type;
        mListData = item;
    }

    @Override
    public int getCount() {
        return mListData.size();
    }

    @Override
    public Object getItem(int position) {
        return mListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        CLog.i("adapter ---> ");

        ServiceFirstItem mData = mListData.get(position);

        if (convertView == null) {
            holder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_servicehistory_first, null);

            holder.mTitleTv     = (TextView) convertView.findViewById(R.id.title_tv);
            holder.mResultTv1   = (TextView) convertView.findViewById(R.id.result_tv1);
            holder.mResultTv2   = (TextView) convertView.findViewById(R.id.result_tv2);
            holder.mResultTv3   = (TextView) convertView.findViewById(R.id.result_tv3);
            holder.mResultTv4   = (TextView) convertView.findViewById(R.id.result_tv4);
            holder.mVisibleView = (View)     convertView.findViewById(R.id.visible_view);
            holder.mVisibleLay  = (LinearLayout) convertView.findViewById(R.id.visible_lay);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        switch (mType){
            case 1:
                holder.mTitleTv.setText(mActivity.getString(R.string.service_history_title_2));
                holder.mVisibleLay.setVisibility(View.VISIBLE);
                holder.mVisibleView.setVisibility(View.VISIBLE);
                break;
            case 3:
                if (position == 0) {
                    holder.mTitleTv.setText(mActivity.getString(R.string.service_history_title_10));
                }else {
                    holder.mTitleTv.setText(mActivity.getString(R.string.service_history_title_11));
                }
                holder.mVisibleLay.setVisibility(View.GONE);
                holder.mVisibleView.setVisibility(View.GONE);
                break;
            case 4:
                holder.mTitleTv.setText(mActivity.getString(R.string.service_history_title_5));
                holder.mVisibleLay.setVisibility(View.GONE);
                holder.mVisibleView.setVisibility(View.GONE);
                break;
        }

        holder.mResultTv1.setText(CDateUtil.getDateTimeFormatyyyyMMddhhmm(mData.getmREQDATE()));
        holder.mResultTv2.setText(CDateUtil.getDateTimeFormatyyyyMMddhhmm(mData.getmUSEDATE()));
        holder.mResultTv3.setText(mData.getmSO_NAME());
        holder.mResultTv4.setText(mData.getmUSE_MEMO());

        return convertView;
    }

    public class ViewHolder {
        public TextView mTitleTv, mResultTv1, mResultTv2, mResultTv3, mResultTv4 ;
        public LinearLayout mVisibleLay;
        public View mVisibleView;
    }

}
