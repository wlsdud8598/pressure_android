package kr.co.hi.medicare.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.co.hi.medicare.R;

import java.util.ArrayList;

import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.bean.ServiceCarMoverItem;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.util.Util;

/**
 * 건강정보 리스트뷰 어댑터.
 */
public class HistoryStrokeMovercarListView_Adapter extends BaseAdapter {

    private Activity mActivity;
    private ArrayList<ServiceCarMoverItem> mListData = new ArrayList<>();

    public HistoryStrokeMovercarListView_Adapter(Activity activity, ArrayList<ServiceCarMoverItem> item) {
        mActivity = activity;
        mListData = item;
    }

    @Override
    public int getCount() {
        return mListData.size();
    }

    @Override
    public Object getItem(int position) {
        return mListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        CLog.i("adapter ---> ");

        ServiceCarMoverItem mData = mListData.get(position);

        if (convertView == null) {
            holder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_servicestrokehistory_movercar, null);

            holder.mTitleTv     = (TextView) convertView.findViewById(R.id.title_tv);
            holder.mResultTv1   = (TextView) convertView.findViewById(R.id.result_tv1);
            holder.mResultTv2   = (TextView) convertView.findViewById(R.id.result_tv2);
            holder.mResultTv3   = (TextView) convertView.findViewById(R.id.result_tv3);
            holder.mResultTv4   = (TextView) convertView.findViewById(R.id.result_tv4);
            holder.mBotLay      = (LinearLayout) convertView.findViewById(R.id.bot_lay);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mTitleTv.setText(mData.getmCOUNT() + "회");
        holder.mResultTv1.setText(CDateUtil.getFormatYYYYMMDD(mData.getmUSEDATE().replace("-","")));
        holder.mResultTv2.setText(mData.getmDEPART()+" - "+mData.getmARRIVAL()+" ("+mData.getmODIVISION()+")");
        holder.mResultTv3.setText(Util.Comma_won(Integer.valueOf(mData.getmDISTANCE()))+"km");
        holder.mResultTv4.setText(Util.Comma_won(Integer.valueOf(mData.getmSCOST()))+" 원");

        if (mListData.size()>1 && mListData.size() == position + 1){
            holder.mBotLay.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    public class ViewHolder {
        public TextView mTitleTv, mResultTv1, mResultTv2, mResultTv3, mResultTv4 ;
        public LinearLayout mBotLay;
    }

}
