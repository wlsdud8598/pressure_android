package kr.co.hi.medicare.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.co.hi.medicare.R;

import java.util.ArrayList;

import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.bean.ServiceRentItem;
import kr.co.hi.medicare.util.CLog;

/**
 * 건강정보 리스트뷰 어댑터.
 */
public class HistoryStrokeRentListView_Adapter extends BaseAdapter {

    private Activity mActivity;
    private ArrayList<ServiceRentItem> mListData = new ArrayList<>();

    public HistoryStrokeRentListView_Adapter(Activity activity, ArrayList<ServiceRentItem> item) {
        mActivity = activity;
        mListData = item;
    }

    @Override
    public int getCount() {
        return mListData.size();
    }

    @Override
    public Object getItem(int position) {
        return mListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        CLog.i("adapter ---> ");

        ServiceRentItem mData = mListData.get(position);

        if (convertView == null) {
            holder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_servicestrokehistory_rent, null);

            holder.mTitleTv     = (TextView) convertView.findViewById(R.id.title_tv);
//            holder.mTitleTv.setVisibility(View.GONE);
            holder.mResultTv1   = (TextView) convertView.findViewById(R.id.result_tv1);
            holder.mResultTv2   = (TextView) convertView.findViewById(R.id.result_tv2);
            holder.mResultTv3   = (TextView) convertView.findViewById(R.id.result_tv3);
            holder.mResultTv4   = (TextView) convertView.findViewById(R.id.result_tv4);
            holder.mResultTv5   = (TextView) convertView.findViewById(R.id.result_tv5);
            holder.mResultTv6   = (TextView) convertView.findViewById(R.id.result_tv6); // 실 사용일
            holder.mBotLay      = (LinearLayout) convertView.findViewById(R.id.bot_lay);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        holder.mTitleTv.setText(mData.getmSCOUNT() + "회");
        holder.mResultTv1.setText(CDateUtil.getFormatYYYYMMDD(mData.getmREQDATE().replace("-",""))); // 신청일자
        holder.mResultTv2.setText(CDateUtil.getFormatYYYYMMDD(mData.getmCONFDATE().replace("-",""))); // 발송일자
        holder.mResultTv3.setText(mData.getmGOOD()); // 제공제품
        holder.mResultTv4.setText(CDateUtil.getFormatYYYYMMDD(mData.getmENDDATE().replace("-",""))); // 반납 예정일
        holder.mResultTv5.setText(CDateUtil.getFormatYYYYMMDD(mData.getmRTNDATE().replace("-",""))); // 실 반납일
        holder.mResultTv6.setText(CDateUtil.getFormatYYYYMMDD(mData.getmUSEDATE().replace("-",""))); // 실 사용일 수정해야함

        if (mListData.size()>1 && mListData.size() == position + 1){
            holder.mBotLay.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    public class ViewHolder {
        public TextView mTitleTv, mResultTv1, mResultTv2, mResultTv3, mResultTv4, mResultTv5, mResultTv6 ;
        public LinearLayout mBotLay;
    }

}
