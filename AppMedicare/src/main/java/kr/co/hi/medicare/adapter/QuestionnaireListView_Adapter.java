package kr.co.hi.medicare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.fragment.health.question.QuestionnaireFragment;

/**
 * 질문 리스트뷰 어댑터.
 */
public class QuestionnaireListView_Adapter extends BaseAdapter {
    private UserInfo user = null;
    private QuestionnaireFragment mQuestionnaireActivity;

    public QuestionnaireListView_Adapter(QuestionnaireFragment activity) {
        mQuestionnaireActivity = activity;
        user = new UserInfo(activity.getContext());
    }

    @Override
    public int getCount() {
        return 9;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mQuestionnaireActivity.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_medi_questionnaire_listview, null);
            holder.item_question_TextView_title = (TextView) convertView.findViewById(R.id.item_question_TextView_title);
            holder.item_question_TextView_level = (TextView) convertView.findViewById(R.id.item_question_TextView_level);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        switch (position) {
            case 0:
                holder.item_question_TextView_title.setText(mQuestionnaireActivity.getContext().getString(R.string.question1));
                break;
            case 1:
                holder.item_question_TextView_title.setText(mQuestionnaireActivity.getContext().getString(R.string.question2));
                break;
            case 2:
                holder.item_question_TextView_title.setText(mQuestionnaireActivity.getContext().getString(R.string.question3));
                break;
            case 3:
                holder.item_question_TextView_title.setText(mQuestionnaireActivity.getContext().getString(R.string.question4));
                break;
            case 4:
                holder.item_question_TextView_title.setText(mQuestionnaireActivity.getContext().getString(R.string.question5));
                break;
            case 5:
                holder.item_question_TextView_title.setText(mQuestionnaireActivity.getContext().getString(R.string.question6));
                break;
            case 6:
                holder.item_question_TextView_title.setText(mQuestionnaireActivity.getContext().getString(R.string.question7));
                break;
            case 7:
                holder.item_question_TextView_title.setText(mQuestionnaireActivity.getContext().getString(R.string.question8));
                break;
            case 8:
                holder.item_question_TextView_title.setText(mQuestionnaireActivity.getContext().getString(R.string.question9));
                break;
//            case 9:
//                holder.item_question_TextView_title.setText(mActivity.getString(R.string.question10));
//                break;
        }
        holder.item_question_TextView_level.setText(getLevel(position + 1));
        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        user = new UserInfo(mQuestionnaireActivity.getContext());
    }

    public class ViewHolder {
        public TextView item_question_TextView_title, item_question_TextView_level;
    }


    private String getLevel(int no)
    {
        switch (user.getQuestionLevel(no))
        {
            case 0:
                return mQuestionnaireActivity.getContext().getString(R.string.diagnosis);
            case 1:
                return mQuestionnaireActivity.getContext().getString(R.string.answer_level1);
            case 2:
                return mQuestionnaireActivity.getContext().getString(R.string.answer_level2);
            case 3:
                return mQuestionnaireActivity.getContext().getString(R.string.answer_level3);
            case 4:
                return mQuestionnaireActivity.getContext().getString(R.string.answer_level4);
        }
        return mQuestionnaireActivity.getContext().getString(R.string.diagnosis);
    }
}
