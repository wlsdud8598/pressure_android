
package kr.co.hi.medicare.charting.charts;

import android.content.Context;
import android.util.AttributeSet;

import kr.co.hi.medicare.charting.data.PressureData;
import kr.co.hi.medicare.charting.interfaces.dataprovider.PresureDataProvider;
import kr.co.hi.medicare.charting.renderer.PressureChartRenderer;
import kr.co.hi.medicare.utilhw.ChartTimeUtil;

/**
 * Financial chart type that draws candle-sticks (OHCL chart).
 *
 * @author Philipp Jahoda
 */
public class PressureStickChart extends BarLineChartBase<PressureData> implements PresureDataProvider {

    private ChartTimeUtil timeClass;

    public PressureStickChart(Context context) {
        super(context);
    }

    public PressureStickChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PressureStickChart(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    private ChartTimeUtil mTimeClass;

    @Override
    protected void init() {
        super.init();

        mRenderer = new PressureChartRenderer(this, mAnimator, mViewPortHandler, getContext());

        getXAxis().setSpaceMin(0.5f);
        getXAxis().setSpaceMax(0.5f);

    }


    @Override
    public PressureData getPresureData() {
        return mData;
    }

    public void setTimeClass(ChartTimeUtil timeClass) {
        this.timeClass = timeClass;
        mRenderer.setTimeClass(timeClass);
    }
}
