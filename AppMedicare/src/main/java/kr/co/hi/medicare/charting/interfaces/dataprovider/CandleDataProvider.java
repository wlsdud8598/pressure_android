package kr.co.hi.medicare.charting.interfaces.dataprovider;

import kr.co.hi.medicare.charting.data.CandleData;

public interface CandleDataProvider extends BarLineScatterCandleBubbleDataProvider {

    CandleData getCandleData();
}
