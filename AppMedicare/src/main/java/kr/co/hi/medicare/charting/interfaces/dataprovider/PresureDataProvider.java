package kr.co.hi.medicare.charting.interfaces.dataprovider;

import kr.co.hi.medicare.charting.data.PressureData;

public interface PresureDataProvider extends BarLineScatterCandleBubbleDataProvider {

    PressureData getPresureData();
}
