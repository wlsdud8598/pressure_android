package kr.co.hi.medicare.charting.interfaces.dataprovider;

import kr.co.hi.medicare.charting.data.SticData;

public interface SticDataProvider extends BarLineScatterCandleBubbleDataProvider {

    SticData getCandleData();
}
