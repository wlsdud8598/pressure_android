package kr.co.hi.medicare.charting.interfaces.datasets;

import kr.co.hi.medicare.charting.data.CEntry;

/**
 * Created by philipp on 21/10/15.
 */
public interface IBarLineScatterCandleBubbleDataSet<T extends CEntry> extends IDataSet<T> {

    /**
     * Returns the color that is used for drawing the highlight indicators.
     *
     * @return
     */
    int getHighLightColor();
}
