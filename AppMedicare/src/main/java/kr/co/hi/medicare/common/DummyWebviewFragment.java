package kr.co.hi.medicare.common;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import kr.co.hi.medicare.utilhw.Logger;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;

/**
 * 질병 위험도 체크.
 */
public class DummyWebviewFragment extends BaseFragmentMedi {
    private final String TAG = DummyWebviewFragment.class.getSimpleName();

    public static final String TITLE     = "title";
    public static final String URL       = "url";
    public static final String Type       = "type";

    private WebView mWebview;
    private String titlePos = "";
    public static String sUrl = "";
    private String stype = "0";

    public static Fragment newInstance() {
        DummyWebviewFragment fragment = new DummyWebviewFragment();
        return fragment;
    }

    public static Fragment newInstance(String Url) {
        DummyWebviewFragment fragment = new DummyWebviewFragment();
        sUrl = Url;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dummy_webview, container, false);
//        mServiceBtn = (Button) view.findViewById(R.id.service_button);

        Bundle bundle = getArguments();
        if (bundle != null) {
            titlePos = bundle.getString(TITLE);
            sUrl = bundle.getString(URL);
            stype = bundle.getString(Type);

            ((CommonToolBar)view.findViewById(R.id.commonToolbar)).setTitle(titlePos);

            Logger.i(TAG, "DummyWebviewFragment.title="+titlePos);
            Logger.i(TAG, "DummyWebviewFragment.sUrl="+sUrl);
            Logger.i(TAG, "DummyWebviewFragment.stype="+stype);

        }

//        if("1".equals(stype))
//        {
//            mServiceBtn.setVisibility(View.VISIBLE);
//            mServiceBtn.setOnClickListener(mClickListener);
//        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (TextUtils.isEmpty(sUrl) == false)
            init(view);
    }

//    View.OnClickListener mClickListener = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            switch (v.getSaveId()) {
//                case R.id.service_button :
//                    IntentUtil.requestPhoneDialActivity(getContext(), getString(R.string.text_tel_no));
//                    break;
//            }
//
//        }
//    };

    /**
     *
     * @param view
     */
    private void init(View view) {

        mWebview = (WebView) view.findViewById(R.id.dummy_webview);

        WebSettings settings = mWebview.getSettings();
        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.setWebViewClient(new WebViewClient());
        settings.setDefaultTextEncodingName("utf-8");
        settings.setBuiltInZoomControls(true); // 줌 허용
        settings.setSupportZoom(true);
        settings.setDisplayZoomControls(false);
        settings.setUseWideViewPort(true);

        settings.setLoadWithOverviewMode(true);
//        mWebview.addJavascriptInterface(new AndroidBridge(), "HybridApp");
        mWebview.setWebViewClient(new WebViewClientClass());
        mWebview.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                new AlertDialog.Builder(getContext())
                        .setTitle("알림")
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok,
                                new AlertDialog.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        result.confirm();
                                    }
                                })
                        .setCancelable(false)
                        .create()
                        .show();
                return true;
            }
        });
        mWebview.loadUrl(sUrl);
    }

    private class WebViewClientClass extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if( url.startsWith("http:") || url.startsWith("https:") )
            {
                return false;
            }
            // tel일경우 아래와 같이 처리해준다.
            else if (url.startsWith("tel:"))
            {
                Intent tel = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                startActivity(tel);

//                IntentUtil.requestPhoneDialActivity(getContext(), tel);
                return true;
            }
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Logger.i(TAG, "onPageFinished.hideprogress");
            hideProgress();
            // progress 가 해지 안 될 경우 처리
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (getActivity() != null)
                        hideProgress();
                }
            }, 1 * 500);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Logger.i(TAG, "onPageStarted.showProgress");
            showProgress();
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            Logger.i(TAG, "onReceivedError.hideprogress");
            hideProgress();
            // progress 가 해지 안 될 경우 처리
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (getActivity() != null)
                        hideProgress();
                }
            }, 1 * 500);
        }

        @Override
        public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            super.onReceivedHttpError(view, request, errorResponse);
            Logger.i(TAG, "onReceivedHttpError.hideprogress");
            hideProgress();
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            Logger.i(TAG, "onReceivedSslError.hideprogress");
            hideProgress();
        }
    }

    @Override
    public void onBackPressed() {
        if (mWebview.canGoBack()) {
            mWebview.goBack();
        } else {
            super.onBackPressed();
        }

    }
}
