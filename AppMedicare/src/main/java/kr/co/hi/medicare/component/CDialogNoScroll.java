package kr.co.hi.medicare.component;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.utilhw.Logger;


/**
 * 공통 메시지 다이얼로그
 */
public class CDialogNoScroll extends Dialog {
    private static final String TAG = CDialogNoScroll.class.getSimpleName();
    private boolean mIsAutoDismiss = true;

    private LinearLayout mTitleLayout, parent_dialog_layout;
    private TextView mTitleView;
    private ImageView mIconImageView;
    private TextView mMessageView;
    private Button mNoButton;
    private View viewTerm;
    private Button mOkButton;
    private String mTitle;
    private String mMessage;
    private LinearLayout mContentLayout;
    private LinearLayout mBtnLayout;

    private View.OnClickListener mNoClickListener;
    private View.OnClickListener mOkClickListener;
    private static DismissListener mDismissListener;

    private static CDialogNoScroll instance;

    private static CDialogNoScroll getInstance(Context context) {
        instance = new CDialogNoScroll(context);
        initDialog(instance);
        return instance;
    }

    private static CDialogNoScroll getInstance(Activity activity) {
        instance = new CDialogNoScroll(activity);
        initDialog(instance);
        return instance;
    }

    private static void initDialog(CDialogNoScroll instance) {
        if (instance == null) {
            return;
        }

        if (instance.mOkButton != null) {
            instance.mOkButton.setVisibility(View.GONE);
            instance.mOkButton.setOnClickListener(null);
        }

        if (instance.mNoButton != null) {
            instance.mNoButton.setVisibility(View.GONE);
            instance.mNoButton.setOnClickListener(null);
            instance.viewTerm.setVisibility(View.GONE);
        }

        if (instance.mBtnLayout != null)
            instance.mBtnLayout.setVisibility(View.GONE);

        if (mDismissListener != null)
            mDismissListener = null;

        if (instance.mTitleView != null)
            instance.mTitleView.setText(instance.getContext().getString(R.string.text_alert));

        instance.setOnDismissListener(null);

        if (instance.isShowing() == false)
            instance.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);

        setContentView(R.layout.custom_dialog_no_scrollview);
        setLayout();
        setClickListener();
    }

    public CDialogNoScroll(Context context) {
        // Dialog 배경을 투명 처리 해준다.
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
    }

    public static CDialogNoScroll showDlg(Context context, String message) {
        final CDialogNoScroll dlg = getInstance(context);
        dlg.setMessage(message);
        dlg.mOkButton.setVisibility(View.VISIBLE);
        dlg.mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlg.dismiss();
            }
        });
        return dlg;
    }

    public static CDialogNoScroll showDlg(Context context, String message, boolean canelable) {
        final CDialogNoScroll dlg = getInstance(context);
        dlg.setMessage(message);
        dlg.mOkButton.setVisibility(View.VISIBLE);
        dlg.mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlg.dismiss();
            }
        });
        dlg.setCancelable(canelable);
        return dlg;
    }

    public static CDialogNoScroll LoginshowDlg(Context context, String title, String message) {
        CDialogNoScroll dlg = getInstance(context);
        dlg.setTitle(title);
        dlg.setMessage(message);

        return dlg;
    }

    public static CDialogNoScroll showDlg(Context context, String title, String message) {
        CDialogNoScroll dlg = getInstance(context);
        dlg.setTitle(title);
        dlg.setMessage(message);

        return dlg;
    }

    public static CDialogNoScroll showDlg(Context context, String message, View.OnClickListener okListener) {
        CDialogNoScroll dlg = getInstance(context);
        dlg.setMessage(message);
        dlg.setOkButton(okListener);

        return dlg;
    }

    public static CDialogNoScroll showDlg(Context context, String title, String message, View.OnClickListener okListener) {
        CDialogNoScroll dlg = getInstance(context);
        dlg.setTitle(title);
        dlg.setMessage(message);
        dlg.setOkButton(okListener);

        return dlg;
    }

    public static CDialogNoScroll showDlg(Activity activity, String message, final CDialogNoScroll.DismissListener dismissListener) {
        final CDialogNoScroll dlg = getInstance(activity);
        mDismissListener = dismissListener;
        dlg.setMessage(message);
        dlg.mOkButton.setVisibility(View.VISIBLE);
        dlg.mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlg.dismiss();
            }
        });

        return dlg;
    }

    public static CDialogNoScroll showDlg(Context context, String message, final CDialogNoScroll.DismissListener dismissListener) {
        final CDialogNoScroll dlg = getInstance(context);
        mDismissListener = dismissListener;
        dlg.setMessage(message);
        dlg.mOkButton.setVisibility(View.VISIBLE);
        dlg.mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlg.dismiss();
            }
        });

        return dlg;
    }

    public static CDialogNoScroll showDlg(Context context, String message, View.OnClickListener okListener, View.OnClickListener noListener) {
        CDialogNoScroll dlg = getInstance(context);
        dlg.setMessage(message);

        dlg.setOkButton(okListener);
        dlg.setNoButton(noListener);

        return dlg;
    }

    public static CDialogNoScroll CallshowDlg(Context context, String message, View.OnClickListener okListener, View.OnClickListener noListener) {
        CDialogNoScroll dlg = getInstance(context);
        dlg.setMessage(message);
        dlg.mOkButton.setText("고객센터\n연결하기");
        dlg.mNoButton.setText("확인");
        dlg.setOkButton(okListener);
        dlg.setNoButton(noListener);

        return dlg;
    }

    public static CDialogNoScroll UpdateshowDlg(Context context, String message, View.OnClickListener okListener, View.OnClickListener noListener) {
        CDialogNoScroll dlg = getInstance(context);
        dlg.setMessage(message);
        dlg.mOkButton.setText("확인");
        dlg.setOkButton(okListener);
        dlg.setNoButton(noListener);

        return dlg;
    }


    public static CDialogNoScroll showDlg(Context context, View view) {
        CDialogNoScroll dlg = getInstance(context);
        dlg.mContentLayout.removeAllViews();
        dlg.mContentLayout.addView(view);
        dlg.mContentLayout.setPadding(0,0,0,0);
        dlg.mOkButton.setVisibility(View.GONE);
        dlg.mBtnLayout.setVisibility(View.GONE);

        return dlg;
    }

    public static CDialogNoScroll showDlg(Context context, View view, LinearLayout.LayoutParams params) {
        CDialogNoScroll dlg = getInstance(context);
        dlg.mContentLayout.removeAllViews();
        dlg.mContentLayout.addView(view,params);
        dlg.mContentLayout.setPadding(0,0,0,0);
        dlg.mOkButton.setVisibility(View.GONE);
        dlg.mBtnLayout.setVisibility(View.GONE);

        return dlg;
    }

    public static CDialogNoScroll showDlg(Context context, View view, int minHeight, int minWidth, boolean autodismiss) {
        CDialogNoScroll dlg = getInstance(context);
        dlg.mContentLayout.removeAllViews();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(minWidth, minHeight);
        dlg.mContentLayout.addView(view,params);
        dlg.mContentLayout.setPadding(0,0,0,0);
        FrameLayout.LayoutParams params1 = (FrameLayout.LayoutParams) dlg.parent_dialog_layout.getLayoutParams();
        params1.width = minWidth;
        dlg.parent_dialog_layout.setLayoutParams(params1);

        dlg.mOkButton.setVisibility(View.GONE);
        dlg.mBtnLayout.setVisibility(View.GONE);

        dlg.mIsAutoDismiss = autodismiss;

        return dlg;
    }




    public static CDialogNoScroll showDlg(Context context, int layout) {
        View view = LayoutInflater.from(context).inflate(layout, null);
        CDialogNoScroll dlg = getInstance(context);
        dlg.mContentLayout.removeAllViews();
        dlg.mContentLayout.addView(view);
        dlg.mContentLayout.setPadding(0,0,0,0);
        dlg.mOkButton.setVisibility(View.GONE);
        dlg.mBtnLayout.setVisibility(View.GONE);

        return dlg;
    }

    public static CDialogNoScroll showDlg(Context context, View view, View.OnClickListener okListener, View.OnClickListener noListener) {
        CDialogNoScroll dlg = getInstance(context);
        dlg.setContentView(view);

        dlg.setOkButton(okListener);
        dlg.setNoButton(noListener);

        return dlg;
    }

    public static CDialogNoScroll showDlg(Context context, String title, String message, View.OnClickListener okListener, View.OnClickListener noListener) {
        CDialogNoScroll dlg = getInstance(context);

        dlg.setTitle(title);
        dlg.setMessage(message);
        dlg.setOkButton(okListener);
        dlg.setNoButton(noListener);

        return dlg;
    }

    /**
     * 타이틀 세팅
     * 타이틀이 없으면 타이틀 영역을 Gone 처리
     * @param title
     */
    public CDialogNoScroll setTitle(String title) {
        if (TextUtils.isEmpty(title)) {
            mTitleLayout.setVisibility(View.GONE);
            mTitleView.setVisibility(View.GONE);
        } else {
            mTitleLayout.setVisibility(View.VISIBLE);
            mTitleView.setVisibility(View.VISIBLE);
            mTitleView.setText(title);
        }
        return instance;
    }

    /**
     * 타이틀 세팅
     * 타이틀이 없으면 타이틀 영역을 Gone 처리
     * @param title
     */
    public void setTitle(String title,boolean food) {
        if (TextUtils.isEmpty(title)) {
            mTitleLayout.setVisibility(View.GONE);
        } else {
            mTitleLayout.setVisibility(View.VISIBLE);
            mTitleView.setText(title);

            if(food){
                mTitleView.setVisibility(View.VISIBLE);
                mTitleLayout.setBackgroundColor(getContext().getResources().getColor(R.color.x245_245_245));

            }
        }
    }

    public CDialogNoScroll setIconView(Drawable drawable) {
        mIconImageView.setImageDrawable(drawable);
        return instance;
    }

    public CDialogNoScroll setIconView(int src) {
        mIconImageView.setImageResource(src);
        return instance;
    }

    public CDialogNoScroll setMessage(String message) {
        mMessageView.setText(message);
        return instance;
    }

    public CDialogNoScroll setOneBtn(int src) {
        mOkButton.setBackgroundResource(src);
        mOkButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorWhite));
        return instance;
    }

    public CDialogNoScroll setClickListener() {   //final View.OnClickListener noClickListener, final View.OnClickListener okClickListener) {
        Logger.i("", "setClickListener=" + mNoButton);

        if (mNoClickListener == null) {
            mNoButton.setVisibility(View.GONE);
            viewTerm.setVisibility(View.GONE);
        }

        mNoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mNoClickListener != null) {
                    mNoClickListener.onClick(v);

                    if (mIsAutoDismiss) {
                        CDialogNoScroll.this.dismiss();
                    }
                } else {
                    CDialogNoScroll.this.dismiss();
                }
            }
        });

        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOkClickListener != null) {
                    mOkClickListener.onClick(v);

                    if (mIsAutoDismiss) {
                        CDialogNoScroll.this.dismiss();
                    }
                } else {
                    CDialogNoScroll.this.dismiss();
                }
            }
        });

        return instance;
    }

    /**
     * 왼쪽 버튼 세팅
     */
    public CDialogNoScroll setNoButton(View.OnClickListener noClickListener) {
        mBtnLayout.setVisibility(View.VISIBLE);
        mNoButton.setVisibility(View.VISIBLE);
        viewTerm.setVisibility(View.VISIBLE);
        setAlertButtonClickListener(mNoButton, noClickListener);

        mOkButton.setBackgroundResource(R.drawable.draw_alert_ok_btn);
        mOkButton.setTextColor(ContextCompat.getColor(getContext(), R.color.colorWhite));
        return instance;
    }

    public CDialogNoScroll setNoButton(String label, View.OnClickListener noClickListener) {
//        mBtnLayout.setVisibility(View.VISIBLE);
//        mNoButton.setVisibility(View.VISIBLE);
//        viewTerm.setVisibility(View.VISIBLE);
        mNoButton.setText(label);
//        setAlertButtonClickListener(mNoButton, noClickListener);
        return setNoButton(noClickListener);
    }

    /**
     * 오른쪽 버튼 세팅
     *
     * @param okClickListener
     */
    public CDialogNoScroll setOkButton(View.OnClickListener okClickListener) {
        mBtnLayout.setVisibility(View.VISIBLE);
        String label = mOkButton.getText().toString();
        setOkButton(label, okClickListener);
        return instance;
    }

    public CDialogNoScroll setOkButton(String label, final View.OnClickListener okClickListener) {
        this.mOkClickListener = okClickListener;
        mBtnLayout.setVisibility(View.VISIBLE);
        mOkButton.setVisibility(View.VISIBLE);
        mOkButton.setText(label);
        setAlertButtonClickListener(mOkButton, okClickListener);
        return instance;
    }

    public void setOkButtonDissmissListenr(Button button, View.OnClickListener clickListener) {
        this.mOkClickListener = clickListener;
    }

    public void setNoButtonDissmissListenr(Button button, View.OnClickListener clickListener) {
        button.hasOnClickListeners();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (mDismissListener != null)
            mDismissListener.onDissmiss();
    }

    public interface DismissListener {
        void onDissmiss();
    }

    private void setAlertButtonClickListener(Button button, final View.OnClickListener clickListener) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null) {
                    clickListener.onClick(v);

                    if (mIsAutoDismiss) {
                        CDialogNoScroll.this.dismiss();
                    }
                } else {
                    CDialogNoScroll.this.dismiss();
                }
            }
        });
    }

    /*
     * Layout
     */
    public void setLayout() {
        mTitleLayout = findViewById(R.id.dialog_title_layout);
        mTitleView = (TextView) findViewById(R.id.dialog_title);
        mIconImageView = (ImageView) findViewById(R.id.dialog_icon_iv);
        mMessageView = (TextView) findViewById(R.id.dialog_content_tv);
        mNoButton = (Button) findViewById(R.id.dialog_btn_no);
        viewTerm = (View) findViewById(R.id.view_term);
        mOkButton = (Button) findViewById(R.id.dialog_btn_ok);
        mContentLayout = (LinearLayout) findViewById(R.id.dialog_content_layout);
        mBtnLayout = (LinearLayout) findViewById(R.id.dialog_btn_layout);
        parent_dialog_layout = findViewById(R.id.parent_dialog_layout);
    }
}