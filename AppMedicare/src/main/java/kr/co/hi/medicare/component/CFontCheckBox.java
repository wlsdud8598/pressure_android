package kr.co.hi.medicare.component;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.widget.CheckBox;

import kr.co.hi.medicare.R;

/**
 * Created by mrsohn on2017. 3. 21..
 */
public class CFontCheckBox extends CheckBox {

    public CFontCheckBox(Context context) {
        super(context);
    }

    public CFontCheckBox(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CFontCheckBox(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void init(AttributeSet attrs) {
        Typeface tf = ResourcesCompat.getFont(getContext(), R.font.notosanskr_regular);
        setTypeface(tf);
//        TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(
//                attrs,
//                R.styleable.TextViewWithFont,
//                0, 0);
//
//        String typeface = typedArray.getString(R.styleable.TextViewWithFont_font);

//        if (typeface != null) {
//            try {
//                Typeface tf = Typeface.createFromAsset(getContext().getAssets(), typeface);
//                setTypeface(tf);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        } else {
//            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), getContext().getString(R.string.notosanskr_regular));
//            setTypeface(tf);
//        }
    }

}