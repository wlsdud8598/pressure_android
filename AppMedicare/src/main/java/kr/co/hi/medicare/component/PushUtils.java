package kr.co.hi.medicare.component;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.utilhw.Logger;


/**
 * 공통 메시지 다이얼로그
 */
public class PushUtils{
    private static final String TAG = PushUtils.class.getSimpleName();

    private static PowerManager.WakeLock mWakeLock;

    public static void acquireWakeLock(Context context){
        Log.i(TAG, "acquireWakeLock : "+mWakeLock);

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(
                PowerManager.FULL_WAKE_LOCK |
                        PowerManager.ACQUIRE_CAUSES_WAKEUP |
                        PowerManager.ON_AFTER_RELEASE, "WAKEUP"
        );
        mWakeLock.acquire();
        Log.i(TAG, "mWakeLock mWakeLock : "+mWakeLock);
    }

    public static void releaseWakeLock(){
        if(mWakeLock != null){
            mWakeLock.release();
            mWakeLock = null;
        }
        Log.i(TAG, "mWakeLock : "+mWakeLock);
    }

}