package kr.co.hi.medicare.component;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.sample.SampleFragmentMedi;


/**
 * Created by MrsWin on 2017-02-16.
 */

public class TabMenuBaseFragment extends BaseFragmentMedi implements TabLayout.OnTabSelectedListener {
    private final String TAG = TabMenuBaseFragmentImpl.class.getSimpleName();

    private boolean mIsInit = false;

    public TabLayout mTabLayout;
    private List<TabObject> mTabObjList = new ArrayList<>();

//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
//        getToolBar().setVisibility(View.GONE);
//    }

//    public static BaseFragment newInstance() {
//        TabMenuBaseFragment fragment = new TabMenuBaseFragment();
//        return fragment;
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_menu_3deps, container, false);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // XXX 백으로 데이터 보내기
        Bundle bundle = new Bundle();
        bundle.putString(SampleFragmentMedi.SAMPLE_BACK_DATA, TAG + " BackData!!!");
        setBackData(bundle);

        mTabLayout = view.findViewById(R.id.tabs_3dpes);
        mTabLayout.addOnTabSelectedListener(this);
//        selectTab(0);
    }

    public void addTab(String tabName, Fragment fragment) {
        mTabObjList.add(new TabMenuBaseFragment.TabObject(tabName, fragment));
    }

    public void init() {
        if (mTabLayout.getTabCount() == 0) {
            for (TabObject tabObj : mTabObjList) {
                mTabLayout.addTab(mTabLayout.newTab().setText(tabObj.tabName));
            }
        }

        TabObject obj = mTabObjList.get(0);
        setChildFragment(obj.fragment);
    }

    private void setChildFragment(Fragment child) {
        FragmentTransaction childFt = getChildFragmentManager().beginTransaction();

        if (!child.isAdded()) {
            childFt.replace(R.id.child_fragment_layout, child);
            childFt.addToBackStack(null);
            childFt.commit();
        }
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int idx = tab.getPosition();
        if (mTabObjList.size() > idx) {
            TabObject obj = mTabObjList.get(idx);
            setChildFragment(obj.fragment);
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }

    public static class TabObject {
        public String tabName;
        public Fragment fragment;

        public TabObject(String tabName, Fragment fragment) {
            this.tabName = tabName;
            this.fragment = fragment;
        }
    }

//    /**
//     * 탭메뉴 인디케이터 margin 사이즈 구하기
//     * @param tabLayout
//     */
//    public void reduceMarginsInTabs(TabLayout tabLayout) {
//        Display display = getActivity().getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int width = size.x;
//
//        int marginOffset = width / mTabLayout.getTabCount();
//        marginOffset = marginOffset / 7; // 탭 메뉴 공백 사이즈 ( 1/7 사이즈)
//
//        View tabStrip = tabLayout.getChildAt(0);
//        if (tabStrip instanceof ViewGroup) {
//            ViewGroup tabStripGroup = (ViewGroup) tabStrip;
//            for (int i = 0; i < ((ViewGroup) tabStrip).getChildCount(); i++) {
//                View tabView = tabStripGroup.getChildAt(i);
//                if (tabView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
//                    ((ViewGroup.MarginLayoutParams) tabView.getLayoutParams()).leftMargin = marginOffset;
//                    ((ViewGroup.MarginLayoutParams) tabView.getLayoutParams()).rightMargin = marginOffset;
//                }
//            }
//
//            tabLayout.requestLayout();
//        }
//    }
}