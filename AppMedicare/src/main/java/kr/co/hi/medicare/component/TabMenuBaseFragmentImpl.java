package kr.co.hi.medicare.component;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.mypage.MyPageFragmentMyInfo;
import kr.co.hi.medicare.fragment.mypage.MyServiceFragment;


/**
 * Created by MrsWin on 2017-02-16.
 */

public class TabMenuBaseFragmentImpl extends TabMenuBaseFragment {
    private final String TAG = TabMenuBaseFragmentImpl.class.getSimpleName();

    public static BaseFragmentMedi newInstance() {
        TabMenuBaseFragmentImpl fragment = new TabMenuBaseFragmentImpl();
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        super.addTab(getString(R.string.my_menu_1), MyPageFragmentMyInfo.newInstance());
        super.addTab(getString(R.string.my_menu_2), MyServiceFragment.newInstance());
        super.init();
    }
}