package kr.co.hi.medicare.customview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import kr.co.hi.medicare.R;


public class ClearEditText extends AppCompatEditText implements  View.OnTouchListener, View.OnFocusChangeListener {
    private Drawable clearDrawable,searchDrawable;
    private OnFocusChangeListener onFocusChangeListener;
    private OnTouchListener onTouchListener;

    public ClearEditText(final Context context) {
        super(context);
        init();
    }

    public ClearEditText(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ClearEditText(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    public void setOnFocusChangeListener(OnFocusChangeListener onFocusChangeListener) {
        this.onFocusChangeListener = onFocusChangeListener;
    }

    @Override
    public void setOnTouchListener(OnTouchListener onTouchListener) {
        this.onTouchListener = onTouchListener;
    }

    private void init() {
        Drawable tempDrawable = ContextCompat.getDrawable(getContext(), R.drawable.commu_btn_22);
        clearDrawable = DrawableCompat.wrap(tempDrawable);
        DrawableCompat.setTintList(clearDrawable,null);
        clearDrawable.setBounds(0, 0, 40, 40);

        searchDrawable = DrawableCompat.wrap(ContextCompat.getDrawable(getContext(), R.drawable.commu_btn_20));
        DrawableCompat.setTintList(searchDrawable, null);
        searchDrawable.setBounds(0, 0, 60, 41);

        setClearIconVisible(false);
        super.setOnTouchListener(this);
        super.setOnFocusChangeListener(this);
    }

    @Override
    public void onFocusChange(final View view, final boolean hasFocus) {
        if (hasFocus) {
            setClearIconVisible(getText().length() > 0);
        } else {
            setClearIconVisible(false);
        }
        if (onFocusChangeListener != null) {
            onFocusChangeListener.onFocusChange(view, hasFocus);
        }
    }

    @Override
    public boolean onTouch(final View view, final MotionEvent motionEvent) {
        final int x = (int) motionEvent.getX();
        if (clearDrawable.isVisible() && x > getWidth() - getPaddingRight() - clearDrawable.getIntrinsicWidth()) {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                setError(null);
                setText(null);
            }
            return true;
        }
        if (onTouchListener != null) {
            return onTouchListener.onTouch(view, motionEvent);
        } else {
            return false;
        }
    }

    @Override
    public final void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
        if (isFocused()) {
            setClearIconVisible(s.length() > 0);
        }
    }



    private void setClearIconVisible(boolean visible) {

        if(visible){
            searchDrawable.setVisible(false, false);
            setCompoundDrawables(null, null,  null    , null);

            clearDrawable.setVisible(true, false);
            setCompoundDrawables(null, null,  clearDrawable    , null);
        }else{

            clearDrawable.setVisible(false, false);
            setCompoundDrawables(null, null,  null    , null);

            searchDrawable.setVisible(true, false);
            setCompoundDrawables(null, null,  searchDrawable    , null);
        }







//        clearDrawable.setVisible(true, false);
//        Drawable tempDrawable = null;
//
//        if(visible){
//            tempDrawable = ContextCompat.getDrawable(getContext(), R.drawable.commu_btn_22);
//        }else{
//            tempDrawable = ContextCompat.getDrawable(getContext(), R.drawable.commu_btn_20);
//        }
//        clearDrawable = DrawableCompat.wrap(tempDrawable);
//        clearDrawable.setVisible(true, false);
//        setCompoundDrawables(null, null, clearDrawable  , null);
    }
}

