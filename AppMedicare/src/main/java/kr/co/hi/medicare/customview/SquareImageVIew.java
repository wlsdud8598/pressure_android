package kr.co.hi.medicare.customview;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class SquareImageVIew extends android.support.v7.widget.AppCompatImageView {

    public SquareImageVIew(Context context) {
        super(context);
    }

    public SquareImageVIew(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageVIew(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth();
        setMeasuredDimension(width, width);
    }
}
