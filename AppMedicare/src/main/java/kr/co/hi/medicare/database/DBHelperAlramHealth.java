package kr.co.hi.medicare.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import kr.co.hi.medicare.utilhw.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import kr.co.hi.medicare.fragment.mypage.MyPageHealthNoticeData;

/**
 * Created by mrsohn on 2017. 3. 20..
 */

public class DBHelperAlramHealth {
    private final String TAG = DBHelperAlramHealth.class.getSimpleName();

    private DBHelper mHelper;
    public DBHelperAlramHealth(DBHelper helper) {
        mHelper = helper;
    }


    public static final String TB_DATA_COMM_ALRAMHEALTH = "tb_data_comm_alramhealth";
    public static String ALRAMHEALTH_IDX = "idx";//고유번호
    public static String ALRAMHEALTH_REGDATE = "regdate";//등록일
    public static String ALRAMHEALTH_MESSAGE = "message";//서버 등록 여부
    public static String ALRAMHEALTH_TIMEHHMM = "time_hhmm";
    public static String ALRAMHEALTH_GUBUN = "gubun";
    public static String ALRAMHEALTH_ISACTIVE = "isactive";

    public static final String SQL_INSERT_ALRAMHEALTH="INSERT INTO "+TB_DATA_COMM_ALRAMHEALTH+" ("+ALRAMHEALTH_REGDATE+", "+ALRAMHEALTH_MESSAGE+", "+ALRAMHEALTH_TIMEHHMM+","+ALRAMHEALTH_GUBUN+") VALUES ( datetime('now','localtime'), \'%s\', \'%s\', \'%s\')";
    public static final String SQL_SELECT_ALRAMHEALTH_ALL = "SELECT * FROM "+TB_DATA_COMM_ALRAMHEALTH+" ORDER BY idx DESC";
    public static final String SQL_DELETE_ALRAMHEALTH = "DELETE FROM "+TB_DATA_COMM_ALRAMHEALTH+" WHERE idx=%d";
    public static final String SQL_DELETE_ALRAMHEALTH_ALL = "DELETE FROM "+TB_DATA_COMM_ALRAMHEALTH+"";
    public static final String SQL_SELECT_ALRAMHEALTH_DUPLICATE = "SELECT * FROM "+TB_DATA_COMM_ALRAMHEALTH+" WHERE gubun=\'%s\' and time_hhmm=\'%s\'";
    public static final String SQL_SELECT_ALRAMHEALTH_IDX = "SELECT * FROM "+TB_DATA_COMM_ALRAMHEALTH+" WHERE idx=%d";
    public static final String SQL_UPDATE_ALRAMHEALTH_ISACTIVE = "UPDATE "+TB_DATA_COMM_ALRAMHEALTH+" SET isactive=\'%s\' where idx=%d";

    // DB를 새로 생성할 때 호출되는 함수
    public String createDb() {
        // 새로운 테이블 생성
        String sql = "CREATE TABLE if not exists "+TB_DATA_COMM_ALRAMHEALTH+" ("
                            + ALRAMHEALTH_IDX+" INTEGER PRIMARY KEY AUTOINCREMENT, "
                            + ALRAMHEALTH_MESSAGE+" TEXT, "
                            + ALRAMHEALTH_GUBUN+" TEXT, "
                            + ALRAMHEALTH_REGDATE+" TEXT, "
                            + ALRAMHEALTH_ISACTIVE+" TEXT DEFAULT 'Y', "
                            + ALRAMHEALTH_TIMEHHMM+" TEXT "
                            +"); ";
        Logger.i(TAG, "onCreate.sql="+sql);
        return sql;
    }

    public boolean deleteDb(int idx){
        String sql = String.format(Locale.US,SQL_DELETE_ALRAMHEALTH,idx);
        return mHelper.transactionExcuteB(sql);
    }

    public boolean deleteDbAll(){
        return mHelper.transactionExcuteB(SQL_DELETE_ALRAMHEALTH_ALL);
    }


    public boolean isDuplicate(String gubun,String time_hhmm) {
        boolean isNew = false;
        SQLiteDatabase db = mHelper.getReadableDatabase();
        Cursor cursor = null;
        try{
            cursor = db.rawQuery(String.format(Locale.US,SQL_SELECT_ALRAMHEALTH_DUPLICATE,gubun,time_hhmm),null);
            cursor.moveToFirst();

            if(cursor.getCount()>0){
                isNew=true;
            }else{
                isNew=false;
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally{
            cursor.close();
        }

        return isNew;
    }


    public int selectIdx(String gubun,String time_hhmm) {
        int idx = 0;
        SQLiteDatabase db = mHelper.getReadableDatabase();
        Cursor cursor = null;
        try{
            cursor = db.rawQuery(String.format(Locale.US,SQL_SELECT_ALRAMHEALTH_DUPLICATE,gubun,time_hhmm),null);

            if(cursor.getCount()>0){
                cursor.moveToFirst();
                idx = cursor.getInt(cursor.getColumnIndex(ALRAMHEALTH_IDX));
            }else{
                idx=0;
            }


        }catch (Exception e){
            e.printStackTrace();
        }finally{
            cursor.close();
        }

        return idx;
    }


    public MyPageHealthNoticeData select(int idx) {
        MyPageHealthNoticeData data = new MyPageHealthNoticeData();
        SQLiteDatabase db = mHelper.getReadableDatabase();
        Cursor cursor = null;
        try{
            cursor = db.rawQuery(String.format(Locale.US,SQL_SELECT_ALRAMHEALTH_IDX,idx),null);
            if(cursor.getCount()==1){
                cursor.moveToFirst();

                data.idx = cursor.getInt(cursor.getColumnIndex(ALRAMHEALTH_IDX));
                data.message = cursor.getString(cursor.getColumnIndex(ALRAMHEALTH_MESSAGE));
                data.gubun = cursor.getString(cursor.getColumnIndex(ALRAMHEALTH_GUBUN));
                data.time_hhmm = cursor.getString(cursor.getColumnIndex(ALRAMHEALTH_TIMEHHMM));
                data.regdate = cursor.getString(cursor.getColumnIndex(ALRAMHEALTH_REGDATE));
                data.isactive = cursor.getString(cursor.getColumnIndex(ALRAMHEALTH_ISACTIVE));
            }else{
                data.idx=0;
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally{
            cursor.close();
        }

        return data;
    }




    public boolean insert(String message,String gubun,String time_hhmm) {
        boolean isSuccess=false;

        try{
            List<String> sql = new ArrayList<>();
            sql.add(String.format(Locale.US, SQL_INSERT_ALRAMHEALTH, message,time_hhmm,gubun)); // 추가

            if(mHelper.transactionExcute(sql))
                isSuccess=true;

        }catch (Exception e){
            isSuccess=false;
        }

        return isSuccess;
    }

    public boolean update(String isActive,int idx){
        boolean isSuccess=false;

        try{
            isSuccess = mHelper.transactionExcuteB(String.format(Locale.US, SQL_UPDATE_ALRAMHEALTH_ISACTIVE, isActive,idx));
        }catch (Exception e){

        }

        return isSuccess;
    }


    public List<MyPageHealthNoticeData> getResultAll(DBHelper helper) {
        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = helper.getReadableDatabase();
        List<MyPageHealthNoticeData> data_list = new ArrayList<>();
        Cursor cursor = db.rawQuery(SQL_SELECT_ALRAMHEALTH_ALL, null);
        try {
            cursor.moveToFirst();
            do{
                MyPageHealthNoticeData data = new MyPageHealthNoticeData();
                data.idx = cursor.getInt(cursor.getColumnIndex(ALRAMHEALTH_IDX));
                data.regdate = cursor.getString(cursor.getColumnIndex(ALRAMHEALTH_REGDATE));
                data.message = cursor.getString(cursor.getColumnIndex(ALRAMHEALTH_MESSAGE));
                data.gubun = cursor.getString(cursor.getColumnIndex(ALRAMHEALTH_GUBUN));
                data.time_hhmm = cursor.getString(cursor.getColumnIndex(ALRAMHEALTH_TIMEHHMM));
                data.isactive = cursor.getString(cursor.getColumnIndex(ALRAMHEALTH_ISACTIVE));
                data_list.add(data);
            }while(cursor.moveToNext());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }

        return data_list;
    }

    public String dropTable() {
        String sql = "DROP TABLE IF EXISTS " + TB_DATA_COMM_ALRAMHEALTH + ";";
        return sql;
    }

}