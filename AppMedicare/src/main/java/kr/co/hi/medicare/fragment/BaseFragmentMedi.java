package kr.co.hi.medicare.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLConnection;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.activity.BaseActivityMedicare;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperWeight;
import kr.co.hi.medicare.fragment.health.HealthFragment;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_misson_goal_alert;
import kr.co.hi.medicare.net.hwNet.ApiData;
import kr.co.hi.medicare.net.hwNet.BaseData;
import kr.co.hi.medicare.net.hwNet.CConnAsyncTask;
import kr.co.hi.medicare.net.hwdata.Tr_get_infomation;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.util.permission.IPermissionResult;
import kr.co.hi.medicare.util.permission.PermissionUtil;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.NetworkUtil;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;
import kr.co.hi.medicare.value.Define;

/**
 * Created by MrsWin on 2017-02-16.
 */

public class BaseFragmentMedi extends Fragment implements IBaseFragment {
    protected final String TAG = getClass().getSimpleName();

    private final int REQUEST_PERMISSIONS_REQUEST_CODE = 33;
    private int mRequestCode = 1111;

    private IPermission mIpermission = null;

    boolean mIsLogin = false;
    public boolean doHideKeyPad=true;

    private static BaseActivityMedicare mBaseActivity;
//    private CommonActionBar mActionBar;

    public static Fragment newInstance(BaseActivityMedicare activity) {
        BaseFragmentMedi fragment = new BaseFragmentMedi();
        mBaseActivity = activity;
        return fragment;
    }

    public void replaceFragment(Fragment fragment) {
        replaceFragment(fragment, null);
    }

    public void replaceFragment(Fragment fragment, Bundle bundle) {
//        mActionBar = mBaseActivity.getCommonActionBar();
        mBaseActivity.replaceFragment(fragment, bundle);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (this instanceof NewActivity.onKeyBackPressedListener) {
            ((NewActivity)context).setOnKeyBackPressedListener((NewActivity.onKeyBackPressedListener)this);
        }
    }

    /**
     * 사용할 레이아웃 또는 View 지정
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        onViewCreated(view, savedInstanceState);
        return view;
    }

    /**
     * 뷰가 생성된 후 세팅
     *
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        CommonToolBar toolBar = getToolBar(view);

        if (toolBar != null) {
            Bundle bundle = getArguments();
            if (bundle != null) {
                String title = getArguments().getString(CommonToolBar.TOOL_BAR_TITLE);
                if (TextUtils.isEmpty(title) == false)
                    toolBar.setTitle(title);

                Logger.i(TAG, "loadActionbar.title=" + title);
            }
        }

//        doAutoLogin();
    }

    @Override
    public void onBackPressed() {
        Logger.i(TAG, "BaseFragment.onBackPressed().getActivity()=" + getActivity());
//        if (getActivity() instanceof MainActivityMedicare) {
//            MainActivityMedicare activity = (MainActivityMedicare) getActivity();
//            activity.superBackPressed();
////        } else if (getActivity() instanceof BaseActivity) {
////            BaseActivity activity = (BaseActivity) getActivity();
////            activity.onBackPressed();
//        } else {
            mBaseActivity.onBackPressed();
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "BaseFragmentMedi onResume()");
        SharedPref.getInstance().initContext(getActivity());

        //이전 페이지나 해당 페이지로 이동시에 키보드가 올라와 있으면 내림 2019-04-04 park add
        // 장시간 stop 상태에서 보는 경우 NullPointException 발생
//        if (getActivity() != null && getActivity().getWindow() != null&&doHideKeyPad) {
//            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//        }else{
//            doHideKeyPad=true;
//        }
//        if (getActivity() instanceof BaseActivity)
//            loadActionbar(((BaseActivity)getActivity()).getCommonActionBar((BaseActivity) getActivity()));
//        doAutoLogin();
    }

    /**
     * 자동로그인 처리
     */
//    private void doAutoLogin() {
//        boolean isAutoLogin = SharedPref.getInstance().getPreferences(SharedPref.IS_AUTO_LOGIN, false);
//        Tr_login loginData = UserInfo.getLoginInfo();
//        if (isAutoLogin && loginData == null) {
//            Tr_login.RequestData requestData = new Tr_login.RequestData();
//            UserInfo userInfo = new UserInfo(getContext());
//            requestData.mber_id = userInfo.getSaveId();//SharedPref.getInstance().getPreferences(SharedPref.SAVED_LOGIN_ID);
//            requestData.mber_pwd = userInfo.getSavedPw();//SharedPref.getInstance().getPreferences(SharedPref.SAVED_LOGIN_PWD);
////            requestData.phone_model = DeviceUtil.getPhoneModelName();
////            requestData.pushk = "";
////            requestData.app_ver = PackageUtil.getVersionInfo(getContext());
//
//            getData(getContext(), Tr_login.class, requestData, new ApiData.IStep() {
//                @Override
//                public void next(Object obj) {
//                    if (obj instanceof Tr_login) {
//                        Tr_login data = (Tr_login) obj;
//                        if ("Y".equals(data.log_yn)) {
//
//                            if(data.mber_bdwgh_app.equals("") || data.mber_bdwgh_app.isEmpty()){
//                                data.mber_bdwgh_app = data.mber_bdwgh;
//                            }
//
////                            Define.getInstance().setLoginInfo(data);
//                        } else {
//                            CDialog.showDlg(getContext(), "로그인에 실패 하였습니다.", new CDialog.DismissListener() {
//                                @Override
//                                public void onDissmiss() {
//                                    getActivity().finish();
//                                }
//                            });
//                        }
//                    }
//                }
//            });
//        }
//    }

    protected void onCreateOptionsMenu(Menu menu) {
        mBaseActivity.onCreateOptionsMenu(menu);
    }

//    private ActionBar getActionBar() {
//        return mBaseActivity.getSupportActionBar();
//    }
    private CommonToolBar mToolBar;
    protected CommonToolBar getToolBar(View view) {
        mToolBar = view.findViewById(R.id.medi_common_toolbar);
        return mToolBar;
    }

    protected void setTitle(int title) {
        if (mToolBar != null)
            mToolBar.setTitle(title);
    }

    protected void setTitle(String title) {
        if (mToolBar != null)
            mToolBar.setTitle(title);
    }

    protected void setVisibleToolBar(int visible) {
        mToolBar.setVisibility(visible);
    }
//
//    public CommonActionBar getCommonActionBar() {
//        return mBaseActivity.getCommonActionBar((BaseActivity) getContext());
//    }

    /**
     * Back 이동시 데이터 전달
     */
    private static Bundle mBackDataBundle;

    protected static void setBackData(Bundle bundle) {
        mBackDataBundle = bundle;
    }

    public static Bundle getBackData() {
        Bundle bundle = mBackDataBundle;
        if (mBackDataBundle != null) {
            mBackDataBundle = new Bundle(); // 초기화
            return bundle;
        } else {
            return new Bundle();
        }
    }

    /**
     * 현재 보여지고 있는 Fragment
     *
     * @return
     */
    public Fragment getVisibleFragment() {
        for (Fragment fragment : getActivity().getSupportFragmentManager().getFragments()) {
            if (fragment != null && fragment.isVisible()) {
                return fragment;
            }
        }
        return null;
    }

    public void showProgress() {
        if (getActivity() instanceof BaseActivityMedicare)
            ((BaseActivityMedicare)getActivity()).showProgress();
    }

    public void hideProgress() {
        if (getActivity() instanceof BaseActivityMedicare)
            ((BaseActivityMedicare)getActivity()).hideProgress();
    }

//    /**
//     * @param type           // 액션바 타입
//     * @param title          // 타이틀
//     * @param clickListener1 // 맨 오른쪽 버튼 처리
//     * @param clickListener2 // 맨 오른쪽 옆 버튼 처리
//     */
//    public void setActionBar(Define.ACTION_BAR type, String title, View.OnClickListener clickListener1, View.OnClickListener clickListener2) {
//        getCommonActionBar().setActionBar(type, title, clickListener1, clickListener2);
//    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent, null);
    }

    @Override
    public void startActivity(Intent intent, @Nullable Bundle options) {
        super.startActivity(intent, options);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode, null);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode, @Nullable Bundle options) {
        super.startActivityForResult(intent, requestCode, options);
    }

    public void startActivityForResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
//        if (getActivity() instanceof BaseActivityMedicare) {
//            actionBar.setActionBackBtnVisible(View.VISIBLE);
//            actionBar.goneActionBarFunctionBtn();
////            actionBar.showActionBar(true);            // 액션바 띄울지 여부
//
//            actionBar.setActionBarTitle("");
//            Bundle bundle = getArguments();
//            if (bundle != null) {
//                String title = getArguments().getString(CommonToolBar.TOOL_BAR_TITLE);
//                if (TextUtils.isEmpty(title) == false)
//                    actionBar.setActionBarTitle(title);
//
//                Logger.i(TAG, "loadActionbar.title=" + title);
//            }
//        }
//    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.i(TAG, TAG + ".onActivityResult");
    }

    /**
     * 로드벨런싱
     *
     * @param context
     * @param cls
     * @param obj
     * @param step
     */
    private void getInformation(final Context context, final Class<? extends BaseData> cls, final Object obj, final ApiData.IStep step) {
        Tr_get_infomation.RequestData reqData = new Tr_get_infomation.RequestData();
        getData(context, Tr_get_infomation.class, reqData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_get_infomation) {
                    Tr_get_infomation data = (Tr_get_infomation) obj;
                    Define.getInstance().setInformation(data);

                    getData(context, cls, obj, step);
                } else {
//                    CDialog.showDlg(context, "네트워크 연결 상태를 확인해주세요.");
                }
            }
        });
    }

    public void getData(final Context context, final Class<? extends BaseData> cls, final Object obj, final ApiData.IStep step) {
        getData(context, cls, obj, true, step, null);
    }

    public void getData(final Context context, final Class<? extends BaseData> cls, final Object obj, final boolean isShowProgress, final ApiData.IStep step, final ApiData.IFailStep failStep) {
        if (isShowProgress)
            showProgress();

        MediNewNetworkModule.doApi(getActivity(), cls, obj, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (isShowProgress)
                    hideProgress();
                step.next(responseData);
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                if (isShowProgress)
                    hideProgress();

                error.printStackTrace();
                if (failStep != null)
                    failStep.fail();

            }
        });
        //        if (context == null) {
//            // 앱을 빠르게 취소 하고 재 시작 시 오류 발생 될 때 있음
//            Logger.e(TAG , "getData context is null");
//            return;
//        }
//        if (NetworkUtil.getConnectivityStatus(context) == false) {
//            CDialog.showDlg(context, "네트워크 연결 상태를 확인해주세요.");
//            return;
//        }
////        String url = "http://wkd.walkie.co.kr/SK/WebService/SK_Mobile_Call.asmx/SK_mobile_Call";
//        String url = Defined.BASE_HTTPS_URL;
//        Logger.i(TAG, "LoadBalance.cls=" + cls + ", url=" + url);
////        if (TextUtils.isEmpty(url) && (cls != Tr_get_infomation.class)) {
//        if (TextUtils.isEmpty(url)) {
//            getInformation(context, cls, obj, step);
//            return;
//        }
//
//        if(!cls.getName().equals(Tr_hra_check_result_input.class.getName())) {
////            if (isShowProgress)
//                // showProgress();
//        }
//
//        CConnAsyncTask.CConnectorListener queryListener = new CConnAsyncTask.CConnectorListener() {
//
//            @Override
//            public Object run() throws Exception {
//
//                ApiData data = new ApiData();
//                return data.getData(context, cls, obj);
//            }
//
//            @Override
//            public void view(CConnAsyncTask.CQueryResult result) {
//                if (isShowProgress)
//                    // hideProgress();
//
//                if (result.result == CConnAsyncTask.CQueryResult.SUCCESS && result.data != null) {
//                    if (step != null) {
//                        step.next(result.data);
//                    }
//
//                } else {
//                    // hideProgress();
//                    if (failStep != null) {
//                        failStep.fail();
//                    } else {
//                        Toast.makeText(getContext(), "데이터 수신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
//                        Log.e(TAG, "CConnAsyncTask error::"+cls.getClass().getName());
//                        // hideProgress();
//                    }
//                }
//            }
//        };
//
//        CConnAsyncTask asyncTask = new CConnAsyncTask();
//        asyncTask.execute(queryListener);
    }

    public void getData(final Context context, final BaseData baseData, final Object obj, final boolean isShowProgress, final ApiData.IStep step, final ApiData.IFailStep failStep) {
        if (context == null) {
            // 앱을 빠르게 취소 하고 재 시작 시 오류 발생 될 때 있음
            Logger.e(TAG , "getData context is null");
            return;
        }
        if (NetworkUtil.getConnectivityStatus(context) == false) {
            CDialog.showDlg(context, "네트워크 연결 상태를 확인해주세요.");
            return;
        }

        MediNewNetworkModule.doApi(getActivity(), baseData, obj, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                step.next(responseData);
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                error.printStackTrace();
                if (failStep != null)
                    failStep.fail();

            }
        });

////        String url = "http://wkd.walkie.co.kr/SK/WebService/SK_Mobile_Call.asmx/SK_mobile_Call";
//        String url = Defined.BASE_HTTPS_URL;
//        Logger.i(TAG, "LoadBalance.cls=" + baseData + ", url=" + url);
////        if (TextUtils.isEmpty(url) && (cls != Tr_get_infomation.class)) {
////        if (TextUtils.isEmpty(url)) {
////            getInformation(context, baseData, obj, step);
////            return;
////        }
//
//        if(!baseData.getClass().getName().equals(Tr_hra_check_result_input.class.getName())) {
//            // if (isShowProgress)
//                // showProgress();
//        }
//
//        CConnAsyncTask.CConnectorListener queryListener = new CConnAsyncTask.CConnectorListener() {
//
//            @Override
//            public Object run() throws Exception {
//
//                ApiData data = new ApiData();
//                return data.getData(baseData, obj);
//            }
//
//            @Override
//            public void view(CConnAsyncTask.CQueryResult result) {
//                if (isShowProgress)
//                    // hideProgress();
//
//                if (result.result == CConnAsyncTask.CQueryResult.SUCCESS && result.data != null) {
//                    if (step != null) {
//                        step.next(result.data);
//                    }
//
//                } else {
//                    // mBaseActivity.hideProgress();
//                    if (failStep != null) {
//                        failStep.fail();
//                    } else {
//                        Toast.makeText(getContext(), "데이터 수신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
//                        Log.e(TAG, "CConnAsyncTask error::"+baseData.getClass().getName());
//                        // hideProgress();
//                    }
//                }
//            }
//        };
//
//        CConnAsyncTask asyncTask = new CConnAsyncTask();
//        asyncTask.execute(queryListener);
    }

    /**
     * 전문 외 일반 api 요청시
     * @param context
     * @param dataCls
     * @param isShowProgress
     * @param step
     * @param failStep
     */
    public void getAPIData(final Context context, final BaseData dataCls, final boolean isShowProgress, final ApiData.IStep step, final ApiData.IFailStep failStep) {
        if (context == null) {
            // 앱을 빠르게 취소 하고 재 시작 시 오류 발생 될 때 있음
            Logger.e(TAG , "getData context is null");
            return;
        }
        if (NetworkUtil.getConnectivityStatus(context) == false) {
            CDialog.showDlg(context, "네트워크 연결 상태를 확인해주세요.");
            return;
        }

//        if(!cls.getName().equals(Tr_hra_check_result_input.class.getName())) {
//        if (isShowProgress)
                // showProgress();
//        }

        CConnAsyncTask.CConnectorListener queryListener = new CConnAsyncTask.CConnectorListener() {

            @Override
            public Object run() throws Exception {

                ApiData data = new ApiData();
                return data.getAPIData(context, dataCls);
            }

            @Override
            public void view(CConnAsyncTask.CQueryResult result) {
                 hideProgress();

                if (result.result == CConnAsyncTask.CQueryResult.SUCCESS && result.data != null) {
                    if (step != null) {
                        step.next(result.data);
                    }

                } else {
                    hideProgress();

                    if (failStep != null) {
                        failStep.fail();
                    } else {
                        Toast.makeText(getContext(), "데이터 수신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "CConnAsyncTask error::"+dataCls.getClass().getName());

                    }
                }
            }
        };

        CConnAsyncTask asyncTask = new CConnAsyncTask();
        asyncTask.execute(queryListener);
    }

    /**
     * 이미지 url에서 이미지를 가져와 ImageView에 세팅한다.
     *
     * @param imgUrl
     * @param iv
     */
    public void getImageData(final String imgUrl, final ImageView iv) {
        if (imgUrl == null) {
            Logger.d(TAG, "getIndexToImageData imgUrl is null");
            return;
        }

        CConnAsyncTask.CConnectorListener queryListener = new CConnAsyncTask.CConnectorListener() {

            @Override
            public Object run() throws Exception {
                URL url = new URL(imgUrl);
                URLConnection conn = url.openConnection();
                conn.connect();
                BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());

                Bitmap bm = BitmapFactory.decodeStream(bis);
                bis.close();
                return bm;
            }

            @Override
            public void view(CConnAsyncTask.CQueryResult result) {
                if (result.result == CConnAsyncTask.CQueryResult.SUCCESS && result.data != null) {
                    Bitmap bm = (Bitmap) result.data;
                    iv.setImageBitmap(bm);
                } else {
                    Logger.e(TAG, "CConnAsyncTask error");
                }
            }
        };

        CConnAsyncTask asyncTask = new CConnAsyncTask();
        asyncTask.execute(queryListener);
    }

    public void finishStep() {
//        if (mBaseActivity != null) {
//            mBaseActivity.finishStep();
//        }

        if (getActivity() instanceof BaseActivityMedicare) {
            ((BaseActivityMedicare)getActivity()).finishStep();
        } else {
            Log.e(TAG, "finishStep() is not BaseActivityMedicare");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public BaseFragmentMedi getFragment(Class<?> cls) {
        BaseFragmentMedi fragment = null;
        try {
            Constructor<?> co = cls.getConstructor();
            fragment = (BaseFragmentMedi) co.newInstance();
        } catch (Exception e) {
            Log.e(TAG, "getFragment", e);
        }
        return fragment;
    }


    /**
     * ################################################################################
     * ###########################   Permission 관련 처리 시작  ##########################
     */

    public void permissionRequest(String[] permissions, int requestCode, IPermissionResult iResult) {
        if (getActivity() instanceof BaseActivityMedicare)
            ((BaseActivityMedicare)getActivity()).permissionRequest(permissions, requestCode, iResult);
        else {
            if (PermissionUtil.isGrantedPermissions(getActivity(), permissions) == false) {
//                mIPermissionResult = iResult;
                PermissionUtil.requestPermissions(getActivity(), permissions, requestCode, null);
            } else {
                // 권한 처리 할 것이 없는 경우
                iResult.onPermissionResult(requestCode, true, null);
            }
        }
    }
    /**
     * ###########################   Permission 관련 처리 끝  ##########################
     * ################################################################################
     */



//    /**
//     * 구글 피트니스 연결 후 화면 이동 처리
//     * @param fragment
//     */
//    public void stratGoogleClient(BaseFragment fragment, final IConnectResult iResult) {
//        if (getActivity() instanceof MainActivityMedicare) {
//            MainActivityMedicare activity = (MainActivityMedicare) getActivity();
//            activity.startGoogleFitness(fragment, iResult);
//        }
//    }

    /**
     * 운동의 목표걸음수, 목표칼로리 생성
     */
    public int getDefaultTargetCalrori() {

        Tr_login login = UserInfo.getLoginInfo();

//        int bmiLevel = StringUtil.getIntVal(login.mber_bmi_level);
        int bmiLevel = 0;
        //목표칼로리, 목표걸음수 계산
        float bdwghGoal = StringUtil.getFloat(login.mber_bdwgh_goal);
        float bdwgh = StringUtil.getFloat(login.mber_bdwgh);
        if (bdwghGoal > bdwgh) {
            bmiLevel = 0;
        } else if (bdwghGoal == bdwgh) {
            bmiLevel = 1;
        } else if (bdwghGoal < bdwgh) {
            bmiLevel = 2;
        }

        int sex = StringUtil.getIntVal(login.mber_sex);
        DBHelper helper = new DBHelper(getContext());
        DBHelperWeight weightDb = helper.getWeightDb();
        DBHelperWeight.WeightStaticData bottomData = weightDb.getResultStatic();
        float weight        = 0.f;
        if(!bottomData.getWeight().isEmpty()) {
            weight          = StringUtil.getFloatVal(login.mber_bdwgh_app);
        }else{
            weight          = StringUtil.getFloatVal(login.mber_bdwgh);
        }
        float height = StringUtil.getFloatVal(login.mber_height);
        int actqy = StringUtil.getIntVal(login.mber_actqy);
        int targetCal = 0;  //목표소모칼로리/1일 (kcal)

        float mWeight;   //표준체중
        float mHeight = height * 0.01f;
        mHeight = StringUtil.getFloatVal(String.format("%.2f", mHeight));

        if (sex == 1) {
            //남성
            mWeight = StringUtil.getFloatVal(String.format("%.1f", (mHeight * mHeight) * 22));
        } else {
            //여성
            mWeight = StringUtil.getFloatVal(String.format("%.1f", (mHeight * mHeight) * 21));
        }
        // 체중 증량 필요
        if (bmiLevel == 0) {
            switch (sex) {
                case 1:
                    targetCal = 300;
                    break;
                case 2:
                    targetCal = 250;
                    break;
            }
        }
        // 체중 유지 필요 : BMI 기준 정상체중군(18.5≤BMI≤22.9)이지만 현재 체중≤표준 체중.
        else if (bmiLevel == 1 && weight <= mWeight) {
            switch (sex) {
                case 1:
                    targetCal = 400;
                    break;
                case 2:
                    targetCal = 300;
                    break;
            }
        }
        // 체중 감량 필요 :  BMI 기준 정상체중군(18.5≤BMI≤22.9)이지만 현재체중>표준체중
        else if ((bmiLevel == 1 && weight > mWeight) || bmiLevel > 1) {
            switch (sex) {
                case 1:
                    targetCal = 500;
//                    switch (actqy) {
//                        case 1:
//                            targetCal = 450;
//                            break;
//                        case 2:
//                            targetCal = 500;
//                            break;
//                        case 3:
//                            targetCal = 600;
//                            break;
//                    }
                    break;
                case 2:
                    targetCal = 400;
//                    switch (actqy) {
//                        case 1:
//                            targetCal = 375;
//                            break;
//                        case 2:
//                            targetCal = 400;
//                            break;
//                        case 3:
//                            targetCal = 500;
//                            break;
//                    }
                    break;
            }
        }


        return targetCal;

        /*
        String retStep ="0";
        String retCalrori;

        if (_step > 0) {
            retStep = "" + _step;
        } else {
            retStep = getStepTargetCalulator(sex, height, weight, targetCal);
        }

        if (_calrori > 0) {
            retCalrori = "" + _calrori;
        } else {
            retCalrori = getCalroriTargetCalulator(sex, height, weight, StringUtil.getIntVal(retStep));
        }

        if ("0".equals(login.goal_mvm_stepcnt)) {
            login.goal_mvm_stepcnt = retStep;
            login.goal_mvm_calory = retCalrori;
        }

        Logger.i(TAG, "setStepTarget.goal_mvm_stepcnt="+login.goal_mvm_stepcnt);
        Logger.i(TAG, "setStepTarget.goal_mvm_calory="+login.goal_mvm_calory);
        Define.getInstance().setLoginInfo(login);
        */
    }

    /**
     * 목표 걸음수 계산
     */
    public String getStepTargetCalulator(int sex, float height, float weight, int calrori) {

        double stepTarget = 0.0f;
        String rtnValue;
        if (sex == 1) {
            stepTarget = ((((height / 100 / (-0.5625 * 3.5 + 4.2294)) * 3.5) / 2) * 60 / (height / 100 / (-0.5625 * 3.5 + 4.2294))) * calrori / (((3.5 + 0.1 * (height / 100 / (-0.5625 * 3.5 + 4.2294) * 3.5 / 2 * 60)) * weight) / 1000 * 5);
        } else {
            stepTarget = ((((height / 100 / (-0.5133 * 3.5 + 4.1147)) * 3.5) / 2) * 60 / (height / 100 / (-0.5133 * 3.5 + 4.1147))) * calrori / (((3.5 + 0.1 * (height / 100 / (-0.5133 * 3.5 + 4.1147) * 3.5 / 2 * 60)) * weight) / 1000 * 5);
        }
        rtnValue = String.format("%.0f", stepTarget);

        if (height==0 || weight==0){
            return "0";
        }else{
            return rtnValue;
        }
    }


    /**
     * 목표 칼로리 계산
     */
    public String getCalroriTargetCalulator(int sex, float height, float weight, int step) {

        double calroriTarget = 0.0f;
        String rtnValue;
        if (sex == 1) {
            calroriTarget = (step * (((3.5 + 0.1 * (height / 100 / (-0.5625 * 3.5 + 4.2294) * 3.5 / 2 * 60)) * weight) / 1000 * 5)) / (((((height / 100 / (-0.5625 * 3.5 + 4.2294)) * 3.5) / 2) * 60) / (height / 100 / (-0.5625 * 3.5 + 4.2294)));
        } else {
            calroriTarget = (step * (((3.5 + 0.1 * (height / 100 / (-0.5133 * 3.5 + 4.1147) * 3.5 / 2 * 60)) * weight) / 1000 * 5)) / (((((height / 100 / (-0.5133 * 3.5 + 4.1147)) * 3.5) / 2) * 60) / (height / 100 / (-0.5133 * 3.5 + 4.1147)));
        }
        rtnValue = String.format("%.0f", calroriTarget);

        if (height==0 || weight==0){
            return "0";
        }else{
            return rtnValue;
        }
    }

    /**
     * 앱 재시작
     */
    public void restartMainActivity() {
        getActivity().finish();

        Intent intent = new Intent(getContext(), MainActivityMedicare.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    /**
     * 권한이 정상적으로 설정 되었는지 확인
     *
     * @param grantResults
     * @return
     */
    private boolean isPermissionGransteds(int[] grantResults) {
        for (int isGranted : grantResults) {
            return isGranted == PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

    public interface IPermission {
        void result(boolean isGranted);
    }

    // 화면 회전시 초기화 방지
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public void health_point_popup(String mission_type, final int img){
        Tr_login login = UserInfo.getLoginInfo();
        Tr_misson_goal_alert.RequestData requestData = new Tr_misson_goal_alert.RequestData();
        SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MISSION_POP, false);

        requestData.mber_sn = login.mber_sn;
        requestData.misson_goal_alert_typ = mission_type;

        MediNewNetworkModule.doApi(getContext(), Tr_misson_goal_alert.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_misson_goal_alert) {
                    Tr_misson_goal_alert data = (Tr_misson_goal_alert)responseData;
                    try {

                        if(data.data_yn.equals("Y")) {
                            String msg = data.misson_goal_txt;
                            CDialog.showDlg(getContext(),msg)
                                    .setPointTvView(data.misson_goal_point+"P");


                        }


                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
            }
        });
    }

    protected int getCurrentFragment() {
        if (getActivity() instanceof MainActivityMedicare) {
            int currentIdx = ((MainActivityMedicare)getActivity()).getCurrentFragment();
            Log.i(TAG, "## getCurrentFragment="+currentIdx);
            return currentIdx;
        }
        return -1;
    }


    protected int getCurrentTopFragment(Fragment fragment) {
        if (fragment.getParentFragment() instanceof HealthFragment) {
            int currentIdx = ((HealthFragment) fragment.getParentFragment()).getCurrentFragment();
            Log.i(TAG, "## getCurrentTopFragment="+currentIdx);
            return currentIdx;
        }
        return -1;
    }

}
