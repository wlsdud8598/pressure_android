package kr.co.hi.medicare.fragment.community;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import kr.co.hi.medicare.net.hwNet.BaseData;

import co.lujun.androidtagview.TagView;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.adapter.CommunityListViewAdapter;
import kr.co.hi.medicare.fragment.community.adapter.LoadMoreListener;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.fragment.community.data.CommunityUserData;
import kr.co.hi.medicare.fragment.community.data.ReceiveDataCode;
import kr.co.hi.medicare.fragment.community.data.SendDataCode;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_DB001;
import kr.co.hi.medicare.net.data.Tr_DB014;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;

import static android.app.Activity.RESULT_OK;
import static android.support.v7.widget.RecyclerView.SCROLL_STATE_IDLE;

/**
 * 2019-02-28 park
 * 커뮤니티 메인 페이지
 */
public class CommunityMainFragment extends BaseFragmentMedi implements LoadMoreListener,SwipeRefreshLayout.OnRefreshListener {
    private final String TAG = CommunityMainFragment.class.getSimpleName();

    private DBHelper mDbHelper;
    //사용자정보
    private Tr_login user;
    //tab레이아웃
    private RadioButton tab_whole,tab_notice;
    private int TAB_STATUS=0;
    //리스트뷰
    private CommunityListViewAdapter mAdapter;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefresh;
    private LinearLayoutManager mLayoutManager;
    private int TotalPage = 1;
    private int firstVisibleItem, visibleItemCount, totalItemCount;
    private int visibleThreshold = 1;
    //글쓰기
    private FloatingActionButton btn_write;
    //검색
//    private ImageView main_toolbar_premium;

    private CommonToolBar commonToolBar;

    public static Fragment newInstance() {
        CommunityMainFragment fragment = new CommunityMainFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_community_main, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        CommonToolBar toolBar = getToolBar(view);
        toolBar.setVisibility(View.VISIBLE);
        toolBar.setLeftIcon(R.drawable.commu_btn_3, mOnClickListener);
        toolBar.setRightIcon(R.drawable.commu_btn_5, mOnClickListener);
        setTabView(R.id.tab_whole);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        getActivity().findViewById(R.id.medi_main_home_toolbar).setVisibility(View.GONE);
        commonToolBar = ((CommonToolBar)getActivity().findViewById(R.id.medi_common_toolbar));
        commonToolBar.setRightIcon(R.drawable.commu_btn_5, mOnClickListener);
        commonToolBar.setLeftIcon(R.drawable.commu_btn_3, mOnClickListener);

//        main_toolbar_premium = getActivity().findViewById(R.id.main_toolbar_premium);
//        main_toolbar_premium.setOnClickListener(mOnClickListener);
//        main_toolbar_premium.setImageResource(0);
//        main_toolbar_premium.setImageResource(R.drawable.commu_btn_5);
//        drawerToggle = getActivity().findViewById(R.id.drawerToggle);
//        drawerToggle.setOnClickListener(mOnClickListener);
    }


    /**
     * 뷰 초기화. 화면 로딩 시 1회 호출
     * @param view fragment view
     */
    private void initView(View view) {

        mDbHelper = new DBHelper(getContext());
        user = UserInfo.getLoginInfo();

        //writer
        btn_write = view.findViewById(R.id.btn_write);
        btn_write.setOnClickListener(mOnClickListener);
        //tabview
        tab_whole = view.findViewById(R.id.tab_whole);
        tab_notice = view.findViewById(R.id.tab_notice);
        tab_whole.setOnClickListener(mOnClickListener);
        tab_notice.setOnClickListener(mOnClickListener);
        //listview
        swipeRefresh = view.findViewById(R.id.swipeRefresh);
        recyclerView = view.findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new CommunityListViewAdapter(getContext(),mOnClickListener,onTagClickListener,user.mber_sn,getCMGUBUN(),CommunityMainFragment.this);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager llManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = llManager.getItemCount();
                firstVisibleItem = llManager.findFirstVisibleItemPosition();

                if (dy>0&&!mAdapter.getMore() && (totalItemCount - visibleItemCount)<= (firstVisibleItem + visibleThreshold)) {
                    onLoadMore();
                }

            }
        });

        swipeRefresh.setOnRefreshListener(this);
//        swipeRefresh.setColorSchemeColors(getResources().getColor(android.R.color.holo_blue_bright),
//                getResources().getColor(android.R.color.holo_green_light),
//                getResources().getColor(android.R.color.holo_orange_light),
//                getResources().getColor(android.R.color.holo_red_light));



        //click 저장
        OnClickListener ClickListener = new OnClickListener(mOnClickListener, view, getContext());


        //커뮤니티

        btn_write.setOnTouchListener(ClickListener);
        view.findViewById(R.id.medi_common_toolbar).findViewById(R.id.common_toolbar_back_icon).setOnTouchListener(ClickListener);
        tab_notice.setOnTouchListener(ClickListener);
        view.findViewById(R.id.medi_common_toolbar).findViewById(R.id.common_toolbar_right_icon).setOnTouchListener(ClickListener);

        //코드부여
        btn_write.setContentDescription(getString(R.string.btn_write));
        view.findViewById(R.id.medi_common_toolbar).findViewById(R.id.common_toolbar_back_icon).setContentDescription(getString(R.string.community_alimi));
        tab_notice.setContentDescription(getString(R.string.tab_notice));
        view.findViewById(R.id.medi_common_toolbar).findViewById(R.id.common_toolbar_right_icon).setContentDescription(getString(R.string.community_search));

    }

    private String getCMGUBUN() {
        if(TAB_STATUS==R.id.tab_notice)
            return SendDataCode.CM_NOTICE;
        else
            return SendDataCode.CM_ALL;
    }

    /**
     * 탭 버튼 동작 정의
     * 화면 최초 로딩 시, 버튼 클릭 시 호출
     * @param id TAB BUTTTON ID
     */
    private boolean setTabView(int id) {

        boolean isAction=false;

        if(TAB_STATUS!=id){
            isAction=true;
            TAB_STATUS=id;
            if(TAB_STATUS==R.id.tab_whole){
                setWriteBtn(View.VISIBLE);
            }else{
                setWriteBtn(View.GONE);
            }

            mAdapter.clearAllItem();
            getListFromServer(PROGRESSBAR_TYPE_CENTER, "1","","");
        }

        return isAction;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //글쓰기 완료 후 다시 커뮤니티 메인 페이지로 이동 시 리스트뷰 갱신
        if(resultCode==RESULT_OK) {
            if (requestCode == CommunityWriteFragment.REQUEST_CODE_WRITEINFO) {
                getListFromServer(PROGRESSBAR_TYPE_CENTER, "1", "", "");
            }else if(requestCode == CommunityCommentFragment.REQUEST_CODE_COMMENTINFO){
               CommunityListViewData comm_data =(CommunityListViewData)data.getSerializableExtra(CommunityCommentFragment.REQUEST_CODE_COMMDATA);
               boolean isDel = false;
               try{
                   isDel = data.getBooleanExtra(CommunityCommentFragment.REQUEST_CODE_ISDEL,false);
               }catch (Exception e){
                   e.printStackTrace();
               }
               if(isDel){
                   mAdapter.deleteItem(comm_data.CM_SEQ);
               }else{
                   mAdapter.updateData(comm_data);
               }
            }else if(requestCode == CommunityNoticeFragment.REQUEST_CODE_NOTICE){
                getListFromServer(PROGRESSBAR_TYPE_CENTER, "1", "", "");
            }
        }
    }

    // 리싸이클러뷰가 스크롤 중에는 글쓰기 버튼을 GONE 상태로 변경
    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if(newState==SCROLL_STATE_IDLE){
                setWriteBtn(View.VISIBLE);
            }else{
                setWriteBtn(View.GONE);
            }

        }
    };

    //태그 목록 중 한개의 아이템 선택 시 해당 태그로 검색 및 검색페이지로 이동
    TagView.OnTagClickListener onTagClickListener = new TagView.OnTagClickListener() {
        @Override
        public void onTagClick(int position, String text) {
            NewActivity.moveToTagPage(getActivity(), text, getString(R.string.comm_community));
        }

        @Override
        public void onTagLongClick(int position, String text) {

        }

        @Override
        public void onTagCrossClick(int position) {

        }
    };

    //클릭 이벤트 정의
    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int viewId = v.getId();
            Bundle bundle = new Bundle();
            switch (viewId){
                //탭버튼
                case R.id.tab_whole:
                case R.id.tab_notice:
                    setTabView(viewId);
                    break;
                //리싸이클러뷰 댓글 모두보기 -> 게시글페이지로 이동
                case R.id.layout_whole:
                case R.id.text:
                case R.id.comment_all:
                    NewActivity.moveToCommentPage(CommunityMainFragment.this,mAdapter.getItem(Integer.parseInt(v.getTag(R.id.comm_main_text).toString())),false,getString(R.string.comm_community),getCMGUBUN());
                    break;
                //댓글 달기 -> 게시글페이지로 이동
                case R.id.btn_comment:
                    NewActivity.moveToCommentPage(CommunityMainFragment.this,mAdapter.getItem(Integer.parseInt(v.getTag().toString())),true,getString(R.string.comm_community),getCMGUBUN());
                    break;

                //검색페이지로 이동
                case R.id.common_toolbar_right_icon:
                    NewActivity.startActivity(CommunityMainFragment.this, CommunitySearchFragment.class,null);
                    break;

                //글쓰기 페이지로 이동
                case R.id.btn_write:
                    NewActivity.moveToWritePage(CommunityMainFragment.this, null,  "");
                    break;

                //원본이미지 확대
                case R.id.image:
                    CommunityListViewData data = new CommunityListViewData();
                    data = (CommunityListViewData)v.getTag(R.id.comm_main_image);
//                    bundle.putString(CommunityImageFragment.KEY_IMAGE_PATH,data.CM_IMG1);
//                    bundle.putString(CommunityImageFragment.KEY_NICKNAME,data.NICK);
//                    NewActivity.startActivity2(CommunityMainFragment.this, CommunityImageFragment.class,bundle);
//

                    NewActivity.moveToImagePage(CommunityMainFragment.this,data.CM_IMG1,data.NICK );

                    break;

                case R.id.common_toolbar_back_icon:
                    NewActivity.startActivityForResult(CommunityMainFragment.this, CommunityNoticeFragment.REQUEST_CODE_NOTICE,CommunityNoticeFragment.class,null );
//                    NewActivity.startActivity(CommunityMainFragment.this,CommunityNoticeFragment.class,null);

                    break;


                case R.id.profile:
                    int position = Integer.parseInt(v.getTag(R.id.comm_main_profile).toString());
                    CommunityUserData userData = new CommunityUserData();
                    userData.NICKNAME = mAdapter.getItem(position).NICK;
                    userData.MBER_SN = mAdapter.getItem(position).MBER_SN;
                    userData.PROFILE_PIC = mAdapter.getItem(position).PROFILE_PIC;
                    NewActivity.moveToProfilePage(getString(R.string.comm_title), userData,getActivity());
                    break;
            }
        }
    };


    /**
     * 서버에 게시글 리스트 데이터 요청 DB002
     * @param loadingType 프로그레스바 로딩 타입
     * @param PG 요청 페이지
     * @param sword 키워드
     * @param tag 태그
     */
    private void getListFromServer(final String loadingType,final String PG,String sword,String tag) {
        final Tr_DB001.RequestData requestData = new Tr_DB001.RequestData();
        boolean isShowProgress=false;

        switch (TAB_STATUS){
            case R.id.tab_notice:
                requestData.CMGUBUN = SendDataCode.CM_NOTICE;
                break;
            default:
                requestData.CMGUBUN = SendDataCode.CM_ALL;
                break;
        }
        requestData.PG_SIZE = PAGE_SIZE;
        requestData.PG = PG;
        requestData.SEQ = user.mber_sn;
        requestData.SWORD = sword;
        requestData.CM_TAG = tag;

        if(loadingType.equals(PROGRESSBAR_TYPE_CENTER))
            isShowProgress=true;

        MediNewNetworkModule.doApi(getContext(), new Tr_DB001(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                //하단 프로그레스바일 경우 프로그레스바 종료
                if(loadingType.equals(PROGRESSBAR_TYPE_BOTTOM))
                    mAdapter.setProgressMore(false);

                if (responseData instanceof Tr_DB001) {
                    Tr_DB001 data = (Tr_DB001)responseData;
                    try {

                        if(data.DATA!=null&&data.DATA.size()>0&&!data.DATA_LENGTH.equals("0")){

                            int requestPage = Integer.parseInt(PG);
                            int receivePage = Integer.parseInt(data.DATA.get(0).TPAGE);

                            if(requestPage<=receivePage){
                                TotalPage = requestPage;
                                if(TotalPage<2){//서버로부터 받은 페이지가 2보다 작으면 클린 후 추가
                                    mAdapter.addAllData(data.DATA);
                                }else{ //
                                    mAdapter.addItemMore(data.DATA);
                                }
                            }
                        }else{
                        }
                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }
                switch (loadingType){
                    case PROGRESSBAR_TYPE_CENTER:

                        break;
                    case PROGRESSBAR_TYPE_BOTTOM:
                        mAdapter.setMore(false);
                        break;
                    case PROGRESSBAR_TYPE_TOP:
                        swipeRefresh.setRefreshing(false);
                        setWriteBtn(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                if(response!=null)
                    Log.e(TAG, response);

                switch (loadingType){
                    case PROGRESSBAR_TYPE_CENTER:
                        break;
                    case PROGRESSBAR_TYPE_BOTTOM:
                        mAdapter.setProgressMore(false);
                        mAdapter.setMore(false);
                        break;
                    case PROGRESSBAR_TYPE_TOP:
                        swipeRefresh.setRefreshing(false);
                        setWriteBtn(View.VISIBLE);
                        break;
                }
            }
        });
    }




    /**
     * 알림리스트 요청 DB014
     */
    private void checkAlramFromServer() {
        final Tr_DB014.RequestData requestData = new Tr_DB014.RequestData();
        boolean isShowProgress=false;

        requestData.SEQ = user.mber_sn;

        MediNewNetworkModule.doApi(getContext(), new Tr_DB014(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";

                if (responseData instanceof Tr_DB014) {
                    Tr_DB014 data = (Tr_DB014)responseData;
                    try {

                        switch (data.RESULT_CODE){
                            case ReceiveDataCode.DB014_SUCCESS:

                                int totalCount =  data.DATA.size();


                                if(totalCount>0) {
                                    if(totalCount>50)
                                        totalCount=50;

                                    int ALM_SEQ = Integer.parseInt(mDbHelper.getmCommunityNotice().getMaxALMSEQ(mDbHelper));

                                    for(int i=0; i<totalCount;i++){
                                        int alm_seq = 0;

                                        try{
                                            if(data.DATA.get(i).ALM_SEQ==null|| data.DATA.get(i).ALM_SEQ.equals(""))
                                                continue;

                                            alm_seq = Integer.parseInt(data.DATA.get(i).ALM_SEQ);
                                        }catch (Exception e){
                                            continue;
                                        }

                                        if(alm_seq>ALM_SEQ){
                                            mDbHelper.getmCommunityNotice().insert(data.DATA.get(i).REGDATE,data.DATA.get(i).MSG, data.DATA.get(i).CM_SEQ, data.DATA.get(i).ALM_SEQ , data.DATA.get(i).PROFILE_PIC, data.DATA.get(i).MBER_GRAD, data.DATA.get(i).CM_GUBUN, data.DATA.get(i).MBER_SN, data.DATA.get(i).NICK);
                                        }else{
                                            mDbHelper.getmCommunityNotice().update(data.DATA.get(i).REGDATE,data.DATA.get(i).MSG, data.DATA.get(i).CM_SEQ, data.DATA.get(i).ALM_SEQ, data.DATA.get(i).PROFILE_PIC, data.DATA.get(i).MBER_GRAD, data.DATA.get(i).CM_GUBUN, data.DATA.get(i).MBER_SN, data.DATA.get(i).NICK);
                                        }
                                    }
                                }

                                break;
                            case ReceiveDataCode.DB014_ERROR_ETC:
//                                errorMessage = getResources().getString(R.string.comm_error_common_9999);
                                break;
                            default:
//                                errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:1";
                                break;
                        }

                        setAlramEvent();

                    } catch (Exception e) {
                        CLog.e(e.toString());
//                        errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:2";
                    }
                }else{
//                    errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:3";
                }

                if(!errorMessage.equals("")){
//                    dialog = DialogCommon.showDialogSingleBtn(getContext(), errorMessage, getResources().getString(R.string.comm_confirm), new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            dialog.dismiss();
//                        }
//                    });
                    CDialog.showDlg(getContext(), errorMessage)
                            .setOkButton(getResources().getString(R.string.comm_confirm), null);
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                if(response!=null)
                    Log.e(TAG, response);

                setAlramEvent();
            }
        });
    }





    private void setAlramEvent(){
        if(commonToolBar!=null) {
            if(mDbHelper.getmCommunityNotice().getisNew(mDbHelper)){
                commonToolBar.setLeftIcon(R.drawable.commu_btn_4,mOnClickListener);

//                drawerToggle.setImageResource(R.drawable.commu_btn_4);
            }else{
//                drawerToggle.setImageResource(R.drawable.commu_btn_3);
                commonToolBar.setLeftIcon(R.drawable.commu_btn_3,mOnClickListener);
            }
        }
    }







    // 리싸이클러뷰 하단 페이징 시
    @Override
    public void onLoadMore()
    {
        mAdapter.setProgressMore(true);
        mAdapter.setMore(true);
        getListFromServer(PROGRESSBAR_TYPE_BOTTOM, Integer.toString(TotalPage+1),"","");
    }

    // 리싸이클러뷰 상단 페이징 시
    @Override
    public void onRefresh() {
        getListFromServer(PROGRESSBAR_TYPE_TOP, "1","","");
    }


    @Override
    public void onResume(){
        super.onResume();
        if (getCurrentFragment() == MainActivityMedicare.HOME_MENU_3) {
            checkAlramFromServer();
        }
    }


//    // CENTER, BOTTOM, TOP
//    private void refreshListView(final String loadingType, final int page){
//
//        String Type = "ALL";
//
//        if(TAB_STATUS==R.id.tab_whole){
//            Type = "ALL";
//        }else{
//            Type = "NOTICE";
//        }
//
//
//        AbsCommonFunc callBack = new AbsCommonFunc() {
//
//            @Override
//            public Object data() {
//                //리스트뷰 데이터 생성
//
//                List<CommunityListViewData> dataList = new ArrayList<>();
//
//                for(int i=0; i<10;i++){
//                    CommunityListViewData data = new CommunityListViewData();
//                    data.CM_TITLE="테스트 게시물"+page;
//                    data.TPAGE = Integer.toString(page);
//                    data.NICK ="김소리"+i;
//                    data.REGDATE = "2019-02-18 오후 06:08:00";
//                    data.RCNT="10";
//                    data.HCNT="20";
//                    data.CM_IMG1 = "https://m.shealthcare.co.kr/HL_MED_COMMUNITY/Upload/HS_HL_M/0001013000005/20160623/2_1466659016849.jpg";
//                    data.PROFILE_PIC = "https://m.shealthcare.co.kr/HL_MED_COMMUNITY/Upload/HS_HL_M/0001013000005/20160623/4_1466659068934.jpg";
//                    data.CM_TAG = new ArrayList<>();
//                    data.CM_TAG.add("#일상");
//                    data.CM_TAG.add("#헬스");
//                    data.CM_TAG.add("#임신");
//
//                    dataList.add(data);
//                }
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
////                return requestDataFromServer.requestServer();
//                return dataList;
//            }
//
//            @Override
//            public void resultCallback(Object o) {
//                ArrayList<CommunityListViewData> dataList = (ArrayList<CommunityListViewData>)o;
//
//                switch (loadingType){
//                    case PROGRESSBAR_TYPE_CENTER:
//
//                        break;
//                    case PROGRESSBAR_TYPE_BOTTOM:
//                        mAdapter.setMore(true);
//                        break;
//                    case PROGRESSBAR_TYPE_TOP:
//                        swipeRefresh.setRefreshing(false);
//                        setWriteBtn(View.VISIBLE);
//                        break;
//                }
//
//
//                TotalPage = page;
//                if(TotalPage<2){//서버로부터 받은 페이지가 2보다 작으면 클린 후 추가
//                    mAdapter.addAllData(dataList);
//                }else{ //
//                    mAdapter.addItemMore(dataList);
//                }
//            }
//
//            @Override
//            public void showDialog() {
//            }
//
//            @Override
//            public void closeDialog() {
//            }
//        };
//
//        new AsyncTaskCommon(callBack).execute();
//
//    }

    //글쓰기 버튼 상태 변경
    private void setWriteBtn(int visibility){
        switch (visibility){
            case View.VISIBLE:
                if (btn_write.getVisibility() != View.VISIBLE && TAB_STATUS==R.id.tab_whole) {
                    btn_write.show();
                }
                break;
            default:
                if (btn_write.getVisibility() == View.VISIBLE) {
                    btn_write.hide();
                }
                break;
        }
    }


}
