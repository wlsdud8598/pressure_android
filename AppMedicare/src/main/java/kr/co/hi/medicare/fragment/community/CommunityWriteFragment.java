package kr.co.hi.medicare.fragment.community;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;

import kr.co.hi.medicare.component.SoftKeyBoard;
import kr.co.hi.medicare.net.hwNet.BaseData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.activity.BaseActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.PictureBaseFragment;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.fragment.community.data.ReceiveDataCode;
import kr.co.hi.medicare.fragment.health.food.HttpAsyncFileTask33;
import kr.co.hi.medicare.fragment.health.food.HttpAsyncTaskInterface;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_DB002;
import kr.co.hi.medicare.net.data.Tr_DB007;
import kr.co.hi.medicare.net.data.Tr_DB015;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.cameraUtil.ProviderUtil;

import static android.app.Activity.RESULT_OK;

/**
 * Created by mrsohn on 2017. 3. 14..
 * 혈당관리
 */

public class CommunityWriteFragment extends PictureBaseFragment implements NewActivity.onKeyBackCatchListener{
    private final String TAG = CommunityWriteFragment.class.getSimpleName();

//    public static final int REQUEST_CODE_GALLERY=999;
    public static final int REQUEST_CODE_WRITEINFO=9999;

    public static final String KEY_CONTENTS="CONTENTS";
    public static final String KEY_IMAGEPATH="IMAGEPATH";

    private TextView btn_cancel,btn_confirm,text_share;
    private TagContainerLayout tag;
    private EditText edit_contents;
    private ImageView image,btn_delete;
    private FloatingActionButton btn_gallery;
    private RelativeLayout layout_image;
    private CommunityListViewData data= new CommunityListViewData();
    private Bitmap bitmap = null;

    private String DEL_FILE="";
//    private Uri outputFileUri;

    private SoftKeyBoard softKeyboard;
    private CoordinatorLayout rootView;
    public static Fragment newInstance() {
        CommunityWriteFragment fragment = new CommunityWriteFragment();
        return fragment;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG,"PICTURE : "+ requestCode +" DATA : "+ data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_CROP) {
                if(outputFileUri!=null){
                    setImage(View.VISIBLE, null,false);
                }
            }
        }



    }

    private void setImage(int visible,Bitmap bitmap,boolean isImage) {

        if(visible==View.VISIBLE){
            setGalleryBtn(View.GONE);
            layout_image.setVisibility(View.VISIBLE);

            if(isImage) {
                Glide
                        .with(getContext())
                        .load(bitmap)
                        .into(image);
            }
        }else{

            image.setImageResource(0);
            layout_image.setVisibility(View.GONE);
            setGalleryBtn(View.VISIBLE);
            outputFileUri=null;
            if(data!=null&&data.CM_SEQ!=null&&Integer.parseInt(data.CM_SEQ)>0&&data.CM_IMG1!=null&&!data.CM_IMG1.equals("")){
                DEL_FILE="CM_IMG1";
            }

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_community_write, container, false);

        btn_cancel = view.findViewById(R.id.btn_cancel);
        btn_confirm = view.findViewById(R.id.btn_confirm);
        tag = view.findViewById(R.id.tag);
        edit_contents = view.findViewById(R.id.edit_contents);
        text_share = view.findViewById(R.id.text_share);
        image = view.findViewById(R.id.image);
        super.ivPreview=image;
        super.isResizing = false;
        btn_delete=view.findViewById(R.id.btn_delete);
        btn_gallery = view.findViewById(R.id.btn_gallery);
        layout_image = view.findViewById(R.id.layout_image);
        rootView = view.findViewById(R.id.root);

        btn_delete.setOnClickListener(mOnClickListener);
        btn_gallery.setOnClickListener(mOnClickListener);
        btn_cancel.setOnClickListener(mOnClickListener);
        btn_confirm.setOnClickListener(mOnClickListener);
        tag.setTagBorderRadius(0);
        tag.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {

                setTag(position);

            }

            @Override
            public void onTagLongClick(int position, String text) {

            }

            @Override
            public void onTagCrossClick(int position) {

            }
        });
        InputMethodManager controlManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        softKeyboard = new SoftKeyBoard(rootView, controlManager);
        softKeyboard.setSoftKeyboardCallback(new SoftKeyBoard.SoftKeyboardChanged()
        {
            @Override
            public void onSoftKeyboardHide()
            {
                new Handler(Looper.getMainLooper()).post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        btn_gallery.setVisibility(View.VISIBLE);
                    }
                });
            }

            @Override
            public void onSoftKeyboardShow()
            {
                new Handler(Looper.getMainLooper()).post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        btn_gallery.setVisibility(View.GONE);
                    }
                });
            }
        });
        return view;
    }

    private void setBundleInfo() {

        Bundle bundle = getArguments();
        if(bundle!=null){
            data = (CommunityListViewData)bundle.getSerializable(KEY_CONTENTS);
            if(data!=null){
                edit_contents.append(data.CM_CONTENT);

                if(data.CM_TAG!=null){
                    for(int i=0; i<data.CM_TAG.size();i++){
                        checkTagList(data.CM_TAG.get(i));
                    }
                }

                if((data.CM_MEAL_LIST!=null&&data.CM_MEAL_LIST.size()>0)|| (data.CM_MEAL!=null&&!data.CM_MEAL.equals(""))){
                    text_share.setText("");
                    text_share.setVisibility(View.VISIBLE);

                    if(data.CM_MEAL_LIST!=null&&data.CM_MEAL_LIST.size()>0){
                        data.CM_MEAL="";
                        for (int i = 0; i < data.CM_MEAL_LIST.size(); i++) {
                            data.CM_MEAL += data.CM_MEAL_LIST.get(i);
                            if(i+1!=data.CM_MEAL_LIST.size())
                                data.CM_MEAL += ",";
                        }


                    }
                    text_share.setText(data.CM_MEAL);
//                    CommonFunction.setTextByPartsString(data.CM_MEAL,text_share,RoundedCornersBackgroundSpan.ALIGN_START,getContext());
                }


            }

            String image = bundle.getString(KEY_IMAGEPATH);

            if(image!=null&&!image.equals("")){
                Glide.with(getContext()).asBitmap().load(image).into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        bitmap = resource;
                        if(bitmap!=null){
                            setImage(View.VISIBLE,bitmap,true);
                        }

                    }
                });
            }
        }

    }


    private void checkTagList(String word){
        List<String> tags = tag.getTags();

        for(int i=0; i<tags.size();i++){
            if(tags.equals(word)){

                setTag(i);

                return;
            }
        }
        tag.addTag(word);
        setTag(tags.size());
    }


    private void setTag(int position){
        int isSelect=-1;

        try{
            isSelect = tag.getTagView(position).getId();
        }catch (Exception e){
            isSelect = -1;
        }

        if(isSelect==-1){
            isSelect=1;
            tag.getTagView(position).setTagBackgroundColor(getResources().getColor(R.color.color_6981ec));
            tag.getTagView(position).setTagTextColor(getResources().getColor(R.color.colorWhite));
        }else{
            isSelect=-1;
            tag.getTagView(position).setTagBackgroundColor(Color.WHITE);
            tag.getTagView(position).setTagTextColor(getResources().getColor(R.color.color_8f909e));
        }

        tag.getTagView(position).setId(isSelect);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getTagListFromServer();

    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int viewId = v.getId();
            switch (viewId){
                case R.id.btn_cancel:
                    exitWritePage();
                    break;
                case R.id.btn_confirm:

                    if(edit_contents.getText().toString().trim().equals("")){
                        CDialog.showDlg(getContext(), getResources().getString(R.string.comm_dialog_write_warning));
                    }else{
                        CDialog.showDlg(getContext(),
                                getResources().getString(R.string.comm_dialog_message_2)).setOkButton(getContext().getResources().getString(R.string.comm_register),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        doWriteContents(DEL_FILE, "",  data==null ? "" : data.CM_SEQ);
                                    }
                                })
                                .setNoButton(getResources().getString(R.string.comm_cancel), null);

                    }




                    break;
                case R.id.btn_gallery:
//                    onGallery();
                    showSelectGalleryCamera();
                    break;

                case R.id.btn_delete:
                    setImage(View.GONE,null,false);
                    break;

            }

        }
    };


    public void addTag(List<String> data){

        if(data!=null&&data.size()>0){
            tag.setTags(data);
        }

    }

    private void setGalleryBtn(int visibility){
        switch (visibility){
            case View.VISIBLE:
                if (btn_gallery.getVisibility() != View.VISIBLE) {
                    btn_gallery.show();
                }
                break;
            default:
                if (btn_gallery.getVisibility() == View.VISIBLE) {
                    btn_gallery.hide();
                }
                break;
        }
    }

    private String getTagItem() {
        String tagInfo="";
        boolean isChoice=false;

        final int selected=1;


            for (int i = 0; i < tag.getTags().size(); i++) {
                if (tag.getTagView(i).getId() == selected) {
                    tagInfo += tag.getTagView(i).getText().toString() + ",";
                    isChoice=true;
                }
            }

            if (isChoice&&tagInfo.lastIndexOf(",") + 1 == tagInfo.length()) {
                tagInfo = tagInfo.substring(0, tagInfo.length() - 1);
            }

        return tagInfo;
    }


    private String getCmmeal() {
        String cm_meal="";

        if(data!=null&&data.CM_MEAL_LIST!=null){
            for(int i=0; i<data.CM_MEAL_LIST.size(); i++){
                cm_meal += data.CM_MEAL_LIST.get(i);

                if(i+1!=data.CM_MEAL_LIST.size()){
                    cm_meal += ",";
                }
            }
        }
        return cm_meal;
    }


    private String getRealpath(Uri uri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String path="";
        Cursor c = getActivity().getContentResolver().query(uri, proj, null, null, null);

        try{
            c.moveToFirst();
            int index = c.getColumnIndex(proj[0]);
            path = c.getString(index);
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            c.close();
        }

        return path;
    }


    /**
     * 서버에 태그 리스트 요청 DB015
     */
    private void getTagListFromServer() {
        final Tr_DB015.RequestData requestData = new Tr_DB015.RequestData();
        boolean isShowProgress=true;

        requestData.DOCNO ="DB015";

        MediNewNetworkModule.doApi(getContext(), new Tr_DB015(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                if (responseData instanceof Tr_DB015) {
                    Tr_DB015 data = (Tr_DB015)responseData;
                    try {
                        if(data.DATA!=null&&data.DATA.size()>0){
                            switch (data.RESULT_CODE){
                                case ReceiveDataCode.DB015_SUCCESS:

                                    List<String> tagList = new ArrayList<>();

                                    for(int i=0; i<data.DATA.size();i++){
                                        tagList.add(data.DATA.get(i).TAG_WORD);
                                    }

                                    addTag(tagList);

                                    break;

                                case ReceiveDataCode.DB015_ERROR_ETC:

                                    break;

                                default:

                                    break;
                            }
                        }
                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }

                setBundleInfo();
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                Log.e(TAG, response);
                setBundleInfo();
            }
        });
    }


    /** 음식사진 업로드.
     */
    private void doWriteContents(String DEL_FILE, String PROFILE_PIC, String CM_SEQ) {

//        if(outputFileUri!=null){
//           long fileSize = ProviderUtil.getOutputFile(outputFileUri).length()/1000;
//            Toast.makeText(getContext(), "size kb :"+fileSize,Toast.LENGTH_LONG ).show();
//        }



        final Tr_DB002.RequestData requestData = new Tr_DB002.RequestData();

        requestData.CM_CONTENT = edit_contents.getText().toString();
        requestData.CM_GUBUN="1";
        requestData.CM_SEQ = CM_SEQ==null ? "" : CM_SEQ  ;
        requestData.SEQ = UserInfo.getLoginInfo().mber_sn;
        requestData.CM_TAG = getTagItem();
        requestData.CM_TITLE=System.currentTimeMillis()+"";
        requestData.PROFILE_PIC=PROFILE_PIC;
        requestData.DEL_FILE=DEL_FILE;
//        requestData.FILE = uploadFilePath;
        requestData.CM_MEAL = data==null ? "" : data.CM_MEAL;

        HashMap<String,String> params = new HashMap<>();
        params.put("DOCNO","DB002");
        params.put("CM_CONTENT",requestData.CM_CONTENT);
        params.put("CM_GUBUN",requestData.CM_GUBUN);
        params.put("CM_SEQ",requestData.CM_SEQ);
        params.put("SEQ",requestData.SEQ);
        params.put("CM_TAG",requestData.CM_TAG);
        params.put("CM_TITLE",requestData.CM_TITLE);
        params.put("PROFILE_PIC",requestData.PROFILE_PIC);
        params.put("DEL_FILE",requestData.DEL_FILE);
        params.put("CM_MEAL",requestData.CM_MEAL);



        HttpAsyncFileTask33 rssTask = new HttpAsyncFileTask33(getString(R.string.community_upload_url), new HttpAsyncTaskInterface() {
            @Override
            public void onPreExecute() {
                ((BaseActivityMedicare)getContext()).showProgress();
            }
            @Override
            public void onPostExecute(String data) {
            }
            @Override
            public void onError() {
                ((BaseActivityMedicare)getContext()).hideProgress();
                CDialog.showDlg(getContext(), getString(R.string.text_network_data_send_fail));
            }
            @Override
            public void onFileUploaded(String result) {

                String errorMessage="";
                Log.d(TAG,"┌-------------------------------- Receive::Tr_DB002 --------------------------------┐");
                Log.d(TAG,"\n\n"+result);
                Log.d(TAG,"\n\n└-------------------------------- Receive::Tr_DB002 --------------------------------┘");
                try {
                    BaseData resObj = new Gson().fromJson(result, Tr_DB002.class);
                    if (resObj instanceof Tr_DB002) {
                        try {
                            final Tr_DB002 resultData = (Tr_DB002) resObj;


                            switch (resultData.RESULT_CODE) {
                                case ReceiveDataCode.DB002_SUCCESS:

                                    int point = 0;

                                    try{
                                        point = Integer.parseInt(resultData.CM_POINT);
                                    }catch (Exception e){
                                        point = 0;
                                    }

                                    if(point>0){
                                        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_get_point, null);
//                                        ((TextView)view.findViewById(R.id.point)).setText(point+"P");
                                        String point_msg = String.format(Locale.US,getString(R.string.comm_dialog_message_7),resultData.CM_POINT);
//                                        setMessageColor(((TextView)view.findViewById(R.id.point_msg)),point_msg, 0,  point_msg.indexOf(")")+1);
                                        final CDialog dlg = CDialog.showDlg(getContext(), getString(R.string.comm_dialog_message_6)+point_msg).setPointTvView(resultData.CM_POINT+"P");

//                                        view.findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View v) {
//                                                dlg.dismiss();
//                                            }
//                                        });
                                        dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialog) {
                                                lastAction(data,resultData);
                                            }
                                        });


                                    }else{
                                        lastAction(data,resultData);
                                    }

                                    break;
                                case ReceiveDataCode.DB002_ERROR_NODATA:
                                    errorMessage = getResources().getString(R.string.comm_error_db002_4444);
                                    break;
                                case ReceiveDataCode.DB002_ALREADY:

                                    errorMessage = getResources().getString(R.string.comm_error_db002_5555);

                                    break;
                                case ReceiveDataCode.DB002_ERROR_DATANEED:

                                    errorMessage = getResources().getString(R.string.comm_error_db002_6666);

                                    break;
                                case ReceiveDataCode.DB002_ERROR_NOMEMBERDATA:

                                    errorMessage = getResources().getString(R.string.comm_error_db002_8888);

                                    break;
                                case ReceiveDataCode.DB002_ERROR_ETC:

                                    errorMessage = getResources().getString(R.string.comm_error_db002_9999);

                                    break;
                                default:

                                    errorMessage = getResources().getString(R.string.comm_error_db002)+"\ncode:1";

                                    break;
                            }

                        }catch (Exception e){
                            errorMessage = getResources().getString(R.string.comm_error_db002)+"\ncode:2";
                        }
                    } else {
                        errorMessage = getResources().getString(R.string.comm_error_db002)+"\ncode:3";
                    }
                }catch (Exception e){
                    errorMessage = getResources().getString(R.string.comm_error_db002)+"\ncode:4";
                }

                if(!errorMessage.equals("")){
                    CDialog.showDlg(getContext(), errorMessage);
                }

                ((BaseActivityMedicare)getContext()).hideProgress();
            }
        });

        rssTask.setParam(ProviderUtil.getOutputFile(outputFileUri),"FILE",params);
        Log.d(TAG,"┌-------------------------------- Send::Tr_DB002 --------------------------------┐");
        Log.d(TAG,"\n\n"+params+"\n"+ProviderUtil.getOutputFile(outputFileUri));
        Log.d(TAG,"\n\n└-------------------------------- Send::Tr_DB002 --------------------------------┘");
        rssTask.execute();
    }

    private void lastAction(final CommunityListViewData data, final Tr_DB002 resultData) {
        if(data!=null&&data.ISSHARE){
            //공유하기로 글 작성 완료시

            CDialog dig =  CDialog.showDlg(getContext(), getResources().getString(R.string.comm_dialog_message_5),false)
                    .setOkButton(getResources().getString(R.string.comm_check_comment), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(resultData.OCM_SEQ!=null&&!resultData.OCM_SEQ.equals("")) {
                                getListFromServer(resultData.OCM_SEQ);
                            }else{
                                exitActivity();
                            }
                        }
                    })
                    .setNoButton(getResources().getString(R.string.text_confirm), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            exitActivity();
                        }
                    });

            dig.setBackgroundOkBtn(R.drawable.draw_alert_no_btn);
            dig.setBackgroundNoBtn(R.drawable.draw_alert_ok_btn);
            dig.setTextColorOkBtn(getResources().getColor(R.color.x143_144_158));
            dig.setTextColorNoBtn(getResources().getColor(R.color.colorWhite));

        }else{
            exitActivity();
        }
    }

    private void setMessageColor(TextView view,String message, int start, int end){
        SpannableStringBuilder sp = new SpannableStringBuilder(message);
        sp.setSpan( new ForegroundColorSpan(getResources().getColor(R.color.x0_22_85)),start,end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        view.setText(sp);
    }


    private void exitActivity() {
        getActivity().setResult(RESULT_OK, null);
        ((NewActivity) getContext()).setonKeyBackCatchListener(null);
        getActivity().finish();
    }


    private void getListFromServer(final String CM_SEQ) {
        final Tr_DB007.RequestData requestData = new Tr_DB007.RequestData();
        boolean isShowProgress=true;

        requestData.SEQ = UserInfo.getLoginInfo().mber_sn;
        requestData.CM_SEQ = CM_SEQ;


        MediNewNetworkModule.doApi(getContext(), new Tr_DB007(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";


                if (responseData instanceof Tr_DB007) {
                    Tr_DB007 data = (Tr_DB007)responseData;
                    try {

                        switch (data.RESULT_CODE){
                            case ReceiveDataCode.DB007_SUCCESS:
                                CommunityListViewData communityListViewData = new CommunityListViewData();
                                communityListViewData.RCNT = data.RCNT;
                                communityListViewData.HCNT = data.HCNT;
                                communityListViewData.REGDATE =data.REGDATE;
                                communityListViewData.CM_TITLE = data.CM_TITLE;
                                communityListViewData.NICK = data.NICK;
                                communityListViewData.CM_SEQ = CM_SEQ;
                                communityListViewData.CM_IMG1="";
                                communityListViewData.PROFILE_PIC = data.PROFILE_PIC;
                                communityListViewData.CM_CONTENT = data.CM_CONTENT;
                                communityListViewData.CM_TAG = data.CM_TAG;
                                communityListViewData.MYHEART = data.MYHEART;
                                communityListViewData.MBER_SN = data.OSEQ;
                                communityListViewData.MBER_GRAD = data.MBER_GRAD;
                                communityListViewData.CM_GUBUN = data.CM_GUBUN;
                                communityListViewData.CM_MEAL = data.CM_MEAL;
                                NewActivity.moveToCommentPage(CommunityWriteFragment.this,communityListViewData,false,getString(R.string.comm_pre),communityListViewData.CM_GUBUN);
                                exitActivity();
                                break;
                            case ReceiveDataCode.DB007_ERROR_NOHAVE:
                                errorMessage = getResources().getString(R.string.comm_error_db007_4444);
                                break;
                            case ReceiveDataCode.DB007_ERROR_NOMEMBER:
                                errorMessage = getResources().getString(R.string.comm_error_db007_6666);
                                break;
                            case ReceiveDataCode.DB007_ERROR_ETC:
                                errorMessage = getResources().getString(R.string.comm_error_db007_9999);
                                break;
                            default:
                                errorMessage = getResources().getString(R.string.comm_error_db007)+"\ncode:1";
                                break;
                        }
                    } catch (Exception e) {
                        CLog.e(e.toString());
                        errorMessage = getResources().getString(R.string.comm_error_db007)+"\ncode:2";
                    }
                }else{
                    errorMessage = getResources().getString(R.string.comm_error_db007)+"\ncode:3";
                }

                if(!errorMessage.equals("")){
                    CDialog.showDlg(getContext(), errorMessage).setOkButton(getString(R.string.comm_confirm), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            exitActivity();
                        }
                    });
                }


            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                Log.e(TAG, response);
                exitActivity();
            }
        });
    }


    @Override
    public void onAttach(Context mContext){
        super.onAttach(mContext);
        ((NewActivity)mContext).setonKeyBackCatchListener(this);
    }

    @Override
    public void onCatch() {
        exitWritePage();
    }


    private void exitWritePage(){

        if(layout_image.getVisibility()==View.GONE&&edit_contents.getText().toString().equals("")){
            getActivity().finish();
        }else{
            CDialog.showDlg(getContext(), getResources().getString(R.string.comm_dialog_message_1))
                    .setOkButton(getResources().getString(R.string.comm_yes), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((NewActivity)getContext()).setonKeyBackCatchListener(null);
                            getActivity().finish();
                        }
                    })
                    .setNoButton(getResources().getString(R.string.comm_no), null);

        }

    }

    @Override
    public void onBackPressed(){
        exitWritePage();
    }

}
