package kr.co.hi.medicare.fragment.community.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.fragment.community.data.CommunityCommentData;
import kr.co.hi.medicare.fragment.community.data.CommunityCommentInfo;
import kr.co.hi.medicare.fragment.community.data.CommunitySearchesData;
import kr.co.hi.medicare.fragment.community.holder.CommunityCommentListViewHolder;


public class CommunitySearcherAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private View.OnClickListener onClickListener;
    private Context context;
    private ArrayList<CommunitySearchesData> data;

    private static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView btn_search;
        TextView word;
        ImageView btn_delete;

        public ViewHolder(View itemView) {
            super(itemView);
            btn_search = itemView.findViewById(R.id.btn_search);
            word = itemView.findViewById(R.id.word);
            btn_delete = itemView.findViewById(R.id.btn_delete);
        }

        public void SetView(CommunitySearchesData data){
            word.setText(data.word);
        }
    }


    public CommunitySearcherAdapter(Context context, View.OnClickListener onClickListener) {
        this.context = context;
        this.onClickListener = onClickListener;
        data = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_community_searches, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ViewHolder){
            if (data.size() > 0) {
                if (position < data.size()) {
                    //뷰홀더의 자원을 초기화//
                    final ViewHolder commViewHolder = (ViewHolder) holder;
                    commViewHolder.btn_delete.setTag(data.get(position));
                    commViewHolder.btn_search.setTag(data.get(position));
                    commViewHolder.word.setTag(data.get(position));
                    commViewHolder.SetView(data.get(position));

                    commViewHolder.word.setOnClickListener(onClickListener);
                    commViewHolder.btn_search.setOnClickListener(onClickListener);
                    commViewHolder.btn_delete.setOnClickListener(onClickListener);
                    return;
                }

//            position -= humanData.getHumanDataList().size();
//              position -= data.getSize();
            }
            throw new IllegalArgumentException("invalid position");
        }


    }

    @Override
    public int getItemCount() {
        if (data==null) {
            return 0;
        }
        return data.size();
    }


    public void delItemAll() {
        if(data!=null){
            this.data.clear();
            notifyDataSetChanged();
        }
    }

    public void addAllItem(List<CommunitySearchesData> data) {
        if(this.data==null)
            this.data = new ArrayList<>();

        if(data!=null){
            this.data.clear();
            this.data.addAll(data);
            notifyDataSetChanged();
        }
    }

    public void addItem(CommunitySearchesData data) {
        if(this.data==null)
            this.data = new ArrayList<>();

        if(data!=null){
            ArrayList<CommunitySearchesData> datas = new ArrayList<>();
            datas.add(data);
            datas.addAll(this.data);
            this.data.clear();
            this.data = datas;
            notifyDataSetChanged();
        }
    }


    public void delItem(CommunitySearchesData subdata) {
        if(this.data!=null){
            for(int i=0; i<this.data.size();i++){
                if(data.get(i).idx==subdata.idx){
                    data.remove(i);
                    notifyDataSetChanged();
                    break;
                }
            }
        }
    }

}
