package kr.co.hi.medicare.fragment.community.data;

import java.util.ArrayList;

// @JsonIgnoreProperties(ignoreUnknown = true)
public class CommunityDataInfo {
    private String AST_LENGTH;

    // 리스트.
    private ArrayList<CommunityListViewData> ADDR_MASS;
    // @JsonProperty(value = "AST_LENGTH")
    public String getAST_LENGTH() {
        return AST_LENGTH;
    }

    public void setAST_LENGTH(String AST_LENGTH) {
        this.AST_LENGTH = AST_LENGTH;
    }

    // @JsonProperty(value = "ADDR_MASS")
    public ArrayList<CommunityListViewData> getADDR_MASS() {
        return ADDR_MASS;
    }

    public void initData(){
        ADDR_MASS = new ArrayList<CommunityListViewData>();
    }

    public int getSize(){

        if(ADDR_MASS==null)
            initData();

        return ADDR_MASS.size();
    }

    public void setADDR_MASS(ArrayList<CommunityListViewData> ADDR_MASS) {
        this.ADDR_MASS = ADDR_MASS;
    }
}