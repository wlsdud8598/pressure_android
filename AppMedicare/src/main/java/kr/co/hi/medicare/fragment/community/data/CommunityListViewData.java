package kr.co.hi.medicare.fragment.community.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**


 */
//// @JsonIgnoreProperties(ignoreUnknown = true)
public class CommunityListViewData implements Serializable {







    @SerializedName("RNUM") //OK?
    public String RNUM;
    @SerializedName("RCNT") //OK
    public String RCNT;
    @SerializedName("HCNT") //OK
    public String HCNT;
    @SerializedName("REGDATE") //ok
    public String REGDATE;
    @SerializedName("CM_TITLE") //ok
    public String CM_TITLE;
    @SerializedName("NICK")//ok
    public String NICK;
    @SerializedName("CM_SEQ")//ok
    public String CM_SEQ;
    @SerializedName("TPAGE") //ok
    public String TPAGE;
    @SerializedName("CM_IMG1")
    public String CM_IMG1;
    @SerializedName("PROFILE_PIC") //ok
    public String PROFILE_PIC;

    @SerializedName("CM_CONTENT") //ok
    public String CM_CONTENT;

    @SerializedName("CM_TAG") //ok
    public List<String> CM_TAG;

    @SerializedName("MYHEART") //ok
    public String MYHEART; //좋아요 클릭유무

    @SerializedName("MBER_SN") //받아야함..
    public String MBER_SN;

    @SerializedName("MBER_GRAD") //받아야함..
    public String MBER_GRAD;

    @SerializedName("CM_GUBUN")
    public String CM_GUBUN;

    @SerializedName("CM_MEAL")
    public String CM_MEAL;

    public boolean ISDETAIL; //자세히보기 클릭 유무
    public boolean ISNODATA; //게시글상세보기 시 데이터가 없을 경우 요청
    public List<String> CM_MEAL_LIST; //글쓰기 공유하기 시에만 로컬 사용
    public boolean ISSHARE; // 공유하기 시 TRUE
}