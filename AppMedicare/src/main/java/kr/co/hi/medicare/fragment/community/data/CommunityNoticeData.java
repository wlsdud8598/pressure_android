package kr.co.hi.medicare.fragment.community.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommunityNoticeData {
    @Expose
    @SerializedName("REGDATE")
    public String REGDATE;
    @Expose
    @SerializedName("MSG")
    public String MSG;
    @Expose
    @SerializedName("CM_SEQ")
    public String CM_SEQ;
    @Expose
    @SerializedName("ALM_SEQ")
    public String ALM_SEQ;
    @Expose
    @SerializedName("PROFILE_PIC")
    public String PROFILE_PIC;
    @Expose
    @SerializedName("MBER_GRAD")
    public String MBER_GRAD;
    @Expose
    @SerializedName("CM_GUBUN")
    public String CM_GUBUN;
    @Expose
    @SerializedName("MBER_SN")
    public String MBER_SN;
    @Expose
    @SerializedName("NICK")
    public String NICK;



    public int IDX;

}
