package kr.co.hi.medicare.fragment.community.data;

public interface ReceiveDataCode {



// 0000 : 입력(수정)성공
// 4444 : 수정 시 수정글 존재하지 않음.
// 5555 : 동일한게시물존재
// 6666 : 필수 값 입력 없음.(커뮤니티 구분,제목,내용)
// 8888 : 회원정보없음
// 9999 : 기타오류
    final String DB002_SUCCESS="0000";
    final String DB002_ERROR_NODATA="4444";
    final String DB002_ALREADY="5555";
    final String DB002_ERROR_DATANEED="6666";
    final String DB002_ERROR_NOMEMBERDATA="8888";
    final String DB002_ERROR_ETC="9999";

// 0000 : 입력성공
// 4444 : 이미등록된 댓글
// 6666 : 회원이 존재하지 않음
// 7777 : 사용중지회원
// 9999 : 기타오류
    final String DB004_SUCCESS="0000";
    final String DB004_ERROR_ALREADY="4444";
    final String DB004_ERROR_NOMEMBER="6666";
    final String DB004_ERROR_REJECTMEMBER="7777";
    final String DB004_ERROR_ETC="9999";

// 0000 : 삭제성공
// 4444 : 삭제실패
// 6666 : 회원이 존재하지 않음
// 9999 : 기타오류
    final String DB005_SUCCESS="0000";
    final String DB005_ERROR_FAIL="4444";
    final String DB005_ERROR_NOMEMBER="6666";
    final String DB005_ERROR_ETC="9999";

// 0000 : 입력성공
// 1000 : 삭제성공
// 6666 : 회원이 존재하지 않음
// 9999 : 기타오류
    final String DB006_SUCCESS="0000";
    final String DB006_SUCCESS_DELETE="1000";
    final String DB006_ERROR_NOMEMBER="6666";
    final String DB006_ERROR_ETC="9999";

// 0000 : 조회성공
// 4444 : 등록된 글이 없습니다.
// 6666 : 회원이 존재하지 않음
// 9999 : 기타오류
    final String DB007_SUCCESS="0000";
    final String DB007_ERROR_NOHAVE="4444";
    final String DB007_ERROR_NOMEMBER="6666";
    final String DB007_ERROR_ETC="9999";


// 0000 : 닉네임 수정 성공
// 1000 : 닉네임조회 성공
// 4444 : 이미 등록된 닉네임
// 5555 : 닉네임 수정 실패
// 7777 : 사용중지 회원
// 9999 : 기타오류
    final String DB008_SUCCESS="0000";
    final String DB008_SUCCESS_QUERY="1000";
    final String DB008_ERROR_ALREADY="4444";
    final String DB008_ERROR_FAIL="5555";
    final String DB008_ERROR_REJECTMEMBER="7777";
    final String DB008_ERROR_ETC="9999";


// 0000 : 삭제성공
// 4444 : 삭제 실패(해당글이 없음)
// 6666 : 회원이 존재하지 않음
// 7777 : 사용중지 회원
// 9999 : 기타오류
    final String DB011_SUCCESS="0000";
    final String DB011_ERROR_NOWRITER="4444";
    final String DB011_ERROR_NOMEMBER="6666";
    final String DB011_ERROR_REJECTMEMBER="7777";
    final String DB011_ERROR_ETC="9999";




    final String DB012_SUCCESS="0000";

    final String DB013_SUCCESS="0000";
    final String DB013_ERROR_ETC="9999";

    final String DB014_SUCCESS="0000";
    final String DB014_ERROR_ETC="9999";


//    0000:성공
//    9999:기타오류
    final String DB015_SUCCESS="0000";
    final String DB015_ERROR_ETC="9999";


//    0000:성공
//    4444:실패
    final String MBER_LOAD_MYPAGE_SUCCESS="0000";
    final String MBER_LOAD_MYPAGE_ERROR_FAIL="4444";


//    0000 : 모션 등록성공
//4444 : 필수 입력값이 없습니다.(등록실패)
//            6666 : 회원이 존재하지 않음
//9999 : 기타오류

    final String DY002_SUCCESS="0000";
    final String DY002_ERROR_NOWRITER="4444";
    final String DY002_ERROR_NOMEMBER="6666";
    final String DY002_ERROR_ETC="9999";
}
