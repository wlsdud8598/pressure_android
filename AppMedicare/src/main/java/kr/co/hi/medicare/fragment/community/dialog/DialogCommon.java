package kr.co.hi.medicare.fragment.community.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.net.hwNet.BaseData;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.fragment.community.data.ReceiveDataCode;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_DB008;
import kr.co.hi.medicare.util.CLog;

public class DialogCommon extends Dialog {

    private TextView btn_cancel,btn_confirm, disease, message, btn_modify,btn_delete;
    private RelativeLayout btn_private, btn_open,btn_kakao,btn_sns;
    private ImageView check_box_1, check_box_2;
    public EditText nick,name;
    private boolean isConfirm=false;

    private TextView nickname,message_1,message_2;

    private UpdateProfile updateProfile =null;


    private boolean mIsAutoDismiss = true;
    private static DialogCommon instance;
    private String DISEASE_OPEN,DISEASE_NM,SEQ,NICK_ORI,NAME_ORI,NAME;
    public static int layout=R.layout.dialog_comm_nick;

    public static DialogCommon showDialog(Context context, String NICK_ORI, String DISEASE_OPEN, String DISEASE_NM, String SEQ, UpdateProfile updateProfile){
        if(layout!=R.layout.dialog_comm_nick)
            instance=null;

        layout=R.layout.dialog_comm_nick;
        DialogCommon dialog = getInstance(context);
        dialog.updateProfile = updateProfile;
        dialog.DISEASE_NM = DISEASE_NM;
        dialog.NICK_ORI = NICK_ORI;
        dialog.DISEASE_OPEN = DISEASE_OPEN;
        dialog.SEQ=SEQ;
        dialog.disease.setText(context.getString(R.string.comm_reg_my_disease)+" "+(DISEASE_NM.equals("") ? "질환 없음" : DISEASE_NM));
        dialog.setCheckBox();
        dialog.setMessageBold(context.getString(R.string.comm_reg_nick),context.getString(R.string.comm_reg_nick).indexOf("닉"), context.getString(R.string.comm_reg_nick).indexOf("닉")+3, R.id.message);
        if(NICK_ORI!=null&&!NICK_ORI.equals("")){
            dialog.nick.setHint(NICK_ORI);
        }

        return dialog;
    }

    public static DialogCommon showDialogNick(Context context, String NICK_ORI, String SEQ, UpdateProfile updateProfile,String DISEASE_OPEN){
        if(layout!=R.layout.dialog_modify_nick)
            instance=null;

        layout=R.layout.dialog_modify_nick;
        DialogCommon dialog = getInstance(context);
        dialog.DISEASE_OPEN = DISEASE_OPEN;
        dialog.updateProfile = updateProfile;
        dialog.NICK_ORI = NICK_ORI;
        dialog.SEQ=SEQ;
        if(NICK_ORI!=null&&!NICK_ORI.equals("")){
            dialog.nick.setHint(NICK_ORI);
        }

        return dialog;
    }


    public static DialogCommon showDialogIntro(Context context, String NICK_New){
        if(layout!=R.layout.dialog_comm_intro)
            instance=null;

        layout=R.layout.dialog_comm_intro;
        DialogCommon dialog = getInstance(context);

//        dialog.nickname.setText(NICK_New.trim()+" "+context.getResources().getString(R.string.comm_reg_message_3));
        dialog.setMessageColor(dialog.nickname,NICK_New.trim()+" "+context.getResources().getString(R.string.comm_reg_message_3),0, NICK_New.trim().length());
        dialog.setMessageBold(context.getString(R.string.comm_reg_message_4),context.getString(R.string.comm_reg_message_4).indexOf("포"), context.getString(R.string.comm_reg_message_4).indexOf("포")+3, R.id.message_1);
        dialog.addMessageBold(context.getString(R.string.comm_reg_message_5)+"    ", context.getString(R.string.comm_reg_message_5).indexOf("게"),context.getString(R.string.comm_reg_message_5).indexOf("게")+6, R.id.message_2);
        dialog.addMessageBold(context.getString(R.string.comm_reg_message_6), context.getString(R.string.comm_reg_message_6).indexOf("댓"),context.getString(R.string.comm_reg_message_6).indexOf("댓")+5, R.id.message_2);

        return dialog;
    }


    public static DialogCommon showDialogDuplication(Context context){
        if(layout!=R.layout.dialog_comm_duplicate_nick)
            instance=null;

        layout=R.layout.dialog_comm_duplicate_nick;
        DialogCommon dialog = getInstance(context);
        return dialog;
    }


    public static DialogCommon showDialogShare(Context context, View.OnClickListener cancel, View.OnClickListener kakao, View.OnClickListener sns){
        if(layout!=R.layout.dialog_comm_share)
            instance=null;

        layout=R.layout.dialog_comm_share;
        DialogCommon dialog = getInstance(context);

        dialog.btn_cancel.setOnClickListener(cancel);
        dialog.btn_kakao.setOnClickListener(kakao);
        dialog.btn_sns.setOnClickListener(sns);
        return dialog;
    }



    public static DialogCommon showDialogCommon(Context context, String btn_positive_text, String btn_negative_text, String dialog_message, View.OnClickListener btn_positive, View.OnClickListener btn_negative){
        if(layout!=R.layout.dialog_comm_common)
            instance=null;

        layout=R.layout.dialog_comm_common;

        DialogCommon dialog = getInstance(context);
        dialog.message.setText(dialog_message);
        dialog.btn_confirm.setOnClickListener(btn_positive);
        dialog.btn_cancel.setOnClickListener(btn_negative);
        dialog.btn_confirm.setText(btn_positive_text);
        dialog.btn_cancel.setText(btn_negative_text);
        return dialog;
    }

    public static DialogCommon showDialogSingleBtn(Context context, String message, String btn_name, View.OnClickListener btn_confirm){
        if(layout!=R.layout.dialog_comm_singlebtn)
            instance=null;

        layout=R.layout.dialog_comm_singlebtn;
        DialogCommon dialog = getInstance(context);
        dialog.message.setText(message);
        dialog.btn_confirm.setOnClickListener(btn_confirm);
        dialog.btn_confirm.setText(btn_name);
        return dialog;
    }



    public static DialogCommon showDialogEditor(Context context, View.OnClickListener btn_modify, View.OnClickListener btn_delete){
        if(layout!=R.layout.dialog_comm_editor)
            instance=null;

        layout=R.layout.dialog_comm_editor;
        DialogCommon dialog = getInstance(context);
        dialog.btn_modify.setOnClickListener(btn_modify);
        dialog.btn_delete.setOnClickListener(btn_delete);

        //click 저장
        kr.co.hi.medicare.component.OnClickListener ClickListener = new kr.co.hi.medicare.component.OnClickListener(null, null, context);


        //커뮤니티

        dialog.btn_modify.setOnTouchListener(ClickListener);
        dialog.btn_delete.setOnTouchListener(ClickListener);

        //코드부여
        dialog.btn_modify.setContentDescription(context.getString(R.string.btn_modify_comunity));
        dialog.btn_delete.setContentDescription(context.getString(R.string.btn_delete_comunity));

        return dialog;
    }








    private void setCheckBox(){
        switch (DISEASE_OPEN){
            case "N":
                check_box_1.setImageResource(R.drawable.commu_check_02);
                check_box_2.setImageResource(R.drawable.commu_check_01);
                break;
            default:
                check_box_1.setImageResource(R.drawable.commu_check_01);
                check_box_2.setImageResource(R.drawable.commu_check_02);
                break;
        }
    }

    private void setMessageColor(TextView view,String message, int start, int end){
        SpannableStringBuilder sp = new SpannableStringBuilder(message);
        sp.setSpan( new ForegroundColorSpan(getContext().getResources().getColor(R.color.x105_129_236)),start,end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        view.setText(sp);
    }


    private void setMessageBold(String message, int start, int end, int viewId){
        SpannableStringBuilder sp = new SpannableStringBuilder(message);
        sp.setSpan( new StyleSpan(Typeface.BOLD),start,end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ((TextView)findViewById(viewId)).setText(sp);
    }


    private void addMessageBold(String message, int start, int end, int viewId){
        SpannableStringBuilder sp = new SpannableStringBuilder(message);
        sp.setSpan( new StyleSpan(Typeface.BOLD),start,end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ((TextView)findViewById(viewId)).append(sp);
    }


    public static DialogCommon getInstance(Context context) {
        if (instance == null||instance.getContext()!=context) {
            instance = new DialogCommon(context);
        }

        instance.show();
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND|WindowManager.LayoutParams.FLAG_FULLSCREEN;
        lpWindow.dimAmount = 0.8f;
//        lpWindow.windowAnimations = R.style.PauseDialogAnimation;




        getWindow().setAttributes(lpWindow);
        setContentView(layout);

        switch (layout){
            case R.layout.dialog_comm_nick:
                setLayout();
                break;
            case R.layout.dialog_modify_nick:
                btn_cancel = (TextView) findViewById(R.id.btn_cancel);
                btn_confirm = (TextView) findViewById(R.id.btn_confirm);
                nick = (EditText) findViewById(R.id.nick);
                btn_confirm.setOnClickListener(onClickListener);
                btn_cancel.setOnClickListener(onClickListener);
                break;
            case R.layout.dialog_comm_duplicate_nick:
                btn_confirm.setOnClickListener(onClickListener);
                break;
            case R.layout.dialog_comm_intro:
                setLayoutIntro();
                break;
            case R.layout.dialog_comm_common:
                btn_cancel = (TextView) findViewById(R.id.btn_cancel);
                btn_confirm = (TextView) findViewById(R.id.btn_confirm);
                message = (TextView) findViewById(R.id.message);
                break;

            case R.layout.dialog_comm_editor:
                btn_modify = (TextView) findViewById(R.id.btn_modify);
                btn_delete = (TextView) findViewById(R.id.btn_delete);
                break;

            case R.layout.dialog_comm_share:
                btn_cancel = (TextView) findViewById(R.id.btn_cancel);
                btn_kakao = (RelativeLayout) findViewById(R.id.btn_kakao);
                btn_sns = (RelativeLayout) findViewById(R.id.btn_sns);
                break;
            case R.layout.dialog_comm_singlebtn:
                btn_confirm = (TextView) findViewById(R.id.btn_confirm);
                message = (TextView) findViewById(R.id.message);
                break;
        }
    }

    public DialogCommon(Context context) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
    }

    /*
     * Layout
     */
    private void setLayout() {
        btn_cancel = (TextView) findViewById(R.id.btn_cancel);
        btn_confirm = (TextView) findViewById(R.id.btn_confirm);
        disease = (TextView) findViewById(R.id.disease);
        message = (TextView) findViewById(R.id.message);
        btn_private = (RelativeLayout) findViewById(R.id.btn_private);
        btn_open = (RelativeLayout) findViewById(R.id.btn_open);
        check_box_1 = (ImageView) findViewById(R.id.check_box_1);
        check_box_2 = (ImageView) findViewById(R.id.check_box_2);
        nick = (EditText) findViewById(R.id.nick);
        btn_confirm.setOnClickListener(onClickListener);
        btn_cancel.setOnClickListener(onClickListener);
        btn_private.setOnClickListener(onClickListener);
        btn_open.setOnClickListener(onClickListener);
    }

    private void setLayoutIntro() {
        btn_confirm = (TextView) findViewById(R.id.btn_confirm);
        btn_confirm.setOnClickListener(onClickListener);
        message_1 = (TextView) findViewById(R.id.message_1);
        message_2 = (TextView) findViewById(R.id.message_2);
        nickname = (TextView) findViewById(R.id.nickname);
    }



    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()){
                case R .id.btn_cancel:
                    dismiss();
                    break;
                case R .id.btn_confirm:

                    switch (layout){
                        case R.layout.dialog_comm_nick:

                            String nickname = nick.getText().toString();

                            if(NICK_ORI.equals("")&&nickname.equals("")&&nickname.trim().equals("")){


                            }else{
                                dismiss();
                                registerNickname(NICK_ORI,nickname, DISEASE_OPEN, SEQ,DISEASE_NM);
                            }
                            break;
                        case R.layout.dialog_modify_nick:

                            String nickname2 = nick.getText().toString();

                            if(NICK_ORI.equals("")&&(nickname2.equals("")||nickname2.trim().equals(""))){


                            }else{
                                dismiss();
                                registerNickname(NICK_ORI,nickname2, DISEASE_OPEN, SEQ,"");
                            }
                            break;
                        case R.layout.dialog_comm_duplicate_nick:
                        case R.layout.dialog_comm_intro:
                            dismiss();
                            break;

                    }
                    break;
                case R .id.btn_private:
                    DISEASE_OPEN="N";
                    setCheckBox();
                    break;
                case R .id.btn_open:
                    DISEASE_OPEN="Y";
                    setCheckBox();
                    break;
            }
        }
    };


    private void disableKeyboard(EditText editText) {

        InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    private void enableKeyboard(EditText editText) {
//    InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.showSoftInput(editText, 0);


        InputMethodManager ime = null;
        ime = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        ime.showSoftInputFromInputMethod(editText.getWindowToken(), InputMethodManager.SHOW_FORCED);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
}


    /**
     * 닉네임 등록 변경 요청 DB008
     * @param NICK 키워드
     */
    private void registerNickname(final String Nick_Ori,final String NICK, final String DISEASE_OPEN, final String SEQ,final String DISEASE_NM) {
        final Tr_DB008.RequestData requestData = new Tr_DB008.RequestData();
        boolean isShowProgress=true;
        requestData.NICK = NICK;
        requestData.DISEASE_OPEN = DISEASE_OPEN;
        requestData.SEQ = SEQ;

        MediNewNetworkModule.doApi(getContext(), new Tr_DB008(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";
                if (responseData instanceof Tr_DB008) {
                    Tr_DB008 data = (Tr_DB008)responseData;
                    try {

                        switch (data.RESULT_CODE){
                            case ReceiveDataCode.DB008_SUCCESS:

                                CommonFunction.setUpdateLoginInfo(getContext());

//                                if(Nick_Ori==null||Nick_Ori.equals("")){
//                                    instance = showDialogIntro(getContext(),NICK);
//                                }else{
//
//                                }

                                if(NICK.equals("")){
                                    updateProfile.updateProfile(Nick_Ori,data.ODISEASE_OPEN,data.DISEASE_NM);
                                }else{
                                    updateProfile.updateProfile(data.ONICK.trim(),data.ODISEASE_OPEN,data.DISEASE_NM);
                                }


                                break;
                            case ReceiveDataCode.DB008_SUCCESS_QUERY:
                                break;
                            case ReceiveDataCode.DB008_ERROR_ALREADY:


                                CDialog dig = CDialog.showDlg(getContext(), getContext().getResources().getString(R.string.comm_reg_message_1)+"\n"+getContext().getResources().getString(R.string.comm_reg_message_2) );
                                dig.setOnDismissListener(new OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        updateProfile.updateProfile("","","");

                                    }
                                });


                                break;
                            case ReceiveDataCode.DB008_ERROR_FAIL:
                                errorMessage = getContext().getResources().getString(R.string.comm_error_db008_5555);

                                break;
                            case ReceiveDataCode.DB008_ERROR_REJECTMEMBER:
                                errorMessage = getContext().getResources().getString(R.string.comm_error_db008_7777);
                                break;
                            case ReceiveDataCode.DB008_ERROR_ETC:
                                errorMessage = getContext().getResources().getString(R.string.comm_error_db008_9999);
                                break;
                            default:
                                errorMessage = getContext().getResources().getString(R.string.comm_error_db008)+"\ncode:1";
                                break;
                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                        errorMessage = getContext().getResources().getString(R.string.comm_error_db008)+"\ncode:2";
                    }
                }else{
                    errorMessage = getContext().getResources().getString(R.string.comm_error_db008)+"\ncode:3";
                }


                if(!errorMessage.equals("")){
//                    instance = showDialogSingleBtn(getContext(), errorMessage, getContext().getResources().getString(R.string.comm_confirm), new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            instance.dismiss();
//                        }
//                    });

                    CDialog.showDlg(getContext(), errorMessage);
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CLog.e(response);
            }
        });
    }


    public interface UpdateProfile{
        void updateProfile(String NICK,String DISEASE_OPEN,String DISEASE_NM);
    }

    public void setIsConfirm(boolean isConfirm){
        this.isConfirm = isConfirm;
    }

    public boolean getIsConfirm(){

        return isConfirm;
    }

    public String getName(){

        return NAME;
    }

}

