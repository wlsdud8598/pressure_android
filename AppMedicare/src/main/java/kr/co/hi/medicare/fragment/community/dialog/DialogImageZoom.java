package kr.co.hi.medicare.fragment.community.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import kr.co.hi.medicare.R;

public class DialogImageZoom extends Dialog {

    private ImageView image, btn_close;
    private TextView title;
    private static DialogImageZoom instance;
    String nick, imagepath;
    private Context context;

//    public static DialogImageZoom showDialogCommon(Context context){
//        DialogImageZoom dialog = getInstance(context);
//        dialog.title.setText(nick);
////        if(!imagepath.equals("")){
////            Glide
////                    .with(context)
////                    .load(imagepath)
////                    .into(dialog.image);
////        }
//        return dialog;
//    }

//    public static DialogImageZoom getInstance(Context context) {
//        if (instance == null||instance.getContext()!=context) {
//            instance = new DialogImageZoom(context);
//        }
//
//        instance.show();
//        return instance;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_community_image);

        image = findViewById(R.id.image);
        btn_close = findViewById(R.id.btn_close);
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();

            }
        });
        title = findViewById(R.id.title);


        if(!imagepath.equals("")){
            Glide
                    .with(getContext())
                    .load(imagepath)
                    .into(image);
        }

//        Glide.with(getContext()).asBitmap().load(imagepath).into(new SimpleTarget<Bitmap>() {
//            @Override
//            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                image.setImageBitmap(resource);
//            }
//        });

        title.setText(nick);

    }

    public DialogImageZoom(Context context,String nick, String imagepath) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen
        );
        this.nick = nick;
        this.imagepath = imagepath;
        this.context = context;
    }

}

