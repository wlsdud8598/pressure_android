package kr.co.hi.medicare.fragment.community.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.NumberPicker;
import android.widget.TextView;

import kr.co.hi.medicare.R;

public class DialogMeberInfoPicker extends Dialog {

    public final static int TYPE_SEX=0;
    public final static int TYPE_HEIGHT=1;
    public final static int TYPE_WEIGHT=2;
    public final static int TYPE_BIRTHDAY=3;
    private int TYPE = TYPE_SEX;
    private int layout = R.layout.dialog_info_picker;
    private boolean isConfirm=false;

    private static DialogMeberInfoPicker instance;
    private Context context;
    private NumberPicker picker1;
    private int defaultValue=0;


    public static DialogMeberInfoPicker getInstance(Context context) {
        if (instance == null||instance.getContext()!=context) {
            instance = new DialogMeberInfoPicker(context);
        }

        instance.show();
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND|WindowManager.LayoutParams.FLAG_FULLSCREEN;
        lpWindow.dimAmount = 0.8f;
        lpWindow.windowAnimations = R.style.PauseDialogAnimation;
        getWindow().setAttributes(lpWindow);

        setContentView(layout);

        switch (layout){
            case R.layout.dialog_info_picker:
                findViewById(R.id.btn_confirm).setOnClickListener(onClickListener);
                picker1 = findViewById(R.id.picker1);
                break;
            default:
//                picker1 = findViewById(R.id.picker1);
//                picker2 = findViewById(R.id.picker2);
//                picker3 = findViewById(R.id.picker3);
                break;
        }


    }

    private void initView() {
        switch (TYPE){
            case TYPE_SEX:
                picker1.setMinValue(0);
                picker1.setMaxValue(1);
                picker1.setDisplayedValues(new String[]{context.getResources().getString(R.string.info_change_sex_man),context.getResources().getString(R.string.info_change_sex_woman)});
                picker1.setValue(defaultValue);
                break;
            case TYPE_HEIGHT:
                picker1.setMinValue(90);
                picker1.setMaxValue(230);
                picker1.setValue(defaultValue);
                break;
            case TYPE_WEIGHT:
                picker1.setMinValue(20);
                picker1.setMaxValue(200);
                picker1.setValue(defaultValue);
                break;
            case TYPE_BIRTHDAY:
//                picker1.setMinValue(1900);
//                picker1.setMinValue(2100);
//                picker1.setValue(defaultValue);
//
//                picker2.setMinValue(1);
//                picker2.setMinValue(12);
//                picker2.setValue(defaultValue1);
//
//                picker3.setMinValue(1);
//                picker3.setMinValue(31);
//                picker3.setValue(defaultValue2);

                break;
        }

    }


    public static DialogMeberInfoPicker showDialog(Context context, int defaultValue, int type ){
        DialogMeberInfoPicker dialog = getInstance(context);
        dialog.defaultValue = defaultValue;
        dialog.TYPE = type;
        dialog.initView();
        return dialog;
    }


    public DialogMeberInfoPicker(Context context) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        this.context = context;
    }


    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()){
                case R .id.btn_confirm:
                    setIsConfirm(true);
                    dismiss();

                    break;
            }
        }
    };

    public NumberPicker getPicker(){

        return picker1;
    }

    public void setIsConfirm(boolean isConfirm){
        this.isConfirm = isConfirm;
    }

    public boolean getIsConfirm(){

        return isConfirm;
    }

}

