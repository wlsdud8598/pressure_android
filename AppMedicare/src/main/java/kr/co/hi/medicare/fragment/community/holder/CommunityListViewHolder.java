package kr.co.hi.medicare.fragment.community.holder;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.net.hwNet.BaseData;
import com.kakao.kakaolink.v2.KakaoLinkResponse;
import com.kakao.kakaolink.v2.KakaoLinkService;
import com.kakao.network.ErrorResult;
import com.kakao.network.callback.ResponseCallback;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import co.lujun.androidtagview.TagContainerLayout;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.fragment.community.data.ReceiveDataCode;
import kr.co.hi.medicare.fragment.community.data.UpdateDataFunction;
import kr.co.hi.medicare.fragment.community.dialog.DialogCommon;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_DB006;
import kr.co.hi.medicare.net.data.Tr_DB011;
import kr.co.hi.medicare.tempfunc.TemporaryFunction;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.util.KakaoUtil;

public class CommunityListViewHolder extends RecyclerView.ViewHolder{

    public static final int TYPE_LIST=0;
    public static final int TYPE_VIEW=1;
    public static final int TYPE_VIEW_GONE=2;
    private DialogCommon dialog;

    public ImageView profile,profile_teduri; // 회원사진
    public TextView nick; // 닉네임
    public TextView date; // 날짜
    public LinearLayout editor; // 편집하기
    public ImageView image; // 게시이미지
    public TextView text; // 게시글내용
    public TextView btn_detail; // 자세히보기
    public TagContainerLayout tag; // 태그
    public ImageView btn_like; // 좋아요
    public ImageView btn_comment; // 댓글 페이지로 이동
    public ImageView btn_share; // 공유하기
    public TextView like; //좋아요 수
    public TextView comment; //댓글 수
    public ImageView comment_all; //댓글모두보기
    public TextView cm_meal;
    public LinearLayout layout_etc;
    public RelativeLayout layout_btns;
    public LinearLayout layout_whole;

    private UpdateDataFunction updateDataFunction;
    private String SEQ="";
    private Context context;
    private String CM_SEQ="";
    private String CM_GUBUN="";
    private Fragment fragment;


    private setAction setAction;

    public CommunityListViewHolder(View view, UpdateDataFunction updateDataFunction, String SEQ, Context context, String CM_GUBUN, Fragment fragment, setAction setAction){
        super(view);
        this.updateDataFunction = updateDataFunction;
        this.context = context;
        this.SEQ = SEQ;
        this.CM_GUBUN = CM_GUBUN;
        this.fragment = fragment;
        this.setAction = setAction;

        profile_teduri = view.findViewById(R.id.profile_teduri);
        btn_detail = view.findViewById(R.id.btn_detail);
        profile = view.findViewById(R.id.profile);
        editor = view.findViewById(R.id.editor);
        image = view.findViewById(R.id.image);
        btn_like = view.findViewById(R.id.btn_like);
        btn_comment = view.findViewById(R.id.btn_comment);
        btn_share = view.findViewById(R.id.btn_share);
        cm_meal = view.findViewById(R.id.cm_meal);
        nick = view.findViewById(R.id.nick);
        date = view.findViewById(R.id.date);
        text = view.findViewById(R.id.text);
        tag = view.findViewById(R.id.tag); //별도구현필요

        like = view.findViewById(R.id.like);
        comment = view.findViewById(R.id.comment);
        comment_all = view.findViewById(R.id.comment_all);
        layout_etc = view.findViewById(R.id.layout_etc);
        layout_btns = view.findViewById(R.id.layout_btns);
        layout_whole = view.findViewById(R.id.layout_whole);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(null, view, context);


        //커뮤니티
        btn_like.setOnTouchListener(ClickListener);
        btn_comment.setOnTouchListener(ClickListener);
        comment_all.setOnTouchListener(ClickListener);
        btn_share.setOnTouchListener(ClickListener);


        //코드부여
        btn_like.setContentDescription(context.getString(R.string.btn_like));
        btn_comment.setContentDescription(context.getString(R.string.btn_comment));
        comment_all.setContentDescription(context.getString(R.string.btn_comment));
        btn_share.setContentDescription(context.getString(R.string.btn_share_community));
    }

    public void setTextRCNT(String RCNT){
        comment.setText(RCNT+context.getResources().getText(R.string.comm_unit));
    }


    public void SetView(final CommunityListViewData data,int type,int position){
        this.CM_SEQ = data.CM_SEQ;
        btn_share.setTag(R.id.comm_main_share,data);
        btn_share.setOnClickListener(mOnClickListener);


        btn_like.setTag(R.id.comm_main_heart,position);
        tag.setTag(R.id.comm_main_text,data.CM_CONTENT);
        nick.setText(data.NICK);
        date.setText(TemporaryFunction.getDateFormat(data.REGDATE)); //파싱필요
        if(data.HCNT!=null&&!data.HCNT.equals(""))
            setLikeView(Integer.parseInt(data.HCNT));

        setTextRCNT(data.RCNT);
        text.setMaxLines(Integer.MAX_VALUE); //초기화
        text.setText("");

        if (data.MBER_SN!=null&&data.MBER_SN.equals(SEQ)) {
            editor.setVisibility(View.VISIBLE);
        } else {
            editor.setVisibility(View.GONE);
        }


        editor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                final int position = Integer.parseInt(v.getTag(R.id.comm_main_editor).toString());

                dialog = DialogCommon.showDialogEditor(context,new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss(); //게시글 수정
//                        final CommunityListViewData data = getItem(position);
                        NewActivity.moveToWritePage(fragment, data, data.CM_IMG1==null ? "" : data.CM_IMG1);

                    }
                },new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss(); // 게시글 삭제
                        CDialog.showDlg(context, context.getResources().getString(R.string.comm_dialog_message_4))
                                .setOkButton(context.getResources().getString(R.string.comm_delete), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        sendDeleteWriter(context,data.CM_SEQ);
                                    }
                                })
                                .setNoButton(context.getResources().getString(R.string.comm_cancel), null);

                    }
                });

            }
        });



        switch (type){
            case TYPE_LIST:
                text.post(new Runnable() {
                    @Override
                    public void run() {

//                        if(data.CM_CONTENT.length()>100){
//                            if(!data.ISDETAIL){
//                                btn_detail.setVisibility(View.VISIBLE);
//                                text.setMaxLines(2);
//                            }else{
//                                btn_detail.setVisibility(View.GONE);
//                                text.setMaxLines(Integer.MAX_VALUE);
//                            }
//
//                        }
//                        text.setText(data.CM_CONTENT);


                            text.setText(data.CM_CONTENT);

                            if(!data.ISDETAIL){
                                if(text.getLineCount()>2){
                                    btn_detail.setVisibility(View.VISIBLE);
                                    text.setMaxLines(2);
                                }else{
                                    btn_detail.setVisibility(View.GONE);
                                    text.setMaxLines(Integer.MAX_VALUE);
                                }
                            }else{
                                btn_detail.setVisibility(View.GONE);
                                text.setMaxLines(Integer.MAX_VALUE);
                            }

                    }
                });

                if(data.CM_IMG1.equals("")){
                    image.setVisibility(View.GONE);
                }else{

                    image.setVisibility(View.VISIBLE);
                    CommonFunction.setImage(context, data.CM_IMG1, image );

//                    Glide
//                            .with(context)
//                            .applyDefaultRequestOptions(new RequestOptions().override(200,200))
//                            .load(CommonFunction.getThumbnail(data.CM_IMG1))
//                            .into(image);
                }

                int RCNT = 0;
                try{
                    RCNT = Integer.parseInt(data.RCNT);
                }catch (Exception e){
                    RCNT=0;
                }

                if(RCNT<1){
                    comment_all.setVisibility(View.INVISIBLE);
                }else{
                    comment_all.setVisibility(View.VISIBLE);
                }

                layout_etc.setVisibility(View.VISIBLE);
                layout_btns.setVisibility(View.VISIBLE);
                break;

            case TYPE_VIEW:
//                btn_detail.setVisibility(View.GONE);
//                image.setVisibility(View.GONE);
//                text.setMaxLines(4);
//                comment_all.setVisibility(View.INVISIBLE);
//                text.setEllipsize(null);
//                editor.setVisibility(View.INVISIBLE);
//                layout_etc.setVisibility(View.VISIBLE);
//                layout_btns.setVisibility(View.VISIBLE);

                btn_detail.setVisibility(View.GONE);
                image.setVisibility(View.GONE);
                text.setMaxLines(Integer.MAX_VALUE);
                comment_all.setVisibility(View.GONE);
                text.setEllipsize(null);
                editor.setVisibility(View.INVISIBLE);

                layout_etc.setVisibility(View.GONE);
                layout_btns.setVisibility(View.GONE);
                text.setText(data.CM_CONTENT); //이상이면 ...처리
                break;


            case TYPE_VIEW_GONE:
                btn_detail.setVisibility(View.GONE);
                image.setVisibility(View.GONE);
                text.setMaxLines(Integer.MAX_VALUE);
                comment_all.setVisibility(View.GONE);
                text.setEllipsize(null);
                editor.setVisibility(View.INVISIBLE);

                layout_etc.setVisibility(View.GONE);
                layout_btns.setVisibility(View.GONE);
                text.setText(data.CM_CONTENT); //이상이면 ...처리
                break;

        }



        if(data.MYHEART==null||!data.MYHEART.equals("Y")){
            btn_like.setImageResource(R.drawable.commu_btn_6);
        }else{
            btn_like.setImageResource(R.drawable.commu_btn_6ov);
        }

        CommonFunction.setProfile(context, data.PROFILE_PIC, profile);
        CommonFunction.setProfileTeduri(context,data.MBER_GRAD,profile_teduri);

        if(data.CM_TAG!=null && data.CM_TAG.size()>0){
            tag.setVisibility(View.VISIBLE);
            tag.setTags(data.CM_TAG);
        }else{
            tag.setVisibility(View.GONE);
        }

        if(data.RCNT==null||data.RCNT.equals("")||Integer.parseInt(data.RCNT)<1){
            comment_all.setVisibility(View.INVISIBLE);
        }

        btn_like.setOnClickListener(mOnClickListener);


        if(data.CM_MEAL!=null&&!data.CM_MEAL.equals("")){
            cm_meal.setVisibility(View.VISIBLE);
            cm_meal.setText(data.CM_MEAL);
//            CommonFunction.setTextByPartsString(data.CM_MEAL,cm_meal, RoundedCornersBackgroundSpan.ALIGN_START,context);
        }else{
            cm_meal.setVisibility(View.GONE);
        }


    }



    /**
     * 좋아요 DB006
     */
    private void sendLikeToServer(final Context context) {
        final Tr_DB006.RequestData requestData = new Tr_DB006.RequestData();
        boolean isShowProgress=true;

        requestData.SEQ = SEQ;
        requestData.CM_SEQ = CM_SEQ;

        MediNewNetworkModule.doApi(context, new Tr_DB006(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";

                if (responseData instanceof Tr_DB006) {
                    Tr_DB006 data = (Tr_DB006)responseData;
                    try {

                        switch (data.RESULT_CODE){
                            case ReceiveDataCode.DB006_SUCCESS:
                                setLikeView(Integer.parseInt(like.getTag(R.id.comm_main_heart).toString())+1);
                                btn_like.setImageResource(R.drawable.commu_btn_6ov);
                                updateDataFunction.setDataHeart(Integer.parseInt(btn_like.getTag(R.id.comm_main_heart).toString()), like.getTag(R.id.comm_main_heart).toString(),"Y");
                                break;
                            case ReceiveDataCode.DB006_SUCCESS_DELETE:
                                setLikeView(Integer.parseInt(like.getTag(R.id.comm_main_heart).toString())-1);
                                btn_like.setImageResource(R.drawable.commu_btn_6);
                                updateDataFunction.setDataHeart(Integer.parseInt(btn_like.getTag(R.id.comm_main_heart).toString()), like.getTag(R.id.comm_main_heart).toString(),"N");
                                break;
                            case ReceiveDataCode.DB006_ERROR_NOMEMBER:
                                errorMessage = context.getResources().getString(R.string.comm_error_db006_6666);
                                break;
                            case ReceiveDataCode.DB006_ERROR_ETC:
                                errorMessage = context.getResources().getString(R.string.comm_error_db006_9999);
                                break;
                            default:
                                errorMessage = context.getResources().getString(R.string.comm_error_db006)+"\ncode:1";
                                break;
                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                        errorMessage = context.getResources().getString(R.string.comm_error_db006)+"\ncode:2";
                    }
                }else{
                    errorMessage = context.getResources().getString(R.string.comm_error_db006)+"\ncode:3";
                }

                if(!errorMessage.equals("")){
//                    dialog = DialogCommon.showDialogSingleBtn(context, errorMessage, context.getResources().getString(R.string.comm_confirm), new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            dialog.dismiss();
//                        }
//                    });
                    CDialog.showDlg(context, errorMessage);
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CLog.e(response.toString());
            }
        });
    }



    private void setLikeView(int HCNT) {
        like.setTag(R.id.comm_main_heart,HCNT);
        like.setText(Integer.toString(HCNT) + context.getResources().getText(R.string.comm_unit));
    }


    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int viewId = v.getId();
            switch (viewId){
                case R.id.btn_like:
                    sendLikeToServer(context);
                    break;


                case R.id.btn_share:

                    final CommunityListViewData data = (CommunityListViewData)v.getTag(R.id.comm_main_share);
                    final String url = "/HL/Share_sns/sns_contents.asp?seq=%s&cm_seq=%s";
                    final String message = String.format(Locale.US,url,data.MBER_SN,data.CM_SEQ);

                    dialog = DialogCommon.showDialogShare(context, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss(); //취소



                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss(); //kakao
                            sendMessageToKakao(data,message);
                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss(); //sns
                            KakaoUtil.sendSMS(context,context.getResources().getString(R.string.comm_kakaotitle)+" ","https://insu.greenpio.com:452"+message );
                        }
                    });


                    break;
            }

        }
    };


    public void sendMessageToKakao(CommunityListViewData data,String url) {

        Map<String, String> templateArgs = new HashMap<String, String>();

        templateArgs.put("title", context.getResources().getString(R.string.comm_kakaotitle));
        templateArgs.put("btTitle", context.getResources().getString(R.string.comm_detail));
        templateArgs.put("params", url);
        KakaoLinkService.getInstance().sendCustom(context, "15187", templateArgs, new ResponseCallback<KakaoLinkResponse>() {
            @Override
            public void onFailure(ErrorResult errorResult) {
//                Log.d(TAG, errorResult.toString());
            }

            @Override
            public void onSuccess(KakaoLinkResponse result) {
//                Log.d(TAG, "Success");
                // 템플릿 밸리데이션과 쿼터 체크가 성공적으로 끝남. 톡에서 정상적으로 보내졌는지 보장은 할 수 없다.
            }
        });
    }



    /**
     * 게시글삭제 db011
     */
    private void sendDeleteWriter(final Context context,final String CM_SEQ) {
        final Tr_DB011.RequestData requestData = new Tr_DB011.RequestData();
        boolean isShowProgress=true;

        requestData.SEQ = SEQ;
        requestData.CM_SEQ = CM_SEQ;

        MediNewNetworkModule.doApi(context, new Tr_DB011(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";

                if (responseData instanceof Tr_DB011) {
                    try {
                        Tr_DB011 data = (Tr_DB011) responseData;

                        switch (data.RESULT_CODE) {
                            case ReceiveDataCode.DB011_SUCCESS:

                                setAction.setActionAfterDelete(CM_SEQ);
//                                deleteItem(CM_SEQ);
                                break;
                            case ReceiveDataCode.DB011_ERROR_NOWRITER:
                                errorMessage = context.getResources().getString(R.string.comm_error_db011_4444);
                                break;
                            case ReceiveDataCode.DB011_ERROR_NOMEMBER:
                                errorMessage = context.getResources().getString(R.string.comm_error_db011_6666);
                                break;
                            case ReceiveDataCode.DB011_ERROR_REJECTMEMBER:
                                errorMessage = context.getResources().getString(R.string.comm_error_db011_7777);
                                break;
                            case ReceiveDataCode.DB011_ERROR_ETC:
                                errorMessage = context.getResources().getString(R.string.comm_error_db011_9999);
                                break;
                            default:
                                errorMessage = context.getResources().getString(R.string.comm_error_db011) + "\ncode:1";
                                break;
                        }
                    }catch (Exception e){
                        errorMessage = context.getResources().getString(R.string.comm_error_db011)+"\ncode:2";
                    }
                }else{
                    errorMessage = context.getResources().getString(R.string.comm_error_db011)+"\ncode:3";
                }

                if(!errorMessage.equals("")){
                    CDialog.showDlg(context, errorMessage);
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CLog.e(response.toString());
            }
        });
    }


    public interface setAction{
        void setActionAfterDelete(String CM_SEQ);
    }


}

