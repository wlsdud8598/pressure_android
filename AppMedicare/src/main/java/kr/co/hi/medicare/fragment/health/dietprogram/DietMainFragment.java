package kr.co.hi.medicare.fragment.health.dietprogram;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import kr.co.hi.medicare.net.hwNet.BaseData;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_mvm_asstb_diet_program_req;
import kr.co.hi.medicare.net.data.Tr_mvm_asstb_diet_program_schedule_list;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.fragment.IBaseFragment;


/**
 * Created by MrsWin on 2017-02-16.
 */

public class DietMainFragment extends BaseFragmentMedi implements IBaseFragment, View.OnClickListener {
    private final String TAG = getClass().getSimpleName();

    private TextView mDescTv1;
    private TextView mDescTv2;
    private TextView mDescTv3;

    private Button mCancel, mConfirm;


    public static Fragment newInstance() {
        DietMainFragment fragment = new DietMainFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_diet_program, container, false);
        return view;
    }


    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDescTv1 = view.findViewById(R.id.diet_program_desc_tv1);
        mDescTv2 = view.findViewById(R.id.diet_program_desc_tv2);
        mDescTv3 = view.findViewById(R.id.diet_program_desc_tv3);

        mDescTv1.setText(getString(R.string.popup_diet_popup_content_1, "0", "0"));
        mDescTv2.setText(getString(R.string.popup_diet_popup_content_2, "0"));
        mDescTv3.setText("0%");

        mCancel = view.findViewById(R.id.cancel_btn);
        mConfirm = view.findViewById(R.id.confirm_btn);

        mCancel.setOnClickListener(this);
        mConfirm.setOnClickListener(this);

        getDietProgramList();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cancel_btn:
                onBackPressed();
                break;
            case R.id.confirm_btn:
                requestDietProgram();
                break;
        }
    }


    /**
     * 다이어트 신청 내용
     */
    private Tr_mvm_asstb_diet_program_schedule_list mDietProgramTr;
    private void getDietProgramList() {
        Tr_mvm_asstb_diet_program_schedule_list.RequestData requestData = new Tr_mvm_asstb_diet_program_schedule_list.RequestData();

        Tr_login login = UserInfo.getLoginInfo();
        requestData.mber_sn = login.mber_sn;

        MediNewNetworkModule.doApi(getContext(), Tr_mvm_asstb_diet_program_schedule_list.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_mvm_asstb_diet_program_schedule_list) {
                    Tr_mvm_asstb_diet_program_schedule_list tr = (Tr_mvm_asstb_diet_program_schedule_list)responseData;
                    try {
                        mDietProgramTr = tr;

                        mDescTv1.setText(getString(R.string.popup_diet_popup_content_1, tr.sch_day,tr.sch_day));
                        mDescTv2.setText(getString(R.string.popup_diet_popup_content_2, tr.sch_user));
                        mDescTv3.setText(tr.sch_per+"%");

                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }else {
                    mDescTv1.setText(getString(R.string.popup_diet_popup_content_1, "-", "-"));
                    mDescTv2.setText(getString(R.string.popup_diet_popup_content_2, "-"));
                    mDescTv3.setText("-%");
                    CDialog.showDlg(getContext(), "데이터 수신에 실패 하였습니다.");
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
            }
        });
    }


    /**
     * 다이어트 프로그램 신청완료 팝업
     */
    private void popupCompleteProgram() {
        CDialog.showDlg(getContext(), getString(R.string.popup_complete_diet_program))
                .setOneBtn(R.drawable.draw_alert_ok_btn);
    }

    /**
     * 다이어트 프로그램 신청완료 팝업
     */
    private void popupYetProgram() {
        CDialog.showDlg(getContext(), getString(R.string.popup_yet_diet_program))
                .setOneBtn(R.drawable.draw_alert_ok_btn);
    }

    /**
     * 다이어트 프로그램 신청
     */
    private void requestDietProgram() {
        Tr_mvm_asstb_diet_program_req.RequestData requestData = new Tr_mvm_asstb_diet_program_req.RequestData();

        Tr_login login = UserInfo.getLoginInfo();
        requestData.mber_sn = login.mber_sn;

        MediNewNetworkModule.doApi(getContext(), Tr_mvm_asstb_diet_program_req.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_mvm_asstb_diet_program_req) {
                    Tr_mvm_asstb_diet_program_req data = (Tr_mvm_asstb_diet_program_req)responseData;
                    try {
                        if ("Y".equals(data.reg_yn)) {

                            // 신청완료
                            popupCompleteProgram();
                        } else {
                            // 이미신청중
                            popupYetProgram();
                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }else {
                    CDialog.showDlg(getContext(), "데이터 수신에 실패 하였습니다.");
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
            }
        });
    }
}