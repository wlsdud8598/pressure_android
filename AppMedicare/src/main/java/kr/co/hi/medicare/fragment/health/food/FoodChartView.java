package kr.co.hi.medicare.fragment.health.food;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import kr.co.hi.medicare.value.TypeDataSet;
import kr.co.hi.medicare.net.bluetooth.manager.DeviceDataUtil;

import java.text.SimpleDateFormat;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.charting.data.BarEntry;
import kr.co.hi.medicare.charting.data.RadarEntry;
import kr.co.hi.medicare.chartview.food.RadarChartView;
import kr.co.hi.medicare.chartview.valueFormat.AxisValueFormatter;
import kr.co.hi.medicare.chartview.food.FoodBarChartView;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperFoodMain;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.hwdata.Tr_get_meal_input_data;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.ChartTimeUtil;
import kr.co.hi.medicare.utilhw.DisplayUtil;
import kr.co.hi.medicare.utilhw.StringUtil;

/**
 *  히스토리화면
 * Created by mrsohn on 2017. 3. 14..
 */

public class FoodChartView {
    private final String TAG = getClass().getSimpleName();

    int recommandCalrori;   //권장칼로리.
    public ChartTimeUtil mTimeClass;

    private FoodBarChartView mChart;
    private RadarChartView mRadarChart;
    private TextView mDateTv;

    private BaseFragmentMedi mBaseFragment;
    private View mView;

    private TextView mBottomCalBreakfastTv;
    private TextView mBottomCalLunchTv;
    private TextView mBottomCalDinnerTv;
    private TextView mBottomMinuteBreakfastTv;
    private TextView mBottomMinuteLunchTv;
    private TextView mBottomMinuteDinnerTv;

    private TextView mCalTitleTv;
    private TextView mMinuteTitleTv;
    private TextView mRadarTitleTv;

    private TextView txttakecal;
    private TextView txtrecomcal;

    private ImageButton imgPre_btn;
    private ImageButton imgNext_btn;

    private ImageView mChartCloseBtn, mChartZoomBtn;
    private View mVisibleView1;
    private View mChartFrameLayout;
    private ScrollView mContentScrollView;
    private View mPerodLayout;
    private View mDateLayout;
    private LinearLayout mDataView, mDataNoneView;
    LinearLayout.LayoutParams params;
    int tempHeight=0;


    public FoodChartView(BaseFragmentMedi baseFragment, View view) {
        mBaseFragment = baseFragment;
        mView = view;

        mDateTv = (TextView) view.findViewById(R.id.txtCurrDate);

        RadioGroup periodRg = (RadioGroup) view.findViewById(R.id.radiogroup_period_type);
        RadioButton radioBtnMonth = (RadioButton) view.findViewById(R.id.radio_food_type_month);
        RadioButton radioBtnWeek = (RadioButton) view.findViewById(R.id.radio_food_type_week);
        RadioButton radioBtnDay = (RadioButton) view.findViewById(R.id.radio_food_type_day);

        mBottomCalBreakfastTv = (TextView) view.findViewById(R.id.food_cal_breakfast);
        mBottomCalLunchTv = (TextView) view.findViewById(R.id.food_cal_lunch);
        mBottomCalDinnerTv = (TextView) view.findViewById(R.id.food_cal_dinner);
        mBottomMinuteBreakfastTv = (TextView) view.findViewById(R.id.food_minute_blackfast);
        mBottomMinuteLunchTv = (TextView) view.findViewById(R.id.food_minute_lunch);
        mBottomMinuteDinnerTv = (TextView) view.findViewById(R.id.food_minute_dinner);

        mCalTitleTv = (TextView) view.findViewById(R.id.food_cal_title);
        mMinuteTitleTv = (TextView) view.findViewById(R.id.food_minute_title);
        mRadarTitleTv = (TextView) view.findViewById(R.id.food_radar_title);

        mChartCloseBtn = view.findViewById(R.id.chart_close_btn);
        mChartZoomBtn = view.findViewById(R.id.landscape_btn);
        mVisibleView1 = view.findViewById(R.id.visible_view1);
        mPerodLayout = view.findViewById(R.id.period_select_layout);
        mDateLayout = view.findViewById(R.id.chart_date_layout);
        mDataView  = view.findViewById(R.id.data_view);
        mDataNoneView  = view.findViewById(R.id.data_none);

        mChartFrameLayout = view.findViewById(R.id.chart_frame_layout);
        mContentScrollView = view.findViewById(R.id.layout_food_chart);

        mChartZoomBtn.setOnClickListener(mClickListener);
        mChartCloseBtn.setOnClickListener(mClickListener);

        periodRg.setOnCheckedChangeListener(mCheckedChangeListener);

        imgPre_btn                  = (ImageButton) view.findViewById(R.id.btn_hiscalLeft);
        imgNext_btn                 = (ImageButton) view.findViewById(R.id.btn_hiscalRight);

        view.findViewById(R.id.btn_hiscalLeft).setOnClickListener(mClickListener);
        view.findViewById(R.id.btn_hiscalRight).setOnClickListener(mClickListener);


        mTimeClass = new ChartTimeUtil(radioBtnDay, radioBtnWeek, radioBtnMonth);
        mRadarChart = new RadarChartView(mBaseFragment.getContext(), view);
        mChart = new FoodBarChartView(mBaseFragment.getContext(), view);
        if (mChart.getXValueFormat() instanceof AxisValueFormatter) {
            ((AxisValueFormatter) mChart.getXValueFormat()).setUnitStr("kcal");
        }

        txttakecal = (TextView) view.findViewById(R.id.txt_takecal);    //  섭취칼로리
        txtrecomcal = (TextView) view.findViewById(R.id.txt_recomcal);  //  권장칼로리

        setRecomandCal();
        setNextButtonVisible();

        params = (LinearLayout.LayoutParams) mChartFrameLayout.getLayoutParams();
        tempHeight = params.height;


        //코드 부여(엄마 건강)
//        radioBtnDay.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtnfoodDay));
//        radioBtnWeek.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtnfoodWeek));
//        radioBtnMonth.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtnfoodMonth));


    }

    private void setRecomandCal() {
        try {
            int dayCnt = 1;
            TypeDataSet.Period period = mTimeClass.getPeriodType();
            if (period == TypeDataSet.Period.PERIOD_DAY) {
                dayCnt = 1;
            } else if (period == TypeDataSet.Period.PERIOD_WEEK) {
                dayCnt = 7;
            }else if (period == TypeDataSet.Period.PERIOD_MONTH){
                dayCnt = mTimeClass.getDayOfMonth();
            }

            int kcal = new DeviceDataUtil().getRecommendCal(mBaseFragment.getContext());
            kcal = dayCnt * kcal;   // 권장 섭취 칼로리


            txtrecomcal.setText(StringUtil.exchangeAmountToStringUnit(""+kcal)+" kcal");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setVisibility(int visibility) {
        mView.setVisibility(visibility);
    }

    public int  getVisibility() {
        return mView.getVisibility();
    }


    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
            if (vId == R.id.btn_hiscalLeft) {
                mTimeClass.calTime(-1);
                getData();
                setRecomandCal();
                setNextButtonVisible();
            } else if (vId == R.id.btn_hiscalRight) {
                // 초기값 일때 다음날 데이터는 없으므로 리턴
                if (mTimeClass.getCalTime() == 0)
                    return;

                mTimeClass.calTime(1);
                getData();
                setRecomandCal();
                setNextButtonVisible();
            } else if(vId == R.id.landscape_btn) {
                MainActivityMedicare mainActivity = ((MainActivityMedicare)mBaseFragment.getActivity());
                mainActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            } else if(vId == R.id.chart_close_btn) {
                MainActivityMedicare mainActivity = ((MainActivityMedicare)mBaseFragment.getActivity());
                mainActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        }
    };

    private void setNextButtonVisible(){
        // 초기값 일때 다음날 데이터는 없으므로 리턴
        if (mTimeClass.getCalTime() == 0) {
            imgNext_btn.setVisibility(View.INVISIBLE);
        }else{
            imgNext_btn.setVisibility(View.VISIBLE);
        }
    }
    /**
     * 일간,주간,월간
     */
    public RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            // 일간, 주간, 월간
            TypeDataSet.Period periodType = mTimeClass.getPeriodType();
            AxisValueFormatter xFormatter = new AxisValueFormatter(periodType);
            xFormatter.setUnitStr("kcal");
            mChart.setXValueFormat(xFormatter);
            mTimeClass.clearTime();         // 날자 초기화

            getData();   // 날자 세팅 후 조회
            setRecomandCal();
        }
    };

    /**
     * 날자 계산 후 조회
     */
    public void getData() {
        long startTime = mTimeClass.getStartTime();
        long endTime = mTimeClass.getEndTime();

        mChart.getBarChart().setDrawMarkers(false); // 새로운 조회시 마커 사라지게 하기

        String format = "yyyy.MM.dd";
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        String startDate = sdf.format(startTime);
        String endDate = sdf.format(endTime);

        if (mTimeClass.getPeriodType() == TypeDataSet.Period.PERIOD_DAY) {
            mDateTv.setText(startDate);
        } else {
            mDateTv.setText(startDate + " ~ " + endDate);
        }

        format = "yyyy-MM-dd";
        sdf = new SimpleDateFormat(format);
        startDate = sdf.format(startTime);
        endDate = sdf.format(endTime);

        getBottomDataLayout(startDate, endDate);


    }

    /**
     * 하단 데이터 세팅하기
     *
     * @param startDate
     * @param endDate
     */
    private void getBottomDataLayout(String startDate, String endDate) {
        mBottomCalBreakfastTv.setText("-");
        mBottomCalLunchTv.setText("-");
        mBottomCalDinnerTv.setText("-");
        mBottomMinuteBreakfastTv.setText("-");
        mBottomMinuteLunchTv.setText("-");
        mBottomMinuteDinnerTv.setText("-");

        DBHelper helper = new DBHelper(mBaseFragment.getContext());

        DBHelperFoodMain db = helper.getFoodMainDb();
        int[] datas = db.getMealSum(startDate, endDate);

        TypeDataSet.Period period = mTimeClass.getPeriodType();
        if (period == TypeDataSet.Period.PERIOD_DAY) {
            // 일별 하단 데이터 세팅
            Tr_get_meal_input_data foodData = db.getResultDay(startDate);
            int morningCal = 0;
            int lunchCal = 0;
            int dinnerCal = 0;

            for (Tr_get_meal_input_data.ReceiveDatas recv : foodData.data_list) {
                String mealType = recv.mealtype;
                Log.i(TAG, "Tr_get_meal_input_data.mealType="+mealType+", calorie="+recv.calorie);
                if (mBaseFragment.getContext().getString(R.string.text_breakfast_code).equals(mealType)
                        || mBaseFragment.getContext().getString(R.string.text_breakfast_snack_code).equals(mealType)) {
                    morningCal += StringUtil.getIntger(recv.calorie);
                    mBottomCalBreakfastTv.setText(morningCal <=0 ? "-" : String.format("%,d", morningCal) + " kcal");
                    mBottomMinuteBreakfastTv.setText(recv.amounttime ==null || recv.amounttime.equals("")? "-" : recv.amounttime + " 분");
                } else if (mBaseFragment.getContext().getString(R.string.text_lunch_code).equals(mealType)
                        || mBaseFragment.getContext().getString(R.string.text_lunch_snack_code).equals(mealType)) {
                    lunchCal += StringUtil.getIntger(recv.calorie);
                    mBottomCalLunchTv.setText(lunchCal <=0 ? "-" : String.format("%,d", lunchCal) + " kcal");
                    mBottomMinuteLunchTv.setText(recv.amounttime ==null || recv.amounttime.equals("")? "-" : recv.amounttime+" 분");
                } else if (mBaseFragment.getContext().getString(R.string.text_dinner_code).equals(mealType)
                        || mBaseFragment.getContext().getString(R.string.text_dinner_snack_code).equals(mealType)) {
                    dinnerCal += StringUtil.getIntger(recv.calorie);
                    mBottomCalDinnerTv.setText(dinnerCal <=0 ? "-" : String.format("%,d", dinnerCal) + " kcal");
                    mBottomMinuteDinnerTv.setText(recv.amounttime ==null || recv.amounttime.equals("")? "-" : recv.amounttime+" 분");
                }
            }
        } else {
            mBottomCalBreakfastTv.setText(datas[0] <=0 ? "-" : String.format("%,d", datas[0])+" kcal");
            mBottomCalLunchTv.setText(datas[1] <=0 ? "-" : String.format("%,d", datas[1])+" kcal");
            mBottomCalDinnerTv.setText(datas[2] <=0 ? "-" : String.format("%,d", datas[2])+" kcal");
            mBottomMinuteBreakfastTv.setText(datas[3] <= 0? "-" : ""+datas[3]+" 분");
            mBottomMinuteLunchTv.setText(datas[4] <= 0? "-" : ""+datas[4]+" 분");
            mBottomMinuteDinnerTv.setText(datas[5] <= 0? "-" : ""+datas[5]+" 분");
        }

        // 섭취칼로리
        int totTakeCal = datas[0] + datas[1] + datas[2];
        txttakecal.setText(StringUtil.exchangeAmountToStringUnit(""+totTakeCal));

        if(totTakeCal <= 0){
            mDataView.setVisibility(View.GONE);
            mDataNoneView.setVisibility(View.VISIBLE);
        }else{
            mDataView.setVisibility(View.VISIBLE);
            mDataNoneView.setVisibility(View.GONE);
        }




        int dayCnt = 1;
        if (period == TypeDataSet.Period.PERIOD_DAY) {
            dayCnt = 1;

        } else if (period == TypeDataSet.Period.PERIOD_WEEK) {
            dayCnt = 7;
        }else if (period == TypeDataSet.Period.PERIOD_MONTH){
            dayCnt = mTimeClass.getDayOfMonth();
        }

        int kcal = new DeviceDataUtil().getRecommendCal(mBaseFragment.getContext());
        kcal = dayCnt * kcal;   // 권장 섭취 칼로리

        List<RadarEntry> entries = db.getRadial(startDate, endDate, (float)totTakeCal,kcal,dayCnt);

        int idx = 0;
        for (RadarEntry entry : entries) {
            Log.i(TAG, "entry["+(idx++)+"]="+entry.getValue());
        }

        mRadarChart.setData(entries);

        new QeuryVerifyDataTask(db).execute();
    }

    public class QeuryVerifyDataTask extends AsyncTask<Void, Void, Void> {

        private DBHelperFoodMain db;
        public QeuryVerifyDataTask(DBHelperFoodMain db) {
            this.db = db;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mBaseFragment.showProgress();
        }

        protected Void doInBackground(Void... params) {

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mBaseFragment.hideProgress();

            TypeDataSet.Period period = mTimeClass.getPeriodType();
            if (period == TypeDataSet.Period.PERIOD_DAY) {
                String toDay = CDateUtil.getFormattedString_yyyy_MM_dd(mTimeClass.getStartTime());
                List<BarEntry> yVals1 = db.getResultDay(toDay, 24);
                mChart.setData(yVals1, mTimeClass);

                mCalTitleTv.setText("일간 영양 섭취 칼로리");
                mMinuteTitleTv.setText("일간 평균 식사 소요시간");
                mRadarTitleTv.setText("일간 영양 섭취 균형도");
            } else if (period == TypeDataSet.Period.PERIOD_WEEK) {
                String startDay = CDateUtil.getFormattedString_yyyy_MM_dd(mTimeClass.getStartTime());
                String endDay = CDateUtil.getFormattedString_yyyy_MM_dd(mTimeClass.getEndTime());
                List<BarEntry> yVals1 = db.getResultWeek(startDay, endDay, 7);
                mChart.setData(yVals1, mTimeClass);

                mCalTitleTv.setText("주간 영양 섭취 칼로리");
                mMinuteTitleTv.setText("주간 평균 식사 소요시간");
                mRadarTitleTv.setText("주간 영양 섭취 균형도");
            } else if (period == TypeDataSet.Period.PERIOD_MONTH) {
                String year = CDateUtil.getFormattedString_yyyy(mTimeClass.getStartTime());
                String month = CDateUtil.getFormattedString_MM(mTimeClass.getStartTime());
                List<BarEntry> yVals1 = db.getResultMonth(year, month, mTimeClass.getDayOfMonth());
                mChart.setData(yVals1, mTimeClass);

                mCalTitleTv.setText("월간 영양 섭취 칼로리");
                mMinuteTitleTv.setText("월간 평균 식사 소요시간");
                mRadarTitleTv.setText("월간 영양 섭취 균형도");
            }
//            mChart.animateY();
            mChart.invalidate();
            setNextButtonVisible();
        }
    }


    /**
     * 가로, 세로모드일때 불필요한 화면 Visible 처리
     */
    public void setVisibleOrientationLayout(Activity activity, final boolean isLandScape) {
        mVisibleView1.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        if(StringUtil.getIntVal(txttakecal.getText().toString()) <= 0 ){
            mDataNoneView.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
            mDataView.setVisibility(isLandScape ? View.GONE : View.GONE);
        } else {
            mDataNoneView.setVisibility(isLandScape ? View.GONE : View.GONE);
            mDataView.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        }

        mChartCloseBtn.setVisibility(isLandScape ? View.VISIBLE : View.GONE);
        mChartZoomBtn.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mPerodLayout.setVisibility(isLandScape ? View.GONE : View.VISIBLE);

        DisplayMetrics dm = mBaseFragment.getContext().getResources().getDisplayMetrics();

        int portHeight = DisplayUtil.getDpToPix(mBaseFragment.getContext(),tempHeight);    // 세로모드일때 사이즈 250dp(레이아웃에 설정되어 있는 사이즈)
        int landHeight = (int) (dm.heightPixels
                - mDateLayout.getMeasuredHeight()               // 날자 표시
                - DisplayUtil.getStatusBarHeight(mBaseFragment.getContext())  // 상태바 높이
                - DisplayUtil.getDpToPix(mBaseFragment.getContext(),10)   // 1dp
        );
        params.height = isLandScape ? landHeight : portHeight;
        mChartFrameLayout.setLayoutParams(params);

        // 가로모드일때 스크롤뷰 막기
        mContentScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return isLandScape;
            }
        });
        //가로모드 전환 시 스크롤 상단으로 위치
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mContentScrollView.smoothScrollTo(0,0);
            }
        }, 100);
    }

    public void onResume() {
        getData();
    }

}
