package kr.co.hi.medicare.fragment.health.message;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import kr.co.hi.medicare.fragment.IBaseFragment;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;


/**
 * Created by MrsWin on 2017-02-16.
 */

public class HealthMessageFragment extends BaseFragmentMedi implements IBaseFragment {
    private final String TAG = getClass().getSimpleName();

    private FragmentPagerAdapter mPagerAdapter;
    private ViewPager mViewPager;
    private RadioButton[] mFragmentRadio;
    private RadioGroup mFragmentGroup;
    private View tabview;
    private TextView mTitle;
    private int topPosition = 0;
    public static String bottomPosition ="";


    private final Fragment[] mFragments = new Fragment[] {
            HealthMessageMainFragment.newInstance(),
            HealthMessageWebviewFragment.newInstance(),

    };

    public static Fragment newInstance() {
        HealthMessageFragment fragment = new HealthMessageFragment();
        return fragment;
    }

    public HealthMessageFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_2tab_toolbar, container, false);
        return view;


    }

//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
//        getToolBar().setVisibility(View.GONE);
//    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            topPosition = bundle.getInt("Danger_Health",0);
            bottomPosition = bundle.getString("Danger_Health_position");


            Log.d(TAG, "HealthMessage_상단텝위치="+topPosition);
            Log.d(TAG, "HealthMessage_하단텝위치="+bottomPosition);

        }


        mTitle = view.findViewById(R.id.common_toolbar_title);
        mTitle.setText(getString(R.string.text_health_message));

        tabview = view;

        mFragmentGroup = view.findViewById(R.id.tab_radiogroup);

        final String[] mFragmentNames = new String[] {
                getString(R.string.text_health_message),
                getString(R.string.text_tip),
        };

        mFragmentRadio = new RadioButton[]{
                view.findViewById(R.id.tab1),
                view.findViewById(R.id.tab2),
        };

        int i = 0;
        for (String name : mFragmentNames) {
            mFragmentRadio[i].setText(name);
            i++;
        }

        mFragmentGroup.setOnCheckedChangeListener(mCheckedChangeListener);

//        if(topPosition == 1){
//            mFragmentRadio[topPosition].performClick();
//        }else{
//            setChildFragment(mFragments[0]);
//        }

        setChildFragment(mFragments[0]);

    }

    public RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            int pos=group.indexOfChild(tabview.findViewById(checkedId));
            setChildFragment(mFragments[pos]);

        }
    };

    private void setChildFragment(Fragment child) {
        FragmentTransaction childFt = getChildFragmentManager().beginTransaction();

        if (!child.isAdded()) {
            childFt.replace(R.id.child_fragment_layout, child);
            childFt.addToBackStack(null);
            childFt.commit();
        }
    }
}