package kr.co.hi.medicare.fragment.health.message;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.value.model.MessageModel;
import kr.co.hi.medicare.net.hwNet.BaseData;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperMessage;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_infra_del_message;
import kr.co.hi.medicare.net.data.Tr_infra_del_message_all;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.fragment.IBaseFragment;


/**
 * Created by MrsWin on 2017-02-16.
 */

public class HealthMessageMainFragment extends BaseFragmentMedi implements IBaseFragment, View.OnClickListener {
    private final String TAG = getClass().getSimpleName();

    private SlideAdapter slideAdapter = new SlideAdapter();
    private Button mDelete;

    List<MessageModel> mMessage_list = new ArrayList<>();


    public static Fragment newInstance() {
        HealthMessageMainFragment fragment = new HealthMessageMainFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_health_message, container, false);
        return view;
    }


    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDelete = view.findViewById(R.id.delete_btn);
        mDelete.setOnClickListener(this);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(this, view, getContext());


        //건강관리
        mDelete.setOnTouchListener(ClickListener);

        //코드부여
        mDelete.setContentDescription(getString(R.string.mDelete));


        RecyclerView rv = view.findViewById(R.id.health_message_recyclerview);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(slideAdapter);

        SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MESSAGE, false);


        // RecyclerView Line
        rv.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        setHealthMessageSqlite();
    }

    /**
     * 건강 메시지 세팅하기
     */
    public void setHealthMessageSqlite() {
        DBHelper helper = new DBHelper(getContext());
        DBHelperMessage db = helper.getMessageDb();
        List<MessageModel> messageList = db.getResultAll(helper);

        slideAdapter.setData(messageList);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.delete_btn:
                CDialog.showDlg(getContext(), "모든 건강메시지를\n삭제하시겠습니까?", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        doDeleteDataAll();
                    }
                }, null);

                break;
        }
    }

    /**
     * 건강메세지 모두 삭제하기
     *
     * @param
     */

    private void doDeleteDataAll() {

        Tr_infra_del_message_all.RequestData requestData  = new Tr_infra_del_message_all.RequestData();
        Tr_login login                                  = UserInfo.getLoginInfo();

        requestData.mber_sn = login.mber_sn;


        MediNewNetworkModule.doApi(getContext(), Tr_infra_del_message_all.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_infra_del_message_all) {
                    Tr_infra_del_message_all data = (Tr_infra_del_message_all)responseData;
                    try {
                        if ("Y".equals(data.data_yn)) {

                            DBHelper helper         = new DBHelper(getContext());
                            DBHelperMessage msgDb   = helper.getMessageDb();
                            msgDb.DeleteAllDb();

                            mMessage_list.clear();
                            slideAdapter.notifyDataSetChanged();

//                            CDialog.showDlg(getContext(), getContext().getString(R.string.delete_success));
                        } else {
                            CDialog.showDlg(getContext(), getContext().getString(R.string.msg_fail_comment));
                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
            }
        });

    }

    class SlideAdapter extends RecyclerView.Adapter<SlideAdapter.SlideViewHolder> {


        @Override
        public SlideAdapter.SlideViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.health_message_slide_list_adapter, parent, false);
            return new SlideAdapter.SlideViewHolder(itemView);
        }

        public void setData(List<MessageModel> message_list) {
            mMessage_list.clear();
            mMessage_list.addAll(message_list);

            notifyDataSetChanged();
        }


        @Override
        public void onBindViewHolder(SlideAdapter.SlideViewHolder holder, final int position) {
            MessageModel msg = mMessage_list.get(position);

            String str = msg.getMessage();

//            if (str.contains("[고혈압성 응급 주의]")) {
//                int int1 = str.indexOf("[고혈압성 응급 주의]");
//                int int2 = str.indexOf("고혈압성 응급의 경우 해당 전문의의 치료가 필요합니다. 반드시 병원에 방문하세요.");
//                String str1 = str.substring(0, int1 - 1);
//                String str2 = str.substring(int1, int1 + "[고혈압성 응급 주의]".length());
//                String str3 = str.substring(int1 + "[고혈압성 응급 주의]".length() + 1, int2 - 1);
//                String str4 = str.substring(int2, int2 + "고혈압성 응급의 경우 해당 전문의의 치료가 필요합니다. 반드시 병원에 방문하세요.".length());
//                String str5 = "";
//
//                if(int2 + "고혈압성 응급의 경우 해당 전문의의 치료가 필요합니다. 반드시 병원에 방문하세요.".length() < str.length()) {
//                    str5 = str.substring(int2 + "고혈압성 응급의 경우 해당 전문의의 치료가 필요합니다. 반드시 병원에 방문하세요.".length());
//                }
//
//                holder.contentTextView.setText(Html.fromHtml(str1.replaceAll("\n", "<br>")
//                        + "<br><font color=\"#EE0029\">"
//                        + str2
//                        + "</font><br>"
//                        + str3.replaceAll("\n", "<br>")
//                        + "<font color=\"#EE0029\">"
//                        + str4
//                        + "</font>"
//                        + str5.replaceAll("\n", "<br>")
//                ));
//            } else {
                holder.contentTextView.setText(msg.getMessage());
//            }

//            msg.getRegdate().substring(5,16);

            holder.dateTextView.setText(CDateUtil.getFormatyymmddHHmm(msg.getRegdate()));
            holder.deleteButton.setOnClickListener(new SlideAdapter.MsgOnClickListener(position) {
                @Override
                public void onClick(View v) {
                    CDialog.showDlg(getContext(), "삭제하시겠습니까?", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            MessageModel msg = mMessage_list.get(position);
                            doDeleteData(position, msg);
                        }
                    }, null);
                }
            });

        }

        public abstract class MsgOnClickListener implements View.OnClickListener {

            protected int index;

            public MsgOnClickListener(int index) {
                this.index = index;
            }
        }



        /**
         * 건강메세지 로우 삭제하기
         *
         * @param model
         */

        private void doDeleteData(final int position, final MessageModel model) {

            Tr_infra_del_message.RequestData requestData  = new Tr_infra_del_message.RequestData();
            Tr_login login                                  = UserInfo.getLoginInfo();

            requestData.mber_sn = login.mber_sn;
            requestData.idx = model.getIdx();


            MediNewNetworkModule.doApi(getContext(), Tr_infra_del_message.class, requestData, new MediNewNetworkHandler() {
                @Override
                public void onSuccess(BaseData responseData) {
                    if (responseData instanceof Tr_infra_del_message) {
                        Tr_infra_del_message data = (Tr_infra_del_message)responseData;
                        try {
                            if ("Y".equals(data.data_yn)) {

                                DBHelper helper         = new DBHelper(getContext());
                                DBHelperMessage msgDb   = helper.getMessageDb();
                                msgDb.DeleteDb(model.getIdx());

                                mMessage_list.remove(position);
                                notifyDataSetChanged();

//                                CDialog.showDlg(getContext(), getContext().getString(R.string.delete_success));
                            } else {
                                CDialog.showDlg(getContext(), getContext().getString(R.string.msg_fail_comment));
                            }

                        } catch (Exception e) {
                            CLog.e(e.toString());
                        }
                    }
                }

                @Override
                public void onFailure(int statusCode, String response, Throwable error) {
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
                }
            });

        }

        @Override
        public int getItemCount() {
            return mMessage_list == null ? 0 : mMessage_list.size();
        }

        class SlideViewHolder extends RecyclerView.ViewHolder {
            public TextView contentTextView;
            public TextView dateTextView;
            public ImageButton deleteButton;
            public SlideViewHolder(View itemView) {
                super(itemView);

                contentTextView = (TextView) itemView.findViewById(R.id.main_slide_adapter_content_textview);
                dateTextView = (TextView) itemView.findViewById(R.id.main_slide_adapter_date_textview);
                deleteButton = (ImageButton) itemView.findViewById(R.id.main_slide_adapter_delete_button);
            }
        }
    }


}