package kr.co.hi.medicare.fragment.health.pressure;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import kr.co.hi.medicare.net.bluetooth.BluetoothManager;
import kr.co.hi.medicare.net.hwNet.BaseData;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.component.CDatePicker;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.health.step.StepInputFragment;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;
import kr.co.hi.medicare.value.model.PressureModel;
import kr.co.hi.medicare.fragment.IBaseFragment;
import kr.co.hi.medicare.net.bluetooth.manager.DeviceDataUtil;

/**
 * Created by insystemscompany on 2017. 2. 28..
 */

public class PressureInputFragment extends BaseFragmentMedi implements IBaseFragment {
    private static final String TAG = PressureInputFragment.class.getSimpleName();

    private TextView mDateTv, mTimeTv;
    private EditText mDiastolicPressureEt, mSystolicPressureEt;
    private LinearLayout mMedicenLayout;

    private int cal_year;
    private int cal_month;
    private int cal_day;
    private int cal_hour;
    private int cal_min;
    private InputMethodManager mImm;

    private CommonToolBar commonActionBar;
    private LinearLayout edit_lv, dialog_btn_layout;

    public static Fragment newInstance() {
        StepInputFragment fragment = new StepInputFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_pressure_input, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mImm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        /**
         * 상단 바 셋팅
         */
        edit_lv = view.findViewById(R.id.edit_lv);
        edit_lv.setVisibility(View.GONE);

        commonActionBar = view.findViewById(R.id.common_bar);
        commonActionBar.setVisibility(View.VISIBLE);

        TextView saveBtn = view.findViewById(R.id.common_toolbar_right_btn);
        saveBtn.setText(getString(R.string.text_save));
        saveBtn.setOnClickListener(mClickListener);

        dialog_btn_layout = view.findViewById(R.id.dialog_btn_layout);
        dialog_btn_layout.setVisibility(View.GONE);

        mMedicenLayout = (LinearLayout) view.findViewById(R.id.pressure_input_medicen_layout);
        mMedicenLayout.setVisibility(View.GONE);

        mDiastolicPressureEt = view.findViewById(R.id.diastolic_pressure_edittext);
        mSystolicPressureEt = view.findViewById(R.id.systolic_pressure);

        mDateTv = (TextView) view.findViewById(R.id.pressure_input_date_textview);
        mTimeTv = (TextView) view.findViewById(R.id.pressure_input_time_textview);

        String now_time = CDateUtil.getForamtyyyyMMddHHmm(new Date(System.currentTimeMillis()));
        java.util.Calendar cal = CDateUtil.getCalendar_yyyy_MM_dd_HH_mm(now_time);
        cal_year = cal.get(Calendar.YEAR);
        cal_month = cal.get(Calendar.MONTH);
        cal_day = cal.get(Calendar.DAY_OF_MONTH);
        cal_hour = cal.get(Calendar.HOUR_OF_DAY);
        cal_min = cal.get(Calendar.MINUTE);

        mDateTvSet(cal_year, cal_month, cal_day);
        mTimeTvSet(cal_hour, cal_min);

        mDateTv.setOnTouchListener(mTouchListener);
        mTimeTv.setOnTouchListener(mTouchListener);

    }

//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
//        //super.loadActionbar(actionBar);
//        actionBar.setWhiteTheme();
//        actionBar.setActionBarTitle( getString(R.string.text_pressure_input));
//    }

    View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                int vId = v.getId();
                if (vId == R.id.pressure_input_date_textview) {
                    showDatePicker(v);
                } else if (vId == R.id.pressure_input_time_textview) {
                    showTimePicker();
                }
            }
            return false;
        }
    };

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
            if (vId == R.id.common_toolbar_right_btn) {
                final  String regDate = mDateTv.getTag().toString();
                if (TextUtils.isEmpty(regDate)) {
                    CDialog.showDlg(getContext(), getString(R.string.date_input_error));
                    return;
                }

                final String timeStr = mTimeTv.getTag().toString();
                if (TextUtils.isEmpty(timeStr)) {
                    CDialog.showDlg(getContext(), getString(R.string.time_input_error));
                    return;
                }

                final String systolicPressure = mSystolicPressureEt.getText().toString();
                if (TextUtils.isEmpty(systolicPressure)) {
                    CDialog.showDlg(getContext(), getString(R.string.presure_systolic_presure_input_error));
                    return;
                }

                final String diastolicPressure = mDiastolicPressureEt.getText().toString();
                if (TextUtils.isEmpty(diastolicPressure)) {
                    CDialog.showDlg(getContext(), getString(R.string.presure_diastolic_presure_input_error));
                    return;
                }

                if(StringUtil.getFloatVal(systolicPressure) < 20.0f || StringUtil.getFloatVal(systolicPressure) >= 300.0f || StringUtil.getFloatVal(diastolicPressure) < 20.0f || StringUtil.getFloatVal(diastolicPressure) >= 300.0f){
                    CDialog.showDlg(getContext(), "20이상 300이하의 값을 입력해주세요.");
                    return;
                }

                //미래시간 입력방지
                if(StringUtil.getLongVal(regDate+timeStr) > StringUtil.getLongVal(CDateUtil.getToday_yyyy_MM_dd_HH_mm())){
                    CDialog.showDlg(getContext(), getString(R.string.message_nowtime_over));
                    return;
                }

                doInputPressure(regDate, timeStr, systolicPressure, diastolicPressure);

//                CDialog.showDlg(getContext(), getString(R.string.presure_regist), new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//
//                    }
//                }, null);
            }
        }
    };

    private boolean DateTimeCheck(String type, int pram1, int pram2, int pram3){
        Calendar cal = Calendar.getInstance();

        if(type.equals("D")){
            cal.set(Calendar.YEAR, pram1);
            cal.set(Calendar.MONTH, pram2);
            cal.set(Calendar.DAY_OF_MONTH, pram3);
            cal.set(Calendar.HOUR_OF_DAY, cal_hour);
            cal.set(Calendar.MINUTE, cal_min);

            if(cal.getTimeInMillis() > System.currentTimeMillis()){
                CDialog.showDlg(getContext(), getString(R.string.message_nowtime_over), new CDialog.DismissListener() {
                    @Override
                    public void onDissmiss() {

                    }
                });
                return false;
            }else{
                return true;
            }
        }else{
            cal.set(Calendar.YEAR, cal_year);
            cal.set(Calendar.MONTH, cal_month);
            cal.set(Calendar.DAY_OF_MONTH, cal_day);
            cal.set(Calendar.HOUR_OF_DAY, pram1);
            cal.set(Calendar.MINUTE, pram2);

            if(cal.getTimeInMillis() > System.currentTimeMillis()){
                CDialog.showDlg(getContext(), getString(R.string.message_nowtime_over), new CDialog.DismissListener() {
                    @Override
                    public void onDissmiss() {

                    }
                });

                return false;
            }else{
                return true;
            }
        }
    }

    private void showDatePicker(View v) {
        GregorianCalendar calendar = new GregorianCalendar();

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        String date = mDateTv.getTag().toString();
        if (TextUtils.isEmpty(date) == false) {
            year = StringUtil.getIntVal(date.substring(0 , 4));
            month = StringUtil.getIntVal(date.substring(4 , 6))-1;
            day = StringUtil.getIntVal(date.substring(6 , 8));
        }

        new CDatePicker(getContext(), dateSetListener, year, month, day, false).show();
    }

    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            if(DateTimeCheck("D",year, monthOfYear, dayOfMonth)) {
                cal_year = year;
                cal_month = monthOfYear;
                cal_day = dayOfMonth;
                mDateTvSet(year, monthOfYear, dayOfMonth);
            }
        }

    };

    private void mDateTvSet(int year, int monthOfYear, int dayOfMonth){
        String msg = String.format("%d.%d.%d", year, monthOfYear + 1, dayOfMonth);
        String tagMsg = String.format("%d%02d%02d", year, monthOfYear + 1, dayOfMonth);
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear + 1);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        //미래시간 입력방지
        if(StringUtil.getLongVal(tagMsg) > StringUtil.getLongVal(CDateUtil.getToday_yyyy_MM_dd())){
            return;
        }

        mDateTv.setText(msg+" "+ CDateUtil.getDateToWeek(tagMsg)+"요일");
        mDateTv.setTag(tagMsg);
    }

    private void showTimePicker() {
        Calendar cal = Calendar.getInstance(Locale.KOREA);
        Date now = new Date();
        cal.setTime(now);

        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        String time = mTimeTv.getTag().toString();
        if (TextUtils.isEmpty(time) == false) {
            hour = StringUtil.getIntVal(time.substring(0, 2));
            minute = StringUtil.getIntVal(time.substring(2 , 4));

            Logger.i(TAG, "hour="+hour+", minute="+minute);
        }

        TimePickerDialog dialog = new TimePickerDialog(getContext(), listener, hour, minute, false);
        dialog.show();
    }

    /**
     * 시간 피커 완료
     */
    private TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            if(DateTimeCheck("S",hourOfDay, minute, 0)) {
                cal_hour = hourOfDay;
                cal_min = minute;
                mTimeTvSet(hourOfDay, minute);
            }
        }
    };

    private void mTimeTvSet(int hourOfDay, int minute){
        // 설정버튼 눌렀을 때
        String amPm = "오전";
        int hour = hourOfDay;
        if (hourOfDay > 11) {
            amPm = "오후";
            if (hourOfDay >= 13)
                hour -= 12;
        } else {
            hour = hour == 0 ? 12 : hour;
        }
        String tagMsg = String.format("%02d%02d", hourOfDay, minute);
        String timeStr = String.format("%02d:%02d", hour, minute);
        mTimeTv.setText(amPm + " " + timeStr);
        mTimeTv.setTag(tagMsg);
    }

    private void doInputPressure(String regDate, String timeStr, String systolicPressure, String diastolicPressure) {

        regDate += timeStr;

        float diastolicPressuref = StringUtil.getFloat(diastolicPressure);
        float systolicPressuref = StringUtil.getFloat(systolicPressure);

        SparseArray<PressureModel> array = new SparseArray<>();
        PressureModel dataModel = new PressureModel();
        dataModel.setIdx(CDateUtil.getForamtyyMMddHHmmssSS(new Date(System.currentTimeMillis())));
        dataModel.setDiastolicPressure(diastolicPressuref);   //이완기혈압
        dataModel.setSystolicPressure(systolicPressuref);    // 수축기압력
        dataModel.setDrugname(""); // 약이름
        dataModel.setRegtype("U"); // D:디바이스 U:직접입력
        dataModel.setRegdate(regDate);

        array.append(0, dataModel);

        new DeviceDataUtil().uploadPresureDB(PressureInputFragment.this, dataModel, new BluetoothManager.IBluetoothResult() {
            @Override
            public void onResult(boolean isSuccess) {
                if (isSuccess) {
                    mDateTv.setText("");
                    mDateTv.setTag("");
                    mTimeTv.setText("");
                    mTimeTv.setTag("");
                    mDiastolicPressureEt.setText("");
                    mSystolicPressureEt.setText("");
                    onBackPressed();
//                    MediNewNetworkModule.doReLoginMediCare(getContext(), new MediNewNetworkHandler() {
//                        @Override
//                        public void onSuccess(BaseData responseData) {
//                            mDateTv.setText("");
//                            mDateTv.setTag("");
//                            mTimeTv.setText("");
//                            mTimeTv.setTag("");
//                            mDiastolicPressureEt.setText("");
//                            mSystolicPressureEt.setText("");
////                            SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MISSION_POP,true);
//                            onBackPressed();
////                            CDialog.showDlg(getContext(), getString(R.string.regist_success), new CDialog.DismissListener() {
////                                @Override
////                                public void onDissmiss() {
////
////                                }
////                            });
//                        }
//
//                        @Override
//                        public void onFailure(int statusCode, String response, Throwable error) {
//                            mDateTv.setText("");
//                            mDateTv.setTag("");
//                            mTimeTv.setText("");
//                            mTimeTv.setTag("");
//                            mDiastolicPressureEt.setText("");
//                            mSystolicPressureEt.setText("");
////                            SharedPref.getInstance().savePreferences(SharedPref.HEALTH_MISSION_POP,true);
//                            onBackPressed();
////                            CDialog.showDlg(getContext(), getString(R.string.regist_success), new CDialog.DismissListener() {
////                                @Override
////                                public void onDissmiss() {
////
////                                }
////                            });
//                        }
//                    });

                } else {
                    CDialog.showDlg(getContext(), getString(R.string.text_regist_fail));
                }
            }
        });
    }


    private boolean isValidSystolic() {
        String text = mSystolicPressureEt.getText().toString();
        return TextUtils.isEmpty(text) == false;
    }

    private boolean isValidDiastolic() {
        String text = mDiastolicPressureEt.getText().toString();
        return TextUtils.isEmpty(text) == false;
    }

    private boolean isValidDate() {
        String text = mDateTv.getText().toString();
        return TextUtils.isEmpty(text) == false;
    }

    private boolean isValidTime() {
        String text = mTimeTv.getText().toString();
        return TextUtils.isEmpty(text) == false;
    }

    @Override
    public void onPause() {
        super.onPause();
        mImm.hideSoftInputFromWindow(mDiastolicPressureEt.getWindowToken(), 0);
        mImm.hideSoftInputFromWindow(mSystolicPressureEt.getWindowToken(), 0);

    }
}