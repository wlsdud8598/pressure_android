package kr.co.hi.medicare.fragment.health.pressure;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.fragment.IBaseFragment;
import kr.co.hi.medicare.value.TypeDataSet;
import kr.co.hi.medicare.net.bluetooth.manager.DeviceDataUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.charting.data.PressureEntry;
import kr.co.hi.medicare.chartview.presure.PresureChartView;
import kr.co.hi.medicare.chartview.valueFormat.AxisValueFormatter3;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperPresure;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.fragment.health.message.HealthMessageFragment;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.ChartTimeUtil;
import kr.co.hi.medicare.utilhw.DisplayUtil;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.SharedPref;

/**
 * Created by insystemscompany on 2017. 2. 28..
 */

public class PressureManageFragment extends BaseFragmentMedi implements IBaseFragment {
    private final String TAG = PressureManageFragment.class.getSimpleName();

    public ChartTimeUtil mTimeClass;
    private PresureChartView mChart;
    private TextView mDateTv;

    private LinearLayout layout_pressure_history;
    private LinearLayout layout_pressure_graph;

    private TextView mStatTv;
    private TextView mBottomSystolicTv;
    private TextView mBottomDiastolcTv;
    private TextView mBottomMinTv;
    private TextView mBottomMaxTv;
    private ImageButton imgPre_btn;
    private ImageButton imgNext_btn;

    private PressureSwipeListView mSwipeListView;

    private AxisValueFormatter3 xFormatter;

    private ImageView mHealthMessage;
    private RelativeLayout message_lv;

    private RadioButton[] mFragmentRadio;
    private RadioGroup mFragmentGroup;
    private View tabview;

    private View mVisibleView1;
    private View mLableLayout;

    private View mChartFrameLayout;
    private ScrollView mContentScrollView;
    private View mPerodLayout;
    private View mDateLayout;
    private ImageView mChartCloseBtn, mChartZoomBtn;

    LinearLayout.LayoutParams params;
    int tempHeight=0;

    private CommonToolBar toolBar;

    public static Fragment newInstance() {
        PressureManageFragment fragment = new PressureManageFragment();
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        message_lv = getActivity().findViewById(R.id.message_lv);
        toolBar =  getActivity().findViewById(R.id.medi_common_toolbar);
//        mHealthMessage = getActivity().toolbar(R.id.common_toolbar_back_btn);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_pressure_manage, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tabview = view;
        mVisibleView1 = view.findViewById(R.id.visible_view1);
        mPerodLayout = view.findViewById(R.id.period_select_layout);
        mDateLayout = view.findViewById(R.id.chart_date_layout);
        mChartFrameLayout = view.findViewById(R.id.chart_frame_layout);
        mChartCloseBtn = view.findViewById(R.id.chart_close_btn);
        mChartZoomBtn = view.findViewById(R.id.landscape_btn);
        mLableLayout = view.findViewById(R.id.chart_label_layout);



        mFragmentGroup = view.findViewById(R.id.tab_radiogroup);

        //탭뷰
        final String[] mFragmentNames = new String[] {
                getString(R.string.text_graph),
                getString(R.string.text_history),
        };

        mFragmentRadio = new RadioButton[]{
                view.findViewById(R.id.tab1),
                view.findViewById(R.id.tab2),
        };

        int i = 0;
        for (String name : mFragmentNames) {
            mFragmentRadio[i].setText(name);
            i++;
        }

        mFragmentGroup.setOnCheckedChangeListener(mCheckedTabListener);

        mDateTv = (TextView) view.findViewById(R.id.period_date_textview);

        layout_pressure_graph  = (LinearLayout) view.findViewById(R.id.layout_pressure_graph);
        layout_pressure_history  = (LinearLayout) view.findViewById(R.id.layout_pressure_history);

        RadioGroup periodRg = (RadioGroup) view.findViewById(R.id.period_radio_group);
        RadioButton radioBtnDay = (RadioButton) view.findViewById(R.id.period_radio_btn_day);
        RadioButton radioBtnWeek = (RadioButton) view.findViewById(R.id.period_radio_btn_week);
        RadioButton radioBtnMonth = (RadioButton) view.findViewById(R.id.period_radio_btn_month);

        mStatTv = (TextView) view.findViewById(R.id.StatTv);
        mBottomSystolicTv = (TextView) view.findViewById(R.id.bottom_avg_min_textview);
        mBottomDiastolcTv = (TextView) view.findViewById(R.id.bottom_avg_max_textview);
        mBottomMinTv = (TextView) view.findViewById(R.id.bottom_min_textview);
        mBottomMaxTv = (TextView) view.findViewById(R.id.bottom_max_textview);

        imgPre_btn                  = (ImageButton) view.findViewById(R.id.pre_btn);
        imgNext_btn                 = (ImageButton) view.findViewById(R.id.next_btn);

        view.findViewById(R.id.pre_btn).setOnClickListener(mClickListener);
        view.findViewById(R.id.next_btn).setOnClickListener(mClickListener);
        view.findViewById(R.id.action_bar_write_btn).setOnClickListener(mClickListener);

        periodRg.setOnCheckedChangeListener(mCheckedChangeListener);

        mTimeClass = new ChartTimeUtil(radioBtnDay, radioBtnWeek, radioBtnMonth);
        mChart = new PresureChartView(getContext(), view);

        mSwipeListView = new PressureSwipeListView(view, PressureManageFragment.this);


        mChartFrameLayout = view.findViewById(R.id.chart_frame_layout);
        mContentScrollView = view.findViewById(R.id.layout_chart_scrollview);

//        mVisibleView1 = view.findViewById(R.id.visible_view1);
//        mVisibleView2 = view.findViewById(R.id.calorie_lv);
//        mVisibleView3 = view.findViewById(R.id.step_lv);

        mChartCloseBtn.setOnClickListener(mClickListener);
        mChartZoomBtn.setOnClickListener(mClickListener);

        setNextButtonVisible();

        params = (LinearLayout.LayoutParams) mChartFrameLayout.getLayoutParams();
        tempHeight = params.height;



        mFragmentRadio[0].setChecked(true);
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();

            if (vId == R.id.pre_btn) {
                mTimeClass.calTime(-1);
                getData();
            } else if (vId == R.id.next_btn) {
                // 초기값 일때 다음날 데이터는 없으므로 리턴
                if (mTimeClass.getCalTime() == 0)
                    return;

                mTimeClass.calTime(1);
                getData();
            } else if(vId == R.id.action_bar_write_btn){
                NewActivity.startActivityForResult(PressureManageFragment.this, 1111, PressureInputFragment.class, new Bundle());
            } else if(vId == R.id.share_write){
                CommunityListViewData sharelist = new CommunityListViewData();
                sharelist.ISSHARE=true;
                sharelist.CM_CONTENT = "";
                sharelist.CM_MEAL_LIST = new ArrayList<>();
                sharelist.CM_MEAL_LIST.add("평균 혈압 "+mBottomSystolicTv.getText().toString()+" / "+mBottomDiastolcTv.getText().toString()+ " mmHg");
                NewActivity.moveToWritePage(PressureManageFragment.this, sharelist,  "");
            } else if(vId == R.id.landscape_btn) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            } else if(vId == R.id.chart_close_btn) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }

            setNextButtonVisible();
        }
    };

    private void setNextButtonVisible(){
        // 초기값 일때 다음날 데이터는 없으므로 리턴
        if (mTimeClass.getCalTime() == 0) {
            imgNext_btn.setVisibility(View.INVISIBLE);
        }else{
            imgNext_btn.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 그래프, 히스토리 탭뷰
     */

    public RadioGroup.OnCheckedChangeListener mCheckedTabListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            int pos=group.indexOfChild(tabview.findViewById(checkedId));
            if(pos == 0){
                layout_pressure_history.setVisibility(View.GONE);
                layout_pressure_graph.setVisibility(View.VISIBLE);

                getData();
            }else {
                layout_pressure_graph.setVisibility(View.GONE);
                layout_pressure_history.setVisibility(View.VISIBLE);

                mSwipeListView.getHistoryData();
            }

        }
    };


    /**
     * 일간,주간,월간
     */
    public RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {

            // 일간, 주간, 월간
            TypeDataSet.Period periodType = mTimeClass.getPeriodType();
            mTimeClass.clearTime();         // 날자 초기화

            if(periodType == TypeDataSet.Period.PERIOD_DAY){
                mStatTv.setText(getString(R.string.daily_statistics));
            }else if(periodType == TypeDataSet.Period.PERIOD_WEEK){
                mStatTv.setText(getString(R.string.weekly_statistics));
            }else if(periodType == TypeDataSet.Period.PERIOD_MONTH){
                mStatTv.setText(getString(R.string.monthly_statistics));
            }

            getData();   // 날자 세팅 후 조회
        }
    };

    /**
     * 날자 계산 후 조회
     */
    private void getData() {
        long startTime = mTimeClass.getStartTime();
        long endTime = mTimeClass.getEndTime();

        xFormatter = new AxisValueFormatter3(mTimeClass.getPeriodType());
        mChart.setXValueFormat(xFormatter);

        String format = "yyyy.MM.dd";
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        String startDate = sdf.format(startTime);
        String endDate = sdf.format(endTime);

        if (mTimeClass.getPeriodType() == TypeDataSet.Period.PERIOD_DAY) {
            mDateTv.setText(startDate);
        } else {
            mDateTv.setText(startDate +" ~ "+endDate);
        }

        format = "yyyy-MM-dd";
        sdf = new SimpleDateFormat(format);
        startDate = sdf.format(startTime);
        endDate = sdf.format(endTime);
        getBottomDataLayout(startDate, endDate);
    }

    /**
     * 하단 데이터 세팅하기
     * @param startDate
     * @param endDate
     */
    private void getBottomDataLayout(String startDate, String endDate) {
        DBHelper helper = new DBHelper(getContext());
        DBHelperPresure db = helper.getPresureDb();
        DBHelperPresure.PressureData bottomData = db.getResultStatic(helper, startDate, endDate);

        mBottomSystolicTv.setText(""+bottomData.getSystolic());
        mBottomDiastolcTv.setText(""+bottomData.getDiastolc());
        mBottomMaxTv.setText(""+bottomData.getMaxsystolic());
        mBottomMinTv.setText(""+bottomData.getMaxdiastolc());

        new QeuryVerifyDataTask().execute();
    }

    public class QeuryVerifyDataTask extends AsyncTask<Void, Void, List<PressureEntry>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress();
        }

        protected List<PressureEntry> doInBackground(Void... params) {
            List<PressureEntry> yVals1 = null;
            DBHelper helper = new DBHelper(getContext());
            DBHelperPresure presureDb = helper.getPresureDb();
            TypeDataSet.Period period = mTimeClass.getPeriodType();

            mChart.setXvalMinMax(mTimeClass);

            if (period == TypeDataSet.Period.PERIOD_DAY) {
                String toDay = CDateUtil.getFormattedString_yyyy_MM_dd(mTimeClass.getStartTime());
                yVals1 = presureDb.getResultDay(helper, toDay);
//                mChart.setLabelCnt(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_WEEK));
            } else if (period == TypeDataSet.Period.PERIOD_WEEK) {
                String startDay = CDateUtil.getFormattedString_yyyy_MM_dd(mTimeClass.getStartTime());
                String endDay = CDateUtil.getFormattedString_yyyy_MM_dd(mTimeClass.getEndTime());

                yVals1 = presureDb.getResultWeek(helper, startDay, endDay);

//                mChart.setLabelCnt(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_WEEK));
            } else if (period == TypeDataSet.Period.PERIOD_MONTH) {
                String startDay = CDateUtil.getFormattedString_yyyy(mTimeClass.getStartTime());
                String endDay = CDateUtil.getFormattedString_MM(mTimeClass.getStartTime());
                // 이번달 최대 일수
                Calendar cal = Calendar.getInstance(); // CDateUtil.getCalendar_yyyyMMdd(startDay);
                cal.setTime(new Date(mTimeClass.getStartTime()));
                int dayCnt = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
                yVals1 = presureDb.getResultMonth(helper, startDay, endDay, dayCnt);

                xFormatter.setMonthMax(dayCnt);

                Logger.i(TAG, "dayCnt="+dayCnt+", month="+(cal.get(Calendar.MONTH)+1) ) ;
                // sqlite 조회 하여 결과 가져오기
//                mChart.setLabelCnt((dayCnt/2));
            }
            return yVals1;
        }

        @Override
        protected void onPostExecute(List<PressureEntry> yVals1) {
            super.onPostExecute(yVals1);
            hideProgress();

            Logger.i(TAG, "yVals1.size="+yVals1.size());
            mChart.setData(yVals1, mTimeClass);
            mChart.invalidate();
            setNextButtonVisible();
        }
    }


    private void setReadyDataView(){

        if (SharedPref.getInstance().getPreferences(SharedPref.HEALTH_MESSAGE, false)) {
            toolBar.setLeftIcon(R.drawable.health_m_btn_01ov);
//            mHealthMessage.setImageResource(R.drawable.health_m_btn_01ov);
            message_lv.setVisibility(View.VISIBLE);
        } else {
            toolBar.setLeftIcon(R.drawable.health_m_btn_01);
//            mHealthMessage.setImageResource(R.drawable.health_m_btn_01);
            message_lv.setVisibility(View.GONE);
        }

        if(!(SharedPref.getInstance().getPreferences(SharedPref.HEALTH_DANGER_POP).equals(CDateUtil.getToday_yyyy_MM_dd()))){
            if(DeviceDataUtil.Danger)
                DeviceDataUtil.showDangerMessage(PressureManageFragment.this, HealthMessageFragment.class,null);

        }

//        if(SharedPref.getInstance().getPreferences(SharedPref.HEALTH_MISSION_POP, false)){
//            health_point_popup("2",R.drawable.main_coin_4p_4); //식사
//        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.i(TAG, "onConfigurationChanged="+newConfig.orientation);
        if (getCurrentFragment() == MainActivityMedicare.HOME_MENU_2) {
            if (getCurrentTopFragment(PressureManageFragment.this) == 3) {
                setVisibleOrientationLayout(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE);
            }
        }
    }

    /**
     * 가로, 세로모드일때 불필요한 화면 Visible 처리
     */
    public void setVisibleOrientationLayout(final boolean isLandScape) {
        mVisibleView1.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
//        mVisibleView2.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
//        mVisibleView3.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mChartCloseBtn.setVisibility(isLandScape ? View.VISIBLE : View.GONE);
        mChartZoomBtn.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mPerodLayout.setVisibility(isLandScape ? View.GONE : View.VISIBLE);

        DisplayMetrics dm = getActivity().getResources().getDisplayMetrics();

        int portHeight = DisplayUtil.getDpToPix(getContext(),tempHeight);    // 세로모드일때 사이즈 250dp(레이아웃에 설정되어 있는 사이즈)
        int landHeight = (int) (dm.heightPixels
                - mDateLayout.getMeasuredHeight()               // 날자 표시
                - mLableLayout.getMeasuredHeight()               // 라벨표시
                - DisplayUtil.getStatusBarHeight(getContext())  // 상태바 높이
        );
        params.height = isLandScape ? landHeight : portHeight;
        mChartFrameLayout.setLayoutParams(params);

        // 가로모드일때 스크롤뷰 막기
        mContentScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return isLandScape;
            }
        });
        //가로모드 전환 시 스크롤 상단으로 위치
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mContentScrollView.smoothScrollTo(0,0);
            }
        }, 100);
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (getCurrentFragment() == MainActivityMedicare.HOME_MENU_2) {
//            if (getCurrentTopFragment(PressureManageFragment.this) == 3) {
                getData();
                mSwipeListView.getHistoryData();
                setReadyDataView();
//            }
//
//        }
    }
}