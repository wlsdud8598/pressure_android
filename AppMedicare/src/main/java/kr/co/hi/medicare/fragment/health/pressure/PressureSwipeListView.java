package kr.co.hi.medicare.fragment.health.pressure;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import kr.co.hi.medicare.net.hwNet.ApiData;
import kr.co.hi.medicare.net.hwNet.BaseData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.common.swipeListview.SwipeMenu;
import kr.co.hi.medicare.common.swipeListview.SwipeMenuCreator;
import kr.co.hi.medicare.common.swipeListview.SwipeMenuItem;
import kr.co.hi.medicare.common.swipeListview.SwipeMenuListView;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperPresure;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_brssr_info_del_data;
import kr.co.hi.medicare.net.data.Tr_brssr_info_edit_data;
import kr.co.hi.medicare.net.data.Tr_get_hedctdata;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.StringUtil;

/**
 * Created by MrsWin on 2017-04-15.
 */

public class PressureSwipeListView {
    private static final String TAG = PressureSwipeListView.class.getSimpleName();
    private PressureSwipeListView.AppAdapter mAdapter;
    private List<Tr_get_hedctdata.DataList> mSwipeMenuDatas = new ArrayList<>();
    private BaseFragmentMedi mBaseFragment;

    public PressureSwipeListView(View view, BaseFragmentMedi baseFragment) {
        mBaseFragment = baseFragment;
        SwipeMenuListView listView = (SwipeMenuListView) view.findViewById(R.id.history_listview);

        mAdapter = new AppAdapter();
        listView.setAdapter(mAdapter);

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                createMenu1(menu);
            }

            private void createMenu1(SwipeMenu menu) {
                SwipeMenuItem item1 = new SwipeMenuItem(mBaseFragment.getContext());
                item1.setBackground(new ColorDrawable(Color.rgb(105,129,236)));//new ColorDrawable(Color.rgb(0xE5, 0x18, 0x5E)));
                item1.setWidth(dp2px(60));
                item1.setIcon(R.drawable.health_m_btn04);
                item1.setTitleSize(12);
                item1.setTitle("수정");
                item1.setTitleColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.colorWhite));
                menu.addMenuItem(item1);
                SwipeMenuItem item2 = new SwipeMenuItem(mBaseFragment.getContext());
                item2.setBackground((new ColorDrawable(Color.rgb(143,144,157))));//(new ColorDrawable(Color.rgb(0xC9, 0xC9, 0xCE)));
                item2.setWidth(dp2px(60));
                item2.setIcon(R.drawable.health_m_btn05);
                item2.setTitleSize(12);
                item2.setTitle("삭제");
                item2.setTitleColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.colorWhite));
                menu.addMenuItem(item2);
            }

        };
        // set creator
        listView.setMenuCreator(creator);

        // step 2. listener item click event
        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:

                        Tr_get_hedctdata.DataList data = (Tr_get_hedctdata.DataList) mAdapter.getItem(position);
                        if (data.regtype.equals("U")) {
                            // 직정입력한 데이터만 수정할 수 있음.
                            new PressureSwipeListView.showModifiDlg(data);
                        } else {
                            // 장치에서 측정된 데이터는 수정할 수 없음.
                            String message = mBaseFragment.getContext().getString(R.string.text_alert_mesage_disable_edit);
                            CDialog.showDlg(mBaseFragment.getContext(), message);
                        }

                        break;
                    case 1:
                        // delete
                        String message = mBaseFragment.getContext().getString(R.string.text_alert_mesage_delete);
                        CDialog.showDlg(mBaseFragment.getContext(), message, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Tr_get_hedctdata.DataList data = (Tr_get_hedctdata.DataList) mAdapter.getItem(position);

                                doDeleteData(position, data);
                            }
                        }, null);

                        break;
                }

                // false:Swipe 닫힘, true:Swipe안닫힘
                return true;
            }
        });
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                mBaseFragment.getContext().getResources().getDisplayMetrics());
    }

    public void getHistoryData() {
        mSwipeMenuDatas.clear();

        DBHelper helper = new DBHelper(mBaseFragment.getContext());
        DBHelperPresure db = helper.getPresureDb();
        //혈압 스와이프 데이터 가져오기 오류
        mSwipeMenuDatas.addAll(db.getResultAll(helper));
        mAdapter.notifyDataSetChanged();
    }

    class AppAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mSwipeMenuDatas.size();
        }

        @Override
        public Object getItem(int position) {
            return mSwipeMenuDatas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            // menu type count
            return 3;
        }

        @Override
        public int getItemViewType(int position) {
            // current menu type
            return position % 3;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(mBaseFragment.getContext(), R.layout.swipe_menu_history_item_view, null);
                new ViewHolder(convertView);
            }
            PressureSwipeListView.AppAdapter.ViewHolder holder = (PressureSwipeListView.AppAdapter.ViewHolder) convertView.getTag();
            final Tr_get_hedctdata.DataList data = (Tr_get_hedctdata.DataList) getItem(position);

            final String yyyyMMddhhss = CDateUtil.getRegDateFormat_yyyyMMdd_HHss(data.reg_de, ".", ":");

//            if (StringUtil.getFloat(data.systolicPressure) == 0f && StringUtil.getFloat(data.diastolicPressure) == 0f) {
//                holder.typeTv.setVisibility(View.GONE);
//                holder.valueTv.setText("투약 (" + data.drugname + ")");
//                holder.beforeAfterTv.setText("");
//                holder.timeTv.setText(yyyyMMddhhss);
//                holder.mldlTv.setVisibility(View.GONE);
//            } else {
                holder.typeTv.setVisibility(View.GONE);
                String value = data.systolicPressure + "/" + data.diastolicPressure;
                holder.typeTv.setText("D".equals(data.regtype) ? mBaseFragment.getContext().getString(R.string.text_device) : mBaseFragment.getContext().getString(R.string.text_direct_enter));
                holder.valueTv.setText(value);
                holder.beforeAfterTv.setText("");//"0".equals(data.before) ? mContext.getString(R.string.text_foodbefore) : mContext.getString(R.string.text_foodafter));
                holder.timeTv.setText(yyyyMMddhhss);
                holder.mldlTv.setVisibility(View.VISIBLE);

                holder.share_write.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent it = new Intent(android.content.Intent.ACTION_SEND);
                        it.setType("text/plain");
                        it.putExtra(Intent.EXTRA_TEXT, "측정혈압("+yyyyMMddhhss+")\n수축기 : "+data.systolicPressure+", 이완기 : "+data.diastolicPressure);
                        mBaseFragment.startActivity(Intent.createChooser(it, "공유"));
                    }
                });
//            }

            return convertView;
        }

        class ViewHolder {
            TextView timeTv;
            TextView beforeAfterTv;
            TextView valueTv;
            TextView typeTv;
            TextView mldlTv;
            ImageButton share_write;

            public ViewHolder(View view) {
                timeTv = (TextView) view.findViewById(R.id.sugar_history_item_time_textview);
                beforeAfterTv = (TextView) view.findViewById(R.id.sugar_history_item_before_after_textview);
                valueTv = (TextView) view.findViewById(R.id.sugar_history_item_sugar_textview);
                typeTv = (TextView) view.findViewById(R.id.sugar_history_item_type_textview);
                mldlTv = (TextView) view.findViewById(R.id.sugar_history_item_mldl_textview);

                share_write = (ImageButton) view.findViewById(R.id.share_write);

                view.setTag(this);
            }
        }
    }

    /**
     * 혈압 삭제하기
     *
     * @param dataList
     */
    private void doDeleteData(final int position, final Tr_get_hedctdata.DataList dataList) {

        DBHelper helper = new DBHelper(mBaseFragment.getContext());
        DBHelperPresure presureDb = helper.getPresureDb();
        presureDb.DeleteDb(dataList.idx);

        mSwipeMenuDatas.remove(position);
        mAdapter.notifyDataSetChanged();

//        Tr_brssr_info_del_data inputData = new Tr_brssr_info_del_data();
//        Tr_brssr_info_del_data.RequestData requestData = new Tr_brssr_info_del_data.RequestData();
//        Tr_login login = UserInfo.getLoginInfo();
//
//        requestData.mber_sn = login.mber_sn;
//        requestData.ast_mass = inputData.getArray(dataList);
//
//        mBaseFragment.getData(mBaseFragment.getContext(), inputData.getClass(), requestData, new ApiData.IStep() {
//            @Override
//            public void next(Object obj) {
//                if (obj instanceof Tr_brssr_info_del_data) {
//                    Tr_brssr_info_del_data data = (Tr_brssr_info_del_data) obj;
//                    if ("Y".equals(data.reg_yn)) {
//
//                        MediNewNetworkModule.doReLoginMediCare(mBaseFragment.getContext(), new MediNewNetworkHandler() {
//                            @Override
//                            public void onSuccess(BaseData responseData) {
//                                DBHelper helper = new DBHelper(mBaseFragment.getContext());
//                                DBHelperPresure presureDb = helper.getPresureDb();
//                                presureDb.DeleteDb(dataList.idx);
//
//                                mSwipeMenuDatas.remove(position);
//                                mAdapter.notifyDataSetChanged();
//
////                                CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.delete_success));
//                            }
//
//                            @Override
//                            public void onFailure(int statusCode, String response, Throwable error) {
//                                DBHelper helper = new DBHelper(mBaseFragment.getContext());
//                                DBHelperPresure presureDb = helper.getPresureDb();
//                                presureDb.DeleteDb(dataList.idx);
//
//                                mSwipeMenuDatas.remove(position);
//                                mAdapter.notifyDataSetChanged();
//
////                                CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.delete_success));
//                            }
//                        });
//
//
//                    } else {
//                        CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.msg_fail_comment));
//                    }
//                }
//            }
//        });
    }

    class showModifiDlg {
        private EditText mPressureSystolicEt;
        private EditText mPressureDiastolicEt;
        private TextView mDateTv;
        private TextView mTimeTv;
        private EditText mMedicenInputEt;
        private String dataIdx;
        private LinearLayout mPressureLayout, dialog_btn_layout;
        private LinearLayout mMedicenLayout;

        private Button dialog_btn_no, dialog_btn_ok;

        private boolean mIsMedicenMode; // 투약정보 수정인지 여부

        /**
         * 수정 Dialog 띄우기
         */
        private showModifiDlg(final Tr_get_hedctdata.DataList data) {
            View modifyView = LayoutInflater.from(mBaseFragment.getContext()).inflate(R.layout.activity_pressure_input, null);

            CommonToolBar commonToolBar = modifyView.findViewById(R.id.common_bar);
            LinearLayout editBar = modifyView.findViewById(R.id.edit_lv);
            commonToolBar.setVisibility(View.GONE);
            editBar.setVisibility(View.VISIBLE);

            dialog_btn_layout = modifyView.findViewById(R.id.dialog_btn_layout);
            dialog_btn_no = modifyView.findViewById(R.id.dialog_btn_no);
            dialog_btn_ok = modifyView.findViewById(R.id.dialog_btn_ok);
            dialog_btn_layout.setVisibility(View.VISIBLE);



            mPressureSystolicEt = modifyView.findViewById(R.id.systolic_pressure);
            mPressureDiastolicEt = modifyView.findViewById(R.id.diastolic_pressure_edittext);
            mMedicenInputEt = modifyView.findViewById(R.id.pressure_medicen_editext);
            mDateTv = (TextView) modifyView.findViewById(R.id.pressure_input_date_textview);
            mTimeTv = (TextView) modifyView.findViewById(R.id.pressure_input_time_textview);
            mPressureLayout = (LinearLayout) modifyView.findViewById(R.id.pressure_input_pressure_layout);
            mMedicenLayout = (LinearLayout) modifyView.findViewById(R.id.pressure_input_medicen_layout);

            mDateTv.setEnabled(false);
            mTimeTv.setEnabled(true);
            dataIdx = data.idx;

            mTimeTv.setOnTouchListener(mTouchListener);

            Calendar cal = CDateUtil.getCalendar_yyyy_MM_dd_HH_mm(data.reg_de);

            // 2017-01-01 금요일 형태로 표시
            String yyyy_mm_dd = CDateUtil.getFormattedString_yyyy_MM_dd(cal.getTimeInMillis());
            String hh_mm = CDateUtil.getFormattedString_hh_mm(cal.getTimeInMillis());
            // 2017-01-01 금요일 형태로 표시
            mDateTv.setText(yyyy_mm_dd + " " + CDateUtil.getDateToWeek(cal) + "요일");
            mDateTv.setTag(StringUtil.getIntString(yyyy_mm_dd));   // yyyyMMdd
            // 오후 1:00 형태로 표시
            mTimeTv.setText(CDateUtil.getAmPmString(cal.get(Calendar.HOUR_OF_DAY)) + " " + cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE));
            mTimeTv.setTag(StringUtil.getIntString(hh_mm));   // HHmm

            mPressureLayout.setVisibility(View.VISIBLE);
            mMedicenLayout.setVisibility(View.GONE);
            mPressureSystolicEt.setText("" + StringUtil.getIntVal(data.systolicPressure));
            mPressureDiastolicEt.setText("" + StringUtil.getIntVal(data.diastolicPressure));

            DisplayMetrics displayMetrics = mBaseFragment.getContext().getResources().getDisplayMetrics();
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;

            final CDialog dlg = CDialog.showDlg(mBaseFragment.getContext(), modifyView,(int)(height*0.9),(int)(width*0.9),false);
                dlg.setTitle("");
            dialog_btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String regDate = mDateTv.getTag().toString();
                    if (TextUtils.isEmpty(regDate)) {
                        CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.date_input_error));
                        return;
                    }

                    String timeStr = mTimeTv.getTag().toString();
                    if (TextUtils.isEmpty(timeStr)) {
                        CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.time_input_error));
                        return;
                    }

                    final String systolicPressure = mPressureSystolicEt.getText().toString();
                    if (TextUtils.isEmpty(systolicPressure)) {
                        CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.presure_systolic_presure_input_error));
                        return;
                    }

                    final String diastolicPressure = mPressureDiastolicEt.getText().toString();
                    if (TextUtils.isEmpty(diastolicPressure)) {
                        CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.presure_diastolic_presure_input_error));
                        return;
                    }

                    if(StringUtil.getFloatVal(systolicPressure) < 20.0f || StringUtil.getFloatVal(systolicPressure) >= 300.0f || StringUtil.getFloatVal(diastolicPressure) < 20.0f || StringUtil.getFloatVal(diastolicPressure) >= 300.0f){
                        CDialog.showDlg(mBaseFragment.getContext(), "20이상 300이하의 값을 입력해주세요.");
                        return;
                    }

                    //미래시간 입력방지
                    if(StringUtil.getLongVal(regDate+timeStr) > StringUtil.getLongVal(CDateUtil.getToday_yyyy_MM_dd_HH_mm())){
                        CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.message_nowtime_over));
                        return;
                    }


                    dlg.dismiss();

                    boolean isDrugMode = StringUtil.getFloat(data.sugar) == 0f && TextUtils.isEmpty(data.drugname) == false;

                    if (isDrugMode) {

                        if (mMedicenInputEt.getText().toString().isEmpty()) {
                            mMedicenInputEt.setText(" ");
                        }

                        data.drugname = mMedicenInputEt.getText().toString();
                    } else {
                        data.systolicPressure = mPressureSystolicEt.getText().toString();
                        data.diastolicPressure = mPressureDiastolicEt.getText().toString();
                        data.drugname = "";
                    }

                    regDate += timeStr;
                    data.reg_de = regDate;

                    doModifyData(data);

                }
            });

            dialog_btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dlg.dismiss();
                }
            });
        }

        View.OnTouchListener mTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    int vId = v.getId();
                    if (vId == R.id.pressure_input_time_textview) {
                        showTimePicker();
                    }
                }
                return false;
            }
        };

        /**
         * 시간 피커 완료
         */
        private void showTimePicker() {
            Calendar cal = Calendar.getInstance(Locale.KOREA);
            Date now = new Date();
            cal.setTime(now);

            int hour = cal.get(Calendar.HOUR_OF_DAY);
            int minute = cal.get(Calendar.MINUTE);
            String time = mTimeTv.getTag().toString();
            if (TextUtils.isEmpty(time) == false) {
                hour = StringUtil.getIntVal(time.substring(0, 2));
                minute = StringUtil.getIntVal(time.substring(2, 4));

                Logger.i(TAG, "hour=" + hour + ", minute=" + minute);
            }

            TimePickerDialog dialog = new TimePickerDialog(mBaseFragment.getContext(), listener, hour, minute, false);
            dialog.show();
        }

        private TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                mTimeTvSet(hourOfDay, minute);
            }
        };

        private void mTimeTvSet(int hourOfDay, int minute) {
            // 설정버튼 눌렀을 때
            String amPm = mBaseFragment.getContext().getString(R.string.text_morning);
            int hour = hourOfDay;
            if (hourOfDay > 11) {
                amPm = mBaseFragment.getContext().getString(R.string.text_afternoon);
                if (hourOfDay >= 13)
                    hour -= 12;
            } else {
                hour = hour == 0 ? 12 : hour;
            }
            String tagMsg = String.format("%02d%02d", hourOfDay, minute);
            String timeStr = String.format("%02d:%02d", hour, minute);
            mTimeTv.setText(amPm + " " + timeStr);
            mTimeTv.setTag(tagMsg);
        }


        private boolean isValidDate() {
            String text = mDateTv.getText().toString();
            return TextUtils.isEmpty(text) == false;
        }

        private boolean isValidTime() {
            String text = mTimeTv.getText().toString();
            return TextUtils.isEmpty(text) == false;
        }

        private boolean isValidSystolic() {
//            if (mIsMedicenMode) {
//                return true;
//
//            } else {
                String text = mPressureSystolicEt.getText().toString();
                String ttext = mPressureDiastolicEt.getText().toString();
                return TextUtils.isEmpty(text) == false && TextUtils.isEmpty(ttext) == false;
//            }
        }


        /**
         * 혈압 수정하기
         *
         * @param dataList
         */
        private void doModifyData(final Tr_get_hedctdata.DataList dataList) {
            DBHelper helper = new DBHelper(mBaseFragment.getContext());
            DBHelperPresure presureDb = helper.getPresureDb();
            presureDb.UpdateDb(dataIdx, mPressureSystolicEt.getText().toString(), mPressureDiastolicEt.getText().toString(), mMedicenInputEt.getText().toString(), dataList.reg_de);

            getHistoryData();

//            Tr_brssr_info_edit_data inputData = new Tr_brssr_info_edit_data();
//            Tr_brssr_info_edit_data.RequestData requestData = new Tr_brssr_info_edit_data.RequestData();
//            Tr_login login = UserInfo.getLoginInfo();
//
//            requestData.mber_sn = login.mber_sn;
//            requestData.ast_mass = inputData.getArray(dataList);

//            mBaseFragment.getData(mBaseFragment.getContext(), inputData.getClass(), requestData, new ApiData.IStep() {
//                @Override
//                public void next(Object obj) {
//                    if (obj instanceof Tr_brssr_info_edit_data) {
//                        Tr_brssr_info_edit_data data = (Tr_brssr_info_edit_data) obj;
//                        if ("Y".equals(data.reg_yn)) {
//
//                            MediNewNetworkModule.doReLoginMediCare(mBaseFragment.getContext(),new MediNewNetworkHandler() {
//                                @Override
//                                public void onSuccess(BaseData responseData) {
//                                    DBHelper helper = new DBHelper(mBaseFragment.getContext());
//                                    DBHelperPresure presureDb = helper.getPresureDb();
//                                    presureDb.UpdateDb(dataIdx, mPressureSystolicEt.getText().toString(), mPressureDiastolicEt.getText().toString(), mMedicenInputEt.getText().toString(), dataList.reg_de);
//
//                                    getHistoryData();
//
////                                    CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.modify_success));
//                                }
//
//                                @Override
//                                public void onFailure(int statusCode, String response, Throwable error) {
//                                    DBHelper helper = new DBHelper(mBaseFragment.getContext());
//                                    DBHelperPresure presureDb = helper.getPresureDb();
//                                    presureDb.UpdateDb(dataIdx, mPressureSystolicEt.getText().toString(), mPressureDiastolicEt.getText().toString(), mMedicenInputEt.getText().toString(), dataList.reg_de);
//
//                                    getHistoryData();
//
////                                    CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.modify_success));
//                                }
//                            });
//
//
//                        } else {
//                            CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.text_update_fail));
//                        }
//                    }
//                }
//            });
        }
    }
}