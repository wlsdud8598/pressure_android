package kr.co.hi.medicare.fragment.health.question;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.adapter.QuestionnaireListView_Adapter;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperLog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;

/**
 * 질병 위험도 체크.
 */
public class QuestionnaireFragment extends BaseFragmentMedi {
    private UserInfo user;
    private QuestionnaireListView_Adapter adapter = null;

    public static Fragment newInstance() {
        QuestionnaireFragment fragment = new QuestionnaireFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_questionnaire, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);
    }

    private void init(View view) {

        ListView activity_question_ListView_list = (ListView) view.findViewById(R.id.activity_question_ListView_list);
        adapter = new QuestionnaireListView_Adapter(QuestionnaireFragment.this);
        activity_question_ListView_list.setAdapter(adapter);
        activity_question_ListView_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                user = new UserInfo(getContext());


                view.setContentDescription("HM03_03"+position+"_001_!");
                if (view.getContentDescription().toString().contains("_!")) {
                    String temp = view.getContentDescription().toString().replace("_!", "");
                    String cod[] = temp.split("_");

                    DBHelper helper = new DBHelper(getContext());
                    DBHelperLog logdb = helper.getLogDb();

                    if (cod.length == 1) {
                        logdb.insert(cod[0], "", "", 0, 1);
                        Log.i(TAG, "view.contentDescription : " + cod[0] + "count : 1");
                    } else if (cod.length == 2) {
                        logdb.insert(cod[0], cod[1], "", 0, 1);
                        Log.i(TAG, "view.contentDescription : " + cod[0] + cod[1] + "count : 1");
                    } else {
                        logdb.insert(cod[0], cod[1], cod[2], 0, 1);
                        Log.i(TAG, "view.contentDescription : " + cod[0] + cod[1] + cod[2] + "count : 1");
                    }

                }

                if(user.getQuestionLevel(position + 1) == 0)
                {
                    Bundle bundle = new Bundle();
                    bundle.putString(WebviewQuestionFragment.TITLE, ""+position);
                    bundle.putString(WebviewQuestionFragment.URL, "file:///android_asset/question" + (position + 1) + ".html");
                    bundle.putString(WebviewQuestionFragment.POS, ""+(position + 1));
//                    replaceFragment(WebviewQuestionFragment.newInstance(), bundle);
                    NewActivity.startActivity(QuestionnaireFragment.this, WebviewQuestionFragment.class, bundle);

                }
                else
                {
                    Bundle bundle = new Bundle();
                    bundle.putString(WebviewQuestionFragment.TITLE, ""+100);
                    bundle.putString(WebviewQuestionFragment.URL, "file:///android_asset/question" + (position + 1) + ".html");
                    bundle.putString("COMMENT", "");
                    bundle.putString(WebviewQuestionFragment.POS, ""+(position + 1));
//                    replaceFragment(WebviewQuestionFragment.newInstance(), bundle);
                    NewActivity.startActivity(QuestionnaireFragment.this, WebviewQuestionFragment.class, bundle);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

}
