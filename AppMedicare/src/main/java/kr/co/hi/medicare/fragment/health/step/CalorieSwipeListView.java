package kr.co.hi.medicare.fragment.health.step;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import kr.co.hi.medicare.net.hwNet.BaseData;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.common.swipeListview.SwipeMenu;
import kr.co.hi.medicare.common.swipeListview.SwipeMenuCreator;
import kr.co.hi.medicare.common.swipeListview.SwipeMenuItem;
import kr.co.hi.medicare.common.swipeListview.SwipeMenuListView;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperCalorie;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_mvm_info_mber_sport_list;
import kr.co.hi.medicare.net.data.Tr_mvm_info_sport_del;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.StringUtil;

import static kr.co.hi.medicare.component.CDialog.showDlg;

/**
 * Created by MrsWin on 2017-04-15.
 */

public class CalorieSwipeListView {
    private static final String TAG = SwipeMenuListView.class.getSimpleName();
    private AppAdapter mAdapter;
    private List<Tr_mvm_info_mber_sport_list.sport_list> mSwipeMenuDatas = new ArrayList<>();
    private BaseFragmentMedi mBaseFragment;
    private Context mContext;
    private SwipeMenuListView listView;
    private String InputDe = "";
    private int mPageNo = 1;
    private int maxpage;
    private boolean mIsLast = false;

    private boolean loading = true;
    private int previousTotal = 0;
    private int visibleThreshold = 10;
    private int firstVisibleItem=0, visibleItemCount=0, totalItemCount=0;

    private int mOldPos = 0;

    public CalorieSwipeListView(View view, BaseFragmentMedi baseFragment, String inputde) {
        mBaseFragment = baseFragment;
        mContext = baseFragment.getContext();
        listView = (SwipeMenuListView) view.findViewById(R.id.ex_listview);

        InputDe = inputde;

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                visibleItemCount = visibleItemCount;
                totalItemCount = totalItemCount;
                firstVisibleItem = firstVisibleItem;

                if(loading) {
                    if(totalItemCount > previousTotal) {
                        previousTotal = totalItemCount;
                        Log.i(TAG, "recycleView end called " + loading + " " + visibleItemCount+"/" + totalItemCount+"/" + firstVisibleItem);
                        loading = false;
                    }
                }
                if(!loading && totalItemCount <= firstVisibleItem + visibleItemCount){
                    Log.i(TAG, "recycleView end called " + loading + " " + visibleItemCount+"/" + totalItemCount+"/" + firstVisibleItem);


                    getHistoryData(true);
                    loading = true;
                }
            }
        });

        if(mOldPos == 0){
            mAdapter = new AppAdapter();

            mPageNo = 0;

            getHistoryData(true);

        }

        listView.setAdapter(mAdapter);



        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                createMenu1(menu);
            }

            private void createMenu1(SwipeMenu menu) {
                SwipeMenuItem item1 = new SwipeMenuItem(mContext);
                item1.setBackground((new ColorDrawable(Color.rgb(143,144,157))));//(new ColorDrawable(Color.rgb(0xC9, 0xC9, 0xCE)));
                item1.setWidth(dp2px(60));
                item1.setIcon(R.drawable.health_m_btn05);
                item1.setTitleSize(12);
                item1.setTitle("삭제");
                item1.setTitleColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.colorWhite));
                menu.addMenuItem(item1);
            }

        };
        // set creator
        listView.setMenuCreator(creator);

        // step 2. listener item click event
        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        String message = mContext.getString(R.string.text_alert_mesage_delete);
                        showDlg(mContext, message)
                                .setOkButton("네", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Tr_mvm_info_mber_sport_list.sport_list data = (Tr_mvm_info_mber_sport_list.sport_list) mAdapter.getItem(position);
                                        doDeleteExData(data);
//                                        setListViewChage();
                                    }
                                })
                                .setNoButton("아니오",null);

                        break;
                }

                return true;
            }
        });

//        setListViewChage();
    }

    public List<Tr_mvm_info_mber_sport_list.sport_list> getHistoryData() {
        return mSwipeMenuDatas;
    }








    public void getHistoryData(boolean is_insert) {

        Tr_mvm_info_mber_sport_list.RequestData requestData = new Tr_mvm_info_mber_sport_list.RequestData();
        Tr_login login = UserInfo.getLoginInfo();
        requestData.mber_sn = login.mber_sn;
        if (!is_insert){
            mPageNo = 0;
            previousTotal = 0;

        }

        if (mPageNo == 0 || maxpage > mPageNo)
            requestData.pageNumber = "" + (++mPageNo);
        else
            return;

        requestData.input_de = InputDe;


        MediNewNetworkModule.doApi(mContext, Tr_mvm_info_mber_sport_list.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_mvm_info_mber_sport_list) {
                    Tr_mvm_info_mber_sport_list data = (Tr_mvm_info_mber_sport_list)responseData;
                    try {
                        List<Tr_mvm_info_mber_sport_list.sport_list> list = data.sport_list;

                        maxpage = StringUtil.getIntVal(data.maxpageNumber);
                        setDataList(list);


                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
            }
        });


    }


    /**
     * 운동 데이터 삭제
     *
     * @param EXData
     */
    private void doDeleteExData(final Tr_mvm_info_mber_sport_list.sport_list EXData) {
        if (TextUtils.isEmpty(EXData.sport_sn)) {
            // 삭제하기
            mSwipeMenuDatas.remove(EXData);
            mAdapter.notifyDataSetChanged();
        } else {
            // 삭제하기
            Tr_mvm_info_sport_del.RequestData requestData = new Tr_mvm_info_sport_del.RequestData();
            Tr_login login = UserInfo.getLoginInfo();
            requestData.mber_sn = login.mber_sn;
            requestData.sport_sn = EXData.sport_sn;
            requestData.active_seq = EXData.active_seq;
            requestData.active_de = EXData.reg_de;


            MediNewNetworkModule.doApi(mContext, Tr_mvm_info_sport_del.class, requestData, new MediNewNetworkHandler() {
                @Override
                public void onSuccess(BaseData responseData) {
                    if (responseData instanceof Tr_mvm_info_sport_del) {
                        Tr_mvm_info_sport_del data = (Tr_mvm_info_sport_del)responseData;
                        try {
                            if ("Y".equals(data.reg_yn)) {
                                DBHelper helper = new DBHelper(mContext);
                                DBHelperCalorie db = helper.getCalorieDb();
                                db.DeleteDb(EXData.sport_sn);

                                mSwipeMenuDatas.remove(EXData);
                                mAdapter.notifyDataSetChanged();
                            } else {
                                CDialog.showDlg(mContext, mContext.getString(R.string.msg_fail_comment));
                            }

                        } catch (Exception e) {
                            CLog.e(e.toString());
                        }
                    }
                }

                @Override
                public void onFailure(int statusCode, String response, Throwable error) {
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
                }
            });
        }
    }

//    private void setListViewChage() {
//        if (mSwipeMenuDatas.size() <= 0) {
//            listView.setVisibility(View.GONE);
//            mEmptyView.setVisibility(View.VISIBLE);
//        } else {
//            mEmptyView.setVisibility(View.GONE);
//            listView.setVisibility(View.VISIBLE);
//        }
//    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                mContext.getResources().getDisplayMetrics());
    }

    public void setData(Tr_mvm_info_mber_sport_list.sport_list data) {
        mSwipeMenuDatas.add(data);
//        setListViewChage();
        mAdapter.notifyDataSetChanged();
    }

    public void setDataList(List<Tr_mvm_info_mber_sport_list.sport_list> data) {
        if (mPageNo == 1) {
            listView.smoothScrollToPosition(0);
            mSwipeMenuDatas.clear();

        }

        mSwipeMenuDatas.addAll(data);
//        setListViewChage();
        mAdapter.notifyDataSetChanged();
    }


    /**
     * @return
     */
    public List<Tr_mvm_info_mber_sport_list.sport_list> getListData() {
        return mSwipeMenuDatas;
    }

    class AppAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mSwipeMenuDatas.size();
        }

        @Override
        public Object getItem(int position) {
            return mSwipeMenuDatas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            // menu type count
            return 3;
        }

        @Override
        public int getItemViewType(int position) {
            // current menu type
            return position % 3;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(mContext, R.layout.swipe_calorie_item_view, null);
                new ViewHolder(convertView);
            }
            ViewHolder holder = (ViewHolder) convertView.getTag();
            Tr_mvm_info_mber_sport_list.sport_list data = (Tr_mvm_info_mber_sport_list.sport_list) getItem(position);

            holder.ex_time.setVisibility(View.VISIBLE);
            holder.ex_time.setText(data.active_de_stime);

            holder.ex_nm.setText(data.sport_nm);
            holder.ex_play_time.setText(data.active_time+"분");
            holder.ex_calorie.setText(data.calory+"kcal");




            return convertView;
        }

        class ViewHolder {
            TextView ex_time, ex_nm, ex_play_time, ex_calorie;
            TextView calorieTv;

            public ViewHolder(View view) {
                ex_time = (TextView) view.findViewById(R.id.ex_time);
                ex_nm = (TextView) view.findViewById(R.id.ex_nm);
                ex_play_time = (TextView) view.findViewById(R.id.ex_play_time);
                ex_calorie = (TextView) view.findViewById(R.id.ex_calorie);

                view.setTag(this);
            }
        }
    }

}
