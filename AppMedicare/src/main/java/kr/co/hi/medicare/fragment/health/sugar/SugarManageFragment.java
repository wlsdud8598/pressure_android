package kr.co.hi.medicare.fragment.health.sugar;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.net.bluetooth.manager.DeviceDataUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.charting.data.SticEntry;
import kr.co.hi.medicare.chartview.sugar.SugarStickChartView;
import kr.co.hi.medicare.chartview.valueFormat.AxisValueFormatter3;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperSugar;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.fragment.health.message.HealthMessageFragment;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.ChartTimeUtil;
import kr.co.hi.medicare.utilhw.DisplayUtil;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.value.Define;
import kr.co.hi.medicare.value.TypeDataSet;

/**
 * Created by mrsohn on 2017. 3. 14..
 * 혈당관리
 */

public class SugarManageFragment extends BaseFragmentMedi {
    private final String TAG = SugarManageFragment.class.getSimpleName();

    public ChartTimeUtil mTimeClass;
    private SugarStickChartView mChart;
    private TextView mDateTv;

    private LinearLayout layout_sugar_history;
    private LinearLayout layout_sugar_graph;

    private TextView mStatTv;
    private TextView mBottomBeforeTv;
    private TextView mBottomAfterTv;
    private TextView mBottomMinTv;
    private TextView mBottomMaxTv;

    private SugarSwipeListView mSwipeListView;
    private RadioGroup mTypeRg;

    private ImageButton imgPre_btn;
    private ImageButton imgNext_btn;

    private AxisValueFormatter3 xFormatter;

    private ImageView mHealthMessage;
    private RelativeLayout message_lv;

    private RadioButton[] mFragmentRadio;
    private RadioGroup mFragmentGroup;
    private View tabview;

    private View mVisibleView1;
    private View mVisibleView2;
    private LinearLayout mLableLayout;
    private ImageButton share_write;
    private View mChartFrameLayout;
    private ScrollView mContentScrollView;
    private View mPerodLayout;
    private View mDateLayout;
    private ImageView mChartCloseBtn, mChartZoomBtn;
    private String eattype;

    LinearLayout.LayoutParams params;
    int tempHeight=0;

    public static Fragment newInstance() {
        SugarManageFragment fragment = new SugarManageFragment();
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        message_lv = getActivity().findViewById(R.id.message_lv);
        mHealthMessage = getActivity().findViewById(R.id.common_toolbar_back_btn);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_sugar_manage, container, false);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //탭뷰
        tabview = view;
        mVisibleView1 = view.findViewById(R.id.visible_view1);
        mVisibleView2 = view.findViewById(R.id.visible_view2);
        mPerodLayout = view.findViewById(R.id.period_select_layout);
        mDateLayout = view.findViewById(R.id.chart_date_layout);
        mChartFrameLayout = view.findViewById(R.id.chart_frame_layout);
        mChartCloseBtn = view.findViewById(R.id.chart_close_btn);
        mChartZoomBtn = view.findViewById(R.id.landscape_btn);
        mContentScrollView = view.findViewById(R.id.layout_chart_scrollview);
        mLableLayout = view.findViewById(R.id.sugar_chart_label_layout);

        mFragmentGroup = view.findViewById(R.id.tab_radiogroup);

        final String[] mFragmentNames = new String[] {
                getString(R.string.text_graph),
                getString(R.string.text_history),
        };

        mFragmentRadio = new RadioButton[]{
                view.findViewById(R.id.tab1),
                view.findViewById(R.id.tab2),
        };

        int i = 0;
        for (String name : mFragmentNames) {
            mFragmentRadio[i].setText(name);
            i++;
        }

        mFragmentGroup.setOnCheckedChangeListener(mCheckedTabListener);

        mDateTv                 = (TextView) view.findViewById(R.id.period_date_textview);
        layout_sugar_graph      = (LinearLayout) view.findViewById(R.id.layout_sugar_graph);
        layout_sugar_history    = (LinearLayout) view.findViewById(R.id.layout_sugar_history);


        mTypeRg                 = (RadioGroup) view.findViewById(R.id.radiogroup_sugar_type);
        RadioButton typeAll     = (RadioButton) view.findViewById(R.id.radio_sugar_type_all);
        RadioButton typeBefore  = (RadioButton) view.findViewById(R.id.radio_sugar_type_before);
        RadioButton typeAfter   = (RadioButton) view.findViewById(R.id.radio_sugar_type_after);
        mTypeRg.setOnCheckedChangeListener(mTypeCheckedChangeListener);

        RadioGroup periodRg         = (RadioGroup) view.findViewById(R.id.period_radio_group);
        RadioButton radioBtnDay     = (RadioButton) view.findViewById(R.id.period_radio_btn_day);
        RadioButton radioBtnWeek    = (RadioButton) view.findViewById(R.id.period_radio_btn_week);
        RadioButton radioBtnMonth   = (RadioButton) view.findViewById(R.id.period_radio_btn_month);

        imgPre_btn                  = (ImageButton) view.findViewById(R.id.pre_btn);
        imgNext_btn                 = (ImageButton) view.findViewById(R.id.next_btn);

        mStatTv             = (TextView) view.findViewById(R.id.StatTv);
        mBottomBeforeTv     = (TextView) view.findViewById(R.id.bottom_sugar_before_textview);
        mBottomAfterTv      = (TextView) view.findViewById(R.id.bottom_sugar_after_textview);
        mBottomMinTv        = (TextView) view.findViewById(R.id.bottom_sugar_min_textview);
        mBottomMaxTv        = (TextView) view.findViewById(R.id.bottom_sugar_max_textview);
        share_write        = view.findViewById(R.id.share_write);



        view.findViewById(R.id.pre_btn).setOnClickListener(mClickListener);
        view.findViewById(R.id.next_btn).setOnClickListener(mClickListener);
        view.findViewById(R.id.action_bar_write_btn).setOnClickListener(mClickListener);
        share_write.setOnClickListener(mClickListener);
        periodRg.setOnCheckedChangeListener(mCheckedChangeListener);

        mTimeClass      = new ChartTimeUtil(radioBtnDay, radioBtnWeek, radioBtnMonth, typeAll, typeBefore, typeAfter);
        mChart          = new SugarStickChartView(getContext(), view, mTimeClass);

        xFormatter = new AxisValueFormatter3(mTimeClass.getPeriodType());
        mChart.setXValueFormat(xFormatter);

        // 스와이프 리스트뷰 세팅 하기
        mSwipeListView  = new SugarSwipeListView(view, SugarManageFragment.this);
        setNextButtonVisible();

        mChartCloseBtn = view.findViewById(R.id.chart_close_btn);
        mChartZoomBtn = view.findViewById(R.id.landscape_btn);
//        mVisibleView1 = view.findViewById(R.id.visible_view1);
//        mVisibleView2 = view.findViewById(R.id.calorie_lv);
//        mVisibleView3 = view.findViewById(R.id.step_lv);

        mChartCloseBtn.setOnClickListener(mClickListener);
        mChartZoomBtn.setOnClickListener(mClickListener);

        params = (LinearLayout.LayoutParams) mChartFrameLayout.getLayoutParams();
        tempHeight = params.height;

        //click 저장
        OnClickListener ClickListener = new OnClickListener(mClickListener, view, getContext());


        //건강관리
        view.findViewById(R.id.tab1).setOnTouchListener(ClickListener);
        view.findViewById(R.id.tab2).setOnTouchListener(ClickListener);
        view.findViewById(R.id.action_bar_write_btn).setOnTouchListener(ClickListener);
        share_write.setOnTouchListener(ClickListener);

        //코드부여
        view.findViewById(R.id.tab1).setContentDescription(getString(R.string.sugar_gra));
        view.findViewById(R.id.tab2).setContentDescription(getString(R.string.sugar_his));
        view.findViewById(R.id.action_bar_write_btn).setContentDescription(getString(R.string.sugar_input));
        share_write.setContentDescription(getString(R.string.share_write_sugar));

        mFragmentRadio[0].setChecked(true);
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();

            if (vId == R.id.pre_btn) {
                mTimeClass.calTime(-1);
                getData();
            } else if (vId == R.id.next_btn) {
                // 초기값 일때 다음날 데이터는 없으므로 리턴
                if (mTimeClass.getCalTime() == 0)
                    return;
                mTimeClass.calTime(1);
                getData();
            } else if(vId == R.id.action_bar_write_btn){
                Bundle bundle = new Bundle();
                bundle.putString("EATTYPE",eattype);
                NewActivity.startActivityForResult(SugarManageFragment.this, 1111, SugarInputFragment.class, bundle);
            } else if(vId == R.id.share_write){
                CommunityListViewData sharelist = new CommunityListViewData();
                sharelist.ISSHARE=true;
                sharelist.CM_CONTENT = "";
                sharelist.CM_MEAL_LIST = new ArrayList<>();
                sharelist.CM_MEAL_LIST.add("평균 혈당 식전 : "+mBottomBeforeTv.getText().toString()+" mm/dL 식후 : "+mBottomAfterTv.getText().toString()+ " mm/dL");
                NewActivity.moveToWritePage(SugarManageFragment.this, sharelist, "");
            } else if(vId == R.id.landscape_btn) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            } else if(vId == R.id.chart_close_btn) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }

            setNextButtonVisible();
        }
    };

    private void setNextButtonVisible(){
        // 초기값 일때 다음날 데이터는 없으므로 리턴
        if (mTimeClass.getCalTime() == 0) {
            imgNext_btn.setVisibility(View.INVISIBLE);
        }else{
            imgNext_btn.setVisibility(View.VISIBLE);
        }
    }


    /**
     * 그래프, 히스토리 탭뷰
     */

    public RadioGroup.OnCheckedChangeListener mCheckedTabListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            int pos=group.indexOfChild(tabview.findViewById(checkedId));
            if(pos == 0){

                layout_sugar_history.setVisibility(View.GONE);
                layout_sugar_graph.setVisibility(View.VISIBLE);

                getData();
            }else {

                layout_sugar_graph.setVisibility(View.GONE);
                layout_sugar_history.setVisibility(View.VISIBLE);

                mSwipeListView.getHistoryData();
            }

        }
    };



    /**
     * 모두, 식전, 식후
     */
    public RadioGroup.OnCheckedChangeListener mTypeCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {

            // 모두, 식전, 식후
            TypeDataSet.EatState EatState = mTimeClass.getEatType();
            String State    = "";
            if (EatState == TypeDataSet.EatState.TYPE_BEFORE) {
                State        = "식전";
            } else if (EatState == TypeDataSet.EatState.TYPE_AFTER) {
                State        = "식후";
            }
            // 일간, 주간, 월간
            TypeDataSet.Period periodType = mTimeClass.getPeriodType();

            if (periodType == TypeDataSet.Period.PERIOD_DAY) {
                mStatTv.setText("일간 " + State + " 통계");
            } else if (periodType == TypeDataSet.Period.PERIOD_WEEK) {
                mStatTv.setText("주간 " + State + " 통계");
            } else if (periodType == TypeDataSet.Period.PERIOD_MONTH) {
                mStatTv.setText("월간 " + State + " 통계");
            }

            getBeforeAndAfterType();
            getData();   // 날자 세팅 후 조회
        }
    };

    /**
     * 모두, 식전, 식후 여부 판단
     *
     * @return
     */
    private int getBeforeAndAfterType() {
        int beforeAntAfter = Define.SUGAR_TYPE_ALL;
        if (mTypeRg.getCheckedRadioButtonId() == R.id.radio_sugar_type_all) {
            eattype = "";
            mChart.setYAxisMinimum(60f, 243, 9);
            beforeAntAfter = Define.SUGAR_TYPE_ALL;
        } else if (mTypeRg.getCheckedRadioButtonId() == R.id.radio_sugar_type_before) {
            // 식전 60~240
            eattype = "";
            mChart.setYAxisMinimum(60f, 152, 9);
            beforeAntAfter = Define.SUGAR_TYPE_BEFORE;
        } else if (mTypeRg.getCheckedRadioButtonId() == R.id.radio_sugar_type_after) {
            // 식후 120~240
            eattype = "2";
            mChart.setYAxisMinimum(60f, 243, 9);
            beforeAntAfter = Define.SUGAR_TYPE_AFTER;
        }
        return beforeAntAfter;
    }

    /**
     * 일간,주간,월간
     */
    public RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {

            // 모두, 식전, 식후
            TypeDataSet.EatState EatState = mTimeClass.getEatType();
            String State    = "";
            if (EatState == TypeDataSet.EatState.TYPE_BEFORE) {
                State       = "식전";
            } else if (EatState == TypeDataSet.EatState.TYPE_AFTER) {
                State       = "식후";
            }

            // 일간, 주간, 월간
            TypeDataSet.Period periodType = mTimeClass.getPeriodType();
            mTimeClass.clearTime();         // 날자 초기화
            if (periodType == TypeDataSet.Period.PERIOD_DAY) {
                mStatTv.setText("일간 " + State + " 통계");
            } else if (periodType == TypeDataSet.Period.PERIOD_WEEK) {
                mStatTv.setText("주간 " + State + " 통계");
            } else if (periodType == TypeDataSet.Period.PERIOD_MONTH) {
                mStatTv.setText("월간 " + State + " 통계");
            }

            xFormatter = new AxisValueFormatter3(periodType);
            mChart.setXValueFormat(xFormatter);

            getData();   // 날자 세팅 후 조회
            setNextButtonVisible();
        }
    };

    /**
     * 날자 계산 후 조회
     */
    private void getData() {
        long startTime = mTimeClass.getStartTime();
        long endTime = mTimeClass.getEndTime();

        String format = "yyyy.MM.dd";
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        String startDate = sdf.format(startTime);
        String endDate = sdf.format(endTime);

        TypeDataSet.Period period = mTimeClass.getPeriodType();
        if (period == TypeDataSet.Period.PERIOD_DAY) {
            mDateTv.setText(startDate);
        } else {
            mDateTv.setText(startDate + " ~ " + endDate);
        }

        format = "yyyy-MM-dd";
        sdf = new SimpleDateFormat(format);
        startDate = sdf.format(startTime);
        endDate = sdf.format(endTime);
        getBottomDataLayout(startDate, endDate);
    }


    public class QeuryVerifyDataTask extends AsyncTask<Void, Void, List<SticEntry>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress();
        }

        protected List<SticEntry> doInBackground(Void... params) {
            DBHelper helper = new DBHelper(getContext());
            DBHelperSugar sugarDb = helper.getSugarDb();
            TypeDataSet.Period period = mTimeClass.getPeriodType();

            // 모두, 식전, 식후 판단
            int beforeAndAfter = getBeforeAndAfterType();

            List<SticEntry> yVals1 = null;
            mChart.setXvalMinMax(mTimeClass);
            if (period == TypeDataSet.Period.PERIOD_DAY) {
                String toDay = CDateUtil.getFormattedString_yyyy_MM_dd(mTimeClass.getStartTime());
                yVals1 = sugarDb.getResultDay(toDay, beforeAndAfter);
            } else if (period == TypeDataSet.Period.PERIOD_WEEK) {
                String startDay = CDateUtil.getFormattedString_yyyy_MM_dd(mTimeClass.getStartTime());
                String endDay = CDateUtil.getFormattedString_yyyy_MM_dd(mTimeClass.getEndTime());

//                mChart.setLabelCnt(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_WEEK));
                yVals1 = sugarDb.getResultWeek(startDay, endDay, beforeAndAfter);

                Log.i(TAG, "PERIOD_WEEK.size=" + yVals1.size());
            } else if (period == TypeDataSet.Period.PERIOD_MONTH) {

                String startDay = CDateUtil.getFormattedString_yyyy(mTimeClass.getStartTime());
                String endDay = CDateUtil.getFormattedString_MM(mTimeClass.getStartTime());

                // 이번달 최대 일수
                Calendar cal = Calendar.getInstance(); // CDateUtil.getCalendar_yyyyMMdd(startDay);
                cal.setTime(new Date(mTimeClass.getStartTime()));
                int dayCnt = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

                xFormatter.setMonthMax(dayCnt);

                Logger.i(TAG, "dayCnt=" + dayCnt + ", month=" + (cal.get(Calendar.MONTH) + 1));
                // sqlite 조회 하여 결과 가져오기
                yVals1 = sugarDb.getResultMonth(startDay, endDay, dayCnt, beforeAndAfter);
//                mChart.setLabelCnt((dayCnt / 2) - 2);
                Log.i(TAG, "PERIOD_MONTH.size=" + yVals1.size());
            }

            return yVals1;
        }

        @Override
        protected void onPostExecute(List<SticEntry> yVals1) {
            super.onPostExecute(yVals1);
            hideProgress();
            mChart.setData(yVals1);
            mChart.invalidate();
        }
    }

    /**
     * 하단 데이터 세팅하기
     *
     * @param startDate
     * @param endDate
     */
    private void getBottomDataLayout(String startDate, String endDate) {
        DBHelper helper = new DBHelper(getContext());
        DBHelperSugar sugarDb = helper.getSugarDb();

        TypeDataSet.EatState EatState = mTimeClass.getEatType();
        int type = 0;
        if (EatState == TypeDataSet.EatState.TYPE_ALL) {
            type = 0;
        } else if (EatState == TypeDataSet.EatState.TYPE_BEFORE) {
            type = 1;
        } else if (EatState == TypeDataSet.EatState.TYPE_AFTER) {
            type = 2;
        }

        DBHelperSugar.SugarStaticData bottomData = sugarDb.getResultStatic(startDate, endDate, type);

        mBottomBeforeTv.setText(Integer.toString(bottomData.getBefsugar()));
        mBottomAfterTv.setText(Integer.toString(bottomData.getAftsugar()));
        mBottomMaxTv.setText(Integer.toString(bottomData.getMaxsugar()));
        mBottomMinTv.setText(Integer.toString(bottomData.getMinsugar()));

        new QeuryVerifyDataTask().execute();
    }

    private void setReadyDataView(){

        if (SharedPref.getInstance().getPreferences(SharedPref.HEALTH_MESSAGE, false)) {
            mHealthMessage.setImageResource(R.drawable.health_m_btn_01ov);
            message_lv.setVisibility(View.VISIBLE);
        } else {
            mHealthMessage.setImageResource(R.drawable.health_m_btn_01);
            message_lv.setVisibility(View.GONE);
        }

        if(!(SharedPref.getInstance().getPreferences(SharedPref.HEALTH_DANGER_POP).equals(CDateUtil.getToday_yyyy_MM_dd()))){
            if(DeviceDataUtil.Danger)
                DeviceDataUtil.showDangerMessage(SugarManageFragment.this, HealthMessageFragment.class,null);

        }

        if(SharedPref.getInstance().getPreferences(SharedPref.HEALTH_MISSION_POP, false)){
            health_point_popup("4",R.drawable.main_coin_4p_4); //식사

        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.i(TAG, "onConfigurationChanged="+newConfig.orientation);
        if (getCurrentFragment() == MainActivityMedicare.HOME_MENU_2) {
            if (getCurrentTopFragment(SugarManageFragment.this) == 4) {
                setVisibleOrientationLayout(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE);
            }
        }
    }

    /**
     * 가로, 세로모드일때 불필요한 화면 Visible 처리
     */
    public void setVisibleOrientationLayout(final boolean isLandScape) {
        mVisibleView1.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mVisibleView2.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
//        mVisibleView3.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mChartCloseBtn.setVisibility(isLandScape ? View.VISIBLE : View.GONE);
        mChartZoomBtn.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mPerodLayout.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        share_write.setVisibility(isLandScape ? View.GONE : View.VISIBLE);


        DisplayMetrics dm = getActivity().getResources().getDisplayMetrics();

        int portHeight = DisplayUtil.getDpToPix(getContext(),tempHeight);    // 세로모드일때 사이즈 250dp(레이아웃에 설정되어 있는 사이즈)
        int landHeight = (int) (dm.heightPixels
                - mDateLayout.getMeasuredHeight()               // 날자 표시
                - mLableLayout.getMeasuredHeight()               // 혈당 라벨 표시
                - mTypeRg.getMeasuredHeight()
                - DisplayUtil.getStatusBarHeight(getContext())  // 상태바 높이
                - DisplayUtil.getDpToPix(getContext(),10)
        );
        params.height = isLandScape ? landHeight : portHeight;
        mChartFrameLayout.setLayoutParams(params);


        // 가로모드일때 스크롤뷰 막기
        mContentScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return isLandScape;
            }
        });
        //가로모드 전환 시 스크롤 상단으로 위치
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mContentScrollView.smoothScrollTo(0,0);
            }
        }, 100);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getCurrentFragment() == MainActivityMedicare.HOME_MENU_2) {
            if (getCurrentTopFragment(SugarManageFragment.this) == 4) {
                getData();  // 차트 데이터 Refresh
                mSwipeListView.getHistoryData();    // 히스토리 Refresh
                setReadyDataView();
            }
        }
    }
}
