package kr.co.hi.medicare.fragment.health.sugar;

import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;

import kr.co.hi.medicare.net.hwNet.ApiData;
import kr.co.hi.medicare.net.hwNet.BaseData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.common.swipeListview.SwipeMenu;
import kr.co.hi.medicare.common.swipeListview.SwipeMenuCreator;
import kr.co.hi.medicare.common.swipeListview.SwipeMenuItem;
import kr.co.hi.medicare.common.swipeListview.SwipeMenuListView;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperSugar;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_bdsg_info_del_data;
import kr.co.hi.medicare.net.data.Tr_bdsg_info_edit_data;
import kr.co.hi.medicare.net.data.Tr_get_hedctdata;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.StringUtil;

import static kr.co.hi.medicare.component.CDialog.showDlg;

/**
 * Created by MrsWin on 2017-04-15.
 */

public class SugarSwipeListView {
    private static final String TAG = SwipeMenuListView.class.getSimpleName();
    private AppAdapter mAdapter;
    private List<Tr_get_hedctdata.DataList> mSwipeMenuDatas = new ArrayList<>();
    private BaseFragmentMedi mBaseFragment;

    public SugarSwipeListView(View view, BaseFragmentMedi baseFragment) {
        mBaseFragment = baseFragment;
        SwipeMenuListView listView = (SwipeMenuListView) view.findViewById(R.id.sugar_history_listview);

        mAdapter = new AppAdapter();
        listView.setAdapter(mAdapter);

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                createMenu1(menu);
            }

            private void createMenu1(SwipeMenu menu) {

                SwipeMenuItem item1 = new SwipeMenuItem(mBaseFragment.getContext());
                item1.setBackground(new ColorDrawable(Color.rgb(105,129,236)));//new ColorDrawable(Color.rgb(0xE5, 0x18, 0x5E)));
                item1.setWidth(dp2px(60));
                item1.setIcon(R.drawable.health_m_btn04);
                item1.setTitleSize(12);
                item1.setTitle("수정");
                item1.setTitleColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.colorWhite));
                menu.addMenuItem(item1);
                SwipeMenuItem item2 = new SwipeMenuItem(mBaseFragment.getContext());
                item2.setBackground((new ColorDrawable(Color.rgb(143,144,157))));//(new ColorDrawable(Color.rgb(0xC9, 0xC9, 0xCE)));
                item2.setWidth(dp2px(60));
                item2.setIcon(R.drawable.health_m_btn05);
                item2.setTitleSize(12);
                item2.setTitle("삭제");
                item2.setTitleColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.colorWhite));
                menu.addMenuItem(item2);
            }

        };
        // set creator
        listView.setMenuCreator(creator);

        // step 2. listener item click event
        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:

                        Tr_get_hedctdata.DataList data = (Tr_get_hedctdata.DataList) mAdapter.getItem(position);
                        if (data.regtype.equals("U")) {
                            // 직접입력한 데이터만 수정할 수 있음.
                            new showModifiDlg(data);
                        } else {
                            // 장치에서 측정된 데이터는 수정할 수 없음.
                            String message = mBaseFragment.getContext().getString(R.string.text_alert_mesage_disable_edit);
                            CDialog.showDlg(mBaseFragment.getContext(), message);
                        }

                        break;
                    case 1:
                        String message = mBaseFragment.getContext().getString(R.string.text_alert_mesage_delete);
                        showDlg(mBaseFragment.getContext(), message, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Tr_get_hedctdata.DataList data = (Tr_get_hedctdata.DataList) mAdapter.getItem(position);

                                doDeleteData(position, data);

                            }
                        }, null);

                        break;
                }

                // false:Swipe 닫힘, true:Swipe안닫힘
                return false;
            }
        });


        listView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

            @Override
            public void onSwipeStart(int position) {
                // swipe start
                Tr_get_hedctdata.DataList data = (Tr_get_hedctdata.DataList) mAdapter.getItem(position);
                if (data.regtype.equals("U")) {

                }
            }

            @Override
            public void onSwipeEnd(int position) {
                // swipe end
            }
        });
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                mBaseFragment.getContext().getResources().getDisplayMetrics());
    }

    public void getHistoryData() {
        mSwipeMenuDatas.clear();

        DBHelper helper = new DBHelper(mBaseFragment.getContext());
        DBHelperSugar sugarDb = helper.getSugarDb();
        mSwipeMenuDatas.addAll(sugarDb.getResult());
        mAdapter.notifyDataSetChanged();
    }

    class AppAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mSwipeMenuDatas.size();
        }

        @Override
        public Object getItem(int position) {
            return mSwipeMenuDatas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            // menu type count
            return 3;
        }

        @Override
        public int getItemViewType(int position) {
            // current menu type
            return position % 3;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(mBaseFragment.getContext(), R.layout.swipe_menu_history_item_view, null);
                new ViewHolder(convertView);
            }
            ViewHolder holder = (ViewHolder) convertView.getTag();
            Tr_get_hedctdata.DataList data = (Tr_get_hedctdata.DataList) getItem(position);

            Calendar cal = CDateUtil.getCalendar_yyyy_MM_dd_HH_mm(data.reg_de);
            String yyyyMMddhhss = CDateUtil.getRegDateFormat_yyyyMMdd_HHss(data.reg_de, ".", ":");

            Logger.i(TAG, "data.sugar=" + data.sugar + ", data.drugname=" + data.drugname);
            if (StringUtil.getFloat(data.sugar) <= 0f) {
                holder.typeTv.setVisibility(View.GONE);
                holder.sugarTv.setText("투약 (" + data.drugname + ")");
                holder.beforeAfterTv.setText("");
                holder.mldlTv.setVisibility(View.GONE);
            } else {
                holder.typeTv.setVisibility(View.GONE);
                holder.typeTv.setText("D".equals(data.regtype) ? mBaseFragment.getContext().getString(R.string.text_device) : mBaseFragment.getContext().getString(R.string.text_direct_enter));
                holder.sugarTv.setText(data.sugar);
                Log.d("data.before","data.before:"+data.before);
                holder.beforeAfterTv.setText("2".equals(data.before) ? mBaseFragment.getContext().getString(R.string.text_foodafter) : mBaseFragment.getContext().getString(R.string.text_foodbefore));
                holder.mldlTv.setVisibility(View.VISIBLE);
                holder.mldlTv.setText("mg/dL");
            }

            holder.timeTv.setText(yyyyMMddhhss);

            return convertView;
        }

        class ViewHolder {
            TextView timeTv;
            TextView beforeAfterTv;
            TextView sugarTv;
            TextView typeTv;
            TextView mldlTv;

            public ViewHolder(View view) {
                timeTv = (TextView) view.findViewById(R.id.sugar_history_item_time_textview);
                beforeAfterTv = (TextView) view.findViewById(R.id.sugar_history_item_before_after_textview);
                sugarTv = (TextView) view.findViewById(R.id.sugar_history_item_sugar_textview);
                typeTv = (TextView) view.findViewById(R.id.sugar_history_item_type_textview);
                mldlTv = (TextView) view.findViewById(R.id.sugar_history_item_mldl_textview);

                view.setTag(this);
            }
        }
    }

    /**
     * 혈당 수정하기
     *
     * @param dataList
     */
    private void doModifyData(final Tr_get_hedctdata.DataList dataList) {
        Tr_bdsg_info_edit_data inputData = new Tr_bdsg_info_edit_data();
        Tr_bdsg_info_edit_data.RequestData requestData = new Tr_bdsg_info_edit_data.RequestData();
        Tr_login login = UserInfo.getLoginInfo();

        requestData.mber_sn = login.mber_sn;
        requestData.ast_mass = inputData.getArray(dataList);

        mBaseFragment.getData(mBaseFragment.getContext(), inputData.getClass(), requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_bdsg_info_edit_data) {
                    Tr_bdsg_info_edit_data data = (Tr_bdsg_info_edit_data) obj;
                    if ("Y".equals(data.reg_yn)) {

                        MediNewNetworkModule.doReLoginMediCare(mBaseFragment.getContext(), new MediNewNetworkHandler() {
                            @Override
                            public void onSuccess(BaseData responseData) {
                                CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.regist_success), new CDialog.DismissListener() {
                                    @Override
                                    public void onDissmiss() {
                                        updateSugarDb(dataList);
//                                        CDialog.showDlg(mBaseFragment.getContext(), "수정되었습니다.");
                                    }
                                });
                            }

                            @Override
                            public void onFailure(int statusCode, String response, Throwable error) {
                                CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.regist_success), new CDialog.DismissListener() {
                                    @Override
                                    public void onDissmiss() {
                                        updateSugarDb(dataList);
//                                        CDialog.showDlg(mBaseFragment.getContext(), "수정되었습니다.");
                                    }
                                });
                            }
                        });


                    } else {
                        CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.text_update_fail));
                    }
                }
            }
        });
    }

    /**
     * 혈당 DB에서 해당
     *
     * @param dataList
     */
    private void updateSugarDb(Tr_get_hedctdata.DataList dataList) {
        DBHelper helper = new DBHelper(mBaseFragment.getContext());
        DBHelperSugar sugarDb = helper.getSugarDb();
        sugarDb.updateDbSugar(dataList.idx, dataList.sugar, dataList.drugname, dataList.before, dataList.reg_de);

        getHistoryData();
    }

    /**
     * 혈당 삭제하기
     *
     * @param dataList
     */
    private void doDeleteData(final int position, final Tr_get_hedctdata.DataList dataList) {

        Tr_bdsg_info_del_data inputData = new Tr_bdsg_info_del_data();
        Tr_bdsg_info_del_data.RequestData requestData = new Tr_bdsg_info_del_data.RequestData();
        Tr_login login = UserInfo.getLoginInfo();

        requestData.mber_sn = login.mber_sn;
        requestData.ast_mass = inputData.getArray(dataList);

        mBaseFragment.getData(mBaseFragment.getContext(), inputData.getClass(), requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_bdsg_info_del_data) {
                    Tr_bdsg_info_del_data data = (Tr_bdsg_info_del_data) obj;
                    if ("Y".equals(data.reg_yn)) {
                        MediNewNetworkModule.doReLoginMediCare(mBaseFragment.getContext(), new MediNewNetworkHandler() {
                            @Override
                            public void onSuccess(BaseData responseData) {
                                deleteSugarDb(dataList);

                                mSwipeMenuDatas.remove(position);
                                mAdapter.notifyDataSetChanged();

//                                CDialog.showDlg(mBaseFragment.getContext(), "삭제되었습니다.");
                            }

                            @Override
                            public void onFailure(int statusCode, String response, Throwable error) {
                                deleteSugarDb(dataList);

                                mSwipeMenuDatas.remove(position);
                                mAdapter.notifyDataSetChanged();

//                                CDialog.showDlg(mBaseFragment.getContext(), "삭제되었습니다.");
                            }
                        });


                    } else {
                        CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.msg_fail_comment));
                    }
                }
            }
        });
    }

    /**
     * 혈당 DB에서  해당 idx의 데이터를 삭제
     *
     * @param dataList
     */
    private void deleteSugarDb(Tr_get_hedctdata.DataList dataList) {
        DBHelper helper = new DBHelper(mBaseFragment.getContext());
        DBHelperSugar sugarDb = helper.getSugarDb();
        sugarDb.deleteDb(dataList.idx);
    }

    class showModifiDlg {
        private EditText mSugarInputEt;
        private EditText mMedicenInputEt;
        private TextView mDateTv;
        private TextView mTimeTv;
        private String dataIdx;
        private RadioButton mAfter;
        private RadioButton mBefore;
        private LinearLayout mSugarLayout, dialog_btn_layout;
        private LinearLayout mMedicenLayout;

        private boolean mIsMedicenMode; // 투약정보 수정인지 여부

        private Button dialog_btn_no, dialog_btn_ok;

        /**
         * 수정 Dialog 띄우기
         */
        private showModifiDlg(final Tr_get_hedctdata.DataList data) {
            View modifyView = LayoutInflater.from(mBaseFragment.getContext()).inflate(R.layout.activity_sugar_input, null);

            CommonToolBar commonToolBar = modifyView.findViewById(R.id.common_bar);
            LinearLayout editBar = modifyView.findViewById(R.id.edit_lv);
            commonToolBar.setVisibility(View.GONE);
            editBar.setVisibility(View.VISIBLE);

            dialog_btn_layout = modifyView.findViewById(R.id.dialog_btn_layout);
            dialog_btn_no = modifyView.findViewById(R.id.dialog_btn_no);
            dialog_btn_ok = modifyView.findViewById(R.id.dialog_btn_ok);
            dialog_btn_layout.setVisibility(View.VISIBLE);


            mAfter = (RadioButton) modifyView.findViewById(R.id.sugar_type_radio_btn_after);
            mBefore = (RadioButton) modifyView.findViewById(R.id.sugar_type_radio_btn_before);
            mSugarInputEt = modifyView.findViewById(R.id.suger_input_edittext);
            mMedicenInputEt = modifyView.findViewById(R.id.sugar_medicen_editext);
            mDateTv = (TextView) modifyView.findViewById(R.id.sugar_input_date_textview);
            mTimeTv = (TextView) modifyView.findViewById(R.id.sugar_input_time_textview);
            mSugarLayout = (LinearLayout) modifyView.findViewById(R.id.sugar_input_sugar_layout);
            mMedicenLayout = (LinearLayout) modifyView.findViewById(R.id.sugar_input_medicen_layout);

            mDateTv.setEnabled(false);
            mTimeTv.setEnabled(true);

            mTimeTv.setOnTouchListener(mTouchListener);

            dataIdx = data.idx;

            // 식전:1, 식후2 아무것도 선택안하면 0
            mBefore.setChecked(!("2".equals(data.before)));
            mAfter.setChecked(!("1".equals(data.before)));
            Logger.i(TAG, "before=" + data.before);

            Calendar cal = CDateUtil.getCalendar_yyyy_MM_dd_HH_mm(data.reg_de);
            String yyyy_mm_dd = CDateUtil.getFormattedString_yyyy_MM_dd(cal.getTimeInMillis());
            String hh_mm = CDateUtil.getFormattedString_hh_mm(cal.getTimeInMillis());
            // 2017-01-01 금요일 형태로 표시
            mDateTv.setText(yyyy_mm_dd + " " + CDateUtil.getDateToWeek(cal) + "요일");
            mDateTv.setTag(StringUtil.getIntString(yyyy_mm_dd));   // yyyyMMdd
            // 오후 1:00 형태로 표시
            mTimeTv.setText(CDateUtil.getAmPmString(cal.get(Calendar.HOUR_OF_DAY)) + " " + cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE));
            mTimeTv.setTag(StringUtil.getIntString(hh_mm));   // HHmm

            mIsMedicenMode = StringUtil.getFloat(data.sugar) == 0f;
            if (mIsMedicenMode) {

                if (data.drugname.toString().equals(" ")) {
                    data.drugname = "";
                }

                mSugarLayout.setVisibility(View.GONE);
                mMedicenLayout.setVisibility(View.VISIBLE);
                mMedicenInputEt.setText(data.drugname);

                if (data.drugname.toString().equals("")) {
                    data.drugname = " ";
                }
            } else {
                mSugarLayout.setVisibility(View.VISIBLE);
                mMedicenLayout.setVisibility(View.GONE);
                mSugarInputEt.setText(data.sugar);
            }

            DisplayMetrics displayMetrics = mBaseFragment.getContext().getResources().getDisplayMetrics();
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;

            final CDialog dlg = CDialog.showDlg(mBaseFragment.getContext(), modifyView,(int)(height*0.9),(int)(width*0.9),false);
            dlg.setTitle("");
            dialog_btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String regDate = mDateTv.getTag().toString();
                    if (TextUtils.isEmpty(regDate)) {
                        CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.date_input_error));
                        return;
                    }

                    String timeStr = mTimeTv.getTag().toString();
                    if (TextUtils.isEmpty(timeStr)) {
                        CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.time_input_error));
                        return;
                    }

                    final String sugar = mSugarInputEt.getText().toString();
                    if (TextUtils.isEmpty(sugar)) {
                        CDialog.showDlg(mBaseFragment.getContext(), mBaseFragment.getContext().getString(R.string.sugar_input_error));
                        return;
                    }

                    if(StringUtil.getFloatVal(sugar) < 10.0f || StringUtil.getFloatVal(sugar) > 600.0f){
                        CDialog.showDlg(mBaseFragment.getContext(), "혈당은 10이상 600이하 값으로 입력하여주세요.");
                        return;
                    }


                    dlg.dismiss();

                    boolean isDrugMode = StringUtil.getFloat(data.sugar) == 0f && TextUtils.isEmpty(data.drugname) == false;

                    if (isDrugMode) {

                        if (mMedicenInputEt.getText().toString().isEmpty()) {
                            mMedicenInputEt.setText(" ");
                        }

                        data.drugname = mMedicenInputEt.getText().toString();
                    } else {
                        data.before = mBefore.isChecked() ? "1" : "2";
                        data.sugar = mSugarInputEt.getText().toString();
                        data.drugname = "";
                    }

                    regDate += timeStr;
                    data.reg_de = regDate;
                    doModifyData(data);
                }
            });

            dialog_btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dlg.dismiss();
                }
            });


//            if (mIsMedicenMode)
//                dlg.setTitle(mBaseFragment.getContext().getString(R.string.medi_modify));
//            else
//                dlg.setTitle(mBaseFragment.getContext().getString(R.string.sugar_modify));

        }

        View.OnTouchListener mTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    int vId = v.getId();
                    if (vId == R.id.sugar_input_time_textview) {
                        showTimePicker();
                    }
                }
                return false;
            }
        };

        /**
         * 시간 피커 완료
         */
        private void showTimePicker() {
            Calendar cal = Calendar.getInstance(Locale.KOREA);
            Date now = new Date();
            cal.setTime(now);

            int hour = cal.get(Calendar.HOUR_OF_DAY);
            int minute = cal.get(Calendar.MINUTE);
            String time = mTimeTv.getTag().toString();
            if (TextUtils.isEmpty(time) == false) {
                hour = StringUtil.getIntVal(time.substring(0, 2));
                minute = StringUtil.getIntVal(time.substring(2, 4));

                Logger.i(TAG, "hour=" + hour + ", minute=" + minute);
            }

            TimePickerDialog dialog = new TimePickerDialog(mBaseFragment.getContext(), listener, hour, minute, false);
            dialog.show();
        }

        private TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                mTimeTvSet(hourOfDay, minute);
            }
        };

        private void mTimeTvSet(int hourOfDay, int minute) {
            // 설정버튼 눌렀을 때
            String amPm = mBaseFragment.getContext().getString(R.string.text_morning);
            int hour = hourOfDay;
            if (hourOfDay > 11) {
                amPm = mBaseFragment.getContext().getString(R.string.text_afternoon);
                if (hourOfDay >= 13)
                    hour -= 12;
            } else {
                hour = hour == 0 ? 12 : hour;
            }
            String tagMsg = String.format("%02d%02d", hourOfDay, minute);
            String timeStr = String.format("%02d:%02d", hour, minute);
            mTimeTv.setText(amPm + " " + timeStr);
            mTimeTv.setTag(tagMsg);
        }


        private boolean isValidDate() {
            String text = mDateTv.getText().toString();
            return TextUtils.isEmpty(text) == false;
        }

        private boolean isValidTime() {
            String text = mTimeTv.getText().toString();
            return TextUtils.isEmpty(text) == false;
        }

        private boolean isValidSugarAndSugar() {
            if (mIsMedicenMode) {
                return true;
            } else {
                String text = mSugarInputEt.getText().toString();
                return TextUtils.isEmpty(text) == false;
            }
        }

    }
}
