package kr.co.hi.medicare.fragment.health.weight;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.value.TypeDataSet;
import kr.co.hi.medicare.net.bluetooth.manager.DeviceDataUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.charting.data.BarEntry;
import kr.co.hi.medicare.chartview.valueFormat.AxisValueFormatter2;
import kr.co.hi.medicare.chartview.weight.WeightChartView;
import kr.co.hi.medicare.component.CPDialog;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperWeight;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.fragment.health.dietprogram.DietMainFragment;
import kr.co.hi.medicare.fragment.health.message.HealthMessageFragment;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.ChartTimeUtil;
import kr.co.hi.medicare.utilhw.DisplayUtil;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;

import static kr.co.hi.medicare.utilhw.CDateUtil.getForamtyyyyMMddHHmm;

/**
 * Created by insystemscompany on 2017. 2. 28..
 */

public class WeightManageFragment extends BaseFragmentMedi {
    private final String TAG = WeightManageFragment.class.getSimpleName();

    int mRequestCode = 1111;

    Boolean isGraphActive = false;


    public ChartTimeUtil mTimeClass;
    private TextView mDateTv;
    private TextView mWeightTargetTv;
    private TextView mWeightTv;
    private TextView mWeightTargetWaitTv;
    private TextView mWeightDayTv;
    private TextView chartRule;
    private TextView mWeight_avg;

    protected WeightChartView mWeightChart;
//    protected FatChartView mFatChart;

    private WeightSwipeListView mSwipeListView;

    private LinearLayout layout_weight_history;
    private LinearLayout layout_weight_graph;

    private ImageButton imgPre_btn;
    private ImageButton imgNext_btn;

    private DBHelperWeight.WeightStaticData mWeightStaticData;
    private AxisValueFormatter2 xFormatter;

    private ImageView mHealthMessage;
    private RelativeLayout message_lv;

    private RadioButton[] mFragmentRadio;
    private RadioGroup mFragmentGroup;
    private View tabview;
    private DBHelperWeight.WeightStaticData wightavgData;


    private View mVisibleView1;
    private View mPerodLayout;
    private View mDateLayout;
    private View mChartFrameLayout;
    private ScrollView mContentScrollView;
    private ImageView mChartCloseBtn, mChartZoomBtn;
    private ImageView share_write;

    private RadioGroup periodRg;

    LinearLayout.LayoutParams params;
    int tempHeight=0;


    public static Fragment newInstance() {
        WeightManageFragment fragment = new WeightManageFragment();
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        message_lv = getActivity().findViewById(R.id.message_lv);
        mHealthMessage = getActivity().findViewById(R.id.common_toolbar_back_btn);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_weight_manage, container, false);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //탭뷰
        tabview = view;

        mFragmentGroup = view.findViewById(R.id.tab_radiogroup);

        final String[] mFragmentNames = new String[] {
                getString(R.string.text_graph),
                getString(R.string.text_history),
        };

        mFragmentRadio = new RadioButton[]{
                view.findViewById(R.id.tab1),
                view.findViewById(R.id.tab2),
        };

        int i = 0;
        for (String name : mFragmentNames) {
            mFragmentRadio[i].setText(name);
            i++;
        }

        mFragmentGroup.setOnCheckedChangeListener(mCheckedTabListener);

        mDateTv = (TextView) view.findViewById(R.id.period_date_textview);
        mWeightTargetTv = (TextView)view.findViewById(R.id.WeightTargetTv);
        mWeightTv = (TextView)view.findViewById(R.id.WeightTv);
        mWeightTargetWaitTv = (TextView)view.findViewById(R.id.WeightTargetWaitTv);
        mWeightDayTv = (TextView)view.findViewById(R.id.WeightDayTv);
        chartRule  = (TextView)view.findViewById(R.id.chart_rule);
        layout_weight_graph  = (LinearLayout) view.findViewById(R.id.layout_weight_graph);
        layout_weight_history  = (LinearLayout) view.findViewById(R.id.layout_weight_history);
        imgPre_btn                  = (ImageButton) view.findViewById(R.id.pre_btn);
        imgNext_btn                 = (ImageButton) view.findViewById(R.id.next_btn);
        periodRg = (RadioGroup) view.findViewById(R.id.period_radio_group);
        RadioButton radioBtnDay = (RadioButton) view.findViewById(R.id.period_radio_btn_day);
        RadioButton radioBtnWeek = (RadioButton) view.findViewById(R.id.period_radio_btn_week);
        RadioButton radioBtnMonth = (RadioButton) view.findViewById(R.id.period_radio_btn_month);
        RadioButton radioBtnYear = (RadioButton) view.findViewById(R.id.period_radio_btn_year);
        mWeight_avg = view.findViewById(R.id.weight_avg);

        view.findViewById(R.id.pre_btn).setOnClickListener(mClickListener);
        view.findViewById(R.id.next_btn).setOnClickListener(mClickListener);
        view.findViewById(R.id.weight_modal_btn).setOnClickListener(mClickListener);
        view.findViewById(R.id.action_bar_write_btn).setOnClickListener(mClickListener);
        share_write = view.findViewById(R.id.share_write);
        share_write.setOnClickListener(mClickListener);
        periodRg.setOnCheckedChangeListener(mCheckedChangeListener);


        mTimeClass = new ChartTimeUtil(radioBtnDay, radioBtnWeek, radioBtnMonth, radioBtnYear);
        mWeightChart = new WeightChartView(getContext(), view);
//        mFatChart = new FatChartView(getContext(), view);

        TypeDataSet.Period periodType = mTimeClass.getPeriodType();
        mTimeClass.clearTime();         // 날자 초기화

        xFormatter = new AxisValueFormatter2(periodType);
        mWeightChart.setXValueFormat(xFormatter);
//        mFatChart.setXValueFormat(xFormatter);

        mSwipeListView = new WeightSwipeListView(view, WeightManageFragment.this);
        chartRule.setText("일간 : 시간별 최종데이터");


        setNextButtonVisible();

        mChartFrameLayout = view.findViewById(R.id.chart_frame_layout);
        mContentScrollView = view.findViewById(R.id.layout_chart_scrollview);

        mChartCloseBtn = view.findViewById(R.id.chart_close_btn);
        mChartZoomBtn = view.findViewById(R.id.landscape_btn);
        mVisibleView1 = view.findViewById(R.id.visible_view1);
        mPerodLayout = view.findViewById(R.id.period_select_layout);
        mDateLayout = view.findViewById(R.id.chart_date_layout);
//        mVisibleView2 = view.findViewById(R.id.calorie_lv);
//        mVisibleView3 = view.findViewById(R.id.step_lv);

        mChartCloseBtn.setOnClickListener(mClickListener);
        mChartZoomBtn.setOnClickListener(mClickListener);

        params = (LinearLayout.LayoutParams) mChartFrameLayout.getLayoutParams();
        tempHeight = params.height;


        //click 저장
        OnClickListener ClickListener = new OnClickListener(mClickListener, view, getContext());


        //건강관리
        view.findViewById(R.id.tab1).setOnTouchListener(ClickListener);
        view.findViewById(R.id.tab2).setOnTouchListener(ClickListener);
        view.findViewById(R.id.action_bar_write_btn).setOnTouchListener(ClickListener);
        share_write.setOnTouchListener(ClickListener);

        //코드부여
        view.findViewById(R.id.tab1).setContentDescription(getString(R.string.weight_gra));
        view.findViewById(R.id.tab2).setContentDescription(getString(R.string.weight_his));
        view.findViewById(R.id.action_bar_write_btn).setContentDescription(getString(R.string.weight_input));
        share_write.setContentDescription(getString(R.string.share_write_weight));

        mFragmentRadio[0].setChecked(true);
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();

            if (vId == R.id.pre_btn) {
                mTimeClass.calTime(-1);
                getData();
            } else if (vId == R.id.next_btn) {
                // 초기값 일때 다음날 데이터는 없으므로 리턴
                if (mTimeClass.getCalTime() == 0)
                    return;

                mTimeClass.calTime(1);
                getData();
            }else if (vId == R.id.weight_modal_btn){
                new showModifiDlg();
            } else if(vId == R.id.action_bar_write_btn){
                NewActivity.startActivityForResult(WeightManageFragment.this, mRequestCode, WeightInputFragment.class, new Bundle());
            } else if(vId == R.id.share_write){
                CommunityListViewData sharelist = new CommunityListViewData();
                sharelist.ISSHARE=true;
                sharelist.CM_CONTENT = "";
                sharelist.CM_MEAL_LIST = new ArrayList<>();
                sharelist.CM_MEAL_LIST.add("평균 체중 "+mWeight_avg.getText().toString()+" kg");
                NewActivity.moveToWritePage(WeightManageFragment.this, sharelist, "");
            } else if(vId == R.id.landscape_btn) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            } else if(vId == R.id.chart_close_btn) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }

            setNextButtonVisible();
        }
    };

    private void setNextButtonVisible(){
        // 초기값 일때 다음날 데이터는 없으므로 리턴
        if (mTimeClass.getCalTime() == 0) {
            imgNext_btn.setVisibility(View.INVISIBLE);
        }else{
            imgNext_btn.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 그래프, 히스토리 탭뷰
     */

    public RadioGroup.OnCheckedChangeListener mCheckedTabListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            int pos=group.indexOfChild(tabview.findViewById(checkedId));
            if(pos == 0){
                layout_weight_history.setVisibility(View.GONE);
                layout_weight_graph.setVisibility(View.VISIBLE);

                getData();
                getBottomDataLayout();
            }else {
                layout_weight_graph.setVisibility(View.GONE);
                layout_weight_history.setVisibility(View.VISIBLE);

                mSwipeListView.getHistoryData();
            }

        }
    };


    /**
     * 일간,주간,월간
     */
    public RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            TypeDataSet.Period periodType = mTimeClass.getPeriodType();
            mTimeClass.clearTime();         // 날자 초기화

            xFormatter = new AxisValueFormatter2(periodType);
            mWeightChart.setXValueFormat(xFormatter);
//            mFatChart.setXValueFormat(xFormatter);

            getData();   // 날자 세팅 후 조회
        }
    };

    /**
     * 날자 계산 후 조회
     */
    private void getData() {

        long startTime = mTimeClass.getStartTime();
        long endTime = mTimeClass.getEndTime();


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        SimpleDateFormat yearSdf = new SimpleDateFormat("yyyy");

        String startDate = sdf.format(startTime);
        String endDate = sdf.format(endTime);

        if (mTimeClass.getPeriodType() == TypeDataSet.Period.PERIOD_DAY) {
            mDateTv.setText(startDate);
        } else if (mTimeClass.getPeriodType() == TypeDataSet.Period.PERIOD_YEAR) {
            mDateTv.setText(yearSdf.format(startTime));
        } else {
            mDateTv.setText(startDate +" ~ "+endDate);
        }

        String format = "";
        format = "yyyy-MM-dd";
        sdf = new SimpleDateFormat(format);
        startDate = sdf.format(startTime);
        endDate = sdf.format(endTime);


        DBHelper helper = new DBHelper(getContext());
        DBHelperWeight WeightDb = helper.getWeightDb();
        wightavgData = WeightDb.getResultAvgStatic(startDate,endDate);
        mWeight_avg.setText(StringUtil.getNoneZeroString2(wightavgData.getWeightAvg()));


        new QeuryVerifyDataTask().execute();
    }

    public class QeuryVerifyDataTask extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress();
        }

        protected Void doInBackground(Void... params) {

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();

            DBHelper helper = new DBHelper(getContext());
            DBHelperWeight weightDb = helper.getWeightDb();

            TypeDataSet.Period period = mTimeClass.getPeriodType();
            if (period == TypeDataSet.Period.PERIOD_DAY) {
                mWeightChart.setXvalMinMax(-1, 24, 24);

                String toDay = CDateUtil.getFormattedString_yyyy_MM_dd(mTimeClass.getStartTime());
                List<BarEntry> weightYVals = weightDb.getResultDay(toDay, true);
//                setYMinMax(weightYVals);

                mWeightChart.setData(weightYVals, mTimeClass);
//                chartRule.setText("일간 : 시간 별 최종 데이터");
            } else  if (period == TypeDataSet.Period.PERIOD_WEEK) {
                mWeightChart.setXvalMinMax(0, 8, 9);
                String startDay = CDateUtil.getFormattedString_yyyy_MM_dd(mTimeClass.getStartTime());
                String endDay = CDateUtil.getFormattedString_yyyy_MM_dd(mTimeClass.getEndTime());
                List<BarEntry> weightYVals = weightDb.getResultWeek(startDay, endDay, true);
//                setYMinMax(weightYVals);

                mWeightChart.setData(weightYVals, mTimeClass);

            } else  if (period == TypeDataSet.Period.PERIOD_MONTH) {
                int maxX = mTimeClass.getStartTimeCal().getActualMaximum(Calendar.DAY_OF_MONTH)+1;
                xFormatter.setMonthMax(maxX);
                mWeightChart.setXvalMinMax(0, maxX, maxX);

                String startDay = CDateUtil.getFormattedString_yyyy(mTimeClass.getStartTime());
                String endDay = CDateUtil.getFormattedString_MM(mTimeClass.getStartTime());
                List<BarEntry> weightYVals = weightDb.getResultMonth(startDay, endDay, true);
//                setYMinMax(weightYVals);

                mWeightChart.setData(weightYVals, mTimeClass);
            } else  if (period == TypeDataSet.Period.PERIOD_YEAR) {
                mWeightChart.setXvalMinMax(0, 13, 15);

                String startDay = CDateUtil.getFormattedString_yyyy(mTimeClass.getStartTime());
                List<BarEntry> weightYVals = weightDb.getResultYear(startDay, true);
//                setYMinMax(weightYVals);

                mWeightChart.setData(weightYVals, mTimeClass);
            }

//            mWeightChart.animateY();
            mWeightChart.invalidate();
            setNextButtonVisible();
        }
    }

//    /**
//     * y라벨 구하기
//     * @param weightYVals
//     */
//    private void setYMinMax(List<BarEntry> weightYVals) {
//        float yMin = Float.MAX_VALUE;
//        float yMax = Float.MIN_VALUE;
//        Log.i(TAG, "#######yLabelCnt##############");
//        for (BarEntry entry : weightYVals) {
//            float y = entry.getY();
//            if (y != 0 && y < yMin) {
//                yMin = y;
//            }
//
//            if (y != 0 && y > yMax) {
//                yMax = y;
//            }
//        }
//
//        // y min값이 없는 경우
//        if (yMin == Float.MAX_VALUE) {
//            Tr_login user = UserInfo.getLoginInfo();
//            yMin = StringUtil.getFloat(user.mber_bdwgh);
//        }
//        // y max값이 없는 경우
//        if (yMax == Float.MIN_VALUE) {
//            yMax = yMin;
//        }
//
//        yMin = yMin -3;
//        yMax = yMax +3;
//
//        int yLabelCnt = (int)(yMax - yMin);
//        mWeightChart.setYvalMinMax(yMin, yMax, yLabelCnt);
//        Log.i(TAG, "setYMinMax yMin="+yMin+", yMax="+yMax+", yLabelCnt="+yLabelCnt);
//    }

    /**
     * 하단 데이터 세팅하기
     */
    private void getBottomDataLayout() {
        try {
            Tr_login login = UserInfo.getLoginInfo();
            mWeightTargetTv.setText(login.mber_bdwgh_goal);

            DBHelper helper = new DBHelper(getContext());
            DBHelperWeight WeightDb = helper.getWeightDb();
            DBHelperWeight.WeightStaticData bottomData = WeightDb.getResultStatic();
            mWeightStaticData = bottomData;

            String dataWeight = "";
            if(bottomData.getWeight().isEmpty()){
                dataWeight = "0";
            }else{
                dataWeight = bottomData.getWeight();
            }


            float dbWeight = StringUtil.getFloatVal(dataWeight);
            String weight = "";
            if (dbWeight == 0) {
                weight = login.mber_bdwgh;
            } else {
                weight = StringUtil.getNoneZeroString2(StringUtil.getFloatVal(dataWeight));
            }

            mWeightTv.setText(weight);


            float temp = StringUtil.getFloat(dataWeight) - StringUtil.getFloat(login.mber_bdwgh_goal);
            if(bottomData.getWeight().isEmpty()) {
                mWeightTargetWaitTv.setText("--");
            }else if(temp > 0){
                mWeightTargetWaitTv.setText("+" + StringUtil.getNoneZeroMinusString2(temp));
            }else{
                mWeightTargetWaitTv.setText(StringUtil.getNoneZeroMinusString2(temp));
            }

            if(bottomData.getWeight().isEmpty()){
                mWeightDayTv.setText(getString(R.string.recent_measurements));
            }else {
                String time = getForamtyyyyMMddHHmm(new Date(System.currentTimeMillis()));
                String timeStr = bottomData.getRegdate().substring(0, 4) + bottomData.getRegdate().substring(5, 7) + bottomData.getRegdate().substring(8, 10) + bottomData.getRegdate().substring(11, 13) + bottomData.getRegdate().substring(14, 16);
                int dayTime = Integer.parseInt(time.substring(0,8));
                int dayTimeStr = Integer.parseInt(timeStr.substring(0,8));

                if(dayTime == dayTimeStr){
                    mWeightDayTv.setText("최근 측정 (오늘)");
                }else {
                    if (dayTime > dayTimeStr) {

                        mWeightDayTv.setText("최근 측정("+ (getDateDiff(""+dayTimeStr)-1) + "일전)");
                    }else{
                        mWeightDayTv.setText("");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private long getDateDiff(String bDate){

        int year = StringUtil.getIntVal(bDate.substring(0,4));
        int month = StringUtil.getIntVal(bDate.substring(4,6));
        int day = StringUtil.getIntVal(bDate.substring(6,8));
        Calendar thatDay = Calendar.getInstance();
        thatDay.set(Calendar.DAY_OF_MONTH,day);
        thatDay.set(Calendar.MONTH,month-1);
        thatDay.set(Calendar.YEAR, year);

        Calendar today = Calendar.getInstance();
        long diff = today.getTimeInMillis() - thatDay.getTimeInMillis(); //result in millis
        long days = diff / (24 * 60 * 60 * 1000);
        return days;
    }

    class showModifiDlg {
        private TextView mBmrTv;
        private TextView mBmiTv;
        private TextView mObesityTv;
        private TextView mFatTv;
        private TextView mBodyWaterTv;
        private TextView mMuscleTv;
        private Button mConfirmBtn;
        /**
         * 상세정보 Dialog 띄우기
         **/
        private showModifiDlg() {
            View modifyView = LayoutInflater.from(getContext()).inflate(R.layout.activity_weight_modal, null);

            mBmrTv = (TextView) modifyView.findViewById(R.id.weight_bmr_textxview);
            mBmiTv = (TextView) modifyView.findViewById(R.id.weight_bmi_textxview);
            mObesityTv = (TextView) modifyView.findViewById(R.id.weight_obesity_textxview);
            mFatTv = (TextView) modifyView.findViewById(R.id.weight_fat_textxview);
            mBodyWaterTv = (TextView) modifyView.findViewById(R.id.weight_bodywater_textxview);
            mMuscleTv = (TextView) modifyView.findViewById(R.id.weight_muscle_textxview);

            if(mWeightStaticData.getWeight().isEmpty()){
                mBmrTv.setText("0"+" kcal");
                mBmiTv.setText("0"+" kg/m2");
                mObesityTv.setText("0"+" %");
                mFatTv.setText("0"+" %");
                mBodyWaterTv.setText("0"+" %");
                mMuscleTv.setText("0"+" %");
            } else {
                Tr_login login = UserInfo.getLoginInfo();
                Float rHeight = StringUtil.getFloat(login.mber_height);
                float rWeight = StringUtil.getFloat(mWeightStaticData.getWeight());
                int rSex = Integer.parseInt(login.mber_sex);
                int rAge = Integer.parseInt(login.mber_lifyea.substring(0,4));
                String nowYear = CDateUtil.getFormattedString_yyyy(System.currentTimeMillis());

                float Float_result = 0.0f;
                if(rSex == 1){
                    Float_result = (float) (66.47f + (13.75f * rWeight) + (5.0f * rHeight) - (6.76f * ((StringUtil.getFloat(nowYear) - rAge)+1)));
                }else{
                    Float_result = (float) (655.1f + (9.56f * rWeight) + (1.85f * rHeight) - (4.68f * ((StringUtil.getFloat(nowYear) - rAge)+1)));
                }

                mBmrTv.setText(Integer.toString((int) Float_result) + " kcal");
                mBmiTv.setText(String.format("%.1f", StringUtil.getFloatVal(mWeightStaticData.getWeight()) / ((StringUtil.getFloat(login.mber_height) / 100) * (StringUtil.getFloat(login.mber_height) / 100)) )+" kg/m2");
                mFatTv.setText(mWeightStaticData.getObesity()+" %");
                mObesityTv.setText(mWeightStaticData.getFat()+" %");
                mBodyWaterTv.setText(mWeightStaticData.getBodyWater()+" %");
                mMuscleTv.setText(mWeightStaticData.getMuscle()+" %");
            }

            final CPDialog dlg = CPDialog.showDlg(getContext(), modifyView);
            dlg.setTitle(getString(R.string.text_more_info));
            mConfirmBtn = (Button)dlg.findViewById(R.id.right_confirm_btn_close);
            mConfirmBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dlg.dismiss();
                }
            });
        }

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    private void setReadyDataView(){

        if (SharedPref.getInstance().getPreferences(SharedPref.HEALTH_MESSAGE, false)) {
            mHealthMessage.setImageResource(R.drawable.health_m_btn_01ov);
            message_lv.setVisibility(View.VISIBLE);
        } else {
            mHealthMessage.setImageResource(R.drawable.health_m_btn_01);
            message_lv.setVisibility(View.GONE);
        }

        if(!(SharedPref.getInstance().getPreferences(SharedPref.HEALTH_DANGER_POP).equals(CDateUtil.getToday_yyyy_MM_dd()))){
            if(DeviceDataUtil.Danger)
                DeviceDataUtil.showDangerMessage(WeightManageFragment.this, HealthMessageFragment.class, DietMainFragment.class);
        }

        if(SharedPref.getInstance().getPreferences(SharedPref.HEALTH_MISSION_POP, false)){
            health_point_popup("1",R.drawable.main_coin_4p_4); //식사

        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.i(TAG, "onConfigurationChanged="+newConfig.orientation);
        if (getCurrentFragment()  == MainActivityMedicare.HOME_MENU_2) {
            if (getCurrentTopFragment(WeightManageFragment.this) == 2) {
                setVisibleOrientationLayout(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE);
            }
        }
    }

    /**
     * 가로, 세로모드일때 불필요한 화면 Visible 처리
     */
    public void setVisibleOrientationLayout(final boolean isLandScape) {
        mVisibleView1.setVisibility(isLandScape ? View.GONE : View.VISIBLE);

        mChartCloseBtn.setVisibility(isLandScape ? View.VISIBLE : View.GONE);
        mChartZoomBtn.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        share_write.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mPerodLayout.setVisibility(isLandScape ? View.GONE : View.VISIBLE);// 기간선택

        DisplayMetrics dm = getActivity().getResources().getDisplayMetrics();
        int portHeight = DisplayUtil.getDpToPix(getContext(),tempHeight);    // 세로모드일때 사이즈 250dp(레이아웃에 설정되어 있는 사이즈)
        int landHeight = (int) (dm.heightPixels
                            - mDateLayout.getMeasuredHeight()               // 날자 표시
                            - DisplayUtil.getStatusBarHeight(getContext())  // 상태바 높이
                            - DisplayUtil.getDpToPix(getContext(),10)   // 1dp
                            );
        params.height = isLandScape ? landHeight : portHeight;
        mChartFrameLayout.setLayoutParams(params);

        // 가로모드일때 스크롤뷰 막기
        mContentScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return isLandScape;
            }
        });
        //가로모드 전환 시 스크롤 상단으로 위치
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mContentScrollView.smoothScrollTo(0,0);
            }
        }, 100);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getCurrentFragment()  == MainActivityMedicare.HOME_MENU_2) {
            if(getCurrentTopFragment(WeightManageFragment.this) == 2) {
                getData();  // 차트 데이터 Refresh
                getBottomDataLayout(); // 하단 데이터 Refresh
                mSwipeListView.getHistoryData();    // 히스토리 Refresh
                isGraphActive = true;
                setReadyDataView();
            }
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        isGraphActive = false;
    }
    @Override
    public void onStop() {
        super.onStop();
        isGraphActive = false;
    }
}