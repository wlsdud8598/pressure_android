package kr.co.hi.medicare.fragment.home;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import kr.co.hi.medicare.net.hwNet.BaseData;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_mber_home_keyword_list;
import kr.co.hi.medicare.net.hwdata.Tr_login;

import kr.co.hi.medicare.sample.SampleFragmentMedi;
import kr.co.hi.medicare.utilhw.StringUtil;


/**
 * Created by MrsWin on 2017-02-16.
 */

public class KeywordMainFragment extends BaseFragmentMedi {
    private final String TAG = getClass().getSimpleName();

    private boolean mIsMan = false; // 1:남성, 2:여성

    // 연령대 키
    public final String KEY_AGE_10 = "10";
    public final String KEY_AGE_20 = "20";
    public final String KEY_AGE_30 = "30";
    public final String KEY_AGE_40 = "40";
    public final String KEY_AGE_50 = "50";
    public final String KEY_AGE_60 = "60";


    public List<Tr_mber_home_keyword_list.Keyword_info_man_list> man10List = new ArrayList<>();
    public List<Tr_mber_home_keyword_list.Keyword_info_man_list> man20List = new ArrayList<>();
    public List<Tr_mber_home_keyword_list.Keyword_info_man_list> man30List = new ArrayList<>();
    public List<Tr_mber_home_keyword_list.Keyword_info_man_list> man40List = new ArrayList<>();
    public List<Tr_mber_home_keyword_list.Keyword_info_man_list> man50List = new ArrayList<>();
    public List<Tr_mber_home_keyword_list.Keyword_info_man_list> man60List = new ArrayList<>();
    public List<Tr_mber_home_keyword_list.Keyword_info_woman_list> woman10List = new ArrayList<>();
    public List<Tr_mber_home_keyword_list.Keyword_info_woman_list> woman20List = new ArrayList<>();
    public List<Tr_mber_home_keyword_list.Keyword_info_woman_list> woman30List = new ArrayList<>();
    public List<Tr_mber_home_keyword_list.Keyword_info_woman_list> woman40List = new ArrayList<>();
    public List<Tr_mber_home_keyword_list.Keyword_info_woman_list> woman50List = new ArrayList<>();
    public List<Tr_mber_home_keyword_list.Keyword_info_woman_list> woman60List = new ArrayList<>();

    public List<Tr_mber_home_keyword_list.My_keyword_list> myList = new ArrayList<>();

    private RadioButton[] mFragmentRadio;
    private RadioGroup mFragmentGroup;
    private Fragment[] mFragments;
    private View tabview;
    private int pos;

    public static BaseFragmentMedi newInstance() {
        KeywordMainFragment fragment = new KeywordMainFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_2tab_line, container, false);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        CommonToolBar toolBar = getToolBar(view);
        if (toolBar != null) {
            toolBar.setTitle(R.string.지난주상담키워드);
            toolBar.setVisibility(View.VISIBLE);
        }

        // XXX 백으로 데이터 보내기
        Bundle bundle = new Bundle();
        bundle.putString(SampleFragmentMedi.SAMPLE_BACK_DATA, TAG + " BackData!!!");
        setBackData(bundle);

        tabview = view;

        mFragmentGroup = view.findViewById(R.id.tab_radiogroup);

        final String[] mFragmentNames = new String[]{
                getString(R.string.text_mail),
                getString(R.string.text_femail),
        };

        mFragmentRadio = new RadioButton[]{
                 view.findViewById(R.id.tab1),
                 view.findViewById(R.id.tab2),
        };

        mFragments = new Fragment[]{
                KeywordContentFragment.newInstance("1"),
                KeywordContentFragment.newInstance("2"),
        };


        int i = 0;
        for (String name : mFragmentNames) {
            mFragmentRadio[i].setText(name);
            mFragmentRadio[i].setChecked(false);
            i++;
        }


        String sex = UserInfo.getLoginInfo().mber_sex;
        mIsMan = "1".equals(sex);
        pos = mIsMan ? 0 : 1;
        mFragmentGroup.check(mFragmentRadio[pos].getId());


//        String sex = UserInfo.getLoginInfo().mber_sex;
//        int initPosition = "2".equals(sex) ? 0 : 1;
//        setChildFragment(mFragments[initPosition]);
//
//        mFragmentGroup.check(mFragmentRadio[initPosition].getSaveId());
        mFragmentGroup.setOnCheckedChangeListener(mCheckedChangeListener);
        doKeywordData();
    }

    public RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            int pos = group.indexOfChild(tabview.findViewById(checkedId));
            setChildFragment(mFragments[pos]);
        }
    };

    private void setChildFragment(Fragment child) {
        FragmentTransaction childFt = getChildFragmentManager().beginTransaction();

        if (!child.isAdded()) {
            childFt.replace(R.id.child_fragment_layout, child);
            childFt.addToBackStack(null);
            childFt.commit();
        }
    }

    /**
     * 키워드 리스트 전문 요청
     */
    private Tr_mber_home_keyword_list mKeywordTr;
    private void doKeywordData() {
        Tr_login user = UserInfo.getLoginInfo();
        final Tr_mber_home_keyword_list.RequestData requestData = new Tr_mber_home_keyword_list.RequestData();
        requestData.mber_sn = user.mber_sn;
        requestData.pageNumber = "1";
        requestData.sex = UserInfo.getLoginInfo().mber_sex; // 남자:"1", 여성:"2";

        MediNewNetworkModule.doApi(getContext(), Tr_mber_home_keyword_list.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_mber_home_keyword_list) {
                    Tr_mber_home_keyword_list tr = (Tr_mber_home_keyword_list) responseData;
                    mKeywordTr = tr;

                    if(myList.size()>0){
                        myList.clear();
                    }
                    if(tr.my_keyword_list.size() > 0) {
                        myList.addAll(tr.my_keyword_list);
                    } else {
                        Tr_mber_home_keyword_list.My_keyword_list list = new Tr_mber_home_keyword_list.My_keyword_list();
                        list.age_group="0";
                        myList.add(list);
                    }


                    for (Tr_mber_home_keyword_list.Keyword_info_man_list data : tr.keyword_info_man_list) {
                        switch (data.age_group) {
                            case KEY_AGE_10 :
                                man10List.add(data);
                                break;
                            case KEY_AGE_20 :
                                man20List.add(data);
                                break;
                            case KEY_AGE_30 :
                                man30List.add(data);
                                break;
                            case KEY_AGE_40 :
                                man40List.add(data);
                                break;
                            case KEY_AGE_50 :
                                man50List.add(data);
                                break;
                            case KEY_AGE_60 :
                                man60List.add(data);
                                break;
                        }
                    }

                    for (Tr_mber_home_keyword_list.Keyword_info_woman_list data : tr.keyword_info_woman_list) {
                        switch (data.age_group) {
                            case KEY_AGE_10 :
                                woman10List.add(data);
                                break;
                            case KEY_AGE_20 :
                                woman20List.add(data);
                                break;
                            case KEY_AGE_30 :
                                woman30List.add(data);
                                break;
                            case KEY_AGE_40 :
                                woman40List.add(data);
                                break;
                            case KEY_AGE_50 :
                                woman50List.add(data);
                                break;
                            case KEY_AGE_60 :
                                woman60List.add(data);
                                break;
                        }
                    }

                    pagerNotify();
                    setChildFragment(mFragments[pos]);
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(getContext(), getString(R.string.text_network_data_rec_fail));
            }
        });
    }

    /**
     * 컨텐츠 플래그먼트 뷰페이저 업데이트
     */
    private void pagerNotify() {

        String sex = UserInfo.getLoginInfo().mber_sex;
        int pos = mIsMan ? 0 : 1;

        final Fragment fragment = mFragments[pos];
        if (fragment instanceof KeywordContentFragment) {
            Log.i(TAG, "pagerNotify="+pos);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ((KeywordContentFragment)fragment).initPager();
                }
            }, 100);
        }
    }

    public Tr_mber_home_keyword_list getTr() {
        return mKeywordTr;
    }
    
    
    public List<Tr_mber_home_keyword_list.Keyword_info_man_list> getManList(String ageKey) {
        switch (ageKey) {
            case KEY_AGE_10 :
               return man10List;
            case KEY_AGE_20 :
                return man20List;
            case KEY_AGE_30 :
                return man30List;
            case KEY_AGE_40 :
                return man40List;
            case KEY_AGE_50 :
                return man50List;
            case KEY_AGE_60 :
                return man60List;
        }
        return null;
    }


    public List<Tr_mber_home_keyword_list.Keyword_info_woman_list> getWomanList(String ageKey) {
        switch (ageKey) {
            case KEY_AGE_10 :
                return woman10List;
            case KEY_AGE_20 :
                return woman20List;
            case KEY_AGE_30 :
                return woman30List;
            case KEY_AGE_40 :
                return woman40List;
            case KEY_AGE_50 :
                return woman50List;
            case KEY_AGE_60 :
                return woman60List;
        }
        return null;
    }

    public int getMyList() {
        int age = StringUtil.getIntVal(myList.get(0).age_group)/10 - 1;
        return age;
    }
}