package kr.co.hi.medicare.fragment.home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.health.HealthFragment;
import kr.co.hi.medicare.fragment.home.healthinfo.HealthMainFragment;
import kr.co.hi.medicare.fragment.mypage.MyPointMainFragment;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.net.hwdata.Tr_mber_main_call;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.StringUtil;

/**
 *
 **/
public class MissionFragment extends BaseFragmentMedi {

    public static final String BUNDLE_KEY_TODAY_POINT = "BUNDLE_KEY_TODAY_POINT";
    public static final String BUNDLE_KEY_MISSION_LIST = "BUNDLE_KEY_MISSION_LIST";

    private Map<String, PointTv> map = new HashMap<>();

//    private TextView[] mGetPointTvs;
//    private TextView[] mMaxPointTvs;
    private TextView mTodayPointTv,max_point_tv;

    private ArrayList<Tr_mber_main_call.Mission_point_list> mMissionList;



    public static Fragment newInstance() {
        MissionFragment fragment = new MissionFragment();
        return fragment;
    }

    private String 로그인 = "1006";
    private String 건강기록 = "1007";
    private String 일일퀴즈 = "1008";
    private String 건강정보 = "1009";
    private String 걸음수 = "1010";
    private String 글쓰기 = "1011";
    private String 댓글쓰기 = "1012";

    private int temp_todayPoint = 0;
    private int temp_maxPoint = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mission, container, false);

        mTodayPointTv = view.findViewById(R.id.mission_today_point_tv);
        max_point_tv = view.findViewById(R.id.max_point_tv);

        TextView namePointTv1 = view.findViewById(R.id.missinon_point_name1);
        TextView namePointTv2 = view.findViewById(R.id.missinon_point_name2);
        TextView namePointTv3 = view.findViewById(R.id.missinon_point_name3);
        TextView namePointTv4 = view.findViewById(R.id.missinon_point_name4);
        TextView namePointTv5 = view.findViewById(R.id.missinon_point_name5);
        TextView namePointTv6 = view.findViewById(R.id.missinon_point_name6);
        TextView namePointTv7 = view.findViewById(R.id.missinon_point_name7);

        TextView getPointTv1 = view.findViewById(R.id.mission_get_point_tv1);
        TextView getPointTv2 = view.findViewById(R.id.mission_get_point_tv2);
        TextView getPointTv3 = view.findViewById(R.id.mission_get_point_tv3);
        TextView getPointTv4 = view.findViewById(R.id.mission_get_point_tv4);
        TextView getPointTv5 = view.findViewById(R.id.mission_get_point_tv5);
        TextView getPointTv6 = view.findViewById(R.id.mission_get_point_tv6);
        TextView getPointTv7 = view.findViewById(R.id.mission_get_point_tv7);

        TextView maxPointTv1 = view.findViewById(R.id.mission_max_point_tv1);
        TextView maxPointTv2 = view.findViewById(R.id.mission_max_point_tv2);
        TextView maxPointTv3 = view.findViewById(R.id.mission_max_point_tv3);
        TextView maxPointTv4 = view.findViewById(R.id.mission_max_point_tv4);
        TextView maxPointTv5 = view.findViewById(R.id.mission_max_point_tv5);
        TextView maxPointTv6 = view.findViewById(R.id.mission_max_point_tv6);
        TextView maxPointTv7 = view.findViewById(R.id.mission_max_point_tv7);

        map.put(로그인,  new PointTv(namePointTv1,  getPointTv1, maxPointTv1));
        map.put(건강기록, new PointTv(namePointTv2,  getPointTv2, maxPointTv2));
        map.put(일일퀴즈, new PointTv(namePointTv3,  getPointTv3, maxPointTv3));
        map.put(건강정보, new PointTv(namePointTv4,  getPointTv4, maxPointTv4));
        map.put(걸음수,  new PointTv(namePointTv5,  getPointTv5, maxPointTv5));
        map.put(글쓰기,  new PointTv(namePointTv6,  getPointTv6, maxPointTv6));
        map.put(댓글쓰기, new PointTv(namePointTv7,  getPointTv7, maxPointTv7));

        namePointTv2.setOnClickListener(mOnClickListener);
        namePointTv3.setOnClickListener(mOnClickListener);
        namePointTv4.setOnClickListener(mOnClickListener);
        namePointTv5.setOnClickListener(mOnClickListener);
        namePointTv6.setOnClickListener(mOnClickListener);
        namePointTv7.setOnClickListener(mOnClickListener);


        //click 저장
        OnClickListener ClickListener = new OnClickListener(mOnClickListener, view, getContext());


        //미션
        namePointTv2.setOnTouchListener(ClickListener);
        namePointTv3.setOnTouchListener(ClickListener);
        namePointTv4.setOnTouchListener(ClickListener);
        namePointTv5.setOnTouchListener(ClickListener);
        namePointTv6.setOnTouchListener(ClickListener);
        namePointTv7.setOnTouchListener(ClickListener);

        //코드부여
        namePointTv2.setContentDescription(getString(R.string.namePointTv2));
        namePointTv3.setContentDescription(getString(R.string.namePointTv3));
        namePointTv4.setContentDescription(getString(R.string.namePointTv4));
        namePointTv5.setContentDescription(getString(R.string.namePointTv5));
        namePointTv6.setContentDescription(getString(R.string.namePointTv6));
        namePointTv7.setContentDescription(getString(R.string.namePointTv7));


        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        user = new UserInfo(getContext());
        Tr_login user = UserInfo.getLoginInfo();
        CLog.i("user.getNunm() --> " + user.mber_sn);
        CLog.i("user.getNuhp() --> " + user.mber_hp);
//        CLog.i("user.getFpnm() --> " + user.getFpnm());
//        CLog.i("user.getFphp() --> " + user.getFphp());

        if (getArguments() != null) {
            Bundle bundle = getArguments();
            String todayPoint = bundle.getString(BUNDLE_KEY_TODAY_POINT);
            mMissionList = bundle.getParcelableArrayList(BUNDLE_KEY_MISSION_LIST);


            temp_todayPoint = 0;
            temp_maxPoint = 0;

            for (int i = 0; i < mMissionList.size(); i++) {
                Tr_mber_main_call.Mission_point_list point = mMissionList.get(i);
                Log.i(TAG, "point.accml_amt["+i+"]"+point.accml_amt+"P");


                temp_todayPoint =  temp_todayPoint + StringUtil.getIntVal(point.accml_amt);
                temp_maxPoint = temp_maxPoint + StringUtil.getIntVal(point.point_max_day_amt);

                PointTv tvs = map.get(point.point_code);
                tvs.namePointTv.setText(point.point_nm);
                tvs.getPointTv.setText(point.accml_amt+"P");
                tvs.maxPointTv.setText("/"+point.point_max_day_amt+"P");

                tvs.namePointTv.setTextColor("Y".equals(point.mission_yn) ? ContextCompat.getColor(getContext(), R.color.x0_22_85) : ContextCompat.getColor(getContext(), R.color.x143_144_158));
                tvs.getPointTv.setTextColor("Y".equals(point.mission_yn) ? ContextCompat.getColor(getContext(), R.color.x_245_162_0) : ContextCompat.getColor(getContext(), R.color.x143_144_158));
            }
        }

        mTodayPointTv.setText(StringUtil.getFormatPrice(String.valueOf(temp_todayPoint))+"P");
        max_point_tv.setText(String.format(getString(R.string.max_point),StringUtil.getFormatPrice(String.valueOf(temp_maxPoint))));

        view.findViewById(R.id.mission_move_to_mypoint_btn).setOnClickListener(mOnClickListener);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(mOnClickListener, view, getContext());


        //미션
        view.findViewById(R.id.mission_move_to_mypoint_btn).setOnTouchListener(ClickListener);

        //코드부여
        view.findViewById(R.id.mission_move_to_mypoint_btn).setContentDescription(getString(R.string.mission_move_to_mypoint_btn));
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.mission_move_to_mypoint_btn :
                    NewActivity.startActivity(getActivity(), MyPointMainFragment.class, null);
                    break;
                case R.id.missinon_point_name2 :
                    moveToMenu(MainActivityMedicare.HOME_MENU_2);
                    HealthFragment.pos = 0;
                    break;
                case R.id.missinon_point_name3 :
                    replaceFragment(QuizFragment.newInstance(),getArguments());
                    break;
                case R.id.missinon_point_name4 :
                    replaceFragment(HealthMainFragment.newInstance());
                    break;
                case R.id.missinon_point_name5 :
                    moveToMenu(MainActivityMedicare.HOME_MENU_2);
                    HealthFragment.pos = 1;
                    break;
                case R.id.missinon_point_name6 :
                case R.id.missinon_point_name7 :
                    moveToMenu(MainActivityMedicare.HOME_MENU_3);
                    break;
            }

        }
    };

    private void moveToMenu(int idx) {
        Intent intent = new Intent();
        intent.putExtra(MainActivityMedicare.INTENT_KEY_MOVE_MENU, idx);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }


    @Override
    public void onResume() {
        super.onResume();
    }



    class PointTv {
        public TextView namePointTv;
        public TextView getPointTv;
        public TextView maxPointTv;
        public PointTv(TextView namePointTv, TextView getPointTv, TextView maxPointTv) {
            this.namePointTv = namePointTv;
            this.getPointTv = getPointTv;
            this.maxPointTv = maxPointTv;
        }
    }
}
