package kr.co.hi.medicare.fragment.home.alimi;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;

import kr.co.hi.medicare.net.hwNet.ApiData;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.fragment.community.data.CommunityListViewData;
import kr.co.hi.medicare.fragment.community.data.SendDataCode;
import kr.co.hi.medicare.fragment.community.dialog.DialogCommon;
import kr.co.hi.medicare.net.data.Tr_asstb_kbtg_alimi;

import kr.co.hi.medicare.sample.SampleFragmentMedi;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;
import kr.co.hi.medicare.fragment.IBaseFragment;


/**
 * Created by MrsWin on 2017-02-16.
 */

public class NotifierFragment extends BaseFragmentMedi implements IBaseFragment, View.OnClickListener {
    private final String TAG = getClass().getSimpleName();

    private List<Tr_asstb_kbtg_alimi.ChlmReadern> items;

    private RecyclerView recyclerView;
    private MyAdapter Adapter;
    private LinearLayoutManager layoutManager;

    private int mPageNum = 0;
    private int mMaxPage = 0;
    private boolean loading = true;
    private int previousTotal = 0;
    private int visibleThreshold = 10;
    private int firstVisibleItem=0, visibleItemCount=0, totalItemCount=0;

    private int mOldPos = 0;

    //tablet
    private TextView noticonTitle, noticonContent;
    private WebView mNotiWebview;
    private ScrollView mContent_scrollview;
    private String mPdf_url;
    private ImageButton mRight_pdf_down;

    private String mHTML_YN="N";

    private String mIdx = "";

    @Override
    public void onClick(View v) {
    }

    public class NotifierItem {
        String notiTitle;
        String notiContent;
        String notiDate;

        public String getNotiTitle() {
            return notiTitle;
        }
        public String getNotiContent() {
            return notiContent;
        }
        public String getNotiDate() {
            return notiDate;
        }

        public NotifierItem(String notiTitle, String notiContent, String notiDate){
            this.notiTitle = notiTitle;
            this.notiContent = notiContent;
            this.notiDate = notiDate;
        }
    }

    public static BaseFragmentMedi newInstance() {
        NotifierFragment fragment = new NotifierFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notifier_fragment, container, false);

        SharedPref.getInstance().savePreferences(SharedPref.ALARM_NEW_CHECK,false);


        recyclerView = view.findViewById(R.id.noti_list);
        recyclerView.setHasFixedSize(true);

        items = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getContext());


        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

                if(loading) {
                    if(totalItemCount > previousTotal) {
                        previousTotal = totalItemCount;
                        Log.i(TAG, "recycleView end called " + loading + " " + visibleItemCount+"/" + totalItemCount+"/" + firstVisibleItem);
                        loading = false;
                    }
                }
                if(!loading && totalItemCount <= firstVisibleItem + visibleItemCount){
                    Log.i(TAG, "recycleView end called " + loading + " " + visibleItemCount+"/" + totalItemCount+"/" + firstVisibleItem);


                    setNotifierData();
                    loading = true;
                }

                Log.i(TAG, "recycleView end called test " + visibleItemCount+"/" + totalItemCount+"/" + firstVisibleItem);
            }
        });

        if(mOldPos == 0){
            Adapter = new MyAdapter();

            mPageNum = 0;

            setNotifierData();

        }


        recyclerView.setAdapter(Adapter);

        recyclerView.scrollToPosition(mOldPos);
        Log.i(TAG, "NotifierFragment Test : " + mOldPos);

            noticonTitle = view.findViewById(R.id.noticontent_title);
            noticonContent =  view.findViewById(R.id.noticontent_content);
            mNotiWebview = (WebView) view.findViewById(R.id.noti_webview);
            mContent_scrollview = view.findViewById(R.id.content_scrollview);
            mRight_pdf_down = view.findViewById(R.id.right_pdf_down);

        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // XXX 백으로 데이터 보내기
        Bundle bundle = new Bundle();
        bundle.putString(SampleFragmentMedi.SAMPLE_BACK_DATA, TAG+" BackData!!!");
        setBackData(bundle);


    }

    public  class MyAdapter extends RecyclerView.Adapter{

        public  class MyViewHolder extends RecyclerView.ViewHolder {
            TextView notiTitle;
            TextView notiDate;

            MyViewHolder(View view){
                super(view);
                notiTitle = view.findViewById(R.id.noti_title);
                notiDate = view.findViewById(R.id.noti_date);
            }
        }

        private List<Tr_asstb_kbtg_alimi.ChlmReadern> itemslist = new ArrayList<>();
//        }
        public void setData(List<Tr_asstb_kbtg_alimi.ChlmReadern> dataList){
            itemslist.addAll(dataList);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.notifier_list_item, parent, false);
            MyViewHolder holder = new MyViewHolder(v);
            return holder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            final int Position = position;

            MyViewHolder myViewHolder = (MyViewHolder) holder;

            final String title = itemslist.get(position).kbt;
            final String date = CDateUtil.getFormatYYYYMMDD(itemslist.get(position).kbvd);



            myViewHolder.notiTitle.setText(title);
            myViewHolder.notiDate.setText(date);

            myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){

                    CommunityListViewData data = new CommunityListViewData();
                    data.CM_SEQ = itemslist.get(Position).kbta_idx;
                    data.ISNODATA = true;
                    if(TextUtils.isEmpty(UserInfo.getLoginInfo().nickname)){
                        openDialogRegisterNick(data);
                    }else{
                        NewActivity.moveToCommentPage(NotifierFragment.this,data,false,getString(R.string.text_alimi), SendDataCode.CM_NOTICE);
                    }



                }
            });
        }

        @Override
        public int getItemCount() {
            return itemslist.size();
        }
    }

    private void openDialogRegisterNick(final CommunityListViewData data){
        CommonFunction.openDialogRegisterNick(getContext(), new DialogCommon.UpdateProfile() {
            @Override
            public void updateProfile(String NICK, String DISEASE_OPEN,String DISEASE_NM) {
                if(!NICK.equals("")&&!DISEASE_OPEN.equals("")) {
                    final DialogCommon dialogIntro = DialogCommon.showDialogIntro(getContext(), NICK);
                    dialogIntro.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            NewActivity.moveToCommentPage(NotifierFragment.this,data,false,getString(R.string.text_alimi), SendDataCode.CM_NOTICE);
                        }
                    });
                }else{
                    openDialogRegisterNick(data);
                }
            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
//        DisplayTimerUtil.getInstance().setDisplayTimer(getContext(),getString(R.string.ALAM));
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    //알리미 리스트
    public void setNotifierData() {
        Tr_asstb_kbtg_alimi tr = new Tr_asstb_kbtg_alimi();
        Tr_asstb_kbtg_alimi.RequestData requestData = new Tr_asstb_kbtg_alimi.RequestData();
        requestData.mber_sn = UserInfo.getLoginInfo().mber_sn;
        requestData.pushk = "";
        requestData.pln = "";
        if(mPageNum == 0 || mMaxPage > mPageNum)
            requestData.pageNumber = "" + (++mPageNum);
        else
            return;

        getData(getContext(), tr, requestData, true, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_asstb_kbtg_alimi) {
                    Tr_asstb_kbtg_alimi data = (Tr_asstb_kbtg_alimi) obj;
                    if(data.data_yn.equals("Y")) {

                        mMaxPage = StringUtil.getIntVal(data.kbtp);

                        Adapter.setData(data.chlmReadern);
                        Adapter.notifyDataSetChanged();

//
                    }else{
                        Log.i(TAG,"KA001 : 기타오류");
                    }

                }
            }
        }, null);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}