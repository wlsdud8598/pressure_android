package kr.co.hi.medicare.fragment.home.healthinfo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.component.FullImageFragment;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.utilhw.Logger;

/**
 * Created by MrsWin on 2017-03-01.
 */

public class DiseaseMainFragment extends BaseFragmentMedi {
    private final String TAG = DiseaseMainFragment.class.getSimpleName();
    public static String SAMPLE_BACK_DATA = "SAMPLE_BACK_DATA";
    private RecyclerView mRecyclerView;
    private RecyclerAdapter mRecyclerAdapter;

    /**
     * 액션바 세팅
     */
//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
//        super.loadActionbar(actionBar);
//        actionBar.setActionBarTitle(getString(R.string.health_info));
//    }

    public static Fragment newInstance() {
        DiseaseMainFragment fragment = new DiseaseMainFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.disease_main_layout, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerAdapter = new RecyclerAdapter();

        mRecyclerView = view.findViewById(R.id.disease_recycler_view);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mRecyclerAdapter);
    }

    class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

        List<DiseaseItem> list = new ArrayList<>();

        public RecyclerAdapter() {
            list.add(new DiseaseItem("고혈압", R.drawable.icon_disease1, R.drawable.disease_info_01));
            list.add(new DiseaseItem("당뇨병", R.drawable.icon_disease2, R.drawable.disease_info_02));
            list.add(new DiseaseItem("간암", R.drawable.icon_disease3, R.drawable.disease_info_03));
            list.add(new DiseaseItem("폐암", R.drawable.icon_disease4, R.drawable.disease_info_04));
            list.add(new DiseaseItem("위암", R.drawable.icon_disease5, R.drawable.disease_info_05));
            list.add(new DiseaseItem("대장암", R.drawable.icon_disease6, R.drawable.disease_info_06));
            list.add(new DiseaseItem("유방암", R.drawable.icon_disease7, R.drawable.disease_info_07));
            list.add(new DiseaseItem("자궁경부암", R.drawable.icon_disease8, R.drawable.disease_info_08));
            list.add(new DiseaseItem("뇌혈관질환", R.drawable.icon_disease9, R.drawable.disease_info_09));
            list.add(new DiseaseItem("심장질환", R.drawable.icon_disease10, R.drawable.disease_info_10));
            list.add(new DiseaseItem("치매", R.drawable.icon_disease11, R.drawable.disease_info_11));
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.disease_item, parent, false);
            return new ViewHolder(itemView);
        }

        public void setData(List<DiseaseItem> dataList) {
            list.clear();
            list.addAll(dataList);

            notifyDataSetChanged();
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            final DiseaseItem data = list.get(position);
            holder.txtTitle.setText(data.title);
            holder.txtTitle.setCompoundDrawablesWithIntrinsicBounds(0, data.icon_img, 0, 0);
            holder.txtTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString(CommonToolBar.TOOL_BAR_TITLE, getString(R.string.health_info_menu3));
                    bundle.putInt(FullImageFragment.DRAWABLE_IMG, data.resource);
                    NewActivity.startActivity(getActivity(), FullImageFragment.class, bundle);

//                    getData(data.title, "0");
                }
            });
        }

        class DiseaseItem {
            int resource;
            int icon_img;
            String title;

            public DiseaseItem(String title, int icon_img, int resource) {
                this.title = title;
                this.icon_img = icon_img;
                this.resource = resource;
            }
        }

        @Override
        public int getItemCount() {
            return list == null ? 0 : list.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView txtTitle;

            public ViewHolder(View itemView) {
                super(itemView);
                txtTitle = itemView.findViewById(R.id.disease_item_textview);
//                deleteBtn = (Button) itemView.findViewById(R.id.prescript_delete_btn);
            }
        }
    }


//    /**
//     * 걷기왕 미션 상세
//     */
//    public void getData(String title, String pageNumber) {
//        Tr_content_special_bbslist_monthly.RequestData requestData = new Tr_content_special_bbslist_monthly.RequestData();
//        Tr_login login = UserInfo.getLoginInfo();
//
//        requestData.bbs_title = title;
//        requestData.pageNumber = pageNumber;
//
//        getData(getContext(), Tr_content_special_bbslist_monthly.class, requestData, false, new ApiData.IStep() {
//            @Override
//            public void next(Object obj) {
//                if (obj instanceof Tr_content_special_bbslist_monthly) {
//                    Tr_content_special_bbslist_monthly data = (Tr_content_special_bbslist_monthly) obj;
//
//                    Logger.i(TAG, "data.pageNumber="+data.pageNumber);
//
//
//                }
//            }
//        }, null);
//    }


    @Override
    public void onResume() {
        super.onResume();
        // 이전 플래그먼트에서 데이터 받기
        Bundle bundle = BaseFragmentMedi.getBackData();
        String backString = bundle.getString(SAMPLE_BACK_DATA);
        Logger.i("", "backString=" + backString);
    }
}
