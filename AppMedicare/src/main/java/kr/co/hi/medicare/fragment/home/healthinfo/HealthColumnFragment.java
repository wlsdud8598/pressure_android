package kr.co.hi.medicare.fragment.home.healthinfo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;

import kr.co.hi.medicare.net.hwNet.ApiData;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.common.DummyWebviewFragment;
import kr.co.hi.medicare.net.hwdata.Tr_content_special_bbslist_search;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.utilhw.StringUtil;
import kr.co.hi.medicare.fragment.IBaseFragment;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by MrsWin on 2017-02-16.
 *
 */

public class HealthColumnFragment extends BaseFragmentMedi implements IBaseFragment {
    private final String TAG = getClass().getSimpleName();


    private RecyclerView mRecyclerView;
    private RecyclerAdapter mAdapter;

    private boolean mIsLast = false;
    private int mPageNum = 0;
    private int mMaxPage = 0;
    private boolean loading = true;
    private int previousTotal = 0;
    private int visibleThreshold = 10;
    private int firstVisibleItem=0, visibleItemCount=0, totalItemCount=0;

    private int mOldPos = 0;

    private EditText mSearchEt;
    private LinearLayoutManager mLayoutManager;

    private InputMethodManager imm;

    public static Fragment newInstance() {
        HealthColumnFragment fragment = new HealthColumnFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.health_info_main, container, false);
        imm = (InputMethodManager) getContext().getSystemService(getContext().INPUT_METHOD_SERVICE);
        initView(view);
        return view;
    }



    private void initView(View view) {

        mRecyclerView = view.findViewById(R.id.health_info_main_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new RecyclerAdapter();
        mRecyclerView.setAdapter(mAdapter);

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mSearchEt = (EditText) view.findViewById(R.id.health_info_main_search_edittext);
        mSearchEt.setOnKeyListener(mKeyListener);

        view.findViewById(R.id.health_info_main_search_btn).setOnClickListener(mOnClickListener);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(mOnClickListener, view, getContext());


        //홈
        view.findViewById(R.id.health_info_main_search_btn).setOnTouchListener(ClickListener);

        //코드부여
        view.findViewById(R.id.health_info_main_search_btn).setContentDescription(getString(R.string.health_info_main_search_btn));


//        mPageNum = 1;

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if(loading) {
                    if(totalItemCount > previousTotal) {
                        previousTotal = totalItemCount;
                        Log.i(TAG, "recycleView end called " + loading + " " + visibleItemCount+"/" + totalItemCount+"/" + firstVisibleItem);
                        loading = false;
                    }
                }
                if(!loading && totalItemCount <= firstVisibleItem + visibleItemCount){
                    Log.i(TAG, "recycleView end called " + loading + " " + visibleItemCount+"/" + totalItemCount+"/" + firstVisibleItem);


                    getHealthColumnList();
                    loading = true;
                }

                Log.i(TAG, "recycleView end called test " + visibleItemCount+"/" + totalItemCount+"/" + firstVisibleItem);
            }
        });

        if(mOldPos == 0){
            mAdapter = new RecyclerAdapter();

            mPageNum = 0;

            getHealthColumnList();

        }

        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.scrollToPosition(mOldPos);



    }

    View.OnKeyListener mKeyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                mPageNum = 0;
                previousTotal = 0;
                mIsLast = false;
                mList.clear();
                mAdapter.notifyDataSetChanged();
                getHealthColumnList();
                return true;
            }
            return false;
        }
    };

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.health_info_main_search_btn:
                    mPageNum = 0;
                    previousTotal = 0;
                    mIsLast = false;
                    mList.clear();
                    mAdapter.notifyDataSetChanged();
                    getHealthColumnList();
                    break;
            }

        }
    };

    private void clearEditText(){
        mSearchEt.clearFocus();
        InputMethodManager imm2 = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        imm2.hideSoftInputFromWindow (mSearchEt.getWindowToken(),0);
    }


    /**
     * 건강칼럼 정보 가져오기
     */
    private void getHealthColumnList() {
        clearEditText();
//        if (mIsLast)
//            return;

        Tr_login login = UserInfo.getLoginInfo();
        Tr_content_special_bbslist_search.RequestData requestData = new Tr_content_special_bbslist_search.RequestData();

        requestData.mber_sn = login.mber_sn;
        requestData.bbs_title = mSearchEt.getText().toString();
        if(mPageNum == 0 || mMaxPage > mPageNum)
            requestData.pageNumber = "" + (++mPageNum);
        else
            return;

        getData(getContext(), Tr_content_special_bbslist_search.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_content_special_bbslist_search) {
                    Tr_content_special_bbslist_search data = (Tr_content_special_bbslist_search) obj;
                    mMaxPage = StringUtil.getIntVal(data.maxpageNumber);
                    mAdapter.setData(data.bbslist);



//                    if ("0".equals(data.maxpageNumber) || StringUtil.getIntVal(data.maxpageNumber) < mPageNo) {
//                        mPageNo = 1;
//                        mIsLast = true;
//                    } else {
//                        mPageNo++;
//                    }

                }
            }
        });
    }
    List<Tr_content_special_bbslist_search.Bbs> mList = new ArrayList<>();
    class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

        @Override
        public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.health_info_column_item, parent, false);

            return new ViewHolder(itemView);
        }

        public void setData(List<Tr_content_special_bbslist_search.Bbs> dataList) {

            mList.addAll(dataList);
            notifyDataSetChanged();
        }

        @Override
        public void onBindViewHolder(final RecyclerAdapter.ViewHolder holder, final int position) {
            final Tr_content_special_bbslist_search.Bbs data = mList.get(position);

            holder.msubjectTv.setText(data.info_subject);
            holder.mdateTv.setText(StringUtil.getConvertedDateFormat(data.info_day));

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(this.getClass().getSimpleName(),"onClick "+ mList.get(position));
                    Bundle bundle = new Bundle();

                    bundle.putString(DummyWebviewFragment.TITLE,getString(R.string.health_info_menu1) );
                    bundle.putString(DummyWebviewFragment.URL, data.info_title_url);
                    NewActivity.startActivity(HealthColumnFragment.this, DummyWebviewFragment.class, bundle);


                }
            });
        }



        @Override
        public int getItemCount() {
            return mList == null ? 0 : mList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView msubjectTv;
            TextView mdateTv;

            public ViewHolder(View itemView) {
                super(itemView);

                msubjectTv = (TextView) itemView.findViewById(R.id.health_info_column_content_textview);
                mdateTv = (TextView) itemView.findViewById(R.id.health_info_column_day_textview);

            }
        }
    }

    public void reset(int previousTotal, boolean loading) {
        this.previousTotal = previousTotal;
        this.loading = loading;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//        mPageNo = 1;
    }

    @Override
    public void onResume() {
        super.onResume();
        reset(0, true);
    }

    @Override
    public void onPause() {
        super.onPause();
        imm.hideSoftInputFromWindow(mSearchEt.getWindowToken(), 0);

    }

}
