package kr.co.hi.medicare.fragment.login;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import kr.co.hi.medicare.net.hwNet.ApiData;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.hwdata.Tr_login_pwd;
import kr.co.hi.medicare.utilhw.EditTextUtil;
import kr.co.hi.medicare.utilhw.StringUtil;

/**
 * Created by MrsWin on 2017-02-16.
 * 아이디 찾기
 */

public class FindPwdFragment extends BaseFragmentMedi implements NewActivity.onKeyBackPressedListener {

    public static String FIND_PWD_PHONE_NUM = "find_pwd_phone_num";
    public static String FIND_PWD_EMAIL = "find_pwd_email";

    private EditText mIdET;

    private EditText mPhoneNumEt;
    private EditText mRecvPhoneNumEt;

    private Button mConfirmbtn;

    private Boolean Isidcheck;
    private Boolean Isphonecheck;
    private Boolean Isrecvphonecheck;
    private Boolean IsValid;

    public static Fragment newInstance() {
        FindPwdFragment fragment = new FindPwdFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.find_pwd_fragment, container, false);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        EditTextUtil.hideKeyboard(getContext(), mIdET);
        EditTextUtil.hideKeyboard(getContext(), mPhoneNumEt);
    }

//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
//        super.loadActionbar(actionBar);
//        actionBar.setActionBarTitle(getString(R.string.find_password));       // 액션바 타이틀
//    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Isidcheck = false;
        Isphonecheck = false;
        Isrecvphonecheck = false;
        IsValid = false;

        mIdET = view.findViewById(R.id.find_pwd_email_edittext);
        mPhoneNumEt = (EditText)view.findViewById(R.id.find_pwd_phone_num_edittext);
        mRecvPhoneNumEt = (EditText)view.findViewById(R.id.find_pwd_recv_phone_num_edittext);

        mConfirmbtn = view.findViewById(R.id.confrim_btn);

        mIdET.addTextChangedListener(textWatcherInput);
        mPhoneNumEt.addTextChangedListener(textWatcherInput);

        //mRecvPhoneNumEt.addTextChangedListener(textWatcherInput);

        mConfirmbtn.setOnClickListener(mOnClickListener);

        if (getArguments() != null) {
            String phoneNum = getArguments().getString(FIND_PWD_PHONE_NUM);
            String email = getArguments().getString(FIND_PWD_EMAIL);
            mPhoneNumEt.setText(phoneNum);
            mIdET.setText(email);
            EditTextUtil.hideKeyboard(getContext(), mPhoneNumEt);
            EditTextUtil.hideKeyboard(getContext(), mIdET);
        }

        ((CheckBox)view.findViewById(R.id.find_pwd_checkbox1)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    String phoneNum = mPhoneNumEt.getText().toString();
                    mRecvPhoneNumEt.setText(phoneNum);
                    mRecvPhoneNumEt.setEnabled(false);
                    mRecvPhoneNumEt.setFocusableInTouchMode(false);
                    validPhoneNumCheck(mRecvPhoneNumEt.getText().toString());
                } else {
                    mRecvPhoneNumEt.setEnabled(true);
                    mRecvPhoneNumEt.setFocusableInTouchMode(true);
                    mRecvPhoneNumEt.setText("");
                    validPhoneNumCheck(mRecvPhoneNumEt.getText().toString());
                }
            }
        });

        /** font Typeface 적용 */
//        Button typeButton = (Button)view.findViewById(R.id.confrim_btn);
//        ViewUtil.setTypefacenotosanskr_bold(getContext(), typeButton);
    }

    TextWatcher textWatcherInput = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            validIdCheck(mIdET.getText().toString());
            isRegistPhoneNum(mPhoneNumEt.getText().toString());
            //validPhoneNumCheck(mRecvPhoneNumEt.getText().toString());
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            Log.i("beforeTextChanged", s.toString());
        }

        @Override
        public void afterTextChanged(Editable s) {
            validIdCheck(mIdET.getText().toString());
            isRegistPhoneNum(mPhoneNumEt.getText().toString());
            //validPhoneNumCheck(mRecvPhoneNumEt.getText().toString());
        }
    };


    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
            if (R.id.confrim_btn == vId) {

                String id = mIdET.getText().toString();
                String phoneNum = mPhoneNumEt.getText().toString();

                if (StringUtil.isValidEmail(id) == false) {
                    CDialog.showDlg(getContext(), getString(R.string.please_enter_email));
                    return;
                }

                if (StringUtil.isValidPhoneNumber(phoneNum) == false) {
                    CDialog.showDlg(getContext(), getString(R.string.join_step1_phone_num_error));
                    return;
                }
                if(IsValid == true)
                    doFindPwd(id, phoneNum);
            }
        }
    };

    private void BtnEnableCheck() {
        if (Isidcheck == true && Isphonecheck == true) {
            mConfirmbtn.setEnabled(true);
            IsValid = true;
        } else {
            mConfirmbtn.setEnabled(false);
            IsValid = false;
        }
    }

    /**
     * 전화번호 체크
     *
     * @return
     */
    private void isRegistPhoneNum(final String phoneNo) {
        /*mErrTv2.setVisibility(View.INVISIBLE);*/

        if (StringUtil.isValidPhoneNumber(phoneNo) == false) {
            Isphonecheck = false;
        }
        else{
            Isphonecheck = true;
        }
        BtnEnableCheck();
    }

    /**
     * 아이디체크
     * @return
     */
    private void validIdCheck(final String id) {


        if (StringUtil.isValidEmail(id) == false) {
            Isidcheck = false;
        }
        else{
            Isidcheck = true;
        }
        BtnEnableCheck();
    }


    /**
     * 전화번호 체크
     * @return
     */

    private void validPhoneNumCheck(String phoneNum) {

        if (StringUtil.isValidPhoneNumber(phoneNum) == false) {
            Isrecvphonecheck = false;
        }
        else{
            Isrecvphonecheck = true;
        }
        BtnEnableCheck();
    }

    /**
     * 비밀번호 찾기
     */
    private void doFindPwd(String email, String phoneNum) {

        Tr_login_pwd.RequestData requestData = new Tr_login_pwd.RequestData();
        requestData.mber_hp = phoneNum;
        requestData.mber_id = email;
        requestData.sms_snd_hp = phoneNum;
        getData(getContext(), Tr_login_pwd.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_login_pwd) {
                    Tr_login_pwd data = (Tr_login_pwd)obj;
                    // (0000: 일치 / 4444: 불일치)
                    if ("0000".equals(data.result_code)) {
                        CDialog.showDlg(getContext(), getString(R.string.find_pwd_result_message), new CDialog.DismissListener() {
                            @Override
                            public void onDissmiss() {
//                                replaceFragment(_LoginFragment.newInstance());
                                getActivity().finish();
                            }
                        })
                        .setIconView(R.drawable.login_icon1);
                    } else {
//                        IntentUtil.requestPhoneDialActivity(getContext(), getString(R.string.text_tel_no2));
                        CDialog.showDlg(getContext(), getString(R.string.pw_send_fail))
                                .setIconView(R.drawable.login_ico_05)
                                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        String tel = "tel:0215885656";
                                        startActivity(new Intent("android.intent.action.DIAL", Uri.parse(tel)));
                                    }
                                });
                    }
                }
            }
        });
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (getActivity() instanceof NewActivity)
            ((NewActivity)context).setOnKeyBackPressedListener(this);
    }

    @Override
    public void onBack() {
        getActivity().finish();
    }

}
