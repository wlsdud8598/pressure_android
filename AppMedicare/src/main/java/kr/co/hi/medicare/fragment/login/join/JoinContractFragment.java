package kr.co.hi.medicare.fragment.login.join;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;

/**
 * Created by MrsWin on 2017-02-16.
 * 아이디 찾기
 */

public class JoinContractFragment extends BaseFragmentMedi {



    public static final String TITLE     = "title";
    public static final String CONTENT       = "content";

    private String mtitle = "";
    private String mcontent = "";
    private TextView mcontentTv;

    public static Fragment newInstance() {
        JoinContractFragment fragment = new JoinContractFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.join_contract_view, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();

    }

//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
//        //super.loadActionbar(actionBar);
//        actionBar.setWhiteTheme();
//        actionBar.setActionBarTitle(mtitle);       // 액션바 타이틀
//    }

    private void initView(View view) {

        Bundle bundle = getArguments();
        if (bundle != null) {
            mtitle = bundle.getString(TITLE);
            mcontent = bundle.getString(CONTENT);
        }

        mcontentTv = (TextView) view.findViewById(R.id.join_contract_textview);
        mcontentTv.setText(mcontent);
    }

}
