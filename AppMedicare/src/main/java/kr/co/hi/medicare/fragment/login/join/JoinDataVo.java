package kr.co.hi.medicare.fragment.login.join;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;

import java.io.FileDescriptor;

/**
 * Created by MrsWin on 2017-03-26.
 */

public class JoinDataVo implements IBinder {
    private final String TAG = JoinDataVo.class.getSimpleName();

    private String id;              // 아이디
    private String email;              // 이메일
    private String pwd;             // 비밀번호
    private String name;            // 이름
    private String phoneNum;       // 전화번호
    private String city;            // 지역
    private String sex;             // 성별
    private String birth;           // 생년월일
    private String height;          // 키
    private String weight;          // 몸무게
    private String targetWeight;    // 목표몸무게
    private int activeType;  // 활동정보
    private String haveDisease; // 보유질환
    private String medicine;    // 복약중 약
    private String smoke;       // 흡연여부
    private String zone;       // 흡연여부
    private String mber_no;       // 멤버코드
    private String sugartype;  //당뇨타입
    private String sugardate; //당뇨발생일
    private String marketingYn; //마케팅활용 동의여부

    private String mberGrad; // 준회원 정회원 여부


    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getSugartype() {
        return sugartype;
    }
    public void setSugartype(String sugartype) {
        this.sugartype = sugartype;
    }

    public String getSugardate() {
        return sugardate;
    }
    public void setSugardate(String sugardate) {
        this.sugardate = sugardate;
    }

    public String getmber_no() {
        return mber_no;
    }
    public void setmber_no(String mber_no) {
        this.mber_no = mber_no;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getTargetWeight() {
        return targetWeight;
    }

    public void setTargetWeight(String targetWeight) {
        this.targetWeight = targetWeight;
    }

    public int getActiveType() {
        return activeType;
    }

    public void setActiveType(int activeType) {
        this.activeType = activeType;
    }

    public String getHaveDisease() {
        return haveDisease;
    }

    public void setHaveDisease(String haveDisease) {
        this.haveDisease = haveDisease;
    }

    public String getMedicine() {
        return medicine;
    }

    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }

    public String getSmoke() {
        return smoke;
    }

    public void setSmoke(String smoke) {
        this.smoke = smoke;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @Override
    public String getInterfaceDescriptor() throws RemoteException {
        return null;
    }
    public String getMberGrad() {
        return mberGrad;
    }

    public void setMberGrad(String mberGrad) {
        this.mberGrad = mberGrad;
    }
    @Override
    public boolean pingBinder() {
        return false;
    }

    @Override
    public boolean isBinderAlive() {
        return false;
    }


    public String getMber_no() {
        return mber_no;
    }

    public void setMber_no(String mber_no) {
        this.mber_no = mber_no;
    }

    public String getMarketingYn() {
        return marketingYn;
    }

    public void setMarketingYn(String marketingYn) {
        this.marketingYn = marketingYn;
    }

    @Override
    public IInterface queryLocalInterface(String descriptor) {
        return null;
    }

    @Override
    public void dump(FileDescriptor fd, String[] args) throws RemoteException {

    }

    @Override
    public void dumpAsync(FileDescriptor fd, String[] args) throws RemoteException {

    }

    @Override
    public boolean transact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
        return false;
    }

    @Override
    public void linkToDeath(DeathRecipient recipient, int flags) throws RemoteException {

    }



    @Override
    public boolean unlinkToDeath(DeathRecipient recipient, int flags) {
        return false;
    }

    public void logData() {
        Log.i(TAG,"logData "+
        "id="+id
        +"\npwd="+pwd
        +"\nname="+name
        +"\nphoneNum="+phoneNum
        +"\ncity="+city
        +"\nsex="+sex
        +"\nbirth="+birth
        +"\nheight="+height
        +"\nweight="+weight
        +"\ntargetWeight="+targetWeight
        +"\nactiveType="+activeType
        +"\nhaveDisease="+haveDisease
        +"\nmedicine="+medicine
        +"\nsmoke="+smoke
        +"\nzone="+zone
        +"\nemail="+email
        +"\nmber_no="+mber_no
        +"\nmberGrad="+mberGrad
        +"\nmarketingYn="+marketingYn
        );
    }
}
