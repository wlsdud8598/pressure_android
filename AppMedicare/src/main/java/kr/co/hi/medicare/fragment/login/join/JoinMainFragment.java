package kr.co.hi.medicare.fragment.login.join;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;

/**
 * Created by MrsWin on 2017-02-16.
 */

public class JoinMainFragment extends BaseFragmentMedi implements View.OnClickListener {
    private final String TAG = JoinMainFragment.class.getSimpleName();
    private Button joinmainsimplememberauthbtn;
    private Button directEnterBtn;
    private LinearLayout notMemberTv;


    public static Fragment newInstance() {
        JoinMainFragment fragment = new JoinMainFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(this.getClass().getSimpleName(), "onCreate()");

        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.join_step_main_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        notMemberTv = (LinearLayout) view.findViewById(R.id.join_main_not_member_tv);
        notMemberTv.setOnClickListener(this);
        view.findViewById(R.id.join_main_direct_enter_btn).setOnClickListener(this);
        view.findViewById(R.id.join_main_simple_member_auth_btn).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        final Bundle bundle = new Bundle();
        switch (v.getId()) {
            case R.id.join_main_simple_member_auth_btn:
//                replaceFragment(JoinStepSimpleFragment.newInstance());
                NewActivity.startActivity(getActivity(), JoinStepSimpleFragment.class, null);
                break;
            case R.id.join_main_direct_enter_btn:
//                replaceFragment(JoinStep1_2Fragment.newInstance());
                // 정회원 으로 가입
                bundle.putString(JoinStep1_2Fragment.BUNDLE_KEY_IS_MEMBER_GRAD, "10");
                NewActivity.startActivity(getActivity(), JoinStep1_2Fragment.class, bundle);
                break;
            case R.id.join_main_not_member_tv:
//                replaceFragment(JoinStep1_2Fragment.newInstance());
                // 준회원으로 가입
                DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
                int width = displayMetrics.widthPixels;

                CDialog.showDlg(getContext(), getString(R.string.join_welcome_title_join),(int)(width*0.9)).setIconView(R.drawable.join_main_img_03)
                        .setOkButton(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                bundle.putString(JoinStep1_2Fragment.BUNDLE_KEY_IS_MEMBER_GRAD, "20");
                                NewActivity.startActivity(getActivity(), JoinStep1_2Fragment.class, bundle);
                            }
                        })
                        .setMessageGravity(Gravity.LEFT);

                break;
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        Intent intent = new Intent(getActivity(), LoginActivityMedicare.class);
//        startActivity(intent);
//        getActivity().finish();
    }
}