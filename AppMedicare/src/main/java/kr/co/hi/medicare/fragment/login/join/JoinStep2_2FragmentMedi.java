package kr.co.hi.medicare.fragment.login.join;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.co.hi.medicare.net.hwNet.ApiData;
import kr.co.hi.medicare.net.hwNet.BaseData;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.login.LoginFirstInfoFragment1_2;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.net.hwdata.Tr_mber_check_id;
import kr.co.hi.medicare.net.hwdata.Tr_mber_reg;
import kr.co.hi.medicare.utilhw.DeviceUtil;
import kr.co.hi.medicare.utilhw.PackageUtil;
import kr.co.hi.medicare.utilhw.StringUtil;

/**
 * Created by MrsWin on 2017-02-16.
 */

public class JoinStep2_2FragmentMedi extends BaseFragmentMedi implements TextView.OnEditorActionListener {
    private final String TAG = JoinStep2_2FragmentMedi.class.getSimpleName();
    public static final String BUNDLE_KEY_MBER_NM = "bundle_key_mber_nm";

    private EditText mPwd1Et;
    private EditText mPwd2Et;
    private EditText mIdEditText;

    private Button complete_btn;


    private AlertDialog mEmailAlert;

    private JoinDataVo dataVo;

    private boolean isIdCheck, isPwd1Check, isPwd2Check, isEmailCheck;

    private View mCheckedEditText;

    private InputMethodManager mImm;

    public static Fragment newInstance() {
        JoinStep2_2FragmentMedi fragment = new JoinStep2_2FragmentMedi();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.join_step2_2fragment, container, false);

        mImm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

        mIdEditText = view.findViewById(R.id.login_id_edittext);

        mPwd1Et = view.findViewById(R.id.join_step1_pwd_edittext1);
        mPwd2Et = view.findViewById(R.id.join_step1_pwd_edittext2);

        mIdEditText.setOnFocusChangeListener(mFocusChangeListener);
        mPwd1Et.setOnFocusChangeListener(mFocusChangeListener);
        mPwd2Et.setOnFocusChangeListener(mFocusChangeListener);
        mPwd2Et.setOnEditorActionListener(this);

//        mPwd1Et.addTextChangedListener(mWatcher);
//        mPwd2Et.addTextChangedListener(mWatcher);

        view.findViewById(R.id.next_button).setOnClickListener(mOnClickListener);
        LinearLayout joinstep1agreegroup = (LinearLayout) view.findViewById(R.id.join_step1_agreegroup);
        TextView joinstep1personalinfotextview = (TextView) view.findViewById(R.id.join_step1_personal_info_textview);
        TextView joinstep1contracttextview = (TextView) view.findViewById(R.id.join_step1_contract_textview);
        return view;
    }

//    @Override
//    public void loadActionbar(CommonActionBar actionBar) {
//        super.loadActionbar(actionBar);
//        actionBar.setActionBarTitle(getString(R.string.join_inform_step2_2));       // 액션바 타이틀
//    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        Tr_login userInfo = UserInfo.getLoginInfo();

        if (getArguments() != null) {
            String mberNm = getArguments().getString(BUNDLE_KEY_MBER_NM);
            ((TextView)view.findViewById(R.id.join_step2_name_tv)).setText(mberNm);
        } else {
            ((TextView)view.findViewById(R.id.join_step2_name_tv)).setText(userInfo.mber_nm);
        }


        dataVo = new JoinDataVo();
        if (getArguments() != null) {
            dataVo = (JoinDataVo) getArguments().getBinder(JoinStep1_2Fragment.JOIN_DATA);
            if (dataVo != null) {
                dataVo.logData();
            } else {
                dataVo = new JoinDataVo();
            }
        }

        complete_btn = view.findViewById(R.id.next_button);

    }

    TextWatcher mWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            String pwd1 = mPwd1Et.getText().toString();
//            String pwd2 = mPwd2Et.getText().toString();
//            if (pwd1.equals(pwd2)) {
//                validPwdCheck();
//                BtnEnableCheck();
//            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    /**
     * 아이디체크
     *
     * @return
     */
    boolean isValid = false;

    private void BtnEnableCheck() {

        complete_btn.setEnabled(isIdCheck
                && isPwd1Check
                && isPwd2Check);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        // TODO Auto-generated method stub

        //오버라이드한 onEditorAction() 메소드

        if(v.getId()==R.id.join_step1_pwd_edittext2 && actionId== EditorInfo.IME_ACTION_DONE){ // 뷰의 id를 식별, 키보드의 완료 키 입력 검출

            validRePwdCheck();

        }

        return false;
    }

    View.OnFocusChangeListener mFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            mCheckedEditText = v;
            if (v == mIdEditText && hasFocus == false) {
                validIdCheck();
            } else if (v == mPwd1Et && hasFocus == false) {
                validPwdCheck();
            } else if (v == mPwd2Et && hasFocus == false) {
                validRePwdCheck();
            }
            BtnEnableCheck();
        }
    };

    private boolean validCheck() {
        boolean isValid = true;
        if (validIdCheck() == false) {
            isValid = false;
        }

        if (validPwdCheck() == false) {
            isValid = false;
        }

        if (validRePwdCheck() == false) {
            isValid = false;
        }

        BtnEnableCheck();
        return isValid;
    }

    private void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }


    /**
     * 아이디체크
     *
     * @return
     */
    private boolean validIdCheck() {
        final String id = mIdEditText.getText().toString();

        if(id.length() <= 0 ){
            isValid = false;
            isIdCheck = false;
            return  isIdCheck;
        }

        if (StringUtil.isValidEmail(id) == false) {
            CDialog.showDlg(getContext(),  getString(R.string.please_enter_email))
                    .setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    mIdEditText.requestFocus();
                    mIdEditText.setSelection(mIdEditText.getText().toString().length());
                    complete_btn.setEnabled(false);
                    showKeyboard();
                }
            });
//            mIdErrTv.setText(R.string.please_enter_email);
            isValid = false;
            isIdCheck = false;

            BtnEnableCheck();

        } else {
            Tr_mber_check_id.RequestData requestData = new Tr_mber_check_id.RequestData();
            requestData.mber_id = id;
            getData(getContext(), Tr_mber_check_id.class, requestData, new ApiData.IStep() {
                @Override
                public void next(Object obj) {
                    if (obj instanceof Tr_mber_check_id) {
                        Tr_mber_check_id data = (Tr_mber_check_id) obj;
                        // result_code : 조회결과(0000 가입가능 / 4444 아이디 중복)
                        if ("0000".equals(data.result_code)) {  // 중복아님 N
                            isValid = true;
                            isIdCheck = true;

                            dataVo.setId(id);
                            BtnEnableCheck();
                        } else {
                            CDialog.showDlg(getContext(), getString(R.string.join_step1_id_error2))
                            .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    mIdEditText.requestFocus();
                                    mIdEditText.setSelection(mIdEditText.getText().toString().length());
                                    complete_btn.setEnabled(false);
                                    showKeyboard();
                                }
                            });

                            isValid = false;
                            isIdCheck = false;

                            dataVo.setId(null);

                            BtnEnableCheck();
                        }
                    }
                }
            });
        }


        return isIdCheck;
    }

    /**
     * 패스워드 체크
     *
     * @return
     */
    private boolean validPwdCheck() {
        String pwd1 = mPwd1Et.getText().toString();

        if (pwd1.length() > 0) {
            if (!StringUtil.isValidPassword(pwd1) ) {

                CDialog.showDlg(getContext(),  getString(R.string.join_step1_pwd_error_leng8))
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        mPwd1Et.requestFocus();
                        mPwd1Et.setSelection(mPwd1Et.getText().toString().length());
                        complete_btn.setEnabled(false);
                        showKeyboard();
                    }
                });
                isPwd1Check = false;

                return false;
            }
        }

        isPwd1Check = true;
        dataVo.setPwd(pwd1);
        BtnEnableCheck();
        return true;
    }



    /**
     * Re패스워드 체크
     *
     * @return
     */
    private boolean validRePwdCheck() {
        String pwd1 = mPwd1Et.getText().toString();
        String pwd2 = mPwd2Et.getText().toString();

        if (pwd2.length() > 0) {
            if (!StringUtil.isValidPassword(pwd2) ) {

                CDialog.showDlg(getContext(),  getString(R.string.join_step1_pwd_error_leng8))
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                mPwd2Et.requestFocus();
                                mPwd2Et.setSelection(mPwd2Et.getText().toString().length());
                                complete_btn.setEnabled(false);
                                showKeyboard();
                            }
                        });
                isPwd2Check = false;

                return false;
            }
        }



        if (pwd1.length() > 0 && pwd2.length() > 0) {
            if (pwd1.equals(pwd2) == false) {
                CDialog.showDlg(getContext(), getString(R.string.join_step1_pwd_error))
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                mPwd2Et.requestFocus();
                                mPwd2Et.setSelection(mPwd2Et.getText().toString().length());
                                complete_btn.setEnabled(false);
                                showKeyboard();
                            }
                        });
                isPwd1Check = false;
                isPwd2Check = false;
                return false;
            } else {
                isPwd1Check = true;
                isPwd2Check = true;
            }
        } else {
            isPwd1Check = false;
            isPwd2Check = false;
            return false;
        }


        isPwd1Check = true;
        isPwd2Check = true;
        dataVo.setPwd(pwd1);
        BtnEnableCheck();
        return true;
    }


    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
            Bundle bundle = new Bundle();
            if (R.id.next_button == vId) {
              if (validCheck()) {
                  doRegistAndEdit();
              }
            }

            if (mCheckedEditText != null) {
                mCheckedEditText.clearFocus();
            }
        }
    };

    /**
     * 회원가입
     *
     * <테스트 계정공유>
     * 이름, 생년월일, 성별, 현물, 핸드폰번호
     * 인테스트1, 910101, 1, Y, 0100000001
     * 인테스트2, 920101, 1, Y, 0100000002
     * 인테스트3, 930101, 1, Y, 0100000003
     * 인테스트4, 940101, 1, N, 0100000004
     * 인테스트5, 950101, 1, N, 0100000005
     * 인테스트6, 960101, 1, N, 0100000006
     */
    private void doRegistAndEdit() {

        final Tr_mber_reg.RequestData requestData = new Tr_mber_reg.RequestData();
        requestData.mber_id = dataVo.getId(); // 		아이디
        requestData.mber_pwd = dataVo.getPwd(); // 		비밀번호
        requestData.mber_hp = dataVo.getPhoneNum(); // 		휴대폰번호
        requestData.mber_nm = dataVo.getName(); // 		이름
        requestData.mber_lifyea = dataVo.getBirth(); // 		생년(19881203)
        requestData.mber_sex = dataVo.getSex();
        requestData.app_ver = PackageUtil.getVersionInfo(getContext()); // 		앱버전
        requestData.phone_model = DeviceUtil.getPhoneModelName(); // 		휴대폰 모델명
        requestData.mber_grad = dataVo.getMberGrad(); // 		휴대폰 모델명

        if ("20".equals(dataVo.getMberGrad()))
            requestData.marketing_yn = dataVo.getMarketingYn(); // 		마케팅동의여부

        Log.i(TAG, "requestData.mber_id=" + requestData.mber_id);// = dataVo.getId(); // 		아이디
        Log.i(TAG, "requestData.mber_PWD=" + requestData.mber_pwd);//  = dataVo.getPwd(); // 		비밀번호
        Log.i(TAG, "requestData.mber_sex=" + requestData.mber_sex);//
        Log.i(TAG, "requestData.mber_hp=" + requestData.mber_hp);//  = dataVo.getPhoneNum(); // 		휴대폰번호
        Log.i(TAG, "requestData.mber_nm=" + requestData.mber_nm);// = dataVo.getName(); // 		이름
        Log.i(TAG, "requestData.mber_lifyea=" + requestData.mber_lifyea);//  = dataVo.getBirth(); // 		생년(19881203)
        Log.i(TAG, "requestData.marketing_yn=" + requestData.marketing_yn);//  마케팅 동의 여부
        Log.i(TAG, "requestData.pushk=" + ""); // 		푸쉬(정보)
        Log.i(TAG, "requestData.app_ver=" + PackageUtil.getVersionInfo(getContext()));// = PackageUtil.getVersionInfo(getContext()); // 		앱버전
        Log.i(TAG, "requestData.phone_model=" + DeviceUtil.getPhoneModelName());// = DeviceUtil.getPhoneModelName(); // 		휴대폰 모델명

        getData(getContext(), Tr_mber_reg.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_mber_reg) {
                    final Tr_mber_reg data = (Tr_mber_reg) obj;
                    if ("0000".equals(data.result_code) || "2222".equals(data.result_code)) {

                        doMediLoginNew(requestData.mber_id, requestData.mber_pwd);
                    }else if ("1111".equals(data.result_code) || "3333".equals(data.result_code) || "6666".equals(data.result_code) || "7777".equals(data.result_code)) {
                        CDialog.showDlg(getContext(), getString(R.string.already_regist_member));
                    }else if ("5555".equals(data.result_code)) {
                        CDialog.showDlg(getContext(), "정회원 대상자이십니다.\n정회원으로 가입해주시기 바랍니다.");
                    }else if ("4444".equals(data.result_code)) {
                        CDialog.showDlg(getContext(), getString(R.string.join_step1_id_error2));
                    }else if ("5566".equals(data.result_code) || "8888".equals(data.result_code)) {
                        CDialog.showDlg(getContext(), "서비스 대상자가 아닙니다.\n보험 가입 시 제공하셨던 정보가 맞는지 확인해주시기 바랍니다.");
                    }else {
                        complete_btn.setEnabled(false);
                        CDialog.showDlg(getContext(), "회원가입이 실패 했습니다.");
                    }
                }
            }
        });
    }

    /**
     * 신규 로그인 전문
     */
    private void doMediLoginNew(String id, String pwd) {
        final Tr_login.RequestData requestData = new Tr_login.RequestData();

        requestData.mber_id = id;
        requestData.mber_pwd = pwd;

        MediNewNetworkModule.doReLoginMediCare(getContext(), id, pwd, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                final Tr_login data = (Tr_login)responseData;
                CDialog.showDlg(getContext(), getString(R.string.message_finish_member2), new CDialog.DismissListener() {
                    @Override
                    public void onDissmiss() {
                        if ("Y".equals(data.add_reg_yn)) {
                            // 추가 정보 입력으로 이동
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(LoginFirstInfoFragment1_2.BUNDLE_IS_REGIST, true);
                            NewActivity.startActivity(getActivity(), LoginFirstInfoFragment1_2.class, bundle);
                            getActivity().finish();
                        } else {
                            Intent intent = new Intent(getActivity(), MainActivityMedicare.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);

                            getActivity().finish();
                        }
                    }
                });//.setIconView(R.drawable.join_img_03);
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                CDialog.showDlg(getContext(), "사용자 정보 등록에 실패 하였습니다.");
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();

        mIdEditText.setText("");
        mPwd1Et.setText("");
        mPwd2Et.setText("");

        mImm.hideSoftInputFromWindow(mIdEditText.getWindowToken(), 0);
        mImm.hideSoftInputFromWindow(mPwd1Et.getWindowToken(), 0);
        mImm.hideSoftInputFromWindow(mPwd2Et.getWindowToken(), 0);
    }
}
