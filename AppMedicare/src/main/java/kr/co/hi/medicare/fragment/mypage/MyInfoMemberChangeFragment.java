package kr.co.hi.medicare.fragment.mypage;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import kr.co.hi.medicare.net.hwNet.BaseData;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.fragment.login.join.JoinStep1_2Fragment;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_asstb_mber_keep_member;
import kr.co.hi.medicare.net.data.Tr_asstb_mber_keep_member_id_change;
import kr.co.hi.medicare.net.data.Tr_mber_find_yn;

import static android.app.Activity.RESULT_OK;


public class MyInfoMemberChangeFragment extends JoinStep1_2Fragment {
    private final String TAG = MyInfoMemberChangeFragment.class.getSimpleName();
    public static final int REQUEST_CODE =5151;
    public static final String MYPAGE_DATA ="MYPAGE_DATA";
    public static final String MYPAGE_ISLOGIN ="MYPAGE_ISLOGIN";
    private MyPageData myPageData = new MyPageData();

    public static BaseFragmentMedi newInstance() {
        MyInfoMemberChangeFragment fragment = new MyInfoMemberChangeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(this.getClass().getSimpleName(), "onCreate()");
        super.onCreate(savedInstanceState);
        mIsRealMember=true;
        isChildPage=true;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((CommonToolBar)view.findViewById(R.id.toolbar)).setTitle(getResources().getString(R.string.info_change_member));

//        mIsRealMember = UserInfo.getLoginInfo().mber_grad.equals("10"); // 정회원인지 여부
        setView();
    }

    private void setView(){

        Bundle bundleAdd = getArguments();

        if(bundleAdd!=null){
            myPageData = (MyPageData)bundleAdd.getSerializable(MYPAGE_DATA);

//            if(myPageData!=null){
//                if(!myPageData.MBER_NM.equals("")){
//                    mNameEt.setText(myPageData.MBER_NM);
//                }
//
//                if(!myPageData.MBER_LIFYEA.equals("")){
//                    mBirthEt.setText(myPageData.MBER_LIFYEA);
//                }
//                if(!UserInfo.getLoginInfo().mber_hp.equals("")){
//                    mPhoneNumEt.setText(UserInfo.getLoginInfo().mber_hp);
//                }
//                dataVo.setSex(myPageData.MBER_SEX);
//                if(myPageData.MBER_SEX.equals("1")){
//                    mManRb.setChecked(true);
//                }else{
//                    mWomanRb.setChecked(true);
//                }
//            }


        }

    }

    //2019-03-30 아래 함수는 override 해제되면 안됩니다~!
    @Override
    public void isRegistMember(){

        final Tr_mber_find_yn.RequestData requestData = new Tr_mber_find_yn.RequestData();
        requestData.mber_sn = UserInfo.getLoginInfo().mber_sn;
        requestData.mber_nm = mNameEt.getText().toString().trim();
        requestData.mber_lifyea = mBirthEt.getText().toString().trim();
        requestData.mber_hp = mPhoneNumEt.getText().toString().trim();
        requestData.mber_sex = mManRb.isChecked() ? "1" : "2"; //dataVo.getSex();
        myPageData.mber_sn = requestData.mber_sn;
        myPageData.MBER_NM = requestData.mber_nm;
        myPageData.MBER_LIFYEA = requestData.mber_lifyea;
        myPageData.mber_hp = requestData.mber_hp;
        myPageData.MBER_SEX = requestData.mber_sex;


        MediNewNetworkModule.doApi(getContext(), new Tr_mber_find_yn(), requestData,true, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";

                if (responseData instanceof Tr_mber_find_yn) {
                    Tr_mber_find_yn data = (Tr_mber_find_yn)responseData;
                    try {
                        switch (data.data_yn.trim()){
                            case "Y":
                                //가입가능

                                if(data.mber_no!=null&&data.memname!=null)
                                    requestChangeMemberY(requestData.mber_sn, data.mber_no, data.memname);
                                else
                                    errorMessage = data.alert_txt;
                                break;
                            case "N":
                                //가입불가
                                CDialog.showDlg(getContext(), getResources().getString(R.string.mypage_account_msg_4));
                                break;
                            case "YN":
                                //정회원 가입된 아이디
                                CDialog.showDlg(getContext(), getResources().getString(R.string.mypage_account_msg_3));
                                break;

                            case "YY":
                                //웹사이트에 아이디가 존재함
                                if(data.site_memid!=null&&data.app_memid!=null&&data.mber_no!=null&&data.memname!=null)
                                    openDialogYY(requestData.mber_sn,data.mber_no,data.memname,data.site_memid,data.app_memid);
                                else
                                    errorMessage = data.alert_txt;
                                break;
                            default:
                                errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:1";
                                break;
                        }
                    } catch (Exception e) {
                        errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:2";
                    }
                }

                if(!errorMessage.equals("")){
//                    dialog = DialogCommon.showDialogSingleBtn(getContext(), errorMessage, getResources().getString(R.string.comm_confirm), new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            dialog.dismiss();
//                        }
//                    });
                    CDialog.showDlg(getContext(), errorMessage);
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                Log.e(TAG, response);
            }
        });

    }



    private void requestChangeMemberY(String mber_sn, String mber_no, String memname){

        final Tr_asstb_mber_keep_member.RequestData requestData = new Tr_asstb_mber_keep_member.RequestData();

        requestData.mber_sn = mber_sn;
        requestData.mber_no = mber_no;
        requestData.memname = memname;


        MediNewNetworkModule.doApi(getContext(), new Tr_asstb_mber_keep_member(), requestData,true, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";

                if (responseData instanceof Tr_asstb_mber_keep_member) {
                    Tr_asstb_mber_keep_member data = (Tr_asstb_mber_keep_member)responseData;
                    try {
                        switch (data.data_yn.trim()){
                            case "Y":
                                CommonFunction.setUpdateLoginInfo(getContext());
                                myPageData.mber_grad = data.mber_grad;
                                myPageData.MBER_NM = requestData.memname;
                                CDialog dig = CDialog.showDlg(getContext(), getResources().getString(R.string.mypage_account_msg_5));
                                dig.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        exitActivity(true);
                                    }
                                });
                                break;
                            case "N":
                                //가입불가
                                CDialog.showDlg(getContext(), getResources().getString(R.string.mypage_account_msg_4));
                                break;
                            default:
                                errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:1";
                                break;
                        }
                    } catch (Exception e) {
                        errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:2";
                    }
                }

                if(!errorMessage.equals("")){
                    CDialog.showDlg(getContext(), errorMessage);
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                Log.e(TAG, response);
            }
        });

    }


    public void requestChangeMemberYY(String mber_sn, String mber_no, String memname,String sel_site_yn,String app_site_yn,String sel_memid){

        final Tr_asstb_mber_keep_member_id_change.RequestData requestData = new Tr_asstb_mber_keep_member_id_change.RequestData();

        requestData.mber_sn = mber_sn;
        requestData.mber_no = mber_no;
        requestData.memname = memname;

        requestData.sel_site_yn = sel_site_yn;
        requestData.app_site_yn = app_site_yn;
        requestData.sel_memid = sel_memid;


        MediNewNetworkModule.doApi(getContext(), new Tr_asstb_mber_keep_member_id_change(), requestData,true, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String errorMessage="";

                if (responseData instanceof Tr_asstb_mber_keep_member_id_change) {
                    Tr_asstb_mber_keep_member_id_change data = (Tr_asstb_mber_keep_member_id_change)responseData;
                    try {
                        switch (data.data_yn.trim()){
                            case "Y":
                                myPageData.mber_grad = data.mber_grad;
                                myPageData.MBER_NM = requestData.memname;

                                MediNewNetworkModule.doReLoginMediCare(getContext(), data.sel_memid, new UserInfo(getContext()).getSavedPw(), new MediNewNetworkHandler() {
                                    @Override
                                    public void onSuccess(BaseData responseData) {
                                        //로그인이 성공해서 재로그인 안할 경우 2019-03-28 요청으로 로그인 성공유무 상관없이 무조건 로그아웃으로 변경
//                                        CDialog dig = CDialog.showDlg(getContext(), getResources().getString(R.string.mypage_account_msg_5));
//                                        dig.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                                            @Override
//                                            public void onDismiss(DialogInterface dialog) {
//                                                exitActivity(true);
//                                            }
//                                        });


                                        CDialog dig = CDialog.showDlg(getContext(), getResources().getString(R.string.mypage_account_msg_7));
                                        dig.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialog) {
                                                exitActivity(false);
                                            }
                                        });

                                    }

                                    @Override
                                    public void onFailure(int statusCode, String response, Throwable error) {
                                        CDialog dig = CDialog.showDlg(getContext(), getResources().getString(R.string.mypage_account_msg_7));
                                        dig.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialog) {
                                                exitActivity(false);
                                            }
                                        });


                                    }
                                });



                                break;
                            case "N":
                                //가입불가
                                CDialog.showDlg(getContext(), getResources().getString(R.string.mypage_account_msg_4));
                                break;
                            default:
                                errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:1";
                                break;
                        }
                    } catch (Exception e) {
                        errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:2";
                    }
                }

                if(!errorMessage.equals("")){
                    CDialog.showDlg(getContext(), errorMessage);
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                Log.e(TAG, response);
            }
        });

    }


    private void exitActivity(boolean isLogin) {
        Intent info = new Intent();
        info.putExtra(MYPAGE_ISLOGIN,isLogin);
        info.putExtra(MYPAGE_DATA, myPageData);
        getActivity().setResult(RESULT_OK, info);
        onBackPressed();
    }


    private void openDialogYY(final String mber_sn,final String mber_no, final String memname, final String sel_site, final String app_site) {

        final View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_choice_account,null);
        final CDialog dlg = CDialog.showDlg(getContext(), view,true);

        ((TextView)view.findViewById(R.id.account_1)).setText(sel_site);
        ((TextView)view.findViewById(R.id.account_2)).setText(app_site);

        view.findViewById(R.id.account_1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setTag("Y");
                ((TextView)view.findViewById(R.id.account_1)).setBackgroundResource(R.drawable.edittext_box_x105_129_236);
                ((TextView)view.findViewById(R.id.account_1)).setTextColor(getResources().getColor(R.color.x105_129_236));
                ((TextView)view.findViewById(R.id.account_2)).setBackgroundResource(R.drawable.edittext_box_x245_245_245);
                ((TextView)view.findViewById(R.id.account_2)).setTextColor(getResources().getColor(R.color.x143_144_158));
                ((TextView)view.findViewById(R.id.account_2)).setTag("N");
            }
        });

        view.findViewById(R.id.account_2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setTag("Y");
                ((TextView)view.findViewById(R.id.account_2)).setBackgroundResource(R.drawable.edittext_box_x105_129_236);
                ((TextView)view.findViewById(R.id.account_2)).setTextColor(getResources().getColor(R.color.x105_129_236));
                ((TextView)view.findViewById(R.id.account_1)).setBackgroundResource(R.drawable.edittext_box_x245_245_245);
                ((TextView)view.findViewById(R.id.account_1)).setTextColor(getResources().getColor(R.color.x143_144_158));
                ((TextView)view.findViewById(R.id.account_1)).setTag("N");
            }
        });

        view.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlg.dismiss();
            }
        });
        view.findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sel_site_yn = ((TextView)view.findViewById(R.id.account_1)).getTag().toString();
                String app_site_yn = ((TextView)view.findViewById(R.id.account_2)).getTag().toString();
                String sel_memid="";
                if(sel_site_yn.equals("N")&&app_site_yn.equals("N")){
                    CDialog.showDlg(getContext(), getResources().getString(R.string.mypage_account_msg_6));
                }else{
                    dlg.dismiss();
                    if(sel_site_yn.equals("Y")){
                        sel_memid = sel_site;
                    }else{
                        sel_memid = app_site;
                    }
                    requestChangeMemberYY(mber_sn, mber_no, memname, sel_site_yn, app_site_yn, sel_memid);
                }
            }
        });

    }


//    @Override
//    public void doUpdateInfo(){
//        final Tr_mber_modify_mberinfo.RequestData requestData = new Tr_mber_modify_mberinfo.RequestData();
//        boolean isShowProgress=true;
//
//        requestData.mber_sn = UserInfo.getLoginInfo().mber_sn;
//
//        requestData.mber_name = myPageData.MBER_NM;
//        requestData.mber_lifyea = myPageData.MBER_LIFYEA;
//        requestData.mberhp = UserInfo.getLoginInfo().mber_hp;
//        requestData.mber_sex = myPageData.MBER_SEX;
//        requestData.mber_height = myPageData.mber_height;
//        requestData.mber_bdwgh = myPageData.mber_bdwgh;
//        requestData.medicine_yn = UserInfo.getLoginInfo().medicine_yn;
//
//        requestData.actqy = this.requestData.mber_actqy;
//        requestData.disease_nm = this.requestData.disease_nm;
//        requestData.smkng_yn = this.requestData.smkng_yn;
//        requestData.mber_job = this.requestData.job_yn;
//
//
//        MediNewNetworkModule.doApi(getContext(), new Tr_mber_modify_mberinfo(), requestData,isShowProgress, new MediNewNetworkHandler() {
//            @Override
//            public void onSuccess(BaseData responseData) {
//
//                String errorMessage="";
//
//                if (responseData instanceof Tr_mber_modify_mberinfo) {
//                    Tr_mber_modify_mberinfo data = (Tr_mber_modify_mberinfo)responseData;
//                    try {
//                        switch (data.result_code){
//                            case ReceiveDataCode.MBER_LOAD_MYPAGE_SUCCESS:
//                                Intent info = new Intent();
//                                myPageData.actqy = requestData.actqy;
//                                myPageData.DISEASE_NM = requestData.disease_nm;
//                                myPageData.smoking_yn = requestData.smkng_yn;
//                                myPageData.job_yn = requestData.mber_job;
//                                info.putExtra(MYPAGE_DATA, myPageData);
//                                getActivity().setResult(RESULT_OK, info);
//                                finishStep();
//                                break;
//                            case ReceiveDataCode.MBER_LOAD_MYPAGE_ERROR_FAIL:
//                                errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:1";
//                                break;
//
//                            default:
//                                errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:2";
//                                break;
//                        }
//                    } catch (Exception e) {
//                        CLog.e(e.toString());
//                        errorMessage = getResources().getString(R.string.comm_error_common)+"\ncode:3";
//                    }
//                }
//
//                if(!errorMessage.equals("")){
////                    dialog = DialogCommon.showDialogSingleBtn(getContext(), errorMessage, getResources().getString(R.string.comm_confirm), new View.OnClickListener() {
////                        @Override
////                        public void onClick(View v) {
////                            dialog.dismiss();
////                        }
////                    });
//                    CDialog.showDlg(getContext(), errorMessage);
//                }
//
//            }
//
//            @Override
//            public void onFailure(int statusCode, String response, Throwable error) {
//                Log.e(TAG, response);
//            }
//        });
//    }


}