package kr.co.hi.medicare.fragment.mypage;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import kr.co.hi.medicare.net.hwNet.BaseData;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.activity.BaseActivityMedicare;
import kr.co.hi.medicare.activity.LoginActivityMedicare;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.PictureBaseFragment;
import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.fragment.community.data.ReceiveDataCode;
import kr.co.hi.medicare.fragment.community.dialog.DialogCommon;
import kr.co.hi.medicare.fragment.health.food.HttpAsyncFileTask33;
import kr.co.hi.medicare.fragment.health.food.HttpAsyncTaskInterface;
import kr.co.hi.medicare.googleFitness.GoogleFitInstance;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_mber_load_mypage;
import kr.co.hi.medicare.net.data.Tr_mber_profile_send;
import kr.co.hi.medicare.net.hwdata.Tr_get_infomation;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.util.KakaoUtil;
import kr.co.hi.medicare.util.Util;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;
import kr.co.hi.medicare.utilhw.cameraUtil.ProviderUtil;
import kr.co.hi.medicare.value.Define;


public class MyPageFragmentMyInfo extends PictureBaseFragment implements DialogCommon.UpdateProfile{
    private final String TAG = MyPageFragmentMyInfo.class.getSimpleName();

    private MyPageData myPageData = new MyPageData();
    private TextView nick,level,point,member_info,member_info_regdate,mAppVersionTv;
    private ImageView profile,profile_teduri;
    private String appVersion;

    public static BaseFragmentMedi newInstance() {
        MyPageFragmentMyInfo fragment = new MyPageFragmentMyInfo();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mypage_myinfo, container, false);

        nick = view.findViewById(R.id.nick);
        level = view.findViewById(R.id.level);
        point = view.findViewById(R.id.point);
        profile = view.findViewById(R.id.profile);
        super.ivPreview = profile;
        profile_teduri = view.findViewById(R.id.profile_teduri);
        member_info = view.findViewById(R.id.member_info);
        member_info_regdate = view.findViewById(R.id.member_info_regdate);
        mAppVersionTv = view.findViewById(R.id.myinfo_app_version_textview);
        profile.setOnClickListener(mClickListener);
        view.findViewById(R.id.layout_member_info).setOnClickListener(mClickListener);
        nick.setOnClickListener(mClickListener);
        view.findViewById(R.id.layout_point).setOnClickListener(mClickListener);
        view.findViewById(R.id.layout_settings_alram).setOnClickListener(mClickListener);
        view.findViewById(R.id.layout_health_alram).setOnClickListener(mClickListener);
        view.findViewById(R.id.layout_password).setOnClickListener(mClickListener);
        view.findViewById(R.id.layout_privacy_policy).setOnClickListener(mClickListener);
        view.findViewById(R.id.layout_version).setOnClickListener(mClickListener);
        view.findViewById(R.id.btn_logout).setOnClickListener(mClickListener);
        view.findViewById(R.id.layout_plusfriend).setOnClickListener(mClickListener);
        view.findViewById(R.id.btn_picture).setOnClickListener(mClickListener);


        //click 저장
        OnClickListener ClickListener = new OnClickListener(mClickListener, view, getContext());


        //마이페이지
        profile.setOnTouchListener(ClickListener);
        nick.setOnTouchListener(ClickListener);
        view.findViewById(R.id.layout_member_info).setOnTouchListener(ClickListener);
        view.findViewById(R.id.layout_settings_alram).setOnTouchListener(ClickListener);
        view.findViewById(R.id.layout_health_alram).setOnTouchListener(ClickListener);
        view.findViewById(R.id.layout_password).setOnTouchListener(ClickListener);
        view.findViewById(R.id.layout_privacy_policy).setOnTouchListener(ClickListener);

        //코드부여
        profile.setContentDescription(getString(R.string.profile));
        nick.setContentDescription(getString(R.string.nick));
        view.findViewById(R.id.layout_member_info).setContentDescription(getString(R.string.layout_member_info));
        view.findViewById(R.id.layout_settings_alram).setContentDescription(getString(R.string.layout_settings_alram));
        view.findViewById(R.id.layout_health_alram).setContentDescription(getString(R.string.layout_health_alram));
        view.findViewById(R.id.layout_password).setContentDescription(getString(R.string.layout_password));
        view.findViewById(R.id.layout_privacy_policy).setContentDescription(getString(R.string.layout_privacy_policy));

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        setAppVersion();
    }


    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
            Bundle bundle = new Bundle();
            switch (vId) {
                case R.id.profile:
                case R.id.btn_picture:
                    showSelectGalleryCamera();
//                    onGallery();
                    break;
                case R.id.layout_plusfriend:
                    KakaoUtil.kakaoAddFriends(getContext());
                    break;
                case R.id.btn_logout:
                    doLogout();
                    break;
                case R.id.nick:
                    openModifyNickDialog();
                    break;
                case R.id.layout_member_info:
                    bundle.putSerializable(MyInfoSettingFragment.INFO_DATA, myPageData);
//                    NewActivity.startActivity(getActivity(), MyInfoSettingFragment.class,bundle);
                    NewActivity.startActivityForResult(MyPageFragmentMyInfo.this, MyInfoSettingFragment.REQUEST_CODE,MyInfoSettingFragment.class,bundle);
                    break;
                case R.id.layout_point:
                    NewActivity.startActivity(getActivity(), MyPointMainFragment.class, bundle);
                    break;
                case R.id.layout_settings_alram:
                    bundle.putSerializable(AlramSettingFragment.ALRAM_DATA, myPageData);
                    NewActivity.startActivity(getActivity(), AlramSettingFragment.class, bundle);
                    break;
                case R.id.layout_health_alram:
                    NewActivity.startActivity(getActivity(), MyPageFragmentNotification.class, new Bundle());
                    break;
                case R.id.layout_password:
                    NewActivity.startActivity(getActivity(), PasswordSettingFragment.class, new Bundle());
                    break;
                case R.id.layout_privacy_policy:
                    NewActivity.startActivity(getActivity(), MyPageFragmentPravacyPolicy.class, bundle);
                    break;
                case R.id.layout_version:
                    final Tr_get_infomation Tr_info = Define.getInstance().getInformation();
                    final String updateUrl = "market://details?id="+getContext().getPackageName();

                     if ( !TextUtils.isEmpty(myPageData.app_ver)) {
                        try {
                            String appV = appVersion;
                            String svrV = myPageData.app_ver;
                            // 서버버전이 높다면
                            if (StringUtil.getFloat(appV) >= StringUtil.getFloat(svrV)) {
                                CDialog.showDlg(getContext(), getString(R.string.new_version_msg), new CDialog.DismissListener() {
                                    @Override
                                    public void onDissmiss() {

                                    }
                                });
                            } else {
                                CDialog.UpdateshowDlg(getContext(), getString(R.string.update_version), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Tr_get_infomation Tr_info = Define.getInstance().getInformation();
                                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
                                        List<ResolveInfo> resInfos = getContext().getPackageManager().queryIntentActivities(browserIntent, 0);
                                        Log.i("linkStore","list: "+resInfos);

                                        if(resInfos != null || resInfos.size() != 0) {

                                            for (ResolveInfo info : resInfos) {
                                                if (info.activityInfo.packageName.toLowerCase().contains("com.android.vending") ||
                                                        info.activityInfo.name.toLowerCase().contains("com.android.vending")) {

                                                    browserIntent.setPackage(info.activityInfo.packageName);
                                                    break;
                                                }
                                            }
                                        }
                                        startActivity(browserIntent);
                                    }
                                }, null);
                            }
                        } catch (Exception e) {
                        }
                    }
                    break;
            }
        }
    };

    private void openModifyNickDialog() {
        DialogCommon.showDialogNick(getContext(), myPageData.NICKNAME, UserInfo.getLoginInfo().mber_sn, this,myPageData.DISEASE_OPEN);
    }

    private void Logout(){

        FragmentManager fm = getActivity().getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }

        SharedPref.getInstance().savePreferences(SharedPref.IS_AUTO_LOGIN, false);
        SharedPref.getInstance().savePreferences(SharedPref.IS_LOGIN_SUCEESS, false);
        Define.getInstance().setWeatherRequestedTime(-1);

        UserInfo user = new UserInfo(getContext());
        user.setIsAutoLogin(false);      // 자동로그인

        moveToLogin();

        GoogleFitInstance.signOutGoogleClient(getContext(), new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                // 로그인 화면으로 이동 처리
//                        moveToLogin(intent);
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                error.printStackTrace();
                // 로그인 화면으로 이동 처리
//                        moveToLogin(intent);
            }
        });

    }


    private void doLogout() {
        CDialog.showDlg(getContext(), getString(R.string.text_do_you_want_logout), new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Logout();


            }
        }, null);
    }

    private void moveToLogin() {
        Intent intent = new Intent(getContext(), LoginActivityMedicare.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        getActivity().startActivity(intent);
        getActivity().finish();
    }


    private void setAppVersion() {
        String packageName = getContext().getPackageName();
        PackageInfo info = null;
        Tr_get_infomation Tr_info = Define.getInstance().getInformation();
        try {
            info = getContext().getPackageManager().getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
            appVersion = String.format("%.1f", Float.valueOf(info.versionName));
//            mAppVersionTv.setText("앱 버전 V "+appVersion);
            mAppVersionTv.setText(appVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String appV = appVersion;
        String svrV =  myPageData.app_ver;
        if (StringUtil.getFloat(appV) >= StringUtil.getFloat(svrV)) {
            mAppVersionTv.setCompoundDrawablesWithIntrinsicBounds(null,null,null,null);
        }else{
            mAppVersionTv.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ico_07),null,null,null);
        }
    }


    /**
     * 마이프로필정보요청
     */
    private void getMyInfo() {
        final Tr_mber_load_mypage.RequestData requestData = new Tr_mber_load_mypage.RequestData();
        boolean isShowProgress=true;

        requestData.mber_sn = UserInfo.getLoginInfo().mber_sn;

        MediNewNetworkModule.doApi(getContext(), new Tr_mber_load_mypage(), requestData,isShowProgress, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                if (responseData instanceof Tr_mber_load_mypage) {
                    Tr_mber_load_mypage data = (Tr_mber_load_mypage)responseData;
                    try {
                            switch (data.result_code){
                                case ReceiveDataCode.MBER_LOAD_MYPAGE_SUCCESS:
                                    myPageData.mber_sn = data.mber_sn;
                                    myPageData.cm_yn = data.CM_YN;
                                    myPageData.daily_yn = data.DAILY_YN;
                                    myPageData.health_yn = data.HEALTH_YN;
                                    myPageData.heart_yn = data.HEART_YN;
                                    myPageData.notice_yn = data.NOTICE_YN;
                                    myPageData.reply_yn = data.REPLY_YN;
                                    myPageData.MBER_NM = data.MBER_NM;
                                    myPageData.MBER_SEX = data.MBER_SEX;
                                    myPageData.MBER_LIFYEA = data.MBER_LIFYEA;
                                    myPageData.age = data.age;
                                    myPageData.MBER_DE = data.MBER_DE;
                                    myPageData.PROFILE_PIC = data.profile_pic;
                                    myPageData.DISEASE_OPEN = data.DISEASE_OPEN;
                                    myPageData.DISEASE_NM = data.disease_nm;
                                    myPageData.DISEASE_TXT = data.disease_txt;
                                    myPageData.accml_sum_amt = data.accml_sum_amt;
                                    myPageData.POINT_TOTAL_AMT = data.POINT_TOTAL_AMT;
                                    myPageData.mber_height = data.mber_height;
                                    myPageData.mber_bdwgh = data.mber_bdwgh;
                                    myPageData.mber_bdwgh_goal = data.mber_bdwgh_goal;
                                    myPageData.actqy = data.actqy;
                                    myPageData.smoking_yn = data.smoking_yn;
                                    myPageData.job_yn = data.job_yn;
                                    myPageData.mber_grad = data.mber_grad;
                                    myPageData.job_yn = data.job_yn;
                                    myPageData.app_ver = data.app_ver;
                                    myPageData.mber_hp = UserInfo.getLoginInfo().mber_hp;
                                    if(data.NICKNAME==null||data.NICKNAME.trim().equals("")){
                                        myPageData.NICKNAME="";
                                        nick.setText(data.MBER_NM);
                                    }else{
                                        myPageData.NICKNAME = data.NICKNAME;
                                        nick.setText(data.NICKNAME);
                                    }

                                    CommonFunction.setProfile(getContext(), myPageData.PROFILE_PIC, profile);
                                    CommonFunction.setProfileTeduri(getContext(), data.mber_grad, profile_teduri);

                                    int lv  = 1;
                                    try {
                                        lv = Integer.parseInt(data.POINT_TOTAL_AMT)/1000;
                                        if(lv<1)
                                            lv=1;
                                    }catch (Exception e){
                                        lv=1;
                                    }
                                    level.setText("Lv."+lv);


                                    member_info.setText(data.MBER_NM+" / "+ (data.MBER_SEX.equals("1") ? "남" : "여") +" / 만 "+data.age+" 세");
                                    member_info_regdate.setText("가입일 "+getDateFormat(data.MBER_DE));

                                    int point_sum,point_total;

                                    try{
                                        point_sum = Integer.parseInt(data.accml_sum_amt);
                                    }catch (Exception e){
                                        point_sum=0;
                                    }
                                    try{
                                        point_total = Integer.parseInt(data.POINT_TOTAL_AMT);
                                    }catch (Exception e){
                                        point_total=0;
                                    }
                                    point.setText(Util.makeStringComma(point_sum)+" / " +Util.makeStringComma(point_total) + " Point");

                                    CommonFunction.setUpdateLoginInfo(getContext());

                                    setAppVersion();
                                    break;

                                case ReceiveDataCode.MBER_LOAD_MYPAGE_ERROR_FAIL:

                                    break;

                                default:

                                    break;
                            }
                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                Log.e(TAG, response);
            }
        });
    }


    private String getDateFormat(String date){
        String value ="";
        Date date_ori=null;
        try {
            date_ori = new SimpleDateFormat("yyyyMMddHHmm").parse(date);
            value = new SimpleDateFormat("yyyy년 MM월 dd일").format(date_ori);
        }catch (Exception e){
            value="";
        }

        return value;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getCurrentFragment()  == MainActivityMedicare.HOME_MENU_5) {
            if(!isCrop)
                getMyInfo();
            else
                isCrop=false;
        }
    }

    @Override
    public void updateProfile(String NICK, String DISEASE_OPEN,String DISEASE_NM) {
        if(!DISEASE_OPEN.equals("")) {
            myPageData.NICKNAME = NICK;
            myPageData.DISEASE_NM = DISEASE_NM;
            nick.setText(NICK);
        }else{
            openModifyNickDialog();
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_CROP) {
                if(outputFileUri!=null){
                    isCrop=true;
                    updateProfileToServer();
                }
            }
            if(requestCode == MyInfoSettingFragment.REQUEST_CODE){
                Logout();
            }
        }
    }


    private void updateProfileToServer() {

        HashMap<String,String> params = new HashMap<>();
        params.put("mber_sn",UserInfo.getLoginInfo().mber_sn);
        params.put("cmpny_code", "304");

        HttpAsyncFileTask33 rssTask = new HttpAsyncFileTask33(getString(R.string.profile_upload_url), new HttpAsyncTaskInterface() {
            @Override
            public void onPreExecute() {
                ((BaseActivityMedicare)getContext()).showProgress();
            }
            @Override
            public void onPostExecute(String data) {
            }
            @Override
            public void onError() {
                ((BaseActivityMedicare)getContext()).hideProgress();
                CDialog.showDlg(getContext(), getString(R.string.text_network_data_send_fail));
            }
            @Override
            public void onFileUploaded(String result) {

                String errorMessage="";

                try {
                    BaseData resObj = new Gson().fromJson(result, Tr_mber_profile_send.class);
                    if (resObj instanceof Tr_mber_profile_send) {
                        try {
                            final Tr_mber_profile_send resultData = (Tr_mber_profile_send) resObj;

                            switch (resultData.data_yn) {
                                case "Y":
                                    myPageData.PROFILE_PIC = resultData.file_url;
                                    getMyInfo();
//                                    CommonFunction.setProfile(getContext(), myPageData.PROFILE_PIC, profile);
//                                    CommonFunction.setUpdateLoginInfo(getContext());
                                    break;
                                default:
                                    errorMessage = "이미지가 변경되지 않았습니다.\n잠시 후 다시 시도해 주세요.";
                                    break;
                            }

                        }catch (Exception e){
                            errorMessage = getResources().getString(R.string.comm_error_db002)+"\ncode:2";
                        }
                    } else {
                        errorMessage = getResources().getString(R.string.comm_error_db002)+"\ncode:3";
                    }
                }catch (Exception e){
                    errorMessage = getResources().getString(R.string.comm_error_db002)+"\ncode:4";
                }

                if(!errorMessage.equals("")){
                    CDialog.showDlg(getContext(), errorMessage);
                }

                ((BaseActivityMedicare)getContext()).hideProgress();
            }
        });

        rssTask.setParam(new File(ProviderUtil.getOutputFilePath(outputFileUri)),"img_file",params);
        rssTask.execute();
    }


}