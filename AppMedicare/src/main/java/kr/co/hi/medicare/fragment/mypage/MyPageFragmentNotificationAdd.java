package kr.co.hi.medicare.fragment.mypage;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import kr.co.hi.medicare.component.CDialog;

import java.util.GregorianCalendar;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.dialog.DialogAlramGubun;
import kr.co.hi.medicare.fragment.community.dialog.DialogCommon;
import kr.co.hi.medicare.util.AlramUtil;


public class MyPageFragmentNotificationAdd extends BaseFragmentMedi {
    private final String TAG = getClass().getSimpleName();

    private CommonToolBar commonToolbar;
    private TextView gubun,time_hhmm;
    private EditText message;
    private DBHelper mDbHelper;
    private DialogAlramGubun dialogAlramGubun;
    private DialogCommon dialogCommon;
    private ImageView icon;

    public static BaseFragmentMedi newInstance() {
        MyPageFragmentNotificationAdd fragment = new MyPageFragmentNotificationAdd();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mypage_notification_add, container, false);

        mDbHelper = new DBHelper(getContext());

        commonToolbar = view.findViewById(R.id.commonToolbar);
        commonToolbar.setRightTvOnclickListener(mClickListener);
        gubun = view.findViewById(R.id.gubun);
        icon = view.findViewById(R.id.icon);
        time_hhmm = view.findViewById(R.id.time_hhmm);
        message = view.findViewById(R.id.message);
        gubun.setOnClickListener(mClickListener);
        time_hhmm.setOnClickListener(mClickListener);


        GregorianCalendar calendar = new GregorianCalendar();
        setTimeView(calendar.get(calendar.HOUR_OF_DAY),calendar.get(calendar.MINUTE));
        return view;
    }


    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Bundle bundle = new Bundle();
            switch (v.getId()) {
                case R.id.common_toolbar_right_btn://저장

                    String errorMessage="";
                    String time = time_hhmm.getText().toString();
                    int hour=0;
                    int min=0;

                    if(!mDbHelper.getAlramHealth().isDuplicate(gubun.getText().toString(),time)){

                        String msg = message.getText().toString();

                        if(msg.trim().equals("")){
                            msg = message.getHint().toString();
                        }

                        if(mDbHelper.getAlramHealth().insert(msg,gubun.getText().toString(),time)){
                            int idx = mDbHelper.getAlramHealth().selectIdx(gubun.getText().toString(),time);
                            AlramUtil.setAlarm(getContext(),idx,time);
                            onBackPressed();
                            return;
                        }else{
                            errorMessage=getResources().getString(R.string.mypage_health_error_1);
                        }

                    }else{
                        errorMessage=getResources().getString(R.string.mypage_health_error_2);
                    }

                    if(!errorMessage.equals("")) {
//                        dialogCommon = DialogCommon.showDialogSingleBtn(getContext(), errorMessage, getResources().getString(R.string.text_confirm), new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                dialogCommon.dismiss();
//                            }
//                        });
                        CDialog.showDlg(getContext(), errorMessage);
                    }


                    break;
                case R.id.time_hhmm://알람 시간
                    showDatePicker();
                    break;
                case R.id.gubun://알람 종류
                    dialogAlramGubun = DialogAlramGubun.showDialog(getContext(), gubun.getText().toString());
                    dialogAlramGubun.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if(!dialogAlramGubun.getGUBUN().equals("")){
                                gubun.setText(dialogAlramGubun.getGUBUN());
                                message.setHint(dialogAlramGubun.getGUBUN()+ getResources().getString(R.string.mypage_health_hint_common));
                                switch (dialogAlramGubun.getGUBUN()){
                                    case "식사":
                                        message.setHint(getResources().getString(R.string.mypage_health_hint_food));
                                        icon.setImageResource(R.drawable.ico_16);
                                        break;

                                    case "체중":
                                        icon.setImageResource(R.drawable.ico_17);
                                        break;

                                    case "혈압":
                                        icon.setImageResource(R.drawable.ico_18);
                                        break;

                                    default:
                                        icon.setImageResource(R.drawable.ico_19);
                                        break;

                                }


                            }
                        }
                    });
                    break;
            }
        }
    };


    private void showDatePicker() {
        GregorianCalendar calendar = new GregorianCalendar();
        int hour = calendar.get(calendar.HOUR_OF_DAY);
        int minute = calendar.get(calendar.MINUTE);
        TimePickerDialog tpd = new TimePickerDialog(getContext(),onTimeSetListener,hour,minute,false);
        tpd.show();
    }

    TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            setTimeView(hourOfDay,minute);
        }
    };


    private void setTimeView(int hour,int minute){
        time_hhmm.setText(String.format("%02d:%02d", hour, minute));
    }

}