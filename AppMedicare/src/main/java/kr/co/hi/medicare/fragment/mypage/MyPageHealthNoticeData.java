package kr.co.hi.medicare.fragment.mypage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MyPageHealthNoticeData implements Serializable {

    public int idx;
    public String message;
    public String gubun;
    public String time_hhmm;
    public String regdate;
    public String isactive;
    public boolean isDelete;
}
