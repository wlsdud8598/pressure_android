package kr.co.hi.medicare.fragment.mypage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;


public class MyPageMainFragment extends BaseFragmentMedi {
    private final String TAG = getClass().getSimpleName();

    private FragmentPagerAdapter mPagerAdapter;
    private ViewPager mViewPager;
    private RadioButton[] mFragmentRadio;
    private RadioGroup mFragmentGroup;
    private Fragment[] mFragments;
    private View tabview;

    public static BaseFragmentMedi newInstance() {
        MyPageMainFragment fragment = new MyPageMainFragment();
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_2tab_line, container, false);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        // XXX 백으로 데이터 보내기
//        Bundle bundle = new Bundle();
//        bundle.putString(SampleFragmentMedi.SAMPLE_BACK_DATA, TAG + " BackData!!!");
//        setBackData(bundle);

        CommonToolBar toolBar = getToolBar(view);
        toolBar.setVisibility(View.VISIBLE);
        toolBar.setTitle(getContext().getResources().getString(R.string.mypage_title));
        toolBar.getBackBtn().setVisibility(View.GONE);

        tabview = view;

        mFragmentGroup = view.findViewById(R.id.tab_radiogroup);

        final String[] mFragmentNames = new String[]{
                getString(R.string.my_menu_1),
                getString(R.string.my_menu_2),
        };

        mFragments = new Fragment[]{
                MyPageFragmentMyInfo.newInstance(),
                MyServiceFragment.newInstance(),
        };

        mFragmentRadio = new RadioButton[]{
                view.findViewById(R.id.tab1),
                view.findViewById(R.id.tab2),
        };

        int i = 0;
        for (String name : mFragmentNames) {

            mFragmentRadio[i].setText(name);
            i++;
        }

        mFragmentGroup.setOnCheckedChangeListener(mCheckedChangeListener);
        mFragmentRadio[0].setChecked(true);

//        setChildFragment(mFragments[0]);



    }

    public RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            int pos=group.indexOfChild(tabview.findViewById(checkedId));
            setChildFragment(mFragments[pos]);

        }
    };

    private void setChildFragment(Fragment child) {
        FragmentTransaction childFt = getChildFragmentManager().beginTransaction();

        if (!child.isAdded()) {
            childFt.replace(R.id.child_fragment_layout, child);
            childFt.addToBackStack(null);
            childFt.commit();
        }
    }

//    @Override
//    public void onBackPressed() {
//        Fragment fragment = getChildFragmentManager().findFragmentById(R.id.child_fragment_layout);
//        Logger.i(TAG, this.getClass().getSimpleName()+":::onBackPress=");
//        if (fragment instanceof MyServiceFragment) {
//            MyServiceFragment baseFragment = (MyServiceFragment)fragment;
//            baseFragment.onBackPressed();
//        } else {
////            super.onBackPressed();
//            finishStep();
//        }
//    }
}