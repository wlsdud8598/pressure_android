package kr.co.hi.medicare.fragment.mypage;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import kr.co.hi.medicare.net.hwNet.BaseData;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.common.DummyWebviewFragment;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.community.commonfunc.CommonFunction;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_gclife_join;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.StringUtil;

/**

 */

public class MyPointJoinLifecaremallFragment extends BaseFragmentMedi {//implements NewActivity.onKeyBackPressedListener {
    private final String TAG = MyPointJoinLifecaremallFragment.class.getSimpleName();

    private EditText edit_id,edit_pw,edit_pw_check,edit_email;
    private TextView text_valid_id, text_valid_pw , text_valid_mail;
    private TextView mContractErrTv;

    private TextView mAllCheckTextView;
    private CheckBox mAllCheckCheckBox;
    private CheckBox mStep1Cb;
    private CheckBox mStep2Cb;
    private CheckBox mStep3Cb;
    private View mCheckedEditText;

    private Button next_button;

    boolean isValid = false;
    private boolean isIdCheck, isPwCheck, isEmailCheck;

    public static Fragment newInstance() {
        MyPointJoinLifecaremallFragment fragment = new MyPointJoinLifecaremallFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(this.getClass().getSimpleName(), "onCreate()");
        super.onCreate(savedInstanceState);
        isIdCheck = false;
        isPwCheck = false;
        isEmailCheck=false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mypage_point_join_lifecaremall_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        edit_id = view.findViewById(R.id.edit_id);
        edit_email = view.findViewById(R.id.edit_email);
        edit_pw = view.findViewById(R.id.edit_pw);
        edit_pw_check = view.findViewById(R.id.edit_pw_check);

        text_valid_id = view.findViewById(R.id.text_valid_id);
        text_valid_pw = view.findViewById(R.id.text_valid_pw);
        text_valid_mail = view.findViewById(R.id.text_valid_mail);


        mStep1Cb = (CheckBox) view.findViewById(R.id.join_step1_checkbox1);
        mStep2Cb = (CheckBox) view.findViewById(R.id.join_step1_checkbox2);
        mStep3Cb = (CheckBox) view.findViewById(R.id.join_step1_checkbox3);
        mContractErrTv = (TextView) view.findViewById(R.id.join_step1_contract_error_textview);

        next_button = (Button) view.findViewById(R.id.next_button);

//        edit_id.setOnFocusChangeListener(mFocusChangeListener);
//        edit_email.setOnFocusChangeListener(mFocusChangeListener);
//        edit_pw.setOnFocusChangeListener(mFocusChangeListener);
//        edit_pw_check.setOnFocusChangeListener(mFocusChangeListener);


        mAllCheckCheckBox = view.findViewById(R.id.join_step_join1_all_checkbox);
        mAllCheckTextView = view.findViewById(R.id.join_step_all_check_textview);

        mStep1Cb.setOnClickListener(mOnClickListener);
        mStep2Cb.setOnClickListener(mOnClickListener);
        mStep3Cb.setOnClickListener(mOnClickListener);

        mStep1Cb.setOnCheckedChangeListener(mCheckedChageListener);
        mStep2Cb.setOnCheckedChangeListener(mCheckedChageListener);
        mStep3Cb.setOnCheckedChangeListener(mCheckedChageListener);


        edit_id.addTextChangedListener(mWatcher);
        edit_pw.addTextChangedListener(mWatcher);
        edit_pw_check.addTextChangedListener(mWatcher);
        edit_email.addTextChangedListener(mWatcher);


        view.findViewById(R.id.join_step1_contract_textview).setOnClickListener(mOnClickListener);
        view.findViewById(R.id.join_step1_personal_info_textview).setOnClickListener(mOnClickListener);
        view.findViewById(R.id.join_step1_info_share_textview).setOnClickListener(mOnClickListener);
        view.findViewById(R.id.next_button).setOnClickListener(mOnClickListener);

        mAllCheckCheckBox.setOnClickListener(mOnClickListener);
        mAllCheckTextView.setOnClickListener(mOnClickListener);
    }

    @Override
    public void onResume() {
        super.onResume();

        BtnEnableCheck();
    }

    /**
     * 전체 유효성 체크
     * @return
     */

    private boolean validCheck() {

//        if (validIdCheck(false)&&validPwCheck(false)&&validEmailCheck(false)&&validContractCheck()) {
//            isValid = true;
//        }else{
//            isValid = false;
//        }

        if (validIdCheck(true)&&validPwCheck(true)&&validEmailCheck(true)&&validContractCheck()) {
            isValid = true;
        }else{
            isValid = false;
        }


        return isValid;
    }

//    View.OnFocusChangeListener mFocusChangeListener = new View.OnFocusChangeListener() {
//        @Override
//        public void onFocusChange(View v, boolean hasFocus) {
//            mCheckedEditText = v;
//            if (v == edit_id && hasFocus == false) {
//                validIdCheck(true);
//            } else if (v == edit_pw && hasFocus == false) {
//                validPwCheck(true);
//            } else if (v == edit_pw_check && hasFocus == false) {
//                validPwCheck(true);
//            }else if (v == edit_email && hasFocus == false) {
//                validEmailCheck(true);
//            }
//            BtnEnableCheck();
//        }
//    };



    CompoundButton.OnCheckedChangeListener mCheckedChageListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            BtnEnableCheck();
        }
    };



    /**
     * 이메일 유효성 체크
     *
     * @return
     */
    private boolean validEmailCheck(boolean isDialog) {
        String email = edit_email.getText().toString();
        String validMsg="";
        if (email.length() == 0) {
            isEmailCheck = false;
        }else if(email.length()<4){
            validMsg="이메일을 4자리 이상 입력해 주세요.";
            isEmailCheck = false;
        }else if (!StringUtil.isValidEmail(email)) {
            validMsg=getString(R.string.join_agree_error_email_1);
            isEmailCheck = false;
        }else{
            isEmailCheck = true;
        }

        if(!validMsg.equals("")&&isDialog)
            CDialog.showDlg(getContext(), validMsg);

        return isEmailCheck;
    }

    /**
     * 아이디 체크
     *
     * @return
     */
//    private boolean validIdCheck(boolean isDialog) {
//        String id = edit_id.getText().toString().trim();
//        String validMsg="";
//
//        if (id.length() == 0) {
////            validMsg=getString(R.string.join_agree_error_id_1);
//            isIdCheck = false;
//        }else if ((id.length() < 6) || !StringUtil.isSpecialWord(id)) {
//            validMsg=getString(R.string.join_agree_error_id_1);
//            isIdCheck = false;
//        } else {
//            isIdCheck = true;
//        }
//
//        if(!validMsg.equals("")&&isDialog)
//            CDialog.showDlg(getContext(), validMsg);
//
//
//        BtnEnableCheck();
//        return isIdCheck;
//    }
    private boolean validIdCheck(boolean isDialog) {
        String id = edit_id.getText().toString().trim();
        String validMsg="";

        if (id.length() == 0) {
            isIdCheck = false;
        }else if ((id.length() < 4) || !StringUtil.isSpecialWord(id)) {
            validMsg=getString(R.string.join_agree_error_id_1);
            isIdCheck = false;
        } else {
            isIdCheck = true;
        }

        if(!validMsg.equals("")&&isDialog)
            CDialog.showDlg(getContext(), validMsg);


        return isIdCheck;
    }


    TextWatcher mWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            validIdCheck(false);
            BtnEnableCheck();
        }

        @Override
        public void afterTextChanged(Editable s) { }
    };

//    TextWatcher mWatcher2 = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            validPwCheck(false);
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) { }
//    };

//    TextWatcher mWatcher3 = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            validEmailCheck(false);
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) { }
//    };

    /**
     * @return
     */
    private boolean validPwCheck(boolean isDialog) {
        String pwd1 = edit_pw.getText().toString();
        String pwd2 = edit_pw_check.getText().toString();
        String validMsg="";

        if (pwd1.length() == 0 || pwd2.length() == 0) {
            isPwCheck=false;
        } else if (!pwd1.equals(pwd2)) {
            validMsg=getString(R.string.join_agree_error_pw_2);
            isPwCheck = false;
        } else if (!StringUtil.isValidPassword(pwd1) || !StringUtil.isValidPassword(pwd2)) {
            validMsg=getString(R.string.join_agree_error_pw_1);
            isPwCheck = false;
        } else {
            isPwCheck = true;
        }

        if(!validMsg.equals("")&&isDialog)
            CDialog.showDlg(getContext(), validMsg);


        return isPwCheck;
    }

    /**
     * 약관동의 체크 박스
     *
     * @return
     */
    private boolean validContractCheck() {
            // 회원정보 수정모드가 아니라면.
            if (mStep1Cb.isChecked() == false || mStep2Cb.isChecked() == false || mStep3Cb.isChecked() == false) {
                mContractErrTv.setVisibility(View.VISIBLE);
                return false;
            } else {
                mContractErrTv.setVisibility(View.INVISIBLE);
            }
            return true;
    }


    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Bundle bundle = new Bundle();
            String url="";
            switch (v.getId()) {
                case R.id.next_button :
                if (validCheck()) {
                    requestJoinFromServer();
                }
                break;
            case R.id.join_step_join1_all_checkbox :
                boolean isCheck = mAllCheckCheckBox.isChecked();
                mStep1Cb.setChecked(isCheck);
                mStep2Cb.setChecked(isCheck);
                mStep3Cb.setChecked(isCheck);
                break;
            case R.id.join_step_all_check_textview :
                isCheck = !mAllCheckCheckBox.isChecked();
                mAllCheckCheckBox.setChecked(isCheck);
                mStep1Cb.setChecked(isCheck);
                mStep2Cb.setChecked(isCheck);
                mStep3Cb.setChecked(isCheck);
                break;
            case R.id.join_step1_checkbox1 :
            case R.id.join_step1_checkbox2 :
            case R.id.join_step1_checkbox3 :
                mAllCheckCheckBox.setChecked(mStep1Cb.isChecked() && mStep2Cb.isChecked() && mStep3Cb.isChecked());
                break;
            case R.id.join_step1_contract_textview :
                bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_agree_info_2));
//                url = getString(R.string.personal_terms_1_url);
                url = "http://m.shealthcare.co.kr/HL_MED/popup/pop_01_life.html";
                bundle.putString(DummyWebviewFragment.URL, url);
                NewActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
                break;
            case R.id.join_step1_personal_info_textview :
                bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_agree_info_3));
//                url = getString(R.string.personal_terms_2_url);
                url = "http://m.shealthcare.co.kr/HL_MED/popup/pop_02_life.html";
                bundle.putString(DummyWebviewFragment.URL, url);
                NewActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
                break;
            case R.id.join_step1_info_share_textview:
                bundle.putString(DummyWebviewFragment.TITLE, getString(R.string.join_agree_info_4));
//                url = getString(R.string.personal_terms_3_url);
                url = "http://m.shealthcare.co.kr/HL_MED/popup/pop_03_life.html";
                bundle.putString(DummyWebviewFragment.URL, url);
                NewActivity.startActivity(getActivity(), DummyWebviewFragment.class, bundle);
            }

//            if (mCheckedEditText != null) {
//                mCheckedEditText.clearFocus();
//            }
        }
    };

    /**
     * Btn enable true / false
     */
//    private void BtnEnableCheck(){
//        boolean isContacts = mStep1Cb.isChecked() && mStep2Cb.isChecked() && mStep3Cb.isChecked();
//
//
//        next_button.setEnabled(isIdCheck
//                                && isPwCheck
//                                && isEmailCheck
//                                && isContacts);
//    }
    private void BtnEnableCheck(){
        boolean isContacts = mStep1Cb.isChecked() && mStep2Cb.isChecked() && mStep3Cb.isChecked();
        next_button.setEnabled(edit_id.getText().toString().length()>3&&edit_pw.getText().toString().length()>3&&edit_pw_check.getText().toString().length()>3&&edit_email.getText().toString().length()>3&&isContacts);
    }


    public void requestJoinFromServer() {


        final Tr_gclife_join.RequestData requestData = new Tr_gclife_join.RequestData();

        Tr_login info = UserInfo.getLoginInfo();
        requestData.mber_sn = info.mber_sn;
        requestData.id = edit_id.getText().toString().trim();
        requestData.pwd = edit_pw.getText().toString().trim();
        requestData.pwdconf = edit_pw_check.getText().toString().trim();
        requestData.nm = info.mber_nm;
        requestData.email = edit_email.getText().toString().trim();
        requestData.gender = info.mber_sex.equals("1") ? "M" : "W";
        requestData.mber_no = info.seq==null ? "" : info.seq;
        requestData.agree="Y";

        MediNewNetworkModule.doApi(getContext(), new Tr_gclife_join(), requestData,true, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {

                String message="";

                if (responseData instanceof Tr_gclife_join) {
                    Tr_gclife_join data = (Tr_gclife_join)responseData;
                    try {
                        if(data.reg_yn!=null&&data.reg_yn.equals("Y")){
                            CommonFunction.setUpdateLoginInfo(getContext());

                            CDialog dig = CDialog.showDlg(getContext(), "라이프케어몰 회원가입 및 아이디 연동이 완료 되었습니다.");
                            dig.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    onBackPressed();
                                }
                            });

                        }else{
                            message = "라이프케어몰 가입에 실패했습니다.\n"+data.err_msg;
                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                        message = getResources().getString(R.string.comm_error_common)+"\ncode:1";
                    }
                }

                if(!message.equals("")){
                    CDialog.showDlg(getContext(), message);
                }

            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
                Log.e(TAG, response);
                CDialog.showDlg(getContext(), response);
            }
        });















//        final String mberNm = edit_id.getText().toString();
//        Tr_mber_check.RequestData requestData = new Tr_mber_check.RequestData();
//        requestData.mber_nm = mberNm;
//        requestData.mber_lifyea = mBirthEt.getText().toString();
//        requestData.mber_hp = mPhoneNumEt.getText().toString();
////        requestData.mber_nation = "1";
//        requestData.mber_sex = dataVo.getSex();
//
//        if (mIsRealMember) {
//            // 정회원 가입인 경우
//            getData(getContext(), Tr_mber_check.class, requestData, new ApiData.IStep() {
//                @Override
//                public void next(Object obj) {
//                    if (obj instanceof Tr_mber_check) {
//                        Tr_mber_check data = (Tr_mber_check) obj;
//
//                        if ("Y".equals(data.data_yn)) {
////                            dataVo.setmber_no(data.mber_no);
//                            if(!TextUtils.isEmpty(data.mber_id)){
//                                // 아이디가 없으면 회원가입 스텝2로 이동
//                                if (BuildConfig.DEBUG)
//                                    Toast.makeText(getContext(), "디버그메시지:정회원가입", Toast.LENGTH_SHORT).show();
////                                moveTo2Step();
//                            } else {
//                                // 아이디 정보가 있으면 이미 가입된 회원으로 처리
//                                CDialog.showDlg(getContext(), getString(R.string.already_regist_member));
//                            }
//                        } else {
//                            CDialog.showDlg(getContext(), getString(R.string.already_no_service_member));
//                        }
//                    }
//                }
//            });
//        } else {
//            // 준회원 가입인 경우 검증 없이 2/2 로 이동
//            if (BuildConfig.DEBUG)
//                Toast.makeText(getContext(), "디버그메시지:준회원가입", Toast.LENGTH_SHORT).show();
////            moveTo2Step();
//        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
