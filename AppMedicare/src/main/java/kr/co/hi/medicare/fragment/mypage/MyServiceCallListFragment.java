package kr.co.hi.medicare.fragment.mypage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.data.Tr_serviceCounselContents;


public class MyServiceCallListFragment extends BaseFragmentMedi {

    public static final String CALL_LIST_DATA = "CALL_LIST_DATA";

    List<Tr_serviceCounselContents.ServiceCounselContents> list=null;
    ServiceCallListAdapter adapter=null;
    RecyclerView recyclerView=null;

    public static BaseFragmentMedi newInstance() {
        MyServiceCallListFragment fragment = new MyServiceCallListFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mypage_service_call, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        if(bundle!=null){
            list = (List<Tr_serviceCounselContents.ServiceCounselContents>)bundle.getSerializable(CALL_LIST_DATA);
            adapter = new ServiceCallListAdapter();
            recyclerView = view.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(adapter);
            adapter.setData(list);
        }
    }

    class ServiceCallListAdapter extends RecyclerView.Adapter<ServiceCallListAdapter.ViewHolder> {
        List<Tr_serviceCounselContents.ServiceCounselContents> mList = new ArrayList<>();

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.medi_item_service_call2, parent, false);
            return new ViewHolder(itemView);
        }

        public void setData(List<Tr_serviceCounselContents.ServiceCounselContents> dataList) {
            mList.clear();
            mList.addAll(dataList);
            notifyDataSetChanged();
        }


        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            final Tr_serviceCounselContents.ServiceCounselContents data = mList.get(position);
            holder.message.setText(data.title);
            holder.message_reply.setText(data.answer);
            holder.message_question.setText(data.question);
        }

        @Override
        public int getItemCount() {
            return mList == null ? 0 : mList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            private TextView message;
            private TextView message_question;
            private TextView message_reply;

            public ViewHolder(View itemView) {
                super(itemView);
                message = itemView.findViewById(R.id.message);
                message_question = itemView.findViewById(R.id.message_question);
                message_reply = itemView.findViewById(R.id.message_reply);
            }

        }

    }

}