package kr.co.hi.medicare.fragment.mypage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import kr.co.hi.medicare.net.hwNet.BaseData;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.MediNewNetworkModule;
import kr.co.hi.medicare.net.data.MediNewNetworkHandler;
import kr.co.hi.medicare.net.data.Tr_mber_pwd_edit_exe;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;

/**
 * Created by MrsWin on 2017-02-16.
 *
 */

public class PasswordSettingFragment extends BaseFragmentMedi {
    private final String TAG = PasswordSettingFragment.class.getSimpleName();

    private EditText mPwdTv1;
    private EditText mPwdTv2;
    private EditText mPwdTv3;

    private Button mNextBtn;
    private Button mCancelBtn;

    public static Fragment newInstance() {
        PasswordSettingFragment fragment = new PasswordSettingFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mypage_pw_setting, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPwdTv1 = view.findViewById(R.id.mypage_pwd_edittext1);
        mPwdTv2 = view.findViewById(R.id.mypage_pwd_edittext2);
        mPwdTv3 = view.findViewById(R.id.mypage_pwd_edittext3);
        mNextBtn = view.findViewById(R.id.next_button);
        mCancelBtn = view.findViewById(R.id.cancel_button);
        mCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doChangePwd();
            }
        });

        mPwdTv1.addTextChangedListener(mTextWatcher);
        mPwdTv2.addTextChangedListener(mTextWatcher);
        mPwdTv3.addTextChangedListener(mTextWatcher);
        ((TextView)view.findViewById(R.id.mypage_id)).setText(new UserInfo(getContext()).getSaveId());
    }

    TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String pwd1 = mPwdTv1.getText().toString();
            String pwd2 = mPwdTv2.getText().toString();
            String pwd3 = mPwdTv3.getText().toString();


            if(TextUtils.isEmpty(pwd1) == false && TextUtils.isEmpty(pwd2) == false &&TextUtils.isEmpty(pwd3) == false){
//                mNextBtn.setTextColor(Color.BLUE);
                mNextBtn.setEnabled(true);
            }else{
//                mNextBtn.setTextColor(getResources().getColor(R.color.color_BFBFBF));
                mNextBtn.setEnabled(false);
            }

//            mNextBtn.setEnabled(TextUtils.isEmpty(pwd1) == false && TextUtils.isEmpty(pwd2) == false &&TextUtils.isEmpty(pwd3) == false);
        }

        @Override
        public void afterTextChanged(Editable s) {}
    };

    /**
     * 포인트 정보 가져오기
     */
    private void doChangePwd() {
        String pwd1 = mPwdTv1.getText().toString();
        String pwd2 = mPwdTv2.getText().toString();
        final String pwd3 = mPwdTv3.getText().toString();


        if(!pwd1.equals(new UserInfo(getContext()).getSavedPw())){
            CDialog.showDlg(getContext(), getString(R.string.mypage_pw_message_4));
            return;
        }

        if (!pwd2.equals(pwd3)) {
            CDialog.showDlg(getContext(), getString(R.string.mypage_pw_message_1));
            return;
        }

        if (pwd1.equals(pwd3)) {
            CDialog.showDlg(getContext(), getString(R.string.mypage_pw_message_3));
            return;
        }

        if (!StringUtil.isValidPassword(pwd2) || !StringUtil.isValidPassword(pwd3)) {
            CDialog.showDlg(getContext(), getString(R.string.mypage_pw_message_2));
            return;
        }

        UserInfo userInfo = new UserInfo(getContext());
        Tr_mber_pwd_edit_exe.RequestData requestData = new Tr_mber_pwd_edit_exe.RequestData();
        String savedId = userInfo.getSaveId();  //SharedPref.getInstance().getPreferences(SharedPref.SAVED_LOGIN_ID);
        Tr_login login = UserInfo.getLoginInfo();
        requestData.mber_sn = login.mber_sn;
        requestData.mber_id = savedId;
        requestData.bef_mber_pwd = pwd1;
        requestData.aft_mber_pwd = pwd3;

        MediNewNetworkModule.doApi(getContext(), Tr_mber_pwd_edit_exe.class, requestData, new MediNewNetworkHandler() {
            @Override
            public void onSuccess(BaseData responseData) {
                if (responseData instanceof Tr_mber_pwd_edit_exe) {
                    Tr_mber_pwd_edit_exe data = (Tr_mber_pwd_edit_exe) responseData;
                    try {
                        if ("Y".equals(data.reg_yn)) {
                            mPwdTv1.setText("");
                            mPwdTv2.setText("");
                            mPwdTv3.setText("");
                            mPwdTv1.clearFocus();
                            mPwdTv2.clearFocus();
                            mPwdTv3.clearFocus();

                            boolean isAutoLogin = SharedPref.getInstance().getPreferences(SharedPref.IS_AUTO_LOGIN, false);
                            if (isAutoLogin) {
                                SharedPref.getInstance().savePreferences(SharedPref.SAVED_LOGIN_PWD, pwd3);
                            }

                            CDialog.showDlg(getContext(), getString(R.string.mypage_pw_message_5), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    onBackPressed();
                                }
                            });
                        } else {
                            mPwdTv1.setText("");
                            CDialog.showDlg(getContext(), getString(R.string.비밀번호변경실패));
                        }

                    } catch (Exception e) {
                        CLog.e(e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String response, Throwable error) {
//                    Log.e(TAG, response);
//                UIThread(SplashActivityMedicare.this, getString(R.string.confirm), getString(R.string.networkexception));
            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();

    }
}
