package kr.co.hi.medicare.fragment.premium;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.universalvideoview.UniversalMediaController;
import com.universalvideoview.UniversalVideoView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import kr.co.hi.medicare.Defined;
import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.BaseActivityMedicare;
import kr.co.hi.medicare.activity.CustomVideoNotifyActivity;
import kr.co.hi.medicare.activity.CustomVideoPlayActivity;
import kr.co.hi.medicare.activity.CustomVideoPopupActivity;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.net.EServerAPI;
import kr.co.hi.medicare.net.Record;
import kr.co.hi.medicare.util.AlertDialogUtil;
import kr.co.hi.medicare.util.CLog;
import kr.co.hi.medicare.util.JsonUtil;
import kr.co.hi.medicare.util.Util;

/**
 * Created by jihoon on 2016-05-31.
 * 맞춤 동영상 화면
 * @since 0, 1
 */
public class CustomVideoActivity extends BaseActivityMedicare implements UniversalVideoView.VideoViewCallback {

    private static final String SEEK_POSITION_KEY = "SEEK_POSITION_KEY";
    private static final String VIDEO_URL = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";

    // 탑
    private LinearLayout mMonthLayout, mTabLayout;

    // 재활운동 재생전
    private LinearLayout mExerciseBeforeLayout;
    private TextView mExerciseWarningTv, mExerciseAwarenessTv;
    private Button mExercisePlayBtn;
    private ImageView mExerciseImg;

    // 재활운동 재생후
    private LinearLayout mExerciseAfterLayout, mExerciseDescLayout;

    // 동영상
    private View mVideoLayout;
    private UniversalVideoView mVideoView;
    private UniversalMediaController mMediaController;

    // 동영상 사이즈 관련
    private int mSeekPosition;
    private int cachedHeight;
    private boolean isFullscreen;




    private String mType = "E"; // 동영상 타입 E = 재활운동 , R = 특별레시피

    private UserInfo user;
//    private ObjectMapper mapper;

    private String mTotalDate;
    private Calendar calendar = Calendar.getInstance();

    private TextView mMVTitleTv , mMVCateTitleTv , mMvDescriptionTv , mHealthTv , mSpeacialTv , mDateTv;
    private String mML_SEQ, mML_MONTH ,mMV_GUBUN, mMOTION_CODE, mMOTION_TITLE, mMV_CATE_TITLE, mMV_TITLE, mMV_NOTIFY, mMV_CONTENT, mMV_DESCRIPTION ,mMV_URL, mMV_THUMBNAIL, mOSEQ, mRESULT_CODE , mNMONTH , mLMONTH;
    private LinearLayout mOverLapLay1 , mOverLapLay2 ;

    private LinearLayout mPrev , mNext , mNotifyLay;
    private int mResultCode = 0;

    private boolean bFuture = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_video);

        init();
        setEvent();

        requestVideoApi(mType);

    }

    @Override
    protected void onPause() {
        super.onPause();
        CLog.d("onPause ");
//        if (mVideoView != null && mVideoView.isPlaying()) {
//            mSeekPosition = mVideoView.getCurrentPosition();
//            CLog.d("onPause mSeekPosition=" + mSeekPosition);
//            mVideoView.pause();
//        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        CLog.d("onSaveInstanceState Position=" + mVideoView.getCurrentPosition());
        outState.putInt(SEEK_POSITION_KEY, mSeekPosition);
    }

    @Override
    protected void onRestoreInstanceState(Bundle outState) {
        super.onRestoreInstanceState(outState);
        mSeekPosition = outState.getInt(SEEK_POSITION_KEY);
        CLog.d("onRestoreInstanceState Position=" + mSeekPosition);
    }

    /**
     * 초기화
     */
    public void init(){

        mMonthLayout            =   (LinearLayout)  findViewById(R.id.month_layout);
        mTabLayout              =   (LinearLayout)  findViewById(R.id.tab_layout);
        mOverLapLay1            =   (LinearLayout)  findViewById(R.id.overlap_lay1);
        mOverLapLay2            =   (LinearLayout)  findViewById(R.id.overlap_lay2);
        mPrev                   =   (LinearLayout)  findViewById(R.id.prev_btn);
        mNext                   =   (LinearLayout)  findViewById(R.id.next_btn);

        mExerciseBeforeLayout   =   (LinearLayout)  findViewById(R.id.exercise_before_play_layout);
        mExerciseAfterLayout    =   (LinearLayout)  findViewById(R.id.exercise_after_play_layout);
        mExerciseDescLayout     =   (LinearLayout)  findViewById(R.id.exercise_desc_layout);
        mNotifyLay              =   (LinearLayout)  findViewById(R.id.notify_lay);

        mExerciseWarningTv      =   (TextView)      findViewById(R.id.exercise_warning_tv); // 경고
        mExerciseAwarenessTv    =   (TextView)      findViewById(R.id.exercise_awareness_tv);   // 운동자각도
        mMVCateTitleTv          =   (TextView)      findViewById(R.id.mvcatetitle);   // 카테고리제목
        mMVTitleTv              =   (TextView)      findViewById(R.id.mvtitle_tv); // 제목
        mMvDescriptionTv        =   (TextView)      findViewById(R.id.mvdescription_tv); // 동영상설명
        mHealthTv               =   (TextView)      findViewById(R.id.health_btn); // 재활운동
        mSpeacialTv             =   (TextView)      findViewById(R.id.special_btn); // 특별레시피
        mDateTv                 =   (TextView)      findViewById(R.id.date_tv); // 날짜

        mExercisePlayBtn        =   (Button)        findViewById(R.id.exercise_play_btn);
        mExerciseImg            =   (ImageView)     findViewById(R.id.exercise_img_view);

        mOverLapLay1.setVisibility(View.VISIBLE);
        mOverLapLay2.setVisibility(View.GONE);

        mExerciseWarningTv.setText(Html.fromHtml("<u>" + getString(R.string.exercise_caution) + "<u>"));

        // 동영상


        mVideoLayout = findViewById(R.id.exercise_video_layout);
        mVideoView = (UniversalVideoView) findViewById(R.id.exercise_videoView);
        mMediaController = (UniversalMediaController) findViewById(R.id.exercise_media_controller);
        mVideoView.setMediaController(mMediaController);
        setVideoAreaSize();

    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){

//        mapper = new ObjectMapper();
        user = new UserInfo(this);

        int year = calendar.get ( calendar.YEAR );
        int month = calendar.get ( calendar.MONTH ) + 1 ;

        mTotalDate = String.valueOf(year) + Util.getTwoDateFormat(month);
        CLog.i("mTotalDate --> " + mTotalDate);

        mDateTv.setText(Util.getDateSpecialCharacter(CustomVideoActivity.this, mTotalDate, 3));

//        mVideoView.setVideoViewCallback(this);
//
//        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() { // 재생 완료시 기본화면 돌아가기
//            @Override
//            public void onCompletion(MediaPlayer mp) {
//                mExerciseBeforeLayout.setVisibility(View.VISIBLE);
//                mExerciseAfterLayout.setVisibility(View.GONE);
//
//                mExerciseDescLayout.setVisibility(View.VISIBLE);
//                mMonthLayout.setVisibility(View.VISIBLE);
//                mTabLayout.setVisibility(View.VISIBLE);
//            }
//        });
    }

    /**
     * 비디오 뷰 사이즈 조절
     */
    private void setVideoAreaSize() {
        mVideoLayout.post(new Runnable() {
            @Override
            public void run() {
                int width = 1080;
                CLog.d("width = " + width);
                cachedHeight = (int) (width * 405f / 720f);
                CLog.d("cachedHeight = " + cachedHeight);
                ViewGroup.LayoutParams videoLayoutParams = mVideoLayout.getLayoutParams();
                videoLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                videoLayoutParams.height = cachedHeight;
                mVideoLayout.setLayoutParams(videoLayoutParams);
                mVideoView.setVideoPath(VIDEO_URL);
                mVideoView.requestFocus();
            }
        });
    }


    public void requestVideoApi(String mtype) {

        CLog.i("mTotalDate -- > " + mTotalDate);
        CLog.i("mtype --> " + mtype);

        JSONObject jObject = new JSONObject();

        try {
            // {"DOCNO":"DY001","SEQ": "1234567890123","MMONTH":"201606","MV_GUBUN":"E"}

            jObject.put("SEQ", user.getSeq());      // 회원일렬번호
            jObject.put("MMONTH", mTotalDate);      // 날짜
            jObject.put("MV_GUBUN", mtype);      // 타입

        } catch (JSONException e) {
            e.printStackTrace();
        }
//old
//        request(EServerAPI.API_POST, RequestUtil.getJSONParameter(jObject, "DY001"));
        //new
        try {
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            client.setTimeout(10000);
            client.addHeader("Accept-Charset", "UTF-8");
            client.addHeader("User-Agent", new WebView(getBaseContext()).getSettings().getUserAgentString());
            jObject.put("DOCNO","DY001");

            params.put("strJson", jObject.toString());
            CLog.i("url = " + Defined.BASE_HTTPS_URL +"?" +params.toString());
            client.post(Defined.BASE_HTTPS_URL, params, mAsyncHttpHandler);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private AsyncHttpResponseHandler mAsyncHttpHandler = new AsyncHttpResponseHandler() {
        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            CLog.i("onSuccess");

            try {

                int count = headers.length;

                for (int i = 0; i < count; i++) {
                    CLog.v(headers[i].getName() + ": " + headers[i].getValue());
                }

                String response = new String(responseBody, "UTF-8");
                CLog.i("response = " +response);
                JSONObject resultData = null;

                if(response.startsWith("<")){   // api result 문자열 시작값이 "<" 로 시작하는 경우는 xml 폼
                    try {
                        response =  Util.parseXml(response);
                    }catch(Exception e){
                        CLog.e(e.toString());
                    }

                    resultData = new JSONObject(response);

                }else{
                    resultData = new JSONObject(response);
                }

                String resultCode = resultData.getString("RESULT_CODE");
                switch(resultCode){
                    case "0000":
                        try {
                            mResultCode = 0;


                            mML_SEQ = (String) resultData.get("ML_SEQ");
                            mML_MONTH = (String) resultData.get("ML_MONTH");
                            mMV_GUBUN = (String) resultData.get("MV_GUBUN");
                            mMOTION_CODE = (String) resultData.get("MOTION_CODE");
                            mMOTION_TITLE = (String) resultData.get("MOTION_TITLE");
                            mMV_CATE_TITLE = (String) resultData.get("MV_CATE_TITLE");
                            mMV_TITLE = (String) resultData.get("MV_TITLE");
//                        mMV_NOTIFY = (String) resultData.get("MV_NOTIFY");
                            mMV_CONTENT = (String) resultData.get("MV_CONTENT");
                            mMV_DESCRIPTION = (String) resultData.get("MV_DESCRIPTION");
                            mMV_URL = (String) resultData.get("MV_URL");
                            mMV_THUMBNAIL = (String) resultData.get("MV_THUMBNAIL");
                            mOSEQ = (String) resultData.get("OSEQ");
                            mRESULT_CODE = (String) resultData.get("RESULT_CODE");
                            mLMONTH = (String) resultData.get("LMONTH");
                            mNMONTH = (String) resultData.get("NMONTH");

                        }catch (Exception e){
                            CLog.i(e.toString());
                        }
                        UiThread();
                        break;
                    case "4444": // 동영상이 없음
                        UIThread(CustomVideoActivity.this, getString(R.string.confirm), getString(R.string.msg_not_video));
                        mResultCode = 1;
                        break;
                    case "6666": // 회원 존재 X
                        UIThread(CustomVideoActivity.this, getString(R.string.confirm), getString(R.string.no_user));
                        mResultCode = 2;
                        break;
                    case "9999": // 기타
                        UIThread(CustomVideoActivity.this, getString(R.string.confirm), getString(R.string.msg_error));
                        mResultCode = 3;
                        break;
                }
                mMV_NOTIFY = (String) resultData.get("MV_NOTIFY");

            } catch (Exception e) {
                CLog.e(e.toString());
            }

            bFuture = false;
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            CLog.i("onFailure");
            Handler mHandler = new Handler(Looper.getMainLooper());
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    UIThread(CustomVideoActivity.this, getString(R.string.confirm), getString(R.string.networkexception));
                }
            }, 0);
            bFuture = false;
        }
    };


    @Override
    public void request(EServerAPI eServerAPI, Object obj) {
        Record record = new Record();
        record.setEServerAPI(eServerAPI);
        switch (eServerAPI) {
            case API_GET:
                record.setRequestData(obj);
                break;
            case API_POST:
                record.setRequestData(obj);
                break;
        }
        sendApi(record);
    }

    @Override
    public void response(Record record) throws UnsupportedEncodingException {
        String json = new String(record.getData());
        json = json.substring(json.indexOf("{"), json.lastIndexOf("}") + 1);
        Map<String, Object> result = JsonUtil.getJSONParserData(record.getData());
        CLog.e(json);
        if (result.containsKey("RESULT_CODE")) {
            switch ((String) result.get("RESULT_CODE")) {
                case "0000":

                    try {

                        mResultCode = 0;
                        /*
                        ML_SEQ	INT	4	 플레이리스트 일련번호(운동지각도 입력/수정할때 필요합니다)
                        ML_MONTH	VARCHAR	20	 동영상게시 월
                        MV_GUBUN	CHAR	1	 동영상 구분(E:재활운동 ,R: 특별레시피)
                        MOTION_CODE	CHAR	1	 운동지각도 코드(운동지각코드 탭을 참조하세요)
                        MOTION_TITLE	VARCHAR	50	 운동지각도 코드 설명
                        MV_CATE_TITLE	VARCHAR	50	 동영상 카테고리 제목
                        MV_TITLE	VARCHAR	200	 동영상타이틀
                        MV_NOTIFY	VARCHAR	6000	 동영상 주의(권장)사항
                        MV_CONTENT	VARCHAR	6000	 동영상 내용
                        MV_DESCRIPTION	VARCHAR	2000	 동영상 설명
                        MV_URL	VARCHAR	1000	 동영상 URL
                        MV_THUMBNAIL	VARCHAR	1000	 동영상 썸네일 URL
                        OSEQ	VARCHAR	13	 회원일련번호
                        RESULT_CODE	CHAR	4	 결과코드
                         */

                        mML_SEQ = (String) result.get("ML_SEQ");
                        mML_MONTH = (String) result.get("ML_MONTH");
                        mMV_GUBUN = (String) result.get("MV_GUBUN");
                        mMOTION_CODE = (String) result.get("MOTION_CODE");
                        mMOTION_TITLE = (String) result.get("MOTION_TITLE");
                        mMV_CATE_TITLE = (String) result.get("MV_CATE_TITLE");
                        mMV_TITLE = (String) result.get("MV_TITLE");
//                        mMV_NOTIFY = (String) result.get("MV_NOTIFY");
                        mMV_CONTENT = (String) result.get("MV_CONTENT");
                        mMV_DESCRIPTION = (String) result.get("MV_DESCRIPTION");
                        mMV_URL = (String) result.get("MV_URL");
                        mMV_THUMBNAIL = (String) result.get("MV_THUMBNAIL");
                        mOSEQ = (String) result.get("OSEQ");
                        mRESULT_CODE = (String) result.get("RESULT_CODE");
                        mLMONTH = (String) result.get("LMONTH");
                        mNMONTH = (String) result.get("NMONTH");

                    }catch (Exception e){
                        CLog.i(e.toString());
                    }
                    UiThread();
                    break;
                case "4444": // 동영상이 없음
                    UIThread(CustomVideoActivity.this, getString(R.string.confirm), getString(R.string.msg_not_video));
                    mResultCode = 1;
                    break;
                case "6666": // 회원 존재 X
                    UIThread(CustomVideoActivity.this, getString(R.string.confirm), getString(R.string.no_user));
                    mResultCode = 2;
                    break;
                case "9999": // 기타
                    UIThread(CustomVideoActivity.this, getString(R.string.confirm), getString(R.string.msg_error));
                    mResultCode = 3;
                    break;
            }

            // result_code와 관계없이 NOTIFY(주의사항) 은 무조건적으로 보여지게 바뀜 (2016-06-29)
            mMV_NOTIFY = (String) result.get("MV_NOTIFY");
        }
    }

    @Override
    public void networkException(Record record) {
        CLog.e("ERROR STATE : " + record.stateCode);
        UIThread(CustomVideoActivity.this, getString(R.string.confirm), getString(R.string.networkexception));
    }

    private void UIThread(final Activity activity, final String confirm, final String message) {
        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                AlertDialogUtil.onAlertDialog(activity, confirm, message);
            }
        }, 0);
    }

    private void UiThread() {
        final Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {


                mMVCateTitleTv.setText(mMV_TITLE);
                mMvDescriptionTv.setText(mMV_DESCRIPTION);
                CustomImageLoader.displayImage(CustomVideoActivity.this, mMV_THUMBNAIL, mExerciseImg);
                mDateTv.setText(Util.getDateSpecialCharacter(CustomVideoActivity.this, mTotalDate, 3));
                if (mMOTION_TITLE.equals("")) {
                    mExerciseAwarenessTv.setText(getString(R.string.not_register));
                } else {
                    mExerciseAwarenessTv.setText(mMOTION_TITLE);
                }

                if (mLMONTH.equals("")) {
                    mPrev.setVisibility(View.INVISIBLE);
                } else {
                    mPrev.setVisibility(View.VISIBLE);
                }

                if (mNMONTH.equals("")) {
                    mNext.setVisibility(View.INVISIBLE);
                } else {
                    mNext.setVisibility(View.VISIBLE);
                }


            }
        }, 0);


    }

    public void click_event(View v) {
        SimpleDateFormat date = new SimpleDateFormat("yyyyMM");
        switch (v.getId()) {
            case R.id.exercise_play_btn:    // 운동 재생
                switch (mResultCode){
                    case 0:
                        if (mMV_URL != null && !mMV_URL.equals("")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(this);
                            alert.setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(CustomVideoActivity.this, CustomVideoPlayActivity.class);
                                    intent.putExtra(EXTRA_MV_CATE_TITLE, mMV_TITLE);
                                    intent.putExtra(EXTRA_MV_CONTENT, mMV_CONTENT);
                                    intent.putExtra(EXTRA_MV_URL, mMV_URL);
                                    intent.putExtra(EXTRA_TYPE, mType);
                                    startActivity(intent);

                                }
                            }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            alert.setMessage(getString(R.string.exercise_wifi));
                            alert.show();
                        }else {
                            UIThread(CustomVideoActivity.this, getString(R.string.confirm), getString(R.string.msg_not_video));
                        }
                        break;
                    case 1:
                        UIThread(CustomVideoActivity.this, getString(R.string.confirm), getString(R.string.msg_not_video));
                        break;
                    case 2:
                        UIThread(CustomVideoActivity.this, getString(R.string.confirm), getString(R.string.no_user));
                        break;
                    case 3:
                        UIThread(CustomVideoActivity.this, getString(R.string.confirm), getString(R.string.msg_error));
                        break;
                }

                break;
            case R.id.health_btn:
                mType = "E";
                mMVCateTitleTv.setText("");
                mMV_NOTIFY = "";
                mMVTitleTv.setText(getString(R.string.exercise_health));
                mOverLapLay1.setVisibility(View.VISIBLE);
                mOverLapLay2.setVisibility(View.GONE);
                mExerciseWarningTv.setText(Html.fromHtml("<u>" + getString(R.string.exercise_caution) + "<u>"));
                mHealthTv.setBackgroundResource(R.drawable.nutrition_round_white_layout);
                mSpeacialTv.setBackgroundResource(R.drawable.top_round_white_layout);
                requestVideoApi(mType);
                break;
            case R.id.special_btn:
                mType = "R";
                mMVCateTitleTv.setText("");
                mMV_NOTIFY = "";
                mMVTitleTv.setText(getString(R.string.exercise_special));
                mOverLapLay2.setVisibility(View.VISIBLE);
                mOverLapLay1.setVisibility(View.GONE);
                mExerciseWarningTv.setText(Html.fromHtml("<u>" + getString(R.string.exercise_caution2) + "<u>"));
                mHealthTv.setBackgroundResource(R.drawable.top_round_white_layout);
                mSpeacialTv.setBackgroundResource(R.drawable.nutrition_round_white_layout);
                requestVideoApi(mType);
                break;
            case R.id.prev_btn:
                calendar.add(calendar.MONTH , -1);
                mTotalDate = String.valueOf(date.format(calendar.getTime()));
                requestVideoApi(mType);
                break;
            case R.id.next_btn:
                if(bFuture)
                    break;
                bFuture = true;
                calendar.add(calendar.MONTH , +1);
                mTotalDate = String.valueOf(date.format(calendar.getTime()));
                requestVideoApi(mType);
                break;
            case R.id.notify_lay:
//                if (mMV_NOTIFY != null && !mMV_NOTIFY.equals("")) {
                Intent intent = new Intent(CustomVideoActivity.this, CustomVideoNotifyActivity.class);
                if (mType.equals("E")){ // 운동
                    intent.putExtra(EXTRA_TYPE, 0);
                }else { // 레시피
                    intent.putExtra(EXTRA_TYPE, 1);
                }

                intent.putExtra(EXTRA_MV_NOTIFYTITLE, mExerciseWarningTv.getText().toString());
                startActivity(intent);
//                }
                break;
            case R.id.exercise_awareness_tv:
                if (mMV_URL != null && !mMV_URL.equals("")) {
                    Intent intent2 = new Intent(CustomVideoActivity.this, CustomVideoPopupActivity.class);
                    intent2.putExtra(EXTRA_MOTION_CODE, mMOTION_CODE);
                    intent2.putExtra(EXTRA_ML_SEQ, mML_SEQ);
                    startActivityForResult(intent2, 0);
                }
                break;
        }
    }

    @Override
    public void onScaleChange(boolean bool) {
        this.isFullscreen = bool;
        if (isFullscreen) {
            ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
            mVideoLayout.setLayoutParams(layoutParams);

            mExerciseDescLayout.setVisibility(View.GONE);
            mMonthLayout.setVisibility(View.GONE);
            mTabLayout.setVisibility(View.GONE);


        } else {
            ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.height = this.cachedHeight;
            mVideoLayout.setLayoutParams(layoutParams);

            mExerciseDescLayout.setVisibility(View.VISIBLE);
            mMonthLayout.setVisibility(View.VISIBLE);
            mTabLayout.setVisibility(View.VISIBLE);

        }

        switchTitleBar(!isFullscreen);
    }

    /**
     * 네비바 활성 유무
     * @param show ( true - 활성, false - 비활성 )
     */
    private void switchTitleBar(boolean show) {
        Toolbar supportActionBar = (Toolbar) findViewById(R.id.toolbar);
        if (supportActionBar != null) {
            CLog.d("supportActionBar != null");
            if (show) {
                supportActionBar.setVisibility(View.VISIBLE);
            } else {
                supportActionBar.setVisibility(View.GONE);
            }
        }else{
            CLog.d("supportActionBar == null");
        }

    }

    @Override
    public void onPause(MediaPlayer mediaPlayer) {
        CLog.d("onPause");
    }

    @Override
    public void onStart(MediaPlayer mediaPlayer) {
        CLog.d("onStart");
    }

    @Override
    public void onBufferingStart(MediaPlayer mediaPlayer) {
        CLog.d("onBufferingStart");
    }

    @Override
    public void onBufferingEnd(MediaPlayer mediaPlayer) {
        CLog.d("onBufferingEnd");
    }

    @Override
    public void onBackPressed() {
        if (this.isFullscreen) {
            mVideoView.setFullscreen(false);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        CLog.i("resultCode : " + resultCode);
        CLog.i("requestCode :" + requestCode);

        if (resultCode != Activity.RESULT_OK) {
            CLog.i("resultCode != RESULT_OK");
            return;
        }

        switch (requestCode) {
            case 0:
                mMOTION_CODE = data.getStringExtra(EXTRA_MOTION_CODE);

                if (mMOTION_CODE.equals("A")) {
                    mExerciseAwarenessTv.setText(getString(R.string.exercise_feel1));
                } else if (mMOTION_CODE.equals("B")) {
                    mExerciseAwarenessTv.setText(getString(R.string.exercise_feel2));
                } else if (mMOTION_CODE.equals("C")) {
                    mExerciseAwarenessTv.setText(getString(R.string.exercise_feel3));
                } else if (mMOTION_CODE.equals("D")) {
                    mExerciseAwarenessTv.setText(getString(R.string.exercise_feel4));
                } else if (mMOTION_CODE.equals("E")) {
                    mExerciseAwarenessTv.setText(getString(R.string.exercise_feel5));
                } else if (mMOTION_CODE.equals("F")) {
                    mExerciseAwarenessTv.setText(getString(R.string.exercise_feel6));
                } else if (mMOTION_CODE.equals("G")) {
                    mExerciseAwarenessTv.setText(getString(R.string.exercise_feel7));
                } else if (mMOTION_CODE.equals("H")) {
                    mExerciseAwarenessTv.setText(getString(R.string.exercise_feel8));
                } else if (mMOTION_CODE.equals("I")) {
                    mExerciseAwarenessTv.setText(getString(R.string.exercise_feel9));
                }else {

                }

                break;

        }


        super.onActivityResult(requestCode, resultCode, data);
    }

}
