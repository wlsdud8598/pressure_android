package kr.co.hi.medicare.fragment.premium;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.activity.MainActivityMedicare;
import kr.co.hi.medicare.activity.ServiceHistoryActivity;
import kr.co.hi.medicare.activity.ServiceHistoryChoiceActivity;
import kr.co.hi.medicare.activity.ServiceHistoryStrokeActivity;
import kr.co.hi.medicare.component.OnClickListener;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.sample.SampleFragmentMedi;


/**
 * Created by MrsWin on 2017-02-16.
 */

public class PremiumMainFragment extends BaseFragmentMedi {
    private final String TAG = getClass().getSimpleName();

    private FragmentPagerAdapter mPagerAdapter;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private RadioButton[] mFragmentRadio;
    private RadioGroup mFragmentGroup;
    private Fragment[] mFragments;
    private View tabview;
    private TextView mTitle;
    private TextView mRightBtn;

    public static Fragment newInstance() {
        PremiumMainFragment fragment = new PremiumMainFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_2tab_toolbar, container, false);
        return view;
    }


    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // XXX 백으로 데이터 보내기
        Bundle bundle = new Bundle();
        bundle.putString(SampleFragmentMedi.SAMPLE_BACK_DATA, TAG + " BackData!!!");
        setBackData(bundle);
        mTitle = view.findViewById(R.id.common_toolbar_title);
        mTitle.setText("Premium");

        mRightBtn = view.findViewById(R.id.common_toolbar_right_btn);
        mRightBtn.setText("서비스 이력");
        mRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                Tr_login login = UserInfo.getLoginInfo();
                boolean b_cancer = false, b_stroke = false;

                b_cancer = login.pm_yn.equals("Y")? true:false;
                b_stroke = login.st_yn.equals("Y")? true:false;

                if(b_cancer && b_stroke == false) // 암
                    intent = new Intent(getActivity(), ServiceHistoryActivity.class);
                else if(b_cancer == false && b_stroke) // 뇌줄중
                    intent = new Intent(getActivity(), ServiceHistoryStrokeActivity.class);
                else
                    intent = new Intent(getActivity(), ServiceHistoryChoiceActivity.class);
                startActivity(intent);
            }
        });

        tabview = view;

        mFragmentGroup = view.findViewById(R.id.tab_radiogroup);

        final String[] mFragmentNames = new String[]{
                getString(R.string.drawer_menu1_sub2),
                getString(R.string.drawer_menu1_sub4),
        };

        mFragments = new Fragment[]{
                MealFragment.newInstance(),
                CustomVideoFragment.newInstance(),
        };

        mFragmentRadio = new RadioButton[]{
                view.findViewById(R.id.tab1),
                view.findViewById(R.id.tab2),
        };

        int i = 0;
        for (String name : mFragmentNames) {

            mFragmentRadio[i].setText(name);
            i++;
        }

        mFragmentGroup.setOnCheckedChangeListener(mCheckedChangeListener);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(null, view, getContext());


        //프리미엄
        view.findViewById(R.id.tab1).setOnTouchListener(ClickListener);
        view.findViewById(R.id.tab2).setOnTouchListener(ClickListener);
        mRightBtn.setOnTouchListener(ClickListener);

        //코드부여
        view.findViewById(R.id.tab1).setContentDescription(getString(R.string.foodcoaching));
        view.findViewById(R.id.tab2).setContentDescription(getString(R.string.fitvideo));
        mRightBtn.setContentDescription(getString(R.string.serviceview));

        setChildFragment(mFragments[0]);
    }

    public RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            int pos=group.indexOfChild(tabview.findViewById(checkedId));
            setChildFragment(mFragments[pos]);

        }
    };

    private void setChildFragment(Fragment child) {
        FragmentTransaction childFt = getChildFragmentManager().beginTransaction();

        if (!child.isAdded()) {
            childFt.replace(R.id.child_fragment_layout, child);
            childFt.addToBackStack(null);
            childFt.commit();
        }
    }


}