package kr.co.hi.medicare.googleFitness;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResponse;
import com.google.android.gms.fitness.result.DataReadResult;
import com.google.android.gms.tasks.OnSuccessListener;
import kr.co.hi.medicare.net.hwNet.ApiData;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import kr.co.hi.medicare.utilhw.StringUtil;

import static java.text.DateFormat.getDateTimeInstance;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Created by godaewon on 2017. 9. 13..
 */

public class HomeTodayGoogleFitData {
    public static final String TAG = HomeTodayGoogleFitData.class.getSimpleName();
    private int mArrIdx = 0;

    protected Map<Integer, Integer> mGoogleFitStepMap = new HashMap<>();           // 구글 피트니스 조회 데이터 저장
    private Map<Integer, Integer> mDbResultMap = new HashMap<>();   // Sqlite 조회 내용 저장

    public HomeTodayGoogleFitData() {

    }


    /**
     * 일단위 걸음수 조회
     * @param context
     * @param day   // (오늘:0, 어제 -1...)
     * @param iStep
     */
    public void getDayStepCount(final Context context, final int day, final ApiData.IStep iStep) {
        if (GoogleFitInstance.isFitnessAuth(context)) {
            Fitness.getHistoryClient(context,
                    GoogleSignIn.getLastSignedInAccount(context))
                    .readData(queryFitnessDataToday(day))
                    .addOnSuccessListener(new OnSuccessListener<DataReadResponse>() {
                        @Override
                        public void onSuccess(DataReadResponse response) {
                            if (response.getBuckets().size() > day) {
                                for (Bucket bucket : response.getBuckets()) {
                                    List<DataSet> dataSets = bucket.getDataSets();
                                    for (DataSet dataSet : dataSets) {
                                        dumpDataSet(dataSet);
                                    }
                                }

                                int totalStep = 0;
                                for (int i : mGoogleFitStepMap.keySet()) {
                                    Log.i(TAG, "mGoogleFitDataMap.size["+i+"]="+mGoogleFitStepMap.get(i).intValue());
                                    totalStep += mGoogleFitStepMap.get(i).intValue();
                                }

                                Log.i(TAG, "getDayStepCount="+totalStep);
                                iStep.next(""+totalStep);
                            }
                        }
                    });
        } else {
            Log.e(TAG, "구글 피트니스 Noti 작동 태스트 중 계정인증 안됨.");
//            Intent serviceIntent = new Intent(activity, FitnessForeGroundService.class);
            if (context instanceof Activity) {
                GoogleFitInstance.requestGoogleFitnessAuth((Activity)context);
            }
        }
    }

    /**
     * 걸음수 조회 하기
     */
    public DataReadRequest queryFitnessDataToday(int day) {
        DataType dataType1 = DataType.TYPE_STEP_COUNT_DELTA;
        DataType dataType2 = DataType.AGGREGATE_STEP_COUNT_DELTA;

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, day);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
        long startTime = cal.getTimeInMillis();

        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
        long endTime = cal.getTimeInMillis();

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(dataType1, dataType2)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startTime, endTime, MILLISECONDS)
                .build();


        Log.i(TAG, "GoogleStepSave startDate:"+startTime + " endTime:" + endTime  +  " readRequest:"+readRequest);

        return readRequest;
    }

    /**
     * 구글 피트니스 데이터 복잡한 데이터 타입에서 필요한 데이터만 뽑아서 차트 및 데이터에 사용할 데이터 추출
     * @param dataSet
     * @return
     */
    private void dumpDataSet(DataSet dataSet) {
        Log.i(TAG, "====================");
        DateFormat dateFormat = getDateTimeInstance();
        for (DataPoint dp : dataSet.getDataPoints()) {
            Log.i(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(MILLISECONDS)));
            Log.i(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(MILLISECONDS)));
            for (Field field : dp.getDataType().getFields()) {
                Log.i(TAG, "\t Value:" + field.getName() + "=" + dp.getValue(field) + "[" + mArrIdx + "]");
                // sqlite에 저장 여부를 판단할 용도로 사용할 Map
                mGoogleFitStepMap.put(mArrIdx, (int) StringUtil.getFloat(dp.getValue(field).toString()));
            }
        }
        mArrIdx++;
    }

    /**
     * 조회된 구글 피트니스 데이터 데이터 형태로 파싱하기
     * @param dataReadResult
     */
    public void readData(DataReadResult dataReadResult) {
        mArrIdx = 0;
        mGoogleFitStepMap.clear();
        if (dataReadResult.getBuckets().size() > 0) {
            for (Bucket bucket : dataReadResult.getBuckets()) {
                List<DataSet> dataSets = bucket.getDataSets();
                for (DataSet dataSet : dataSets) {
                    dumpDataSet(dataSet);
                }
            }
        }
    }
}
