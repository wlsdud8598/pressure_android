package kr.co.hi.medicare.googleFitness;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;
import kr.co.hi.medicare.value.model.StepModel;
import kr.co.hi.medicare.net.hwNet.ApiData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.net.hwdata.Tr_mvm_info_input_data_mission;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.ChartTimeUtil;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.utilhw.StringUtil;
import kr.co.hi.medicare.value.TypeDataSet;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Created by godaewon on 2017. 9. 13..
 */

public class MissionGoogleFitData {
    public static final String TAG = MissionGoogleFitData.class.getSimpleName();
    private BaseFragmentMedi  mBaseFragment;

    public GoogleApiClient mClient = null;

    private int mArrIdx = 0;

    String _startDate;
    String _endDate;



    protected Map<Integer, Integer> mGoogleFitStepMap = new HashMap<>();           // 구글 피트니스 조회 데이터 저장
    private Map<Integer, Integer> mDbResultMap = new HashMap<>();   // Sqlite 조회 내용 저장
    private List<StepModel> mStepArr = new ArrayList<>();

    public MissionGoogleFitData(BaseFragmentMedi baseFragment, GoogleApiClient client) {
        mBaseFragment = baseFragment;
        mClient = client;
    }

    /**
     * 걸음수 조회 하기
     */
    public DataReadRequest queryFitnessData() {
        /* 조회 타입 설정(칼로리, 걸음수) */
        DataType dataType1 = DataType.TYPE_STEP_COUNT_DELTA;
        DataType dataType2 = DataType.AGGREGATE_STEP_COUNT_DELTA;

        ChartTimeUtil timeClass = new ChartTimeUtil(TypeDataSet.Period.PERIOD_DAY);
        timeClass.setTimeUnit(TimeUnit.DAYS);       // 일단위

        Tr_login login = UserInfo.getLoginInfo();


        Calendar cal = CDateUtil.getCalendar_yyyyMMdd(login.mission_walk_start_de);//Calendar.getInstance();
//        cal.add(Calendar.MONTH, -1);    // Calendar 에서 월은 1을 마이너스 해줘야 함
        cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
        timeClass.setStartTime(cal.getTimeInMillis());
        long startTime = timeClass.getStartTime();

        cal = CDateUtil.getCalendar_yyyyMMdd(login.mission_walk_end_de);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
        timeClass.setEndTime(cal.getTimeInMillis());
        long endTime = timeClass.getEndTime();

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(dataType1, dataType2)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startTime, endTime, MILLISECONDS)
                .build();


        System.out.println( "MissionGoogleFitData startDate:"+startTime + " endTime:" + endTime  +  " readRequest:"+readRequest);

        return readRequest;
    }

    /**
     * 구글 피트니스 데이터 복잡한 데이터 타입에서 필요한 데이터만 뽑아서 차트 및 데이터에 사용할 데이터 추출
     * @param dataSet
     * @return
     */
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");//getDateTimeInstance();
    private void dumpDataSet(DataSet dataSet) {
        Logger.i(TAG, "===========dumpDataSet=========");
//        DateFormat dateFormat = getDateTimeInstance();

        for (DataPoint dp : dataSet.getDataPoints()) {
            Logger.i(TAG, "dumpDataSet.Start: " + dateFormat.format(dp.getStartTime(MILLISECONDS)));
            Logger.i(TAG, "dumpDataSet.End: " + dateFormat.format(dp.getEndTime(MILLISECONDS)));
            for (Field field : dp.getDataType().getFields()) {
                Log.i(TAG, " dumpDataSet.Value:" + field.getName() + "=" + dp.getValue(field) + "[" + mArrIdx + "]");
                // sqlite에 저장 여부를 판단할 용도로 사용할 Map
                int step = (int) StringUtil.getFloat(dp.getValue(field).toString());
                String date = dateFormat.format(dp.getStartTime(MILLISECONDS));
                mGoogleFitStepMap.put(mArrIdx, step);

                StepModel model = new StepModel();
                model.step = ""+step;
                model.regDate = date;
                Logger.i(TAG, "MissionGoogleFitData:dumpDataSet::: "+date+" ::: ["+mArrIdx+"]="+step);
                mStepArr.add(model);
            }
        }

        mArrIdx++;
    }

    /**
     * 조회된 구글 피트니스 데이터 데이터 형태로 파싱하기
     * @param dataReadResult
     */
    public void readData(DataReadResult dataReadResult) {
        mArrIdx = 0;
        mGoogleFitStepMap.clear();
        if (dataReadResult.getBuckets().size() > 0) {
//            Log.i(TAG, "BucketSize=" + dataReadResult.getBuckets().size());
            System.out.println( "MissionGoogleFitData startDate:"+_startDate+" dataReadResult:"+dataReadResult.getBuckets().size());
            for (Bucket bucket : dataReadResult.getBuckets()) {
                List<DataSet> dataSets = bucket.getDataSets();
                for (DataSet dataSet : dataSets) {
                    dumpDataSet(dataSet);
                }
            }

        }
    }

    /**
     * 구글피트니스 오늘 걸음 숫자 구하기
     */
    public class QueryMissionStep extends AsyncTask<Void, Void, Void> {
        DataReadRequest readRequest;
        public QueryMissionStep(BaseFragmentMedi baseFragment) {
            mStepArr.clear();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            mBaseFragment.showProgress();
        }

        protected Void doInBackground(Void... params) {
            // Create the query.
            readRequest = queryFitnessData();

            if(mClient != null){
                DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest)
                        .await(1, TimeUnit.DAYS);
                readData(dataReadResult);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            uploadGoogleFitStepData();
            int totalStep = 0;
            for (int i : mGoogleFitStepMap.keySet()) {
                Logger.i(TAG, "mGoogleFitDataMap.size["+i+"]="+mGoogleFitStepMap.get(i).intValue());
                totalStep += mGoogleFitStepMap.get(i).intValue();
            }

            Logger.i(TAG, "QueryMissionStep.totalStep="+totalStep);

            if (mStepArr.size() > 0) {
                Tr_mvm_info_input_data_mission missionInput = new Tr_mvm_info_input_data_mission();
                Tr_mvm_info_input_data_mission.RequestData requestData = new Tr_mvm_info_input_data_mission.RequestData();

                Tr_login login = UserInfo.getLoginInfo();
                requestData.mber_sn = login.mber_sn;
                requestData.ast_mass = missionInput.getArray(mStepArr);

                mBaseFragment.getData(mBaseFragment.getContext(), Tr_mvm_info_input_data_mission.class, requestData, new ApiData.IStep() {
                    @Override
                    public void next(Object obj) {
                        if (obj instanceof Tr_mvm_info_input_data_mission) {
                            Logger.i(TAG, "미션 걸음정보 업로드 "+mStepArr.size());
                        }
                    }
                });

            }
        }
    }

}
