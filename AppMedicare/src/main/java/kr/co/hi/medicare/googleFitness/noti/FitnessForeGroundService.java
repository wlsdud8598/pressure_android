package kr.co.hi.medicare.googleFitness.noti;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptionsExtension;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Value;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.request.SensorRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import kr.co.hi.medicare.net.hwNet.ApiData;

import java.util.concurrent.TimeUnit;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.googleFitness.GoogleFitInstance;
import kr.co.hi.medicare.googleFitness.HomeTodayGoogleFitData;
import kr.co.hi.medicare.push.NotiDummyActivity;
import kr.co.hi.medicare.utilhw.CDateUtil;
import kr.co.hi.medicare.utilhw.SharedPref;
import kr.co.hi.medicare.utilhw.StringUtil;

public class FitnessForeGroundService extends Service { //implements SensorEventListener {
    private final String TAG = getClass().getSimpleName();
    public static final String PREF_KEY_STEP_MISSION_COMPLETE_NOTIFICATION = "PREF_KEY_STEP_MISSION_COMPLETE_NOTIFICATION";

//    private SensorManager sensorManager;
//    private Sensor stepCountSensor;

    private final int NOTIF_ID = 0x234;

    public static final String START_GOOGLE_FITNESS_SERVICE = "START_GOOGLE_FITNESS_SERVICE";
    public static final String STOP_GOOGLE_FITNESS_SERVICE = "STOP_GOOGLE_FITNESS_SERVICE";

    private int mYestedayStepTotal = 0;
    private int mTodayStepTotal = 0;

    private String mTodayStr;

    public static void startForegroundService(Context context) {
        Intent intent = new Intent(context, FitnessForeGroundService.class);
        if (Build.VERSION.SDK_INT >= 26) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPref.getInstance().initContext(this);
        mTodayStr = CDateUtil.getToday_yyyy_MM_dd();

        if (GoogleFitInstance.isFitnessAuth(FitnessForeGroundService.this)) {
            showNotification();
        } else {
            Toast.makeText(this, "구글 피트니스 연결에 실패했습니다.", Toast.LENGTH_SHORT).show();
            Intent serviceIntent = new Intent(this, FitnessForeGroundService.class);
            stopService(serviceIntent);
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(START_GOOGLE_FITNESS_SERVICE);
        filter.addAction(STOP_GOOGLE_FITNESS_SERVICE);
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(broadcastReceiver, filter);

        startGoogleFitness();

    }

    void showNotification() {
        Notification noti = getMyActivityNotification();
        startForeground(NOTIF_ID, noti);
    }


    private void updateNotification() {
        Notification notification = getMyActivityNotification();
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIF_ID, notification);
    }

    /**
     * Notification 버튼 클릭
     */
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "broadcastReceiver intent.getAction()="+intent.getAction());
            if (intent.getAction().equals(START_GOOGLE_FITNESS_SERVICE)) {
                FitnessForeGroundService.startForegroundService(FitnessForeGroundService.this);
            } else if (intent.getAction().equals(STOP_GOOGLE_FITNESS_SERVICE)) {
                Intent serviceIntent = new Intent(FitnessForeGroundService.this, FitnessForeGroundService.class);
                stopService(serviceIntent);
            } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                startGoogleFitness();
            } else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                removeSensorClient();
            }
        }
    };


    private Notification getMyActivityNotification(){
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.fitness_notification_service);
//        Intent rewIntent = new Intent(START_GOOGLE_FITNESS_SERVICE);    // 시작버튼
//        PendingIntent rewPendingIntent = PendingIntent.getBroadcast(this, NOTIF_ID, rewIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//        remoteViews.setOnClickPendingIntent(R.id.sms_service_start, rewPendingIntent);

        Intent stopIntent = new Intent(STOP_GOOGLE_FITNESS_SERVICE);    // 종료버튼 처리
        PendingIntent stopPendingIntent = PendingIntent.getBroadcast(this, NOTIF_ID, stopIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.stop_service_btn, stopPendingIntent);
        remoteViews.setTextViewText(R.id.log_time_textview, "어제 걸음수 : "+StringUtil.getFormatPrice(""+mYestedayStepTotal));
         remoteViews.setTextViewText(R.id.noti_step_textview, "오늘 걸음수 : "+StringUtil.getFormatPrice(""+mTodayStepTotal));



        Intent notificationIntent = new Intent(this, NotiDummyActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);


        NotificationCompat.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String CHANNEL_ID = "fitness_noti_service_channel";
            String CHANNEL_NAME = "메디케어 걸음수 측정";
            @SuppressLint("WrongConstant")
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_LOW);
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        } else {
            builder = new NotificationCompat.Builder(this);
        }
        builder.setSound(null);
        builder.setVibrate(new long[] {0});
        builder.setStyle(new NotificationCompat.DecoratedCustomViewStyle());
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        builder.setSmallIcon(R.mipmap.ic_stat_notify)
//                .setContent(remoteViews)
                .setCustomContentView(remoteViews)
                .setContentIntent(pendingIntent);
        return builder.build();
    }


    protected void startGoogleFitness() {
        if (GoogleFitInstance.isFitnessAuth(FitnessForeGroundService.this)) {
            Fitness.getRecordingClient(this, GoogleSignIn.getLastSignedInAccount(this))
                    .subscribe(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Log.i(TAG, "Successfully subscribed!");
//                                readData();
                                getStepCount(-1, new ApiData.IStep() {
                                    @Override
                                    public void next(Object obj) {
                                        // 오늘걸음수 조회
                                        getStepCount(0, null);
                                    }
                                });
                            } else {
                                Log.w(TAG, "There was a problem subscribing.", task.getException());
                            }
                        }
                    });
        } else {
            Log.e(TAG, "구글 피트니스 Noti 작동 태스트 중 계정인증 안됨.");
            Intent serviceIntent = new Intent(FitnessForeGroundService.this, FitnessForeGroundService.class);
            stopService(serviceIntent);
        }
    }


    /**
     * 걸음수 가져오기
     * @param day 오늘 : 0, 하루전 : -1 ...
     * @param iStep
     */
    private void getStepCount(final int day, final ApiData.IStep iStep) {
        new HomeTodayGoogleFitData().getDayStepCount(FitnessForeGroundService.this, day, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof String) {
                    int stepCount = StringUtil.getIntVal(obj.toString());
                    if (day == -1) {
                        Log.i(TAG, "어제걸음수="+stepCount);
                        mYestedayStepTotal = stepCount;
                        if (iStep != null) {
                            iStep.next(obj);
                        }
                    } else {
                        Log.i(TAG, "오늘걸음수="+stepCount);
                        mTodayStepTotal = stepCount;

                        Notification notification = getMyActivityNotification();
                        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.notify(NOTIF_ID, notification);

                        startGoogleSensorClient();
                    }
                }
            }
        });
    }

//    /**
//     * 10000보 달성시 미션 성공 등록
//     */
//    private void todayStepMissionAlert() {
//        if (mTodayStepTotal >= 10000) {
//
//            final String today = CDateUtil.getToday_yyyy_MM_dd();
//            String prefToday = SharedPref.getInstance().getPreferences(HomeFragmentMedicare.PREF_KEY_STEP_MISSION_COMPLETE);    // 홈화면에서 5000보 미션 완료 노티 띄웠었는지 여부
//            String prefTodayNoti = SharedPref.getInstance().getPreferences(PREF_KEY_STEP_MISSION_COMPLETE_NOTIFICATION);        // Notification에서 5000보 미션 등록 되었는지 여부
//            if (today.equals(prefTodayNoti))
//                return;
//
//            if (today.equals(prefToday) == false) {
//                Tr_mvm_misson_goal_alert.RequestData requestData = new Tr_mvm_misson_goal_alert.RequestData();
//                requestData.mber_sn = UserInfo.getLoginInfo().mber_sn;
//                requestData.misson_work_typ = "1";
//
//                MediNewNetworkModule.doApi(FitnessForeGroundService.this, Tr_mvm_misson_goal_alert.class, requestData, false, false, new MediNewNetworkHandler() {
//                    @Override
//                    public void onSuccess(BaseData responseData) {
////                        SharedPref.getInstance().savePreferences(HomeFragmentMedicare.PREF_KEY_STEP_MISSION_COMPLETE, today);
//                        SharedPref.getInstance().savePreferences(PREF_KEY_STEP_MISSION_COMPLETE_NOTIFICATION, today);
//                    }
//
//                    @Override
//                    public void onFailure(int statusCode, String response, Throwable error) {
//
//                    }
//                });
//            }
//        }
//    }

    private Task<Void> mSencsorClient;

    /**
     * 구글피트니스 센서API 등록
     */
    private void startGoogleSensorClient() {
        if (mSencsorClient != null)
            return;

        Log.i(TAG, "startGoogleSensorClient()");

        GoogleSignInOptionsExtension fitnessOptions =
                FitnessOptions.builder()
                        .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                        .build();

        GoogleSignInAccount gsa = GoogleSignIn.getAccountForExtension(FitnessForeGroundService.this, fitnessOptions);

        SensorRequest sensorRequest = new SensorRequest.Builder()
                .setDataType(DataType.TYPE_STEP_COUNT_DELTA)
                .setSamplingRate(60, TimeUnit.SECONDS)  // sample once per minute
                .build();

        mSencsorClient = Fitness.getSensorsClient(FitnessForeGroundService.this, gsa)
                .add(sensorRequest, googleFitStepCountListener);
    }

    /**
     * 구글피트니스 SensorApi Listener 해제
     */
    private void removeSensorClient() {
        Log.i(TAG, "removeSensorClient()");
        GoogleFitInstance.unregisterSensorFitnessDataListener(this, googleFitStepCountListener);
        mSencsorClient = null;
    }

    OnDataPointListener googleFitStepCountListener = new OnDataPointListener() {
        @Override
        public void onDataPoint(DataPoint dataPoint) {
            for (Field field : dataPoint.getDataType().getFields()) {
                Value val = dataPoint.getValue(field);
                Log.i(TAG, "Detected DataPoint field: " + field.getName());
                Log.i(TAG, "Detected DataPoint value: " + val);
                mTodayStepTotal += val.asInt();

            }
            newDayRefresh();
//            todayStepMissionAlert();

            Notification notification = getMyActivityNotification();
            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(NOTIF_ID, notification);

//            todayStepMissionAlert();
        }
    };

    /**
     * 새로운날로 갱신 되면 처리
     */
    private void newDayRefresh() {
        String today = CDateUtil.getToday_yyyy_MM_dd();
        if (today.equals(mTodayStr) == false) {
            startGoogleFitness();
        }
    }



//    @Override
//    public void onSensorChanged(SensorEvent event) {
//        if (event.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
//            String steps = String.valueOf(event.values[0]);
//            Log.i(TAG, "Sensor Step Count : " + steps);
//
//            updateNotification();
//        } else if (event.sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {
//            String steps = String.valueOf(event.values[0]);
//            updateNotification();
//        }
//    }
//
//
//    @Override
//    public void onAccuracyChanged(Sensor sensor, int accuracy) {
//
//    }
//
//    private void startSensor() {
//        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
//        stepCountSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
////        stepCountSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
//        if (stepCountSensor == null) {
//            Log.e(TAG, "센서 없음");
//            if (BuildConfig.DEBUG)
//                Toast.makeText(FitnessForeGroundService.this, "서비스 센서 없음", Toast.LENGTH_SHORT).show();
//
//        } else {
//            sensorManager.registerListener(this, stepCountSensor, SensorManager.SENSOR_DELAY_FASTEST);
//        }
//    }

//    private void stopSensor() {
//        if (sensorManager != null)
//            sensorManager.unregisterListener(this);
//    }

    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
        removeSensorClient();
//        stopSensor();
    }


}