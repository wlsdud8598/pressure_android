package kr.co.hi.medicare.googleFitness.noti;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.activity.BaseActivityMedicare;


public class FitnessMainActivity extends BaseActivityMedicare {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fitness_activity_main);


        findViewById(R.id.send_sms_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                SMSUtil.sendSMS(FitnessMainActivity.this,"01022587654", "테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지");
            }
        });

        findViewById(R.id.service_start_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                FitnessForeGroundService.showNotification(FitnessMainActivity.this);
                Intent intent = new Intent(FitnessMainActivity.this, FitnessMainActivity.class);
                if (Build.VERSION.SDK_INT >= 26) {
                    startForegroundService(intent);
                }
                else {
                    startService(intent);
                }
            };
        });
    }


}
