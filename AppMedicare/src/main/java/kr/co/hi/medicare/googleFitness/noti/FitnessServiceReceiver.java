package kr.co.hi.medicare.googleFitness.noti;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class FitnessServiceReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // an Intent broadcast.
//        throw new UnsupportedOperationException("Not yet implemented");
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            FitnessForeGroundService.startForegroundService(context);
        }
    }

//    private void showNotification(Context context) {
//        Intent intent = new Intent(context, FitnessForeGroundService.class);
//        if (Build.VERSION.SDK_INT >= 26) {
//            context.showNotification(intent);
//        } else {
//            context.startService(intent);
//        }
//    }
}
