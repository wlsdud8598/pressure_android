package kr.co.hi.medicare.net;

public abstract class NetworkListener
{
	public abstract void onResponse(Record a_oRecord);
    public abstract void onNetworkException(Record a_oRecord);
}
