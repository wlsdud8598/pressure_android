package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 영양코칭 >  식사기록 삭제

 OSEQ : 회원일련번호 RESULT_CODE : 결과코드
 0000 : 삭제성공 4444 : 등록된 식사기록이 없습니다. 6666 : 회원이 존재하지 않음 9999 : 기타오류
 */

public class Tr_DX004 extends BaseData {

    public static class RequestData {

        public String EDATE;
        public String SDATE;
        public String SEQ;
        public String DOCNO;
    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof RequestData) {
            JSONObject body = getBaseJsonObj();
            RequestData data = (RequestData) obj;

            body.put("DOCNO", "DX004");
            body.put("SEQ", data.SEQ);
            body.put("SDATE", data.SDATE);
            body.put("EDATE", data.EDATE);

            return body;
        }

        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("AST_LENGTH")
    public String ast_length;

}