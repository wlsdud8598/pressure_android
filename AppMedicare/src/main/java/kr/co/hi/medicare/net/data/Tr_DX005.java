package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 영양코칭 > 영양톡 주간이력 DATA조회

 AST_LENGTH : 배열의 원소 개수 ADDR_MASS :
 HABITS_GUBUN : 식습관 구분 (K:유지해야할 식습관 ,R:고쳐야할 식습관) HABITS_MEMO : 식습관 코칭메모 HB_IMG1 : 이미지1 HB_IMG2 : 이미지2 HB_IMG3 : 이미지3 LDATE : 코칭 시작일 기준 시작일 NDATE : 코칭 종료일 기준 시작일
 0000 : 조회성공 4444 : 등록된 코칭데이터가 없습니다. 6666 : 회원이 존재하지 않음 9999 : 기타오류
 */

public class Tr_DX005 extends BaseData {

    public static class RequestData {
//        {"DOCNO":"DX005","SEQ":"0001013000015","SDATE":"20190101","EDATE":"20190201"}
        public String EDATE;
        public String SDATE;
        public String SEQ;
        public String DOCNO;
    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof RequestData) {
            JSONObject body = getBaseJsonObj();
            RequestData data = (RequestData) obj;

            body.put("DOCNO", "DX005");
            body.put("SEQ", data.SEQ);
            body.put("SDATE", data.SDATE);
            body.put("EDATE", data.EDATE);


            return body;
        }

        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("AST_LENGTH")
    public String ast_length;

}