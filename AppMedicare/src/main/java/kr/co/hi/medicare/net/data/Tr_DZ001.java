package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 서비스이력보기 > 본인인증(API확인중)

 RESULT_CODE : 결과코드
 0000 : 조회성공 4444 : 등록된 이력이 없습니다. 6666 : 회원이 존재하지 않음 9999 : 기타오류
 */

public class Tr_DZ001 extends BaseData {

    public static class RequestData {
//        strJson={"DOCNO":"DZ002","SEQ":"0001013000015"}
//        public String DOCNO;
//        public String SEQ;

    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof RequestData) {
            JSONObject body = getBaseJsonObj();
            RequestData data = (RequestData) obj;

//            body.put("DOCNO", "DZ001");
//            body.put("SEQ", data.SEQ);


            return body;
        }

        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("AST_LENGTH")
    public String ast_length;

}