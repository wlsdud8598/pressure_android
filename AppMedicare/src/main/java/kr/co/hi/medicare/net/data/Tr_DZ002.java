package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 서비스이력보기 > 심리케어 서비스 이력보기

 AST_LENGTH : 배열의 원소 개수 ADDR_MASS : 배열 PS_SEQ : 심리케어 일련번호 REQDATE : 신청일자 USEDATE : 이용일자 SO_NAME : 서비스(제휴)기관명 USE_MEMO : 서비스 이용내역(메모) STATE_NAME : 서비스상태 ,서버에서 코드없이 멘트 드립니다.(이용중,해지,완료) SCOUNT : 서비스 회차 OSEQ : 회원일련번호 RESULT_CODE : 결과코드
 0000 : 조회성공 4444 : 등록된 이력이 없습니다. 6666 : 회원이 존재하지 않음 9999 : 기타오류
 */

public class Tr_DZ002 extends BaseData {

    public static class RequestData {
//        strJson={"DOCNO":"DZ002","SEQ":"0001013000015"}
        public String DOCNO;
        public String SEQ;

    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof RequestData) {
            JSONObject body = getBaseJsonObj();
            RequestData data = (RequestData) obj;

            body.put("DOCNO", "DZ002");
            body.put("SEQ", data.SEQ);


            return body;
        }

        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("AST_LENGTH")
    public String ast_length;

}