package kr.co.hi.medicare.net.data;

/**
 *
 * 알리미 리스트
 *
 * input값
 * insures_code : 회사코드
 * mber_sn : 회원고유키값
 * api_code : api 키값
 * pushk :
 * pageNumber :
 * pln :
 *
 * output 값
 * api_code : 호출코드명
 * insures_code : 회사코드
 * KBTP : 알리미 총 페이지 수
 * kbta_idx : 알리미 게시물 일련번호
 * kbt : 알리미 제목
 * sub_tit : 알리미 서브타이틀 (본문 요약,HTML태그 제거)
 * kbc	VARCHAR : 알리미 내용
 * kaimg : 알리미 이미지 URL
 * ka_timg : 알리미 타이틀 이미지 URL
 * kbt_pdf : 알리미 첨부파일 URL(PDF)
 * html_yn : HTML 여부 (N : 기본, Y:HTML)
 * notice_typ : 알리미 구분(1:기본, 2:이벤트용)
 * kbvd : 알리미 게재일 ex)20180905 ,날짜형식 변경 2018-09-05 -->20180905
 */

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


public  class Tr_asstb_kbtg_alimi extends BaseData {
    private final String TAG = getClass().getSimpleName();

    public Tr_asstb_kbtg_alimi() {
//    super.conn_url ="https://wkd.walkie.co.kr:443/HS_HL/ws.asmx/getJson";
    }

    public static class RequestData {
        public String mber_sn;
        public String pushk;
        public String pageNumber;
        public String pln;

    }


    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof RequestData) {

            JSONObject body = new JSONObject();

            RequestData data = (RequestData) obj;
            body.put("api_code", TAG.replace("Tr_", ""));
            body.put("insures_code", INSURES_CODE);
            body.put("mber_sn", data.mber_sn);
            body.put("pushk", data.pushk);
            body.put("pageNumber", data.pageNumber);
            body.put("pln", data.pln);

            return body;
        }

        return super.makeJson(obj);
    }

/**************************************************************************************************/
/***********************************************RECEIVE********************************************/
    /**************************************************************************************************/
    @SerializedName("insures_code")
    public String insures_code;
    @SerializedName("api_code")
    public String api_code;

    @SerializedName("kbtp")
    public String kbtp;

    @SerializedName("data_yn")
    public String data_yn;

    @SerializedName("chlmReadern")
    public List<ChlmReadern> chlmReadern;

    public static class ChlmReadern {
        @SerializedName("kbvd")
        public String kbvd;
        @SerializedName("notice_typ")
        public String notice_typ;
        @SerializedName("html_yn")
        public String html_yn;
        @SerializedName("kbt_pdf")
        public String kbt_pdf;
        @SerializedName("ka_timg")
        public String ka_timg;
        @SerializedName("kaimg")
        public String kaimg;
        @SerializedName("kbc")
        public String kbc;
        @SerializedName("sub_tit")
        public String sub_tit;
        @SerializedName("kbt")
        public String kbt;
        @SerializedName("kbta_idx")
        public String kbta_idx;
    }


}
