package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 정회원전환 Y
 * input 값
 * insures_code: 회사코드
 * mber_sn: 회원키
 * mber_no:
 * memname: 이름
 * <p>
 * output값
 * api_code: 호출코드명
 * insures_code: 회사코드
 * mber_sn : 회원키
 * mber_grad : 정회원:10/준회원:20
 * data_yn: 데이터수정여부
 */

public  class Tr_asstb_mber_keep_member_id_change extends BaseData {
	private final String TAG = Tr_asstb_mber_keep_member_id_change.class.getSimpleName();

	public static class RequestData {
		public String memname;
		public String mber_no;
		public String mber_sn; //회원 일련번호
		public String sel_site_yn;
		public String app_site_yn;
		public String sel_memid;
	}

	public Tr_asstb_mber_keep_member_id_change() {
		super.conn_url = "http://m.shealthcare.co.kr/HL_MED/WebService/MED_MOBILE_CALL.asmx/MED_mobile_Call";
	}

	@Override
	public JSONObject makeJson(Object obj) throws JSONException {
		if (obj instanceof RequestData) {
			JSONObject body = getBaseJsonObj();

			RequestData data = (RequestData) obj;
			body.put("api_code", getApiCode(TAG));
			body.put("insures_code", INSURES_CODE);
			body.put("mber_sn", data.mber_sn);
			body.put("mber_no", data.mber_no);
			body.put("memname", data.memname);

			body.put("sel_site_yn", data.sel_site_yn);
			body.put("app_site_yn", data.app_site_yn);
			body.put("sel_memid", data.sel_memid);
			return body;
		}

		return super.makeJson(obj);
	}

	/**************************************************************************************************/
	/***********************************************RECEIVE********************************************/
	/**************************************************************************************************/

	@Expose
	@SerializedName("data_yn")
	public String data_yn;
	@Expose
	@SerializedName("sel_memid")
	public String sel_memid;
	@Expose
	@SerializedName("app_site_yn")
	public String app_site_yn;
	@Expose
	@SerializedName("sel_site_yn")
	public String sel_site_yn;
	@Expose
	@SerializedName("mber_grad")
	public String mber_grad;
	@Expose
	@SerializedName("mber_sn")
	public String mber_sn;
	@Expose
	@SerializedName("insures_code")
	public String insures_code;
	@Expose
	@SerializedName("api_code")
	public String api_code;
}
