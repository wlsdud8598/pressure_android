package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.hi.medicare.utilhw.Logger;

/**
 *임직원몰가입
 input 값
 insures_code : 회사코드
 agree : 동의여부
 id : 임직원몰 id
 pwd : 비밀번호
 pwdconf : 비밀번호확인
 nm : 성명
 email : email정보
 gender : 성별(남자:M / 여자: W)
 mber_no : 대체key(juminnum) * 준회원은 대체키가 없으므로 공백으로전송한다
 mber_sn : 회원key

 output값
 api_code : 호출코드명
 insures_code : 회사코드
 gclife_id : 임직원몰 id
 err_msg : 에러메세지
 reg_yn : 처리
 */

public class Tr_gclife_join extends BaseData {
    private final String TAG = Tr_gclife_join.class.getSimpleName();

    public static class RequestData {
        public String mber_sn;
        public String mber_no;
        public String gender;
        public String email;
        public String nm;
        public String pwdconf;
        public String pwd;
        public String id;
        public String agree;
    }


    public Tr_gclife_join() {

        super.conn_url = "http://m.shealthcare.co.kr/HL_MED/WebService/MED_MOBILE_CALL.asmx/MED_mobile_Call";

    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        Logger.i(TAG, "makeJson.obj=" + obj);
        if (obj instanceof Tr_gclife_join.RequestData) {
            JSONObject body = new JSONObject();
            Tr_gclife_join.RequestData data = (Tr_gclife_join.RequestData) obj;

            body.put("api_code", getApiCode(TAG) ); //
            body.put("insures_code", INSURES_CODE);
            body.put("mber_sn", data.mber_sn); //  1000
            body.put("mber_no", data.mber_no);
            body.put("gender", data.gender);
            body.put("email", data.email);
            body.put("nm", data.nm);
            body.put("pwdconf", data.pwdconf);
            body.put("pwd", data.pwd);
            body.put("id", data.id);
            body.put("agree", data.agree);

            return body;
        }

        return super.makeJson(obj);
    }


    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("api_code") // mber_main_call",
    public String api_code; //
    @SerializedName("insures_code") // 303",
    public String insures_code; //
    @SerializedName("reg_yn")
    public String reg_yn;
    @SerializedName("gclife_id")
    public String gclife_id;
    @SerializedName("err_msg")
    public String err_msg;
}
