package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 input값
 insures_code : 회사코드
 HP_SEQ : Token Key값
 DEVICE_KIND : 디바이스구분 (A:안드로이드 / I : 아이폰)
 APP_VER : App버전

 output 값
 api_code : 호출코드명
 insures_code : 회사코드
 device_kind : 디바이스구분 (A:안드로이드 / I : 아이폰)
 now_app_ver : 최신App version
 */

public class Tr_get_app_info extends BaseData {
	private final String TAG = getClass().getSimpleName();

	public static class RequestData {
		public String HP_SEQ;
		public String APP_VER;
	}

	@Override
	public JSONObject makeJson(Object obj) throws JSONException {
		if (obj instanceof RequestData) {
			JSONObject body = getBaseJsonObj();

			RequestData data = (RequestData) obj;

			body.put("api_code", getApiCode(TAG));
			body.put("HP_SEQ", data.HP_SEQ);
			body.put("DEVICE_KIND", "A");
			body.put("APP_VER", data.APP_VER);

			return body;
		}

		return super.makeJson(obj);
	}

	/**************************************************************************************************/
	/***********************************************RECEIVE********************************************/
	/**************************************************************************************************/
	@SerializedName("api_code")
	public String api_code;
	@SerializedName("insures_code")
	public String insures_code;
	@SerializedName("device_kind")
	public String device_kind;
	@SerializedName("now_app_ver")
	public String now_app_ver;

}
