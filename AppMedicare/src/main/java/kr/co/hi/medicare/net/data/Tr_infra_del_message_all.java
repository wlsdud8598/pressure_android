package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;
import kr.co.hi.medicare.utilhw.Logger;

import org.json.JSONException;
import org.json.JSONObject;

/* 건강메세지 삭제하기
  json={
"api_code":"infra_del_message_all"
,"insures_code":"304"
,"app_code":"android"
,"mber_sn":"1032"
}
*/

public class Tr_infra_del_message_all extends BaseData {
    private final String TAG = Tr_infra_del_message_all.class.getSimpleName();

    public static class RequestData {

        public String mber_sn; //12121212",
    }

    public Tr_infra_del_message_all() throws JSONException {

    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        Logger.i(TAG, "makeJson.obj=" + obj);
        if (obj instanceof Tr_infra_del_message_all.RequestData) {
            JSONObject body = new JSONObject();
            Tr_infra_del_message_all.RequestData data = (Tr_infra_del_message_all.RequestData) obj;
            body.put("api_code", getApiCode(TAG));
            body.put("insures_code", INSURES_CODE);
            body.put("app_code", APP_CODE);

            body.put("mber_sn", data.mber_sn); // 1

            return body;
        }

        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/


    @SerializedName("api_code")
    public String api_code;
    @SerializedName("insures_code")
    public String insures_code;
    @SerializedName("maxpageNumber")
    public String maxpageNumber;
    @SerializedName("data_yn")
    public String data_yn;

}
