package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 로그인

 식사기록 DB입력/수정(API확인중)
 [CONFLICT]
 POST
 식사기록 주간리스트
 POST
 로그인
 POST
 식사기록 보기

 No Environment
 식사기록 주간리스트
 Examples (0)
 AST_LENGTH : 배열의 원소 개수 ADDR_MASS : 배열 WDATE : 식사등록일 EX)20160601 MEAL_WHEN : 식사구분 B:아침, L:점심, D:저녁 , S:간식 MC_CONTENT : 식사메모 UPFILE1 : 이미지1 UPFILE2 : 이미지2 UPFILE3 : 이미지3 UPFILE4 : 이미지4 UPFILE5 : 이미지5 EVAL_YN : 평가 (Y/N) LDATE : 검색시작일 기준 이전날짜 ex)20160301 NDATE : 검색종료일 기준 다음날짜 ex)20160301 OSEQ : 회원일련번호 RESULT_CODE : 결과코드
 0000 : 조회성공 4444 : 등록된 식사기록이 없습니다. 6666 : 회원이 존재하지 않음 9999 : 기타오류
 */

public class Tr_login_medi extends BaseData {



    public static class RequestData {
        public String EMAIL;
        public String PASSWD;
        public String APPVER;
        public String DOCNO;
    }

//	public Tr_login_medi(Context context) {
//		mContext = context;
////
//	}

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof RequestData) {
            JSONObject body = getBaseJsonObj();

            RequestData data = (RequestData) obj;

//            String refreshedToken = FirebaseInstanceId.getInstance().getToken();    // 토큰값.

            body.put("EMAIL", data.EMAIL);
            body.put("PASSWD", data.PASSWD);
            body.put("APPVER", data.APPVER);
            body.put("DOCNO", "DM002");

            return body;
        }

        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("RESULT_CODE")
    public String resultCode;
    @SerializedName("NEW_LINK")
    public String newLink;
    @SerializedName("NEW_APPVER")
    public String newAppver;
    @SerializedName("FPHP")
    public String fphp;
    @SerializedName("FPNM")
    public String fpnm;
    @SerializedName("NUHP")
    public String nuhp;
    @SerializedName("NUNM")
    public String nunm;
    @SerializedName("HPNUM")
    public String hpnum;
    @SerializedName("MEMNAME")
    public String memname;
    @SerializedName("STYN")
    public String styn;
    @SerializedName("PMYN")
    public String pmyn;
    @SerializedName("NICK")
    public String nick;
    @SerializedName("OSEQ")
    public String oseq;
    @SerializedName("DOCNO")
    public String docno;

}
