package kr.co.hi.medicare.net.data;

/**
 *
 * 정회원전환
 *
 * input 값
 * insures_code: 회사코드
 * mber_name: 이름
 * mber_lifyea: 생년월일
 * mberhp: 전화번호
 * mber_sex: 성별 1:남자, 2:여자
 *
 * output값
 * api_code: 호출코드명
 * insures_code: 회사코드
 * data_yn: 데이터수정여부
 */

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;


public class Tr_mber_grad_chg extends BaseData {
    private final String TAG = getClass().getSimpleName();

    public Tr_mber_grad_chg() {
//    super.conn_url ="https://wkd.walkie.co.kr:443/HS_HL/ws.asmx/getJson";
    }

    public static class RequestData {
        public String mber_name; // 1000
        public String mber_lifyea; // 99
        public String mberhp; // 99
        public String mber_sex; // 99
    }


    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof RequestData) {

            JSONObject body = new JSONObject();

            RequestData data = (RequestData) obj;
            body.put("api_code", TAG.replace("Tr_", ""));
            body.put("insures_code", INSURES_CODE);
            body.put("mber_name", data.mber_name); // 1000",
            body.put("mber_lifyea", data.mber_lifyea); // 1000",
            body.put("mber_hp", data.mberhp); // 1000",
            body.put("mber_sex", data.mber_sex); // 99

            return body;
        }

        return super.makeJson(obj);
    }

/**************************************************************************************************/
/***********************************************RECEIVE********************************************/
    /**************************************************************************************************/
    @SerializedName("insures_code")
    private String insures_code;
    @SerializedName("api_code")
    private String api_code;

    @SerializedName("data_yn")
    private String data_yn;

}
