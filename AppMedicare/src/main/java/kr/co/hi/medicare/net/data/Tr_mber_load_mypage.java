package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 마이페이지 정보 mber_load_mypage
 * input 값
 * insures_code : 회사코드
 * mber_sn : 회원키
 * <p>
 * output값
 * api_code : 호출코드명
 * insures_code : 회사코드
 * mber_sn : 대체번호
 * MBER_NM : 회원명
 * MBER_SEX : 성별
 * NICKNAME : 닉네임
 * MBER_LIFYEA : 출생년도
 * age : 나이
 * MBER_DE : 회원가입일
 * PROFILE_PIC : 프로필img
 * DISEASE_OPEN : 질환 Y/N
 * DISEASE_NM : 질환명
 * POINT_TOTAL_AMT : 적립포인트
 * mber_height : 신장
 * mber_bdwgh : 체중
 * mber_bdwgh_goal : 목표체중
 * actqy : 활동량(1/2/3)
 * smoking_yn : 흡연여부
 * job_yn : 직업유무
 * NOTICE_YN : 공지알림
 * HEALTH_YN : 건강알림
 * HEART_YN : 좋아요알림
 * REPLY_YN : 답글알림
 * DAILY_YN : 일일미션알림
 * CM_YN : 커뮤니티알림
 * mber_grad : 회원등급(10:정회원 / 20:준회원)
 * result_code 코드
 * <p>
 * 0000 성공 / 4444 실패
 */

public class Tr_mber_load_mypage extends BaseData {
	private final String TAG = Tr_mber_load_mypage.class.getSimpleName();

	public static class RequestData {
		public String mber_sn; //회원 일련번호
	}

	public Tr_mber_load_mypage() {
		super.conn_url = "http://m.shealthcare.co.kr/HL_MED/WebService/MED_MOBILE_CALL.asmx/MED_mobile_Call";
	}

	@Override
	public JSONObject makeJson(Object obj) throws JSONException {
		if (obj instanceof RequestData) {
			JSONObject body = getBaseJsonObj();

			RequestData data = (RequestData) obj;
			body.put("api_code", getApiCode(TAG));
			body.put("insures_code", INSURES_CODE);
			body.put("mber_sn", data.mber_sn);
			body.put("device_kind", "A");

			return body;
		}

		return super.makeJson(obj);
	}

	/**************************************************************************************************/
	/***********************************************RECEIVE********************************************/
	/**************************************************************************************************/

	@Expose
	@SerializedName("result_code")
	public String result_code;
	@Expose
	@SerializedName("mber_grad")
	public String mber_grad;
	@Expose
	@SerializedName("CM_YN")
	public String CM_YN;
	@Expose
	@SerializedName("DAILY_YN")
	public String DAILY_YN;
	@Expose
	@SerializedName("REPLY_YN")
	public String REPLY_YN;
	@Expose
	@SerializedName("HEART_YN")
	public String HEART_YN;
	@Expose
	@SerializedName("HEALTH_YN")
	public String HEALTH_YN;
	@Expose
	@SerializedName("NOTICE_YN")
	public String NOTICE_YN;
	@Expose
	@SerializedName("job_yn")
	public String job_yn;
	@Expose
	@SerializedName("smoking_yn")
	public String smoking_yn;
	@Expose
	@SerializedName("actqy")
	public String actqy;
	@Expose
	@SerializedName("mber_bdwgh_goal")
	public String mber_bdwgh_goal;
	@Expose
	@SerializedName("mber_bdwgh")
	public String mber_bdwgh;
	@Expose
	@SerializedName("mber_height")
	public String mber_height;
	@Expose
	@SerializedName("POINT_TOTAL_AMT")
	public String POINT_TOTAL_AMT;
	@Expose
	@SerializedName("disease_nm")
	public String disease_nm;
	@Expose
	@SerializedName("disease_txt")
	public String disease_txt;
	@Expose
	@SerializedName("DISEASE_OPEN")
	public String DISEASE_OPEN;
	@Expose
	@SerializedName("profile_pic")
	public String profile_pic;
	@Expose
	@SerializedName("MBER_DE")
	public String MBER_DE;
	@Expose
	@SerializedName("age")
	public String age;
	@Expose
	@SerializedName("MBER_LIFYEA")
	public String MBER_LIFYEA;
	@Expose
	@SerializedName("NICKNAME")
	public String NICKNAME;
	@Expose
	@SerializedName("MBER_SEX")
	public String MBER_SEX;
	@Expose
	@SerializedName("MBER_NM")
	public String MBER_NM;
	@Expose
	@SerializedName("mber_sn")
	public String mber_sn;
	@Expose
	@SerializedName("insures_code")
	public String insures_code;
	@Expose
	@SerializedName("api_code")
	public String api_code;
	@Expose
	@SerializedName("app_ver")
	public String app_ver;
	@Expose
	@SerializedName("accml_sum_amt")
	public String accml_sum_amt;
}
