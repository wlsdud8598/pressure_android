package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 회원정보변경
 * input 값
 * insures_code : 회사코드
 * mber_sn : 회원key
 * mber_name : 회원명
 * mber_lifyea : 생년월일
 * mberhp : 회원전화번호
 * mber_sex : 성별
 * mber_height : 신장
 * mber_bdwgh : 체중
 * actqy : 활동량(1/2/3)
 * disease_nm : 병명
 * medicine_yn : 투약여부
 * smkng_yn : 흡연여부
 * mber_job : 직업유무
 * <p>
 * output값
 * api_code : 호출코드명
 * insures_code : 회사코드
 * result_code : 결과코드 0000 성공/4444 실패
 * <p>
 * 0000 성공 / 4444 실패
 */

public class Tr_mber_modify_mberinfo extends BaseData {
	private final String TAG = Tr_mber_modify_mberinfo.class.getSimpleName();

	public static class RequestData {
		public String mber_sn; //회원 일련번호
		public String mber_name;
		public String mber_lifyea;
		public String mberhp;
		public String mber_sex;
		public String mber_height;
		public String mber_bdwgh;
		public String actqy;
		public String disease_nm;
		public String disease_txt;
		public String medicine_yn;
		public String smkng_yn;
		public String mber_job;
	}

	public Tr_mber_modify_mberinfo() {
		super.conn_url = "http://m.shealthcare.co.kr/HL_MED/WebService/MED_MOBILE_CALL.asmx/MED_mobile_Call";
	}

	@Override
	public JSONObject makeJson(Object obj) throws JSONException {
		if (obj instanceof RequestData) {
			JSONObject body = getBaseJsonObj();
			RequestData data = (RequestData) obj;
			body.put("api_code", getApiCode(TAG));
			body.put("insures_code", INSURES_CODE);
			body.put("mber_sn", data.mber_sn);
			body.put("mber_name", data.mber_name);
			body.put("mber_lifyea", data.mber_lifyea);
			body.put("mberhp", data.mberhp);
			body.put("mber_sex", data.mber_sex);
			body.put("mber_height", data.mber_height);
			body.put("mber_bdwgh", data.mber_bdwgh);
			body.put("actqy", data.actqy);
			body.put("disease_nm", data.disease_nm);
			body.put("disease_txt", data.disease_txt);
			body.put("medicine_yn", data.medicine_yn);
			body.put("smkng_yn", data.smkng_yn);
			body.put("mber_job", data.mber_job);
			return body;
		}

		return super.makeJson(obj);
	}

	/**************************************************************************************************/
	/***********************************************RECEIVE********************************************/
	/**************************************************************************************************/

	@Expose
	@SerializedName("result_code")
	public String result_code;
	@Expose
	@SerializedName("insures_code")
	public String insures_code;
	@Expose
	@SerializedName("api_code")
	public String api_code;
}
