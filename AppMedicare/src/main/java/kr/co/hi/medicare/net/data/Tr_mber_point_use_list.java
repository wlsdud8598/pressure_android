package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import kr.co.hi.medicare.utilhw.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 *
 포이트 내역 리스트
 내정보에서 푸시여부설정

 //페이징 되어야 함.


 input 값
 insures_code : 회사코드
 mber_sn : 회원key
 start_de : 시작일
 end_de : 종료일
 pageNumber : 페이지no

 output값
 api_code : 호출코드명
 pageNumber :
 maxpageNumber :
 point_usr_amt :
 point_user_sum_amt :
 point_day_list : 배열
 point_code : 포인트코드
 point_txt : 포인트명
 accml_amt : 획득포인트
 accml_sum_amt : 누적포인트
 remain_point_amt : 잔여(가용)포인트
 input_de : 입력일
 */

public class Tr_mber_point_use_list extends BaseData {
    private final String TAG = Tr_mber_point_use_list.class.getSimpleName();

    public static class RequestData {

        public String mber_sn;
        public String start_de;
        public String end_de;
        public String pageNumber;

    }

    public Tr_mber_point_use_list() {
        super.conn_url = "http://m.shealthcare.co.kr/HL_MED/WebService/MED_MOBILE_CALL.asmx/MED_mobile_Call";
    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        if (obj instanceof Tr_mber_point_use_list.RequestData) {
            JSONObject body = new JSONObject();
            Tr_mber_point_use_list.RequestData data = (Tr_mber_point_use_list.RequestData) obj;

            body.put("api_code", getApiCode(TAG) );
            body.put("insures_code", INSURES_CODE);
            body.put("mber_sn", data.mber_sn);
            body.put("start_de", data.start_de);
            body.put("end_de", data.end_de);
            body.put("pageNumber", data.pageNumber);

            return body;
        }

        return super.makeJson(obj);
    }

    public JSONArray getArray(Tr_get_hedctdata.DataList dataList) {
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        try {
            obj.put("idx" , dataList.idx ); //170410173713859",
            array.put(obj);
        } catch (JSONException e) {
            Logger.e(e);
        }

        return array;
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/
    @SerializedName("api_code")
    public String api_code; //
    @SerializedName("pageNumber")
    public String pageNumber; //
    @SerializedName("maxpageNumber")
    public String maxpageNumber; //
    @SerializedName("point_usr_amt")
    public String point_usr_amt = "0"; //
    @SerializedName("point_user_sum_amt")
    public String point_user_sum_amt = "0"; //

    @SerializedName("point_day_list")
    public List<pointDay> point_day_list = new ArrayList<>();

    public class pointDay {
        @SerializedName("point_code")
        public String point_code;
        @SerializedName("point_txt")
        public String point_txt;
        @SerializedName("accml_amt")
        public String accml_amt;
        @SerializedName("accml_sum_amt")
        public String accml_sum_amt;
        @SerializedName("remain_point_amt")
        public String remain_point_amt;
        @SerializedName("input_de")
        public String input_de;


    }
}
