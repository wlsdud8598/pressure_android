package kr.co.hi.medicare.net.data;

import android.content.Context;
import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import kr.co.hi.medicare.database.DBHelper;
import kr.co.hi.medicare.database.DBHelperLog;

/**
 * 회원검색 menu_log_hist
 * Input 값
 * "api_code":"menu_log_hist",
 *    "insures_code":"304",
 *    "mber_sn":"115232",
 *    "DATA_LENGTH":"2",
 *    "DATA":[
 * <p>
 * Output 값
 * 	"api_code": "menu_log_hist",
 * 	"insures_code": "304",
 * 	"mber_sn": "115232",
 * 	"result_code": "0000"
 */

public class Tr_menu_log_hist extends BaseData {


	public static class RequestData {
		public String mber_sn;
		public String DATA_LENGTH;
		public JSONArray DATA;
	}

	public Tr_menu_log_hist() {

	}

	public static JSONArray getArray(Context context) {
		DBHelper helper = new DBHelper(context);
		DBHelperLog db = helper.getLogDb();

		JSONArray array = new JSONArray();

		for (DBHelperLog.Data data : db.getlog()) {
			JSONObject obj = new JSONObject();
			try {
				if(data.s_cod.equals("")){
					obj.put("L_CODE",data.l_cod);
					obj.put("M_CODE",data.m_cod);
					obj.put("R_TIME",data.time);
					obj.put("R_COUNT",data.count);
					obj.put("R_DATE",data.regdate);

				}else if(data.m_cod.equals("") && data.s_cod.equals("")){
					obj.put("L_CODE",data.l_cod);
					obj.put("R_TIME",data.time);
					obj.put("R_COUNT",data.count);
					obj.put("R_DATE",data.regdate);
				}else{
					obj.put("L_CODE",data.l_cod);
					obj.put("M_CODE",data.m_cod);
					obj.put("S_CODE",data.s_cod);
					obj.put("R_TIME",data.time);
					obj.put("R_COUNT",data.count);
					obj.put("R_DATE",data.regdate);
				}


				array.put(obj);
			} catch (JSONException e) {
				Log.e("Tr_menu_log_hist_ERROR",e.toString());
			}
		}
		return array;
	}

	@Override
	public JSONObject makeJson(Object obj) throws JSONException {
		if (obj instanceof RequestData) {
			JSONObject body = getBaseJsonObj();

			RequestData data = (RequestData) obj;

			body.put("api_code", "menu_log_hist");
			body.put("insures_code", INSURES_CODE);
			body.put("mber_sn", data.mber_sn);
			body.put("DATA_LENGTH",data.DATA_LENGTH);
			body.put("DATA",data.DATA);

			return body;
		}

		return super.makeJson(obj);
	}

	/**************************************************************************************************/
	/***********************************************RECEIVE********************************************/
	/**************************************************************************************************/


	@Expose
	@SerializedName("api_code")
	public String api_code;
	@Expose
	@SerializedName("insures_code")
	public String insures_code;
	@Expose
	@SerializedName("mber_sn")
	public String mber_sn;
	@Expose
	@SerializedName("result_code")
	public String result_code;


}
