package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import kr.co.hi.medicare.utilhw.Logger;

/**
 다이어트프로그램 스케쥴
 Input 값 api_code : api 코드명 insures_code : 회사코드(108) mber_sn : 기존회원키값
 Output 값 api_code : api 코드명 insures_code : 회원사 코드 sch_day : 스케줄 일자(30일동안) sch_user : 사용자 수 sch_per : 고객만족도 schedule_day : 1일, 2일,3일 schedule_subject : 제목
 data_yn : 저장완료
 */


public class Tr_mvm_asstb_diet_program_schedule_list extends BaseData {
    private final String TAG = Tr_mvm_asstb_diet_program_schedule_list.class.getSimpleName();

    public static class RequestData {

        public String mber_sn; //12121212",
    }

    public Tr_mvm_asstb_diet_program_schedule_list() throws JSONException {

    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        Logger.i(TAG, "makeJson.obj=" + obj);
        if (obj instanceof Tr_mvm_asstb_diet_program_schedule_list.RequestData) {
            JSONObject body = new JSONObject();
            Tr_mvm_asstb_diet_program_schedule_list.RequestData data = (Tr_mvm_asstb_diet_program_schedule_list.RequestData) obj;
            body.put("api_code", getApiCode(TAG)); // bdsg_info_input_data
            body.put("mber_sn", data.mber_sn); //  1000
            body.put("insures_code", INSURES_CODE);

            return body;
        }

        return super.makeJson(obj);
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/


    @SerializedName("api_code")
    public String api_code; //
    @SerializedName("insures_code")
    public String insures_code; //

    @SerializedName("data_yn")
    public String data_yn;

    @SerializedName("sch_per")
    public String sch_per = "0";
    @SerializedName("sch_user")
    public String sch_user = "0";
    @SerializedName("sch_day")
    public String sch_day = "0";
    @SerializedName("sch_day_cnt")
    public String sch_day_cnt = "0";

    @SerializedName("schedule_list")
    public List<Schedule_list> schedule_list;
    public static class Schedule_list {
        @SerializedName("schedule_subject")
        public String schedule_subject;
        @SerializedName("schedule_day")
        public String schedule_day;
    }

}
