package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.utilhw.Logger;

/**
 *
  운동 리스트
 input값
 insures_code : 회사코드
 mber_sn : 회원고유키값
 api_code : api 키값
 pageNumber : 페이지 첫번째 페이지
 input_de : 해당날짜

 output 값
 api_code : 호출코드명
 pageNumber : 현제페이지
 maxpageNumber : 총페이지
 sport_list : 2차배열
 sport_sn : 저장된 고유키값(운동)
 sport_nm : 운동명
 active_seq : 운동관련된 운동종류 키값
 active_time : 시간당(09, 13) 등등 시간당  칼로리
 calory : 칼로리(시간당 칼로리)
 reg_de : 등록날짜
 */

public class Tr_mvm_info_mber_sport_list extends BaseData {
    private final String TAG = Tr_mvm_info_mber_sport_list.class.getSimpleName();

    public static class RequestData {

        public String mber_sn;
        public String pageNumber;
        public String input_de;
    }

    public Tr_mvm_info_mber_sport_list() {
    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        Logger.i(TAG, "makeJson.obj=" + obj);
        if (obj instanceof Tr_mvm_info_mber_sport_list.RequestData) {
            JSONObject body = new JSONObject();
            Tr_mvm_info_mber_sport_list.RequestData data = (Tr_mvm_info_mber_sport_list.RequestData) obj;

            body.put("api_code", getApiCode(TAG) );
            body.put("insures_code", INSURES_CODE);
            body.put("app_code", APP_CODE);
            body.put("mber_sn", data.mber_sn);
            body.put("pageNumber", data.pageNumber);
            body.put("input_de", data.input_de);

            return body;
        }

        return super.makeJson(obj);
    }

    public JSONArray getArray(Tr_get_hedctdata.DataList dataList) {
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        try {
            obj.put("idx" , dataList.idx ); //170410173713859",
            array.put(obj);
        } catch (JSONException e) {
            Logger.e(e);
        }

        return array;
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/
    @SerializedName("api_code")
    public String api_code; //
    @SerializedName("pageNumber")
    public String pageNumber; //
    @SerializedName("maxpageNumber")
    public String maxpageNumber; //
    @SerializedName("sport_list")
    public List<sport_list> sport_list = new ArrayList<>(); //

    public class sport_list {
        @SerializedName("sport_sn")
        public String sport_sn;
        @SerializedName("sport_nm")
        public String sport_nm;
        @SerializedName("active_seq")
        public String active_seq;
        @SerializedName("active_time")
        public String active_time;
        @SerializedName("active_de_stime")
        public String active_de_stime;
        @SerializedName("active_de_etime")
        public String active_de_etime;
        @SerializedName("calory")
        public String calory;
        @SerializedName("reg_de")
        public String reg_de;
    }

}
