package kr.co.hi.medicare.net.data;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import kr.co.hi.medicare.utilhw.Logger;

/**
 *
  운동 리스트 삭제
 input값
 insures_code : 회사코드
 mber_sn : 회원고유키값
 api_code : api 키값
 sport_sn : 저장된 고유키값(운동)
 active_seq : 운동 고유키값(달리기 고유키값)
 active_de : 운동 등록날짜

 output 값
 api_code : 호출코드명
 insures_code : 회사코드
 reg_yn : 삭제여부
 */

public class Tr_mvm_info_sport_del extends BaseData {
    private final String TAG = Tr_mvm_info_sport_del.class.getSimpleName();

    public static class RequestData {

        public String mber_sn;
        public String sport_sn;
        public String active_seq;
        public String active_de;

    }

    public Tr_mvm_info_sport_del() {
    }

    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        Logger.i(TAG, "makeJson.obj=" + obj);
        if (obj instanceof Tr_mvm_info_sport_del.RequestData) {
            JSONObject body = new JSONObject();
            Tr_mvm_info_sport_del.RequestData data = (Tr_mvm_info_sport_del.RequestData) obj;

            body.put("api_code", getApiCode(TAG) );
            body.put("insures_code", INSURES_CODE);
            body.put("mber_sn", data.mber_sn);
            body.put("sport_sn", data.sport_sn);
            body.put("active_seq", data.active_seq);
            body.put("active_de", data.active_de);

            return body;
        }

        return super.makeJson(obj);
    }

    public JSONArray getArray(Tr_get_hedctdata.DataList dataList) {
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        try {
            obj.put("idx" , dataList.idx ); //170410173713859",
            array.put(obj);
        } catch (JSONException e) {
            Logger.e(e);
        }

        return array;
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/
    @SerializedName("api_code")
    public String api_code; //
    @SerializedName("insures_code")
    public String insures_code; //
    @SerializedName("reg_yn")
    public String reg_yn; //

}
