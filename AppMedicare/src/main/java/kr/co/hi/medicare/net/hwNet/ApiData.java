package kr.co.hi.medicare.net.hwNet;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Constructor;

import kr.co.hi.medicare.Defined;
import kr.co.hi.medicare.utilhw.JsonLogPrint;
import kr.co.hi.medicare.utilhw.Logger;
import kr.co.hi.medicare.value.Define;

public class ApiData {
	private final String	TAG			= ApiData.class.getSimpleName();
	public static final int	TYPTE_NONE	= -1;

	private int				trMode		= -1;
	private String fingerNUrl;

	/**
	 * 통신하여 json 데이터 Class<?> 세팅
	 * @param cls
	 * @return
	 */

	public Object getData(Context context, Class<?> cls, Object obj) {
		JSONObject body = null;
		IBaseData dataCls = null;
		try {
			Class<?> cl = Class.forName(cls.getName());
			Constructor<?> co = cl.getConstructor();
			dataCls = (BaseData) co.newInstance();

			body = dataCls.makeJson(obj);

		} catch (Exception e) {
            try {
                Class<?> cl = Class.forName(cls.getName());
                Constructor<?> co = cl.getConstructor(Context.class);
                dataCls = (BaseData) co.newInstance(context);

                body = dataCls.makeJson(obj);
            } catch (Exception e1) {
                e1.printStackTrace();
                Log.e(TAG, "ApiData Class 생성 실패", e);
            }
		}

		if (dataCls instanceof BaseData) {
		    BaseData baseData = (BaseData) dataCls;
			fingerNUrl = baseData.conn_url;
		    Logger.d(TAG, "baseData.conn_url="+baseData.conn_url);
        }

        String url = Defined.BASE_HTTPS_URL;

        if (Define.getInstance().getInformation() != null) {
            // 로드벨런싱 후 Url
            if (TextUtils.isEmpty(Define.getInstance().getInformation().apiURL))
                url = Define.getInstance().getInformation().apiURL;
        }

        Logger.i(TAG, "ApiData.url="+url);
        ConnectionUtil connectionUtil = new ConnectionUtil(url);

        final String trName = cls.getClass().getSimpleName();
		String result = null;
		if (body != null) {
			if(fingerNUrl.contains("https://app.fingern.co.kr:1334/gcross/chat/channel")){
				result = connectionUtil.doConnection(body, trName, fingerNUrl);
			}else{
				result = connectionUtil.doConnection(body, trName);
			}

        }


		if (result.startsWith("{") == false) {
			result = result.substring(result.indexOf("{"));
		}

		if (result != null)
			result = result.substring(0, result.lastIndexOf("}")+1);

		Log.i(TAG, "ApiData.result="+result);


		if (TextUtils.isEmpty(result)) {
			Logger.e(TAG, "getData.result="+result);
		} else {
			Logger.i(TAG, "####################### API RESULT."+trName+" #####################");
			JsonLogPrint.printJson(trName, result);
			Logger.i(TAG, "####################### API RESULT."+trName+" #####################");
		}

		Gson gson = new Gson();
		return gson.fromJson(result, dataCls.getClass());
	}

	public Object getData(BaseData baseData, Object obj) {
		JSONObject body = null;
//		IBaseData dataCls = null;

//		if (dataCls instanceof BaseData) {
//			BaseData baseData = (BaseData) dataCls;
			try {
				body = baseData.makeJson(obj);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			fingerNUrl = baseData.conn_url;
			Logger.d(TAG, "baseData.conn_url="+baseData.conn_url);
//		}


//		if (Define.getInstance().getInformation() != null) {
//			// 로드벨런싱 후 Url
//			if (TextUtils.isEmpty(Define.getInstance().getInformation().apiURL))
//				url = Define.getInstance().getInformation().apiURL;
//		}

		Logger.i(TAG, "ApiData.url="+baseData.conn_url);
		ConnectionUtil connectionUtil = new ConnectionUtil(baseData.conn_url);

        final String trName = baseData.getClass().getSimpleName();
		String result = null;
		if (body != null) {
			if(fingerNUrl.contains("https://app.fingern.co.kr:1334/gcross/chat/channel")){
				result = connectionUtil.doConnection(body, trName, fingerNUrl);
			}else{
				result = connectionUtil.doConnection(body, trName);
			}

		}

		if (TextUtils.isEmpty(result)) {
			Logger.e(TAG, "getData.result="+result);
		} else {
			Logger.i(TAG, "####################### API RESULT."+trName+" #####################");
			JsonLogPrint.printJson(trName, result);
			Logger.i(TAG, "####################### API RESULT."+trName+" #####################");
		}

		Gson gson = new Gson();
		return gson.fromJson(result, baseData.getClass());
	}



	/**
	 * 오픈 API용 커넥션 유틸
	 * @param context
	 * @return
	 */
//	public Object getAPIData(Context context, Class<? extends BaseData> cls, Object obj) {
	public Object getAPIData(Context context, BaseData dataCls) {
		String params = null;
//		BaseData dataCls = null;
//		try {
//			Class<?> cl = Class.forName(cls.getName());
//			Constructor<?> co = cl.getConstructor();
//			dataCls = (BaseData) co.newInstance();
//
////			if (obj != null)
////				body = dataCls.makeJson(obj);
//
//		} catch (Exception e) {
//			try {
//				Class<?> cl = Class.forName(cls.getName());
//				Constructor<?> co = cl.getConstructor(Context.class);
//				dataCls = (BaseData) co.newInstance(context);
//
////				if (obj != null)
////					body = dataCls.makeJson(obj);
//			} catch (Exception e1) {
//				e1.printStackTrace();
//				Log.e(TAG, "ApiData Class 생성 실패", e);
//			}
//		}

		String url = dataCls.getConnUrl();
        final String trName = dataCls.getClass().getSimpleName();

		Logger.i(TAG, "ApiData.url="+url);
		ConnectionAPIUtil connectionUtil = new ConnectionAPIUtil(url);

		String result ="";

		if(url.contains("https://app.fingern.co.kr:1334/gcross/chat/channel")){

			result = connectionUtil.doConnection(dataCls, params,url);
//			result = connectionUtil.doConnection(dataCls, params);
		}else{
			result = connectionUtil.doConnection(dataCls, params);
		}



		if (TextUtils.isEmpty(result)) {
			Logger.e(TAG, "getData.result="+result);
		} else {
			Logger.i(TAG, "####################### API RESULT."+trName+" #####################");
			JsonLogPrint.printJson(trName, result);
			Logger.i(TAG, "####################### API RESULT."+trName+" #####################");
		}

		Gson gson = new Gson();
//		if(url.contains("https://app.fingern.co.kr:1334/gcross/chat/channel")) {
        if(result.startsWith("[")) {
            // json 배열 처리
            return dataCls.gsonFromArrays(gson, result);
		}else{
            // json 단일 처리
			return gson.fromJson(result, dataCls.getClass());
		}
	}


	public interface IStep {
		void next(Object obj);
	}

    public interface IFailStep {
        void fail();
    }
}
