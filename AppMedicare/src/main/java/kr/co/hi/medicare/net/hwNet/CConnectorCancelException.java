/**
 * 2011. 12. 21. 오후 12:46:10
 * shinys
 */
package kr.co.hi.medicare.net.hwNet;

/**
 * @author shinys
 *
 */
public class CConnectorCancelException extends Exception {

	private static final long	serialVersionUID	= -4010738603632585307L;

	public CConnectorCancelException(String message) {
		super(message);
	}
}
