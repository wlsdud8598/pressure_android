package kr.co.hi.medicare.net.hwNet;

import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import kr.co.hi.medicare.Defined;
import kr.co.hi.medicare.utilhw.JsonLogPrint;
import kr.co.hi.medicare.utilhw.Logger;


public class ConnectionUtil {
    private final String TAG = ConnectionUtil.class.getSimpleName();

    private String mUrl;
    private StringBuffer paramSb = new StringBuffer();

    public ConnectionUtil() {
    }

    public ConnectionUtil(String url) {
        mUrl = url;
    }


    public String getParam() {
        String params = paramSb.toString();
        if ("".equals(params.trim()) == false)
            params = params.substring(0, params.length() - 1);

        return params;
    }


    public String doConnection(JSONObject body, String tr) {
        URL mURL;
        HttpURLConnection conn = null;
        int mIntResponse = 0;
        String result = "";
        try {
//            Tr_get_infomation info = Define.getInstance().getInformation();
//            if (info != null && TextUtils.isEmpty(info.apiURL) == false) {
//                mURL = new URL(info.apiURL);
//            } else {
//                mURL = new URL(Defined.BASE_HTTPS_URL);
//            }

            mURL = new URL(Defined.BASE_HTTPS_URL);

            conn = (HttpURLConnection) mURL.openConnection();
            conn.setConnectTimeout(3 * 1000);
            conn.setReadTimeout(3 * 1000);
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setRequestProperty("Accept-Charset", "EUC-KR");

            Log.i(TAG, "###############  ConnectionUtil." + tr + "  ###############");
            Log.i(TAG, "url=" + mURL);


            OutputStream os = conn.getOutputStream();
            os.write("json=".getBytes("EUC-KR"));
//            os.write(body.toString().getBytes("EUC-KR"));
            if (body != null) {
                String params = URLEncoder.encode(body.toString(), "EUC-KR");
                os.write(params.getBytes());

                JsonLogPrint.printJson(tr, body.toString());
                Logger.i(TAG, "encordingParams="+params);
            } else {
                Logger.e(TAG, "Param is Null");
            }

            Log.i(TAG, "###############  ConnectionUtil." + tr + "  ###############");
            os.flush();
            os.close();
            mIntResponse = conn.getResponseCode();
        } catch (Exception e) {
            Log.d("hsh", "getJSONData ex" + e);
            mIntResponse = 1000;
        }

        if (mIntResponse == HttpURLConnection.HTTP_OK) {
            BufferedReader br;
            try {
                br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                String line;
                StringBuffer sb = new StringBuffer();
                while ((line = br.readLine()) != null) {
                    sb.append(line+ "\n");
                }

                br.close();

                result = sb.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
        }
        // 불필요 부분 제거
//        result = result.replace("<?xml version=\"1.0\" encoding=\"utf-8\"?><string xmlns=\"http://tempuri.org/\">", "").replace("</string>", "");

        Logger.i(TAG, "doConnection.result=" + result);
        return result;
    }

    public String doConnection(JSONObject body, String tr,String fingernUrl) {
        URL mURL;
        StringBuilder sb = new StringBuilder();
        HttpURLConnection urlConnection = null;
        int mIntResponse = 0;
        String result = "";
        try {

            URL url = new URL(fingernUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(3 * 1000);
            urlConnection.setReadTimeout(3 * 1000);
            urlConnection.connect();
            //You Can also Create JSONObject here
            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
            out.write(body.toString());// here i sent the parameter
            out.close();
            int HttpResult = urlConnection.getResponseCode();
            Log.i(TAG,  "HttpResult="+HttpResult);
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                Log.e("new Test", "" + sb.toString());
                return sb.toString();
            } else {
                Log.e(" ", "" + urlConnection.getResponseMessage());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
//        } catch (JSONException e) {
//            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return null;
    }

//    public static synchronized String encodeURI(String s) {
//        String[] findList = {"#", "+", "&", "%", " "};
//        String[] replList = {"%23", "%2B", "%26", "%25", "%20"};
//        return StringUtils.replaceEach(s, findList, replList);
//    }

}
