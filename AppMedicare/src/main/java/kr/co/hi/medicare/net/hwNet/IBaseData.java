package kr.co.hi.medicare.net.hwNet;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mrsohn on 2017. 3. 30..
 */

public interface IBaseData {

    JSONObject makeJson(Object obj) throws JSONException;
    String getConnUrl();
}
