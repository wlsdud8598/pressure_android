package kr.co.hi.medicare.net.hwNet.openApi;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import java.util.ArrayList;
import java.util.List;

/**
 *
 측정소별 실시간 측정정보 조회( 미세먼지 데이터 가져오기)

 */

public class Tr_getMsrstnAcctoRltmMesureDnsty extends BaseData {
    public static class RequestData {

//        public String mber_sn;
//        public String misson_typ;
//        public String pageNumber;

    }

    public Tr_getMsrstnAcctoRltmMesureDnsty(String stationName) {
        super.conn_url = "http://openapi.airkorea.or.kr/openapi/services/rest/ArpltnInforInqireSvc/getMsrstnAcctoRltmMesureDnsty?"
                + "stationName="+stationName
                + "&dataTerm=DAILY"
                + "&pageNo=1"
                + "&numOfRows=3"
                + "&ServiceKey=6X8hEZQCvsn5VtIdt814GOBzVrIfBHxoPXgdI9qzhu2qZSSr2I5bVeDgngLHZf%2BNexDN59smKq7uCs66QOQYNg%3D%3D"
                + "&ver=1.3"
                + "&_returnType=json";
    }

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("totalCount")
    public int totalCount;
    @SerializedName("list")
    public List<VoList> list = new ArrayList<>();
    public class VoList {
        @SerializedName("dataTime")
        public String dataTime;
        @SerializedName("mangName")
        public String mangName;
        @SerializedName("so2Value")
        public String so2Value;
        @SerializedName("coValue")
        public String coValue;
        @SerializedName("o3Value")
        public String o3Value;
        @SerializedName("no2Value")
        public String no2Value;
        @SerializedName("pm10Value")
        public String pm10Value;
        @SerializedName("pm10Value24")
        public String pm10Value24;
        @SerializedName("pm25Value")
        public String pm25Value;
        @SerializedName("pm25Value24")
        public String pm25Value24;
        @SerializedName("khaiValue")
        public String khaiValue;
        @SerializedName("khaiGrade")
        public String khaiGrade;
        @SerializedName("so2Grade")
        public String so2Grade;
        @SerializedName("coGrade")
        public String coGrade;
        @SerializedName("o3Grade")
        public String o3Grade;
        @SerializedName("no2Grade")
        public String no2Grade;
        @SerializedName("pm10Grade")
        public String pm10Grade;
        @SerializedName("pm25Grade")
        public String pm25Grade;
        @SerializedName("pm10Grade1h")
        public String pm10Grade1h;
        @SerializedName("pm25Grade1h")
        public String pm25Grade1h;
    }

}
