package kr.co.hi.medicare.net.hwNet.openApi;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.fragment.home.dust.GeoPoint;
import kr.co.hi.medicare.fragment.home.dust.GeoTrans;
import kr.co.hi.medicare.net.hwNet.BaseData;

import java.util.ArrayList;
import java.util.List;

/**
 * 미세먼지 측정소 리스트 가져오기
 */

public class Tr_getNearbyMsrstnList extends BaseData {
    private final String TAG = Tr_getNearbyMsrstnList.class.getSimpleName();

    public Tr_getNearbyMsrstnList(double x, double y) {

        GeoPoint in_pt = new GeoPoint(x, y);
        System.out.println("geo in : xGeo="  + in_pt.getX() + ", yGeo=" + in_pt.getY());
        GeoPoint tm_pt = GeoTrans.convert(GeoTrans.GEO, GeoTrans.TM, in_pt);
        System.out.println("tm : xTM=" + tm_pt.getX() + ", yTM=" + tm_pt.getY());
//        GeoPoint katec_pt = GeoTrans.convert(GeoTrans.TM, GeoTrans.KATEC, tm_pt);
//        System.out.println("katec : xKATEC=" + katec_pt.getX() + ", yKATEC=" + katec_pt.getY());
//        GeoPoint out_pt = GeoTrans.convert(GeoTrans.KATEC, GeoTrans.GEO, katec_pt);
//        System.out.println("geo out : xGeo=" + out_pt.getX() + ", yGeo=" + out_pt.getY());
//        GeoPoint in2_pt = new GeoPoint(latitude, latitude);
//        System.out.println("geo distance between (127,38) and (128,38) =" + GeoTrans.getDistancebyGeo(in_pt, in2_pt) + "km");


        super.conn_url = "http://openapi.airkorea.or.kr/openapi/services/rest/MsrstnInfoInqireSvc/getNearbyMsrstnList?"
        + "serviceKey=6X8hEZQCvsn5VtIdt814GOBzVrIfBHxoPXgdI9qzhu2qZSSr2I5bVeDgngLHZf%2BNexDN59smKq7uCs66QOQYNg%3D%3D"
        + "&tmX="+tm_pt.getX()   //244148.546388"
        + "&tmY="+tm_pt.getY()    //412423.75772"
        + "&_returnType=json";
    }


    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/
    @SerializedName("list")
    public List<VoList> list = new ArrayList<>();

    public class VoList {
        @SerializedName("addr") //: "경기 이천시 창전동 영창로 163번길 28기타",
        public String addr;
        @SerializedName("stationName")// : "창전동",
        public String stationName;
    }
}
