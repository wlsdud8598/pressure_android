package kr.co.hi.medicare.net.hwdata;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.hi.medicare.UserInfo;

/**
 *
 FUNCTION NAME	login_add_aft	추가정보입력


 Input 값
 insures_code: 회사코드
 token: 정회원/준회원구분
 mber_sn: 회원key
 mber_height: 신장
 mber_bdwgh: 체중
 mber_bdwgh_goal: 목표체중
 mber_actqy: 활동량
 disease_nm: 질환정보
 smkng_yn: 흡연여부
 sugar_typ: 당뇨여부
 sugar_occur_de: 당뇨발생년도
 job_yn: 직업여부
 mber_id: 회원id

 Output 값
 api_code: 호출코드명
 insures_code: 회사코드
 point_alert_yn: 포인트알림여부
 point_alert_am	: 포인트적립여부
 reg_yn: 기본정보등록여부 (Y/N)
*/

public class Tr_login_add_aft extends BaseData {
	private final String TAG = Tr_login_add_aft.class.getSimpleName();

	public Tr_login_add_aft(Context context) {
	    super.mContext = context;
    }


	public static class RequestData {
//        public String api_code;      //login_add_aft"
//        public String insures_code;      //304"
//        public String token;      //deviceTo123123ke"
//        public String mber_sn;
        public String mber_height;
        public String mber_bdwgh;
        public String mber_bdwgh_goal;

        public String mber_actqy;
        public String disease_nm;
        public String disease_txt;
        public String smkng_yn;
//        public String sugar_typ;
//        public String sugar_occur_de;
        public String job_yn;
        public String mbar_id;
        
	}

	@Override
	public JSONObject makeJson(Object obj) throws JSONException {
		if (obj instanceof RequestData) {
			JSONObject body = new JSONObject();
			RequestData data = (RequestData) obj;
			Tr_login userInfo = UserInfo.getLoginInfo();
//            UserInfo user = new UserInfo(mContext);

			body.put("api_code", "login_add_aft");
			body.put("insures_code", INSURES_CODE);
            body.put("token", DEVICE_TOKEN);
            body.put("mber_sn", userInfo.mber_sn);//   //1918"

            body.put("mber_height", data.mber_height);//   //182"
            body.put("mber_bdwgh", data.mber_bdwgh);//   //79"
            body.put("mber_bdwgh_goal", data.mber_bdwgh_goal);//   //65"
            body.put("mber_actqy", data.mber_actqy);//   //1"
            body.put("disease_nm", data.disease_nm);//   //1,2,3,"
            body.put("disease_txt", data.disease_txt);//   //1,2,3,"
            body.put("smkng_yn", data.smkng_yn);//   //Y"
            body.put("sugar_typ", "");//   //1"
            body.put("sugar_occur_de", "");//   //2015"
            body.put("job_yn", data.job_yn);//   //Y"
            body.put("mbar_id", data.mbar_id);//   //moon2@theinsystems.com"

			return body;
		}

		return super.makeJson(obj);
	}

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("api_code")        //		api 코드명 string
	public String api_code;        //		api 코드명 string
	@SerializedName("insures_code")//		회원사 코드
	public String insures_code;    //		회원사 코드
	@SerializedName("mber_sn")    // 회원정보
	public String mber_sn;
	@SerializedName("point_alert_yn") // 최초 포인트 지급여부(Y 지급 , N 지급함)
	public String point_alert_yn;
	@SerializedName("point_alert_amt") // 지급 포인트
	public String point_alert_amt;
	@SerializedName("reg_yn") // 성공여부
	public String reg_yn;
}
