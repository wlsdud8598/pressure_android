package kr.co.hi.medicare.net.hwdata;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;


import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 FUNCTION NAME	login_pwd	비밀번호찾기

 Input 값
 insures_code: 회사코드
 token: 정회원/준회원구분
 mber_hp: 휴대폰번호
 mber_id: 회원id

 Output 값
 api_code: 호출코드명
 insures_code: 회사코드
 result_code: 결과코드 (0000: 일치 / 4444: 불일치)
 send_mail_yn: 메일발송 성공여부 Y/N

 */

public class Tr_login_pwd extends BaseData {
	private final String TAG = Tr_login_pwd.class.getSimpleName();


	public static class RequestData {
		public String mber_hp;
		public String mber_id;
		public String sms_snd_hp;  //받는 핸드폰번호
	}

	public Tr_login_pwd() throws JSONException {

	}

	@Override
	public JSONObject makeJson(Object obj) throws JSONException {
		if (obj instanceof RequestData) {
			JSONObject body = new JSONObject();//getBaseJsonObj("login_id");

			RequestData data = (RequestData) obj;
            body.put("api_code", "login_pwd");
            body.put("insures_code", INSURES_CODE);
            body.put("token", DEVICE_TOKEN);
            body.put("mber_hp", data.mber_hp);
            body.put("mber_id", data.mber_id);
			body.put("sms_snd_hp", data.sms_snd_hp);

			return body;
		}

		return super.makeJson(obj);
	}

    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

	@SerializedName("api_code")        //		api 코드명 string
	public String api_code;        //		api 코드명 string
	@SerializedName("insures_code")//		회원사 코드
	public String insures_code;    //		회원사 코드
	@SerializedName("send_mail_yn")
	public String send_mail_yn;
	@SerializedName("result_code")
	public String result_code;

}
