package kr.co.hi.medicare.net.hwdata;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import kr.co.hi.medicare.net.hwNet.BaseData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kr.co.hi.medicare.utilhw.Logger;

/**
 *
 * 메인페이지 (미션,키워드, 건강정보, 퀴즈, 알림, 콜센터번호 등..)
 *
 * input값
 * insures_code : 회사코드
 * mber_sn : 회원키값
 * input_de : 호출 하는 날짜(현재날짜)
 *
 * output 값
 * api_code : 호출코드명
 * insures_code : 회사도크
 * mber_sn : 회원번호
 * day_health_amt : 오늘의 건강점수
 * user_point_amt : 오늘의 내포인트
 * total_accml_amt : 누적포인트
 * health_view_de : 건강정보 제일 최신월일시간
 *
 * misson_goal_alert_yn : 로그인 미션 알림 여부( y, n)
 * misson_goal_point : 로그인시 미션 포인트 금액( 지금은 1 포인트가 넘어간다)
 *
 * mission_point_list : (포인트 관련 2차 배열)
 * point_code : 포인트 코드
 * point_nm : 포인트명
 * point_max_day_amt : 하루 최고 포인트
 * accml_amt : 획득한 포인트
 * mission_yn : 성공여부
 *
 * top_age_group : 회원이 속한 그룹( 30대)
 * top_age_sex : 회원이 속한 성별
 * keyword_list : 키워드 2차배열
 * keyword_sn : 키워드 고유키값
 * keyword_nm : 1위
 * keyword_txt : 키워드문구
 *
 * quiz_day : 일일퀴즈
 * quiz_sn : 퀴즈 일련번호
 * quiz_question : 퀴즈 문제
 * quiz_explain : 퀴즈 답변
 * quiz_replay : 퀴즈 답변 (o, x)   1(O) , 0(X)
 *
 * health_list : 건강정보
 * health_sn : 건강정보 고유키값
 * health_nm : 건강정보 제목
 * health_txt : 건강정보 txt
 *
 * call_list : 콜정보
 * call_typ : 콜 typ
 * call_nm : 콜센터이름
 * call_hp : 콜센터 번호
 *
 * day_tip_list : 팁 정보
 * tip_typ : 팁 타입
 * tip_nm : 팁 내용
 *
 * alimi_list : 알리미 정보
 * alimi_yn : 알리미 여부
 * alimi_subject : 알리미 제목
 * alimi_os : 알리미 os( 안드로이드, 아이폰)
 * notice_sn : 알리미 고유키값 ( 혹시 상세페이지 넘어갈때 대비하여)
 */

public class Tr_mber_main_call extends BaseData {
    private final String TAG = Tr_mber_main_call.class.getSimpleName();

    public static class RequestData {
        public String mber_sn;
        public String input_de;

    }


    @Override
    public JSONObject makeJson(Object obj) throws JSONException {
        Logger.i(TAG, "makeJson.obj=" + obj);
        if (obj instanceof RequestData) {
            JSONObject body = new JSONObject();
            RequestData data = (RequestData) obj;

            body.put("api_code", getApiCode(TAG)); //
            body.put("insures_code", INSURES_CODE);
            body.put("mber_sn", data.mber_sn); //  1000
            body.put("input_de", data.input_de); //

            return body;
        }

        return super.makeJson(obj);
    }


    /**************************************************************************************************/
    /***********************************************RECEIVE********************************************/
    /**************************************************************************************************/

    @SerializedName("api_code") // mber_main_call",
    public String api_code; //
    @SerializedName("insures_code") // 303",
    public String insures_code; //
    @SerializedName("mber_sn") // 1344",
    public String mber_sn; //
    @SerializedName("data_yn") // Y"
    public String data_yn; //

    @SerializedName("health_view_de")
    public String health_view_de;
    @SerializedName("total_accml_amt")
    public String total_accml_amt;
    @SerializedName("user_point_amt")
    public String user_point_amt;
    @SerializedName("day_health_amt")
    public String day_health_amt;

    @SerializedName("misson_goal_alert_yn")
    public String misson_goal_alert_yn;
    @SerializedName("misson_goal_point")
    public String misson_goal_point;

    @SerializedName("top_age_group")
    public String top_age_group;
    @SerializedName("top_age_sex")
    public String top_age_sex;


    @SerializedName("keyword_sn")
    public String keyword_sn;
    @SerializedName("keyword_nm")
    public String keyword_nm;
    @SerializedName("keyword_txt")
    public String keyword_txt;


    @SerializedName("quiz_day")
    public List<Quiz_day> quiz_day;
    @SerializedName("keyword_list")
    public List<Tr_mber_main_call.keyword_list> keyword_list;
    @SerializedName("mission_point_list")
    public ArrayList<Mission_point_list> mission_point_list;
    @SerializedName("keyword_info_woman_list")
    public List<Keyword_info_woman_list> keyword_info_woman_list;
    @SerializedName("keyword_info_man_list")
    public List<Keyword_info_man_list> keyword_info_man_list;
    @SerializedName("day_tip_list")
    public List<Day_tip_list> day_tip_list;
    @SerializedName("health_call_list")
    public List<Health_call_list> health_call_list;
    @SerializedName("health_info_list")
    public List<Health_info_list> health_info_list;
    @SerializedName("alimi_list")
    public List<Alimi_list> alimi_list;

    public class Day_tip_list {
        @SerializedName("tip_nm")
        public String tip_nm;
        @SerializedName("tip_typ")
        public String tip_typ;
    }

    public class keyword_list {
        @SerializedName("top_age_group")
        public String top_age_group;
        @SerializedName("top_age_sex")
        public String top_age_sex;
        @SerializedName("keyword_sn")
        public String keyword_sn;
        @SerializedName("keyword_nm")
        public String keyword_nm;
        @SerializedName("keyword_txt")
        public String keyword_txt;
    }

    public class Health_call_list {
        @SerializedName("call_hp")
        public String call_hp;
        @SerializedName("call_nm")
        public String call_nm;
        @SerializedName("call_typ")
        public String call_typ;
    }

    public class Health_info_list {
        @SerializedName("health_txt")
        public String health_txt;
        @SerializedName("health_nm")
        public String health_nm;
        @SerializedName("health_sn")
        public String health_sn;
    }

    public class Quiz_day  {
        @SerializedName("quiz_replay")
        public String quiz_replay;
        @SerializedName("quiz_explain")
        public String quiz_explain;
        @SerializedName("quiz_question")
        public String quiz_question;
        @SerializedName("quiz_sn")
        public String quiz_sn;
    }

    public class Alimi_list {
        @SerializedName("alimi_yn")
        public String alimi_yn;
        @SerializedName("alimi_subject")
        public String alimi_subject;
        @SerializedName("alimi_os")
        public String alimi_os;
        @SerializedName("notice_sn")
        public String notice_sn;
    }

    public static class Mission_point_list implements Parcelable {
        @SerializedName("mission_yn")
        public String mission_yn;
        @SerializedName("accml_amt")
        public String accml_amt;
        @SerializedName("point_max_day_amt")
        public String point_max_day_amt;
        @SerializedName("point_nm")
        public String point_nm;
        @SerializedName("point_code")
        public String point_code;

        protected Mission_point_list(Parcel in) {
            mission_yn = in.readString();
            accml_amt = in.readString();
            point_max_day_amt = in.readString();
            point_nm = in.readString();
            point_code = in.readString();
        }

        public static final Creator<Mission_point_list> CREATOR = new Creator<Mission_point_list>() {
            @Override
            public Mission_point_list createFromParcel(Parcel in) {
                return new Mission_point_list(in);
            }

            @Override
            public Mission_point_list[] newArray(int size) {
                return new Mission_point_list[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mission_yn);
            dest.writeString(accml_amt);
            dest.writeString(point_max_day_amt);
            dest.writeString(point_nm);
            dest.writeString(point_code);
        }
    }


    public class Keyword_info_woman_list implements Parcelable {
        @SerializedName("keyword_txt")
        public String keyword_txt;
        @SerializedName("keyword_nm")
        public String keyword_nm;
        @SerializedName("keyword_sn")
        public String keyword_sn;
        @SerializedName("view_order")
        public String view_order;
        @SerializedName("age_sex")
        public String age_sex;
        @SerializedName("age_group")
        public String age_group;

        protected Keyword_info_woman_list(Parcel in) {
            keyword_txt = in.readString();
            keyword_nm = in.readString();
            keyword_sn = in.readString();
            view_order = in.readString();
            age_sex = in.readString();
            age_group = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(keyword_txt);
            dest.writeString(keyword_nm);
            dest.writeString(keyword_sn);
            dest.writeString(view_order);
            dest.writeString(age_sex);
            dest.writeString(age_group);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public final Creator<Keyword_info_woman_list> CREATOR = new Creator<Keyword_info_woman_list>() {
            @Override
            public Keyword_info_woman_list createFromParcel(Parcel in) {
                return new Keyword_info_woman_list(in);
            }

            @Override
            public Keyword_info_woman_list[] newArray(int size) {
                return new Keyword_info_woman_list[size];
            }
        };
    }


    public class Keyword_info_man_list implements Parcelable {
        @SerializedName("keyword_txt")
        public String keyword_txt;
        @SerializedName("keyword_nm")
        public String keyword_nm;
        @SerializedName("keyword_sn")
        public String keyword_sn;
        @SerializedName("view_order")
        public String view_order;
        @SerializedName("age_sex")
        public String age_sex;
        @SerializedName("age_group")
        public String age_group;

        protected Keyword_info_man_list(Parcel in) {
            keyword_txt = in.readString();
            keyword_nm = in.readString();
            keyword_sn = in.readString();
            view_order = in.readString();
            age_sex = in.readString();
            age_group = in.readString();
        }

        public final Creator<Keyword_info_man_list> CREATOR = new Creator<Keyword_info_man_list>() {
            @Override
            public Keyword_info_man_list createFromParcel(Parcel in) {
                return new Keyword_info_man_list(in);
            }

            @Override
            public Keyword_info_man_list[] newArray(int size) {
                return new Keyword_info_man_list[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(keyword_txt);
            dest.writeString(keyword_nm);
            dest.writeString(keyword_sn);
            dest.writeString(view_order);
            dest.writeString(age_sex);
            dest.writeString(age_group);
        }
    }

}
