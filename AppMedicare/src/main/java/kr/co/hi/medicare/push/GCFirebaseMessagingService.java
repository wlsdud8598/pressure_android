package kr.co.hi.medicare.push;

import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.component.PushUtils;
import kr.co.hi.medicare.utilhw.StringUtil;

/**
 *
 * 안드로이드
 I_BUFFER2="1"
 // 공지사항
 { '[식이] 침묵의 살인자 “고혈압” 예방을 위한 ,'TITLE':'건강칼럼' ,'I_BUFFER2':'1'}}"

 I_BUFFER2="2"
 // 새건강정보
 { '나도 모르게 생긴 탄수화물 중독증! "&SMS_CN&"','TITLE':'Monthly health' ,'I_BUFFER2':'2'}}"

 I_BUFFER2 ="3"
 // 게시글
 { '피로회복에 좋은 보양식 ‘주꾸미’,'TITLE':'이달의 건강식단!','I_BUFFER2':'3'}}"

 I_BUFFER2 ="4"
 // 게시글 댓글
 { '000님 지난3일 동안 혈당측정을 안하셨어요 ,'TITLE':'혈당측정해주세요~','I_BUFFER2':'4'}}"

 I_BUFFER2 ="5"
 // 일일미션
 { '어제의 일일 미션으로 33P 를 획득 하셨습니다. ,'TITLE':'일일미션','I_BUFFER2':'5'}}"

 I_BUFFER2 ="6"
 // 커뮤니티언급
 { '걷기왕 미션에 실패 하였습니다. 다음 번에는 꼭 성공하세요! "&SMS_CN&"','TITLE':'미션 실폐' ,'I_BUFFER2':'6'}}"

 I_BUFFER2 ="7"
 // 커뮤니티좋아요
 { '꾸준히 혈당 체크에 실패 하였습니다. 다음 번에는 꼭 성공하세요! "&SMS_CN&"','TITLE':'미션 실폐' ,'I_BUFFER2':'7'}}"


 타입 : 1(공지사항) 2(새건강정보) 3(게시글) 4(게시글 댓글), 5(일일미션) , 6(커뮤니티 언급), 7(커뮤니티 좋아요)
 HIST_SN : 상세페이지 고유키값
 1|0|공지사항
 2|0|새건강 정보
 3|0|게시글
 4|0|게시글 댓글
 5|0|일일미션
 6|0|커뮤니티 언급
 7|0|커뮤니티 좋아요


 <푸시 왔을때 터치 시 이동 페이지>
 1. 공지 - 메인 공지리스트
 2. 새건강정보 - 건강탭 열림
 7. 게시글좋아요 - 커뮤니티알림리스트
 4,6. 게시글댓글 - 커뮤니티알림리스트
 5. 일일미션달성 - 마이페이지 포인트내역


 결론 : 타입 : 1(공지사항) 2(새건강정보)  4(게시글 댓글), 5(일일미션), 6(커뮤니티 언급), 7(커뮤니티 좋아요)
 으로 정했습니다.


// 메디케어 curl FCM 보내기
 curl -X POST --header \
 "Authorization: key=AAAAlajrSKI:APA91bEqPolCJO3zfW1POso4NmOhEdowA4eECivxu_u0zhoFdiR7aaL4eBKn0sNoPpvqAoiQa6CP5g7xBxk3ik9rDEt7MT5CJCrM5hu6WMUOqjo8zAv_fJm276cUN47Sbx4No8tCBALO" \
 --header "Content-Type: application/json" https://fcm.googleapis.com/fcm/send -d \
 '{"to" : "c3tmzw3O1ys:APA91bGt8RGVTODcJPig4j4WwBeIePiX2KaJbYtOqiL1g5chNz4qpMZ0zn5iL8mhB9k_-5otuCd2gBkPdCP-w-mLgG842tdTWcSmMyevh5Q4v5Od-T-g695nSd6q8PvLZE8-5IePwNFl",
 "priority" : "high",
 "notification" : {    "body" : "7|0|좋아요",    "title" : "BG Title"  },
 "data" : {    "title" : "FG Title",    "message" : "7|0|좋아요"  }}'


 */

public class GCFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = GCFirebaseMessagingService.class.getSimpleName();
    // {idx=139, seq=1414, buffer1=, buffer2=, message= 1|0|건강칼럼|[식이] 침묵의 살인자 “고혈압” 예방을 위한}

    int iStatePush=0;

    public static final String PUSH_MENU_FRAG = "push_menu_frag";


    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        PushUtils.acquireWakeLock(this);
        Log.i(TAG, "From: " + remoteMessage.getFrom());
        Log.i(TAG, "onMessageReceived.remoteMessage="+remoteMessage);
        if (remoteMessage.getData() == null) {
            Log.e(TAG, "************ FCM 메시지가 없습니다.");
            return;
        }
        String message = remoteMessage.getData().get("message");
        if (TextUtils.isEmpty(message)) {
            Log.e(TAG, "onMessageReceived::"+remoteMessage.getData());
            Log.e(TAG, "onMessageReceived.message::"+message);
            return;
        }
        String[] msg = message.split("\\|");
        if (msg.length > 4 || msg.length < 3) {
            Log.e(TAG, "FCM msg arr length="+message);
            return;
        }
        Log.d(TAG, "FCM msg arr length="+message);
        FcmData data = null;
        if(msg.length == 3){
            data = new FcmData(msg[2], msg[1], msg[0]);
        } else if(msg.length == 4) {
            data = new FcmData(msg[3], msg[2], msg[1], msg[0]);
        }

        Log.i(TAG, remoteMessage.getData().toString());
//          XXX 푸시는 테스트 전이라 막아 놓음(2018.04.28)
          showPushMessage(data);
    }

    /**
     * 푸시 메시지를 전달 받으면 상태표시바에 표시함
     */
    public void showPushMessage(FcmData data) {
        if (data != null) {

            int notiId = (int)(System.currentTimeMillis()/1000) + (int) (Math.random() * 10000) + StringUtil.getIntVal(data.menuId);

            Log.i(TAG,"notiid : "+notiId);
            NotiDummyActivity.showPushMessage(this, notiId, getString(R.string.app_name), data.alert, data.menuId,data.url);
        }
    }

    public static class FcmData{
        public String url;
        public String alert;
        public String hist_sn;
        public String menuId;



        public FcmData(String url,String alert, String hist_sn, String menuId) {
            this.alert = alert;
            this.hist_sn = hist_sn;
            this.menuId = menuId;
            this.url = url;
        }

        public FcmData(String alert, String hist_sn, String menuId) {
            this.alert = alert;
            this.hist_sn = hist_sn;
            this.menuId = menuId;
        }
    }
}