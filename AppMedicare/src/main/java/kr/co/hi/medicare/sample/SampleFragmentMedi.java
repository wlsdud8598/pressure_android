package kr.co.hi.medicare.sample;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import kr.co.hi.medicare.R;
import kr.co.hi.medicare.UserInfo;
import kr.co.hi.medicare.activity.NewActivity;
import kr.co.hi.medicare.common.CommonToolBar;
import kr.co.hi.medicare.component.CDialog;
import kr.co.hi.medicare.fragment.BaseFragmentMedi;
import kr.co.hi.medicare.fragment.login.LoginFirstInfoFragment1_2;
import kr.co.hi.medicare.fragment.login.join.JoinStep1_2Fragment;
import kr.co.hi.medicare.fragment.login.join.JoinStep2_2FragmentMedi;
import kr.co.hi.medicare.fragment.login.join.JoinStepSimpleFragment;
import kr.co.hi.medicare.net.hwdata.Tr_login;
import kr.co.hi.medicare.activity.ServiceHistoryActivity;
import kr.co.hi.medicare.activity.ServiceHistoryChoiceActivity;
import kr.co.hi.medicare.activity.ServiceHistoryStrokeActivity;

/**
 * Created by MrsWin on 2017-03-01.
 */

public class SampleFragmentMedi extends BaseFragmentMedi {
    private final String TAG = SampleFragmentMedi.class.getSimpleName();
    public static String SAMPLE_BACK_DATA = "SAMPLE_BACK_DATA";

    public static Fragment newInstance() {
        SampleFragmentMedi fragment = new SampleFragmentMedi();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sample_layout_medi, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setScreenInfo(view);

        // 공통 툴바 커스터마이징 코드
        CommonToolBar toolBar = getToolBar(view);
        toolBar.setTitle("타이틀");
//        toolBar.setLeftIcon(R.drawable.arrow_left);

        // 플래그먼트 화면 이동
        view.findViewById(R.id.sample_new_activity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                replaceFragment(SampleFragmentMedi.newInstance(), bundle);
            }
        });

        // 새 액티비티 시작
        view.findViewById(R.id.sample_replace_fragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                NewActivity.startActivity(getActivity(), SampleFragmentMedi.class, bundle);
            }
        });
        // 1버튼 alert
        view.findViewById(R.id.sample_aleft_1_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CDialog.showDlg(getContext(), "1버튼 메시지")
                .setTitle("타이틀")
                .setIconView(R.drawable.login_m_ico_05);
            }
        });
        // 1버튼 alert
        view.findViewById(R.id.sample_aleft_1_full_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CDialog.showDlg(getContext(), "1버튼 메시지풀컬러 확인")
                        .setOneBtn(R.drawable.draw_alert_ok_btn); //단일버튼 색 변경
            }
        });
        // 2버튼 alert & 메시지 좌측 정렬
        view.findViewById(R.id.sample_aleft_2_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CDialog.showDlg(getContext(), "2버튼 메시지")
                        .setOkButton("확인", null)
                        .setNoButton("취소", null)
                        .setMessageGravity(Gravity.LEFT)
                        .setIconView(R.drawable.login_m_ico_05);        //아이콘 적용
            }
        });


        /**
         * 구글 피트니스 걸음 테스트
         */
        view.findViewById(R.id.google_fitness_noti_test).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(SampleFragmentGoogleFitness.newInstance());
            }
        });

        /**
         * 서비스 이력
         */
        view.findViewById(R.id.service_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                Tr_login login = UserInfo.getLoginInfo();
                boolean b_cancer = false, b_stroke = false;

                b_cancer = login.pm_yn.equals("Y")? true:false;
                b_stroke = login.st_yn.equals("Y")? true:false;

                if(b_cancer && b_stroke == false) // 암
                    intent = new Intent(getActivity(), ServiceHistoryActivity.class);
                else if(b_cancer == false && b_stroke) // 뇌줄중
                    intent = new Intent(getActivity(), ServiceHistoryStrokeActivity.class);
                else
                    intent = new Intent(getActivity(), ServiceHistoryChoiceActivity.class);
                startActivity(intent);

            }
        });


        view.findViewById(R.id.join_simple_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(JoinStepSimpleFragment.newInstance());
            }
        });
        view.findViewById(R.id.join_1_2_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(JoinStep1_2Fragment.newInstance());
            }
        });
        view.findViewById(R.id.join_2_2_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(JoinStep2_2FragmentMedi.newInstance());
            }
        });
        view.findViewById(R.id.join_firstinfo_1_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                replaceFragment(LoginFirstInfoFragment1_2.newInstance());
                NewActivity.startActivity(getActivity(), LoginFirstInfoFragment1_2.class, null);
            }
        });
        view.findViewById(R.id.join_firstinfo_2_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                replaceFragment(LoginFirstInfoFragment2_2.newInstance());
                NewActivity.startActivity(getActivity(), LoginFirstInfoFragment1_2.class, null);
            }
        });
    }

    private void setScreenInfo(View view) {
        TextView screenInfoTv = view.findViewById(R.id.sample_screen_info_tv);

        DisplayMetrics dm = new DisplayMetrics();
        WindowManager mgr = (WindowManager)getContext().getSystemService(Context.WINDOW_SERVICE);
        mgr.getDefaultDisplay().getMetrics(dm);

        Log.d("TAG", "densityDPI = " + dm.densityDpi);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int deviceWidth = displayMetrics.widthPixels;
        int deviceHeight = displayMetrics.heightPixels;
        // 꼭 넣어 주어야 한다. 이렇게 해야 displayMetrics가 세팅이 된다.
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int dipWidth = (int) (120 * displayMetrics.density);
        int dipHeight = (int) (90 * displayMetrics.density);
        Log.i(TAG,"displayMetrics.density : " + displayMetrics.density);
        Log.i(TAG,"deviceWidth : " + deviceWidth + ", deviceHeight : " + deviceHeight);


        screenInfoTv.setText("densityDpi:"+dm.densityDpi+"   ("+getString(R.string.device_dpi)+")"
                +"\ndensity="+dm.density
                +"\nxdpi="+dipWidth
                +"\nydpi="+dipHeight
                +"\nscaledDensity="+dm.scaledDensity
                +"\nwidthPixels="+deviceWidth
                +"\nheightPixels="+deviceHeight);

        Log.i(TAG, "xdpi=" + displayMetrics.xdpi);
        Log.i(TAG, "ydpi=" + displayMetrics.ydpi);


        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();

        display.getSize(size);

        Log.i(TAG, ">>> size.x : " + size.x + ", size.y : " + size.y);

    }

    @Override
    public void onResume() {
        super.onResume();
        // 이전 플래그먼트에서 데이터 받기
        Bundle bundle = getBackData();
        String backString = bundle.getString(SAMPLE_BACK_DATA);
        Log.i("", "backString=" + backString);
    }

    @Override
    public void onBackPressed() {
        super.finishStep();
    }



}
