package kr.co.hi.medicare.tempfunc;

import android.os.AsyncTask;

import java.util.List;

public class AsyncTaskCommon extends AsyncTask<String, String, Object> {

    private AbsCommonFunc callBack=null;

    public AsyncTaskCommon (AbsCommonFunc callBack){
        this.callBack = callBack;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if(callBack!=null){
            callBack.showDialog();
        }
    }

    @Override
    protected Object doInBackground(String... strings) {

        return callBack.data();
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);

        if(callBack!=null){
            callBack.closeDialog();
        }

        callBack.resultCallback(result);

    }

}
