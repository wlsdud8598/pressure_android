package kr.co.hi.medicare.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

/**
 * 다이얼로그.
 */
public class AlertDialogUtil {

    public static void onAlertDialog(Activity activity, String confirm, String message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setPositiveButton(confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();     //닫기
            }
        });
        alert.setMessage(message);
        alert.show();
    }

    public static void onAlertDialog(Activity activity, String confirm, String message, final DialogInterface.OnClickListener posListener) {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setPositiveButton(confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();     //닫기
                posListener.onClick(dialog, which);
            }
        });
        alert.setMessage(message);
        alert.show();
    }

    public static void onAlertDialog(final Activity activity, String confirm, String message, final boolean isFinsh) {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setPositiveButton(confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();     //닫기
                if (isFinsh) activity.finish();
            }
        });
        alert.setMessage(message);
        alert.show();
    }

    /**
     * 버튼 2개 종료 팝업
     * @param activity  액티비티
     * @param confirm   확인버튼
     * @param cancel    취소버튼
     * @param message   내용
     * @param isFinish  종료 유무 ( true - 화면종료, false - 화면종료 안함 )
     */
    public static void onAlertDialogTwoBtn(final Activity activity, String confirm, String cancel, String message, final boolean isFinish){
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setPositiveButton(confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(isFinish) {
                    activity.finish();
                }
                dialog.dismiss();
            }
        })
                .setNegativeButton(cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alert.setMessage(message);
        alert.show();
    }

    public static void onAlertDialogIntent(final Activity activity, String confirm, String message, final Intent intent, final boolean isFinsh) {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setPositiveButton(confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                activity.startActivity(intent);
                if(isFinsh)
                {
                    activity.finish();
                }
            }
        });
        alert.setMessage(message);
        alert.show();
    }


    public static void onAlertDialogTwoBtnIntent(final Activity activity, String confirm, String cancel, String message, final Intent intent, final boolean isFinsh) {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setPositiveButton(confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                activity.startActivity(intent);
                if(isFinsh)
                {
                    activity.finish();
                }
            }
        })
        .setNegativeButton(cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.setMessage(message);
        alert.show();
    }
}
