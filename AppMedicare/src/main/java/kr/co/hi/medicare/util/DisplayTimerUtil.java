package kr.co.hi.medicare.util;

import android.content.Context;
import android.util.Log;

import kr.co.hi.medicare.net.hwNet.ApiData;
import kr.co.hi.medicare.net.hwNet.BaseData;
import kr.co.hi.medicare.net.hwNet.CConnAsyncTask;

import kr.co.hi.medicare.utilhw.NetworkUtil;


public class DisplayTimerUtil {
    private final String TAG = DisplayTimerUtil.class.getSimpleName();
    private static DisplayTimerUtil mUtil;

    private String savedDisplayName = null;
    private long saveTime = 0L;

    public static DisplayTimerUtil getInstance() {
        if (mUtil == null) {
            mUtil = new DisplayTimerUtil();
        }

        return mUtil;
    }

    /**
     * 화면에 머물렀던 시간 기록
     *
     *
     */


    public void setDisplayTimer(Context context) {
        saveTime = System.currentTimeMillis();
    }


    /**
     * 화면에 머물렀던 시간 기록
     *
     * @param displayName
     */
    public void setDisplayTimer(Context context,String displayName) {
        long currentTime = System.currentTimeMillis();

        if (saveTime > 0) {
            final long second = (currentTime - saveTime) / 1000L;


//            String temp = displayName;
//            DBHelper helper = new DBHelper(context);
//            DBHelperLog logdb = helper.getLogDb();
//
//            if(temp.contains("_")) {
//                final String cod[] = temp.split("_");
//                Log.i(TAG, "setDisplayTimer:" + cod[0] + ":" + second + "초");
//
//
//
//
//                if (cod.length == 1) {
//                    logdb.insert(cod[0], "", "", 0, 1);
//                    Log.i(TAG, "view.contentDescription : " + cod[0] + "count : 1");
//                } else if (cod.length == 2) {
//                    logdb.insert(cod[0], cod[1], "", 0, 1);
//                    Log.i(TAG, "view.contentDescription : " + cod[0] + cod[1] + "count : 1");
//                } else {
//                    logdb.insert(cod[0], cod[1], cod[2], 0, 1);
//                    Log.i(TAG, "view.contentDescription : " + cod[0] + cod[1] + cod[2] + "count : 1");
//                }
//
//                logdb.insert(cod[0], "", "", (int)second, 0);
//            }else if(temp.contains("!")){
//                String cod = temp.replace("!","");
//                Log.i(TAG, "setDisplayTimer:" + cod + ":" + second + "초");
////                DBHelper helper = new DBHelper(context);
////                DBHelperLog logdb = helper.getLogDb();
//
//                logdb.insert(cod,"","",(int)second,0);
//            }
//
//
////            new Handler().postDelayed(new Runnable() {
////                @Override
////                public void run() {
////
////                }
////            }, 300);
//
////            Tr_KM003 tr = new Tr_KM003();
////            Tr_KM003.RequestData requestData = new Tr_KM003.RequestData();
////            getData(context, tr, requestData);
        }
//
//        saveTime = currentTime;
//        savedDisplayName = displayName;
    }


    public void getData(final Context context, final BaseData cls, final Object obj) {
        if (context == null) {
            // 앱을 빠르게 취소 하고 재 시작 시 오류 발생 될 때 있음
            Log.e(TAG, "getData context is null");
            return;
        }
        if (NetworkUtil.getConnectivityStatus(context) == false) {
            return;
        }

        CConnAsyncTask.CConnectorListener queryListener = new CConnAsyncTask.CConnectorListener() {

            @Override
            public Object run() throws Exception {

                ApiData data = new ApiData();
                return data.getData(cls, obj);
            }

            @Override
            public void view(CConnAsyncTask.CQueryResult result) {

                if (result.result == CConnAsyncTask.CQueryResult.SUCCESS && result.data != null) {
//                    if (step != null) {
//                        step.next(result.data);
//                    }
                } else {
//                    if (failStep != null) {
//                        failStep.fail();
//                    } else {
//                        Toast.makeText(getContext(), "데이터 수신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
//                        Log.e(TAG, "CConnAsyncTask error=" + result.errorStr);
//                        hideProgress();
//                    }
                }
            }
        };

        CConnAsyncTask asyncTask = new CConnAsyncTask();
        asyncTask.execute(queryListener);
    }
}
