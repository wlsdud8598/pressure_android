package kr.co.hi.medicare.utilhw;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import kr.co.hi.medicare.value.Define;

import java.io.File;

import kr.co.hi.medicare.R;

/**
 * Created by mrsohn on 2017. 3. 21..
 */

public class ViewUtil {

    private static final String TAG = ViewUtil.class.getSimpleName();

    /**
     * 이미지 URL에서 이미지를 bitmap로 가져온 후 ImageView 에 세팅
     *
     * @param idx
     * @param iv
     */
    public static void getIndexToImageData(final String idx, final ImageView iv, final ImageView dv, final Context context, final TextView kaltv, final TextView kaltv2) {
        if (TextUtils.isEmpty(idx)) {
            Logger.e(TAG, "getIndexToImageData idx is null");
            return;
        }

        // 로컬에서 받아오기
        Bitmap bitmap;
        try {
            String path = Define.getFoodPhotoPath(idx);
            Logger.i(TAG, "getIndexToImageData.path="+path);
            File imgFile = new File(path);
            if (imgFile.exists()) {
                bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                BitmapDrawable ob = new BitmapDrawable(context.getResources(), bitmap);
                iv.setBackground(ob);
                dv.setImageResource(R.drawable.health_m_r_02);
                dv.setBackground(null);
                kaltv.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
                kaltv2.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
            }else{
                dv.setVisibility(View.VISIBLE);
                String kal = kaltv.getText().toString();
                if(kal.equals("0")){
                    dv.setImageResource(R.drawable.health_m_r_01);
                } else {
                    dv.setImageResource(R.drawable.health_m_r_02);
                }
                kaltv.setTextColor(ContextCompat.getColor(context, R.color.color_001655));
                kaltv2.setTextColor(ContextCompat.getColor(context, R.color.color_001655));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public static void setTypeface(Typeface typeface, View view) {
        if (view instanceof TextView) {
            if (((TextView) view).getTypeface() == null)
                ((TextView) view).setTypeface(typeface);
        } else if (view instanceof EditText) {
            if (((EditText)view).getTypeface() == null)
                ((EditText)view).setTypeface(typeface);
        } else if (view instanceof Button) {
            if (((Button) view).getTypeface() == null)
                ((Button) view).setTypeface(typeface);
        } else if (view instanceof RadioButton) {
            if (((RadioButton) view).getTypeface() == null)
                ((RadioButton) view).setTypeface(typeface);
        } else if (view instanceof CheckBox) {
            if (((TextView) view).getTypeface() == null)
                ((TextView) view).setTypeface(typeface);
        } else if (view instanceof CheckedTextView) {
            if (((CheckedTextView) view).getTypeface() == null)
                ((CheckedTextView) view).setTypeface(typeface);
        } else if (view instanceof ToggleButton) {
            if (((ToggleButton) view).getTypeface() == null)
                ((ToggleButton) view).setTypeface(typeface);
        }
    }

    /**
     * notosanskr_bold.otf
     *
     * @param context
     * @param view
     */
    public static void setTypefacenotosanskr_bold(Context context, View view) {
        Typeface typeface = ResourcesCompat.getFont(context, R.font.notosanskr_regular);
        setTypeface(typeface, view);
    }

    /**
     * notosanskr_medium.otf
     *
     * @param context
     * @param view
     */
    public static void setTypefacenotosanskr_medium(Context context, View view) {
        Typeface typeface = ResourcesCompat.getFont(context, R.font.notosanskr_medium);
        setTypeface(typeface, view);
    }

    /**
     * notosanskr_regular.otf
     *
     * @param context
     * @param view
     */
    public static void setTypefacenotosanskr_regular(Context context, View view) {
        Typeface typeface = ResourcesCompat.getFont(context, R.font.notosanskr_regular);
        setTypeface(typeface, view);
    }
}
